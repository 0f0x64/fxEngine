Texture2D tex0 : register(t0);
cbuffer ConstantBuffer : register(b0)
{
	float4 View;
	float4 Pos;
	float4 sSize;
	float4 uvScale;
	float4 uvOffset;
};
struct VS_INPUT
{
	float4 Pos : POSITION;
	float2 uv : TEXCOORD0;
};
struct VS_OUTPUT
{
	float4 Pos : SV_POSITION;
	float2 uv : TEXCOORD0;
};
VS_OUTPUT VS(VS_INPUT input)
{
	VS_OUTPUT output= (VS_OUTPUT)0;
	float2 p=input.Pos.xy*sSize.xy+Pos.xy;
	output.Pos =float4(p,0,1);
	output.uv = input.uv*uvScale.x+uvOffset.xy;
	return output;
};