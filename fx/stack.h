namespace stack
{
	int CommandsCount;
	BYTE* data,*data2,*textdata;
	#define MAXDATASIZE 25000000

	int PrecalcStatus = 0; 
	int LoopPoint = 0;

	#ifndef EditMode
		#include "resource.h"
	#endif

	void init()
	{
		data = new BYTE[MAXDATASIZE];
		ZeroMemory(data, MAXDATASIZE);		

		#ifndef EditMode		
			HRSRC		rec;
			HGLOBAL		handle;
			rec = FindResource(NULL, MAKEINTRESOURCE(IDR_RAW1), "RAW");
			handle = LoadResource(NULL, rec);
			memcpy(data, LockResource(handle), SizeofResource(NULL, rec));
		#endif

		#ifdef EditMode
			data2 = new BYTE[MAXDATASIZE];
			ZeroMemory(data2, MAXDATASIZE);
			textdata = new BYTE[MAXDATASIZE];
			ZeroMemory(textdata, MAXDATASIZE);

		#endif

	}

}