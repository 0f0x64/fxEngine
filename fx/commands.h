#define EditorInfoOffset 8
#define EditorInfoOffsetX 0
#define EditorInfoOffsety 2
#define EditorInfoOffsetSelected 4
#define EditorInfoOffsetMode 5

namespace Commands
{

	BYTE* currentPos;
	BYTE currentCmd;
	int cursor = 0;
	double cursorF = 0;
	BYTE* loopPoint = NULL;
	bool resCreationFlag = false;

	float lerp(float a, float b, float v)
	{
		return (a*(1.0f - v) + b * v);
	}

	double lerp_d(double a, double b, double v)
	{
		return (double)(a*(1.0 - v) + b * v);
	}

	float clamp(float a, float _min, float _max)
	{
		return min(_max, max(a, _min));
	}

	double clamp_d(double a, double _min, double _max)
	{
		return min(_max, max(a, _min));
	}

	int clamp_int(int a, int _min, int _max)
	{
		return min(_max, max(a, _min));
	}

	INT32 clamp_int32(int a, int _min, int _max)
	{
		return min(_max, max(a, _min));
	}

	float frac(float a)
	{
		return a - (float)floor(a);
	}

	double frac_d(double a)
	{
		return a - (double)floor(a);
	}

	double sign(double x)
	{
		return (x >= 0) ? 1 : -1;
	}

#ifdef EditMode

	void _Log(char* text)
	{
		SetWindowText(hWnd, text);
	}

	void _NLog(int n)
	{
		char sss[22];
		_itoa(n, sss, 10);
		SetWindowText(hWnd, sss);
	}

#endif

//reflection/introspection macro
#define CMD_DATA

#define pU8 unsigned char
#define pS8 signed char
#define pU16 unsigned short
#define pS16 signed short
#define pS32 INT32
#define pName char Name[32];
#define pEnum unsigned char 
#define pEnumAssign unsigned char
#define pText char

#define EnvOn
#define EnumList(...)
#define DefValue(...)

#include "cmd_music.h"

BYTE shaderSlotCounter = 0;

//#ifdef EditMode
BYTE* resourceCreationPtr[4][255];//0=shader 1=texture 2=scene 3=paramBuffer
char resourceCreationName[4][255][32];//0=shader 1=texture 2=scene 3=paramBuffer
char resourceCreationName2[4][255][32];//0=shader 1=texture 2=scene 3=paramBuffer
//#endif

#ifdef EditMode

bool isAnyElementSelected = false;

bool RT_GetSelStatus(BYTE* cp)
{
	return *(cp + CmdDesc[*cp].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1;
}
#endif

void ExecuteChildren()
{
	#ifdef EditMode
		isAnyElementSelected = false;
	#endif

	BYTE* cp = currentPos;
	BYTE* ep = currentPos + CmdDesc[*cp].Size;
	while (CmdDesc[*ep].Level > CmdDesc[*cp].Level)
	{
		currentPos = ep;
		currentCmd = *currentPos;

		if (RT_GetSelStatus(currentPos)) isAnyElementSelected = true;

		CmdDesc[*ep].Routine();
		ep += CmdDesc[*ep].Size;
	}
}

int overrideCull = 0;
int overridePS = 0;
int overrideBlend = 0;
int overrideStageCopy = 0;

int sceneCamera = 0;

	void Release()
	{

	}

	#pragma pack(push, 1)
	struct Project_
	{
		pName
	};
	#pragma pack(pop)

	bool precalcIsDone = false;
	int customConstBufferSlotCounter = 0;
	int customConstBufferSlotCounterBias = 0;

	void Project()
	{
		precalcIsDone = false;
		customConstBufferSlotCounter = 0;
		customConstBufferSlotCounterBias = 0;
	}

#ifdef EditMode
	int objDrawCallCouter =0;
	UINT64 totalPolyCount = 0;
#endif

	void MainLoop()
	{
		loopPoint = currentPos;

		if (!precalcIsDone)
		{
			customConstBufferSlotCounterBias = customConstBufferSlotCounter;
			precalcIsDone = true;
		}
		else
		{
			customConstBufferSlotCounter = customConstBufferSlotCounterBias;
		}

#ifdef EditMode
		objDrawCallCouter =0 ;
		totalPolyCount = 0;
#endif
	}

	void EndDraw()
	{

	}

	typedef struct 
	{
		BYTE* pointer;
		int left;
		int right;
	} callstack;

	//BYTE* CallStack[256];
	callstack CallStack[256];
	BYTE CallStackPointer = 0;
	bool SceneCallFlag = false;

	#pragma pack(push, 1)
	struct Scene_
	{
		pName
	};
	#pragma pack(pop)

	void Scene()
	{
		if (CallStackPointer > 0)
		{
			CallStackPointer--;
		}
		else
		{
			return;
		}

		currentPos = CallStack[CallStackPointer].pointer;




	}

	void EndScene()
	{
		sceneCamera = 0;

		if (CallStackPointer > 0)
		{
			CallStackPointer--;
		}
		else
		{
			return;
		}

		currentPos = CallStack[CallStackPointer].pointer;
		
	}

	bool SceneLocateFlag = false;
	BYTE* ScenePointer[256];
	
	struct perObjectConstStruct
	{
		XMMATRIX modelTransform;
		XMMATRIX normalTransform;
		XMFLOAT4 cull;
	};

	perObjectConstStruct perObjectConst;

#pragma pack (push,1)
	struct BlendMode_
	{
		pEnum alpha; EnumList(off,on,pm1,pm2)
		pEnum operation; EnumList(add,revsub,sub,min,max)
	};
#pragma pack (pop)

	BYTE blendIndex = 0;

	void BlendMode()
	{
		BlendMode_* in = (BlendMode_*)(currentPos + 1);
		blendIndex = in->alpha* 5 + in->operation;

		float blendFactor2[4];
		blendFactor2[0] = 0.0f;
		blendFactor2[1] = 0.0f;
		blendFactor2[2] = 0.0f;
		blendFactor2[3] = 0.0f;

		dx::g_pImmediateContext->OMSetBlendState(dx::bs[blendIndex], blendFactor2, 0xffffffff);
	}

	

	struct ConstantBufferWave
	{
		XMFLOAT4 point[735];
	};
	ConstantBufferWave WaveData;

	ID3D11Buffer*           pConstantBufferWave = NULL;
	ID3D11Buffer*           pConstantBufferParams = NULL;
	ID3D11Buffer*			pConstantBufferObjectInfo = NULL;
	ID3D11Buffer*           pConstantBufferCustomParams = NULL;
	ID3D11Buffer*			pConstantBufferCustomParamsImmediate = NULL;
	ID3D11Buffer*			p3dObjectTransform = NULL;

	int roundUp(int n, int r)
	{
		return 	n - (n % r) + r;
	}

BYTE pointIndex;

#pragma pack (push,1)
struct Curve_
{
};
#pragma pack (pop)

void Curve()
{
	pointIndex = 0;
}

#pragma pack (push,1)
	struct Point3d_
	{
		pS16 x;
		pS16 y;
		pS16 z;
	};
#pragma pack (pop)

void Point3d()
{
	Point3d_* in = (Point3d_*)(currentPos + 1);
	pointIndex++;
}

#pragma pack (push,1)
struct CameraYPR_
{
	pS16 x;
	pS16 yaw;
	pS16 pitch;
	pS16 roll;
	pS16 ofsX;
	pS16 ofsY;
	pS16 ofsZ;
	pU8 angle;DefValue (1,45,180)
	pEnum ortho; EnumList(off, on)
	pEnum relativePos; EnumList(off, on)
	pEnum storeAsDepth; EnumList(off, on)
	pEnum freeCamera; EnumList(off, on)
};
#pragma pack (pop)

bool isDepthCamera;

void CameraYPR()
{
	
	CameraYPR_* in = (CameraYPR_*)(currentPos + 1);

		if (in->x > Commands::cursorF)
		{
			return;
		}



	XMMATRIX rotationX = XMMatrixRotationX(in->yaw*PI/180.f);
	XMMATRIX rotationY = XMMatrixRotationY(in->pitch*PI / 180.f);
	XMMATRIX rotationZ = XMMatrixRotationZ(in->roll*PI / 180.f);
	XMMATRIX trans = XMMatrixTranslation(0, 0, 1);
	float d = .01;
	XMMATRIX trans2 = XMMatrixTranslation(in->ofsX*d, in->ofsY*d, in->ofsZ*d);

	XMMATRIX temp = XMMatrixIdentity();
	temp = XMMatrixMultiply(temp, rotationX);
	temp = XMMatrixMultiply(temp, rotationY);
	temp = XMMatrixMultiply(temp, rotationZ);
	temp = XMMatrixMultiply(temp, trans);
	temp = XMMatrixMultiply(temp, trans2);

	if (in->storeAsDepth == 0)
	{
		dx::CameraView = temp;

		if (in->freeCamera == 1)
		{
			dx::CameraView = dx::View2;
		}
	}
	else
	{
		dx::DepthView = temp;
	}

	/*XMVECTOR Eye = XMVectorSet(0.0f, 0.0f, -1.0f, 0.0f);
	XMVECTOR At = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	View = XMMatrixLookAtLH(Eye, At, Up);*/

	if (in->storeAsDepth == 0)
	{

		if (in->ortho == 0)
		{
			float angle = max(in->angle, 1);
			dx::CameraProjection = XMMatrixPerspectiveFovLH(angle * PI / 180.0, dx::width / (FLOAT)dx::height, 0.01f, 100.0f);
			if (in->freeCamera == 1) dx::CameraProjection = dx::Projection;
		}
		else
		{
			float angle = max(in->angle/10., .1);
			dx::CameraProjection = XMMatrixOrthographicLH(angle, angle, 0.01, 100.f);
			if (in->freeCamera == 1) dx::CameraProjection = dx::Projection;
		}
	}
	else
	{
		if (in->ortho == 0)
		{
			float angle = max(in->angle, 1);
			dx::DepthProjection = XMMatrixPerspectiveFovLH(angle * PI / 180.0, dx::width / (FLOAT)dx::height, 0.01f, 100.0f);
		}
		else
		{
			float angle = max(in->angle/10., .1);
			dx::DepthProjection = XMMatrixOrthographicLH(angle, angle, 0.01, 100.f);
		}

	}
}

	void SetSamplerPS(BYTE slot, BYTE sampler)
	{
		dx::g_pImmediateContext->PSSetSamplers(slot, 1, &dx::pSampler[sampler]);
	}

	void SetSamplerVS(BYTE slot, BYTE sampler)
	{
		dx::g_pImmediateContext->VSSetSamplers(slot, 1, &dx::pSampler[sampler]);
	}

	void CreateStage()
	{
		int tn = 0;

		if (dx::currentRTn == -1)
		{
			tn = 7;
			ID3D11Resource* pResource = NULL;
			dx::g_pRenderTargetView->GetResource(&pResource);
			dx::g_pImmediateContext->CopyResource(dx::temptexture[tn], pResource);

			dx::g_pDepthStencilView->GetResource(&pResource);
			dx::g_pImmediateContext->CopyResource(dx::tempdepth[7], pResource);
		}
		else
		{
			tn = dx::RTStageColorIndex[dx::currentRTn];
			dx::g_pImmediateContext->CopyResource(dx::temptexture[tn], dx::pTexture[dx::currentRTn]);

			tn = dx::RTStageDepthIndex[dx::currentRTn];
			dx::g_pImmediateContext->CopyResource(dx::tempdepth[tn], dx::pDepth[dx::currentRTn]);
			
		}

	}

	int subDiv = 1;

#pragma pack (push,1)
		struct SubdivFactor_
	{
		pU8 subdiv; DefValue(1, 1, 32)
	};
#pragma pack (pop)

	void SubdivFactor()
	{
		SubdivFactor_* in = (SubdivFactor_*)(currentPos + 1);
		subDiv = in->subdiv;
	}

	int texQuality = 0;
	int texBitDepth = 0;

#pragma pack (push,1)
	struct SetTexturesQuality_
	{
		pEnum quality; EnumList(standart, quater, half, x2, x4);
		pEnum bitDepth; EnumList(dontTouch, half, quater);
	};
#pragma pack (pop)

	void SetTexturesQuality()
	{
		SetTexturesQuality_* in = (SetTexturesQuality_*)(currentPos + 1);
		texQuality = in->quality;
		texBitDepth = in->bitDepth;
	}

#pragma pack (push,1)
	struct Override_
	{
		pEnum cull; EnumList(off, none, front, back)
		pEnum ps; EnumList(off, toNull)
		pEnum blend; EnumList(off, disable, on, pm1, pm2)
		pEnum stageCopy; EnumList(off, diable, enable)
	};
#pragma pack (pop)

	void Override()
	{
		Override_* in = (Override_*)(currentPos + 1);
		overrideCull = in->cull;
		overridePS = in->ps;
		overrideBlend = in->blend;
		overrideStageCopy = in->stageCopy;
	}

#pragma pack (push,1)
	struct Clear_
	{
		pEnum color; EnumList(off, on)
		pEnum depth; EnumList(off, on)
		pU8 r;
		pU8 g;
		pU8 b;
		pU8 a;
	};
#pragma pack (pop)

	void Clear()
	{
		Clear_* in = (Clear_*)(currentPos + 1);
		int slice = dx::RenderTargetViewSlice[dx::currentRTn];
		XMVECTORF32 col = { in->r / 255., in->g / 255., in->b / 255., in->a / 255. };

		if (dx::currentRTn == -1)
		{
			if (in->color)
			{
				dx::g_pImmediateContext->ClearRenderTargetView(dx::g_pRenderTargetView, col);
			}

			if (in->depth)
			{
				dx::g_pImmediateContext->ClearDepthStencilView(dx::g_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0.0);
			}

		}
		else
		{
			if (in->color)
			{
				dx::g_pImmediateContext->ClearRenderTargetView(dx::RenderTargetView[dx::currentRTn][slice][0], col);
			}

			if (in->depth)
			{
				dx::g_pImmediateContext->ClearDepthStencilView(dx::DepthStencilView[dx::currentRTn][slice], D3D11_CLEAR_DEPTH, 1.0f, 0.0);
			}
		}
	}


#pragma pack (push,1)
	struct SetCull_
	{
		pEnum cull; EnumList(none, front, back)
	};
#pragma pack (pop)

	void SetCull()
	{
		SetCull_* in = (SetCull_*)(currentPos + 1);
		int cull = in->cull;
		if (overrideCull > 0)
		{
			cull = overrideCull-1;
		}
		dx::g_pImmediateContext->RSSetState(dx::g_pRasterState[cull]);

		perObjectConst.cull = XMFLOAT4(cull,0,0,0);
	}


#pragma pack (push,1)
	struct DefineParams_
	{
		pName;
		pU8 shader;
		pS16 p0;
		pS16 p1;
		pS16 p2;
		pS16 p3;
		pS16 p4;
		pS16 p5;
		pS16 p6;
		pS16 p7;
		pS16 p8;
		pS16 p9;
		pS16 p10;
		pS16 p11;
		pS16 p12;
		pS16 p13;
		pS16 p14;
		pS16 p15;
	};
#pragma pack (pop)

	dx::ParamConstBuffer customConstBuffer[256];

	void DefineParams()
	{
		DefineParams_* in = (DefineParams_*)(currentPos + 1);

//#ifdef EditMode
//		resourceCreationPtr[3][customConstBufferSlotCounter] = currentPos + 1;
//		resourceCreationPtr[3][customConstBufferSlotCounter + 1] = NULL;
//#endif

		if (!pConstantBufferCustomParams)
		{
			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = roundUp(sizeof(customConstBuffer[0]), 16);
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.StructureByteStride = 16;

			HRESULT hr = dx::g_pd3dDevice->CreateBuffer(&bd, NULL, &pConstantBufferCustomParams);
		}

		for (int x = 0; x < 15; x++)
		{
			customConstBuffer[customConstBufferSlotCounter].p[x] = *(short int*)(&in->p0 + x);
		}

		customConstBufferSlotCounter++;
	}




	float transformPosX, transformPosY, transformPosZ;//data for array
	float transformSizeX, transformSizeY, transformSizeZ;//data for array

#pragma pack (push,1)
	struct Transform_
	{
		pS16 x;
		pS16 y;
		pS16 z;
		pS16 sx;
		pS16 sy;
		pS16 sz;
		pS16 rx;
		pS16 ry;
		pS16 rz;

		CMD_DATA
		float pX, pY;
	};
#pragma pack (pop)


	void Transform()
	{
		Transform_* in = (Transform_*)(currentPos + 1);

		transformPosX = in->x;
		transformPosY = in->y;
		transformPosZ = in->z;

		transformSizeX = in->sx;
		transformSizeY = in->sy;
		transformSizeZ = in->sz;

		if (!p3dObjectTransform)
		{
			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = roundUp(sizeof(perObjectConst), 16);
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.StructureByteStride = 16;

			HRESULT hr = dx::g_pd3dDevice->CreateBuffer(&bd, NULL, &p3dObjectTransform);
		}

		float scaleFactor = 1000.;
	
		XMMATRIX trans = XMMatrixTranslation(in->x / scaleFactor, in->y / scaleFactor, in->z / scaleFactor);
		XMMATRIX scale = XMMatrixScaling(in->sx / scaleFactor, in->sy / scaleFactor, in->sz / scaleFactor);
		XMMATRIX rotX = XMMatrixRotationX(in->rx * PI / 180.);
		XMMATRIX rotY = XMMatrixRotationY(in->ry * PI / 180.);
		XMMATRIX rotZ = XMMatrixRotationZ(in->rz * PI / 180.);

		XMMATRIX normal = XMMatrixIdentity();
		normal = XMMatrixMultiply(normal, scale);
		normal = XMMatrixMultiply(normal, rotX);
		normal = XMMatrixMultiply(normal, rotY);
		normal = XMMatrixMultiply(normal, rotZ);
		//perObjectConst.normalTransform = XMMatrixTranspose(normal);
		perObjectConst.normalTransform = XMMatrixInverse(NULL, normal);

		XMMATRIX model = XMMatrixIdentity();
		model = XMMatrixMultiply(model, scale);
		model = XMMatrixMultiply(model, rotX);
		model = XMMatrixMultiply(model, rotY);
		model = XMMatrixMultiply(model, rotZ);
		model = XMMatrixMultiply(model, trans);
		perObjectConst.modelTransform = XMMatrixTranspose(model);

		dx::g_pImmediateContext->UpdateSubresource(p3dObjectTransform, 0, NULL, &perObjectConst, 0, 0);
		dx::g_pImmediateContext->VSSetConstantBuffers(5, 1, &p3dObjectTransform);
		dx::g_pImmediateContext->PSSetConstantBuffers(5, 1, &p3dObjectTransform);


#ifdef EditMode

		//create position info for editor
		XMVECTOR pt = XMVectorSet(0, 0, 0, 1);
		pt = XMVector4Transform(pt, model);
		pt = XMVector4Transform(pt, dx::CameraView);
		pt = XMVector4Transform(pt, dx::CameraProjection);
		in->pX = dx::iaspect * XMVectorGetX(pt) / XMVectorGetW(pt);
		in->pY = XMVectorGetY(pt) / XMVectorGetW(pt);

#endif // EditMode

	}

#pragma pack (push,1)
	struct SetParams_
	{
		pS16 p0;
		pS16 p1;
		pS16 p2;
		pS16 p3;
		pS16 p4;
		pS16 p5;
		pS16 p6;
		pS16 p7;
		pS16 p8;
		pS16 p9;
		pS16 p10;
		pS16 p11;
		pS16 p12;
		pS16 p13;
		pS16 p14;
		pS16 p15;
	};
#pragma pack (pop)


	dx::ParamConstBuffer customConstBufferImmediate;

	void SetParams()
	{
		SetParams_* in = (SetParams_*)(currentPos + 1);

		if (!pConstantBufferCustomParamsImmediate)
		{
			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = roundUp(sizeof(customConstBufferImmediate), 16);
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.StructureByteStride = 16;

			HRESULT hr = dx::g_pd3dDevice->CreateBuffer(&bd, NULL, &pConstantBufferCustomParamsImmediate);
		}

		for (int x = 0; x < 15; x++)
		{
			customConstBufferImmediate.p[x] = *(short int*)(&in->p0 + x);
		}

		float scaleFactor = 1000.;
		XMMATRIX model = XMMatrixIdentity();
		XMMATRIX trans = XMMatrixTranslation(in->p0 / scaleFactor, in->p1 / scaleFactor, in->p2 / scaleFactor);
		XMMATRIX scale = XMMatrixScaling(in->p3/scaleFactor, in->p4/scaleFactor, in->p5/scaleFactor);
		XMMATRIX rotX = XMMatrixRotationX(in->p7 * PI / 180.);
		XMMATRIX rotY = XMMatrixRotationY(in->p8 * PI / 180.);
		XMMATRIX rotZ = XMMatrixRotationZ(in->p9 * PI / 180.);

		model = XMMatrixMultiply(model, rotX);
		model = XMMatrixMultiply(model, rotY);
		model = XMMatrixMultiply(model, rotZ);
		customConstBufferImmediate.Normal = XMMatrixTranspose(model);
		

		model = XMMatrixIdentity();
		model = XMMatrixMultiply(model, scale);
		model = XMMatrixMultiply(model, rotX);
		model = XMMatrixMultiply(model, rotY);
		model = XMMatrixMultiply(model, rotZ);
		model = XMMatrixMultiply(model, trans);
		customConstBufferImmediate.Model = XMMatrixTranspose(model);

		dx::g_pImmediateContext->UpdateSubresource(pConstantBufferCustomParamsImmediate, 0, NULL, &customConstBufferImmediate, 0, 0);
		dx::g_pImmediateContext->VSSetConstantBuffers(3, 1, &pConstantBufferCustomParamsImmediate);
		dx::g_pImmediateContext->PSSetConstantBuffers(3, 1, &pConstantBufferCustomParamsImmediate);

	}

#pragma pack (push,1)
	struct ApplyParams_
	{
		pU8 paramSet;
	};
#pragma pack (pop)

	void ApplyParams()
	{
		ApplyParams_* in = (ApplyParams_*)(currentPos + 1);

		auto t = in->paramSet;
		dx::g_pImmediateContext->UpdateSubresource(pConstantBufferCustomParams, 0, NULL, &customConstBuffer[t], 0, 0);
		//dx::g_pImmediateContext->UpdateSubresource(pConstantBufferCustomParams, 0, NULL, &customConstBuffer[t], 0, 0);
		dx::g_pImmediateContext->VSSetConstantBuffers(2, 1, &pConstantBufferCustomParams);
		dx::g_pImmediateContext->PSSetConstantBuffers(2, 1, &pConstantBufferCustomParams);
	}

	int prevStageSampler = 0;

#pragma pack (push,1)
	struct SetPrevStageFilter_
	{
		pEnum sampler; EnumList(linear, nearest)
		pEnum wrap; EnumList(off, u, v, uv)
	};
#pragma pack (pop)

	void SetPrevStageFilter()
	{
		SetPrevStageFilter_* in = (SetPrevStageFilter_*)(currentPos + 1);
		prevStageSampler = in->sampler * 4 + in->wrap;		
	}

	int textureDimensions[16];

#pragma pack (push,1)
	struct SetTexture_
	{
		pU8 slot;
		pU8 texture;
		pEnum sampler; EnumList (linear, nearest,compare)
		pEnum wrap; EnumList(off, u, v, uv)
		pEnum mode; EnumList(pixel, vertex, both)
		pEnum spectex; EnumList(off, depth, prevStageColor, prevStageDepth)
	};
#pragma pack (pop)

	bool stageUsed = false;

	void SetTexture()
	{
		SetTexture_* in = (SetTexture_*)(currentPos + 1);

		int sampler = in->sampler * 4 + in->wrap;

		if (in->sampler == 2) sampler = 8;

		int tn = 0; int tnd = 0;
		if (in->spectex >= 2)
		{
			if (overrideStageCopy == 0 || overrideStageCopy == 2 )
			{
				CreateStage();
				stageUsed = true;
			}
			tn = dx::RTStageColorIndex[dx::currentRTn];
			tnd = dx::RTStageDepthIndex[dx::currentRTn];
		}

		switch (in->spectex)
		{
		case 0:
			//standart textures
			{
				if (in->mode == 1 || in->mode == 2)
				{
					dx::g_pImmediateContext->VSSetShaderResources(in->slot, 1, &dx::TextureResView[in->texture]);
					SetSamplerVS(in->slot, sampler);
				}

				if (in->mode == 0 || in->mode == 2)
				{
					dx::g_pImmediateContext->PSSetShaderResources(in->slot, 1, &dx::TextureResView[in->texture]);
					SetSamplerPS(in->slot, sampler);					
				}

				textureDimensions[in->slot] = max(dx::Tsize[in->texture].x, dx::Tsize[in->texture].y);
			}
			break;

		case 1:
			//depth
			SetSamplerPS(in->slot, sampler);
			dx::g_pImmediateContext->PSSetShaderResources(in->slot, 1, &dx::DepthResView[in->texture]);
			break;

		case 2:
			//prev.stage color
			SetSamplerPS(in->slot, sampler);
			dx::g_pImmediateContext->PSSetShaderResources(in->slot, 1, &dx::TempTextureResView[tn]);
			break;
		case 3:
			//prev.stage depth
			SetSamplerPS(in->slot, sampler);
			dx::g_pImmediateContext->PSSetShaderResources(in->slot, 1, &dx::TempDepthResView[tnd]);
			break;

		}
	}

	void UpdateWave()
	{
		if (!pConstantBufferWave)
		{
			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = roundUp(sizeof(ConstantBufferWave), 16);
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.StructureByteStride = 16;

			HRESULT hr = dx::g_pd3dDevice->CreateBuffer(&bd, NULL, &pConstantBufferWave);
		}

		/*if (pConstantBufferWave)
{
	for (int x = 0; x < 735 - 1; x++)
	{
		WaveData.point[x].x = lerp(WaveData.point[x + 1].x, WaveData.point[x].x, .15);
	}
	int i = 0;
	float amp = *(signed short*)(sound::channel[MAXCHANNELS - 1] + (i + max(cursorM, 0) * FRAMELEN * 2) * 2);
	//WaveData.point[734].x = volumeL[MAXCHANNELS - 1];
	WaveData.point[734].x = amp / 32767.f;
	WaveData.point[734].y = 0;
	WaveData.point[734].z = 734;
	dx::g_pImmediateContext->UpdateSubresource(pConstantBufferWave, 0, NULL, &WaveData, 0, 0);
	dx::g_pImmediateContext->VSSetConstantBuffers(2, 1, &pConstantBufferWave);
}*/
	}


	byte minLod;
	byte maxLod;
	float lodScale;
	float lodOffset;
	bool lodUsed;
	

#pragma pack (push,1)
	struct LodParams_
	{
		pU8 minLod; DefValue(0, 0, 16)
		pU8 maxLod; DefValue(0, 0, 16)
		pS8 scale; DefValue(-100, 0, 100)
		pS8 offset; DefValue(-100, 0, 100)
	};
#pragma pack (pop)

	void LodParams()
	{
		LodParams_* in =  (LodParams_*)(currentPos + 1);
		minLod = in->minLod;
		maxLod = in->maxLod;
		lodOffset = in->offset / 10.;
		lodScale = 1+in->scale / 10.;
		lodUsed = true;
	}


	struct ConstantBufferParams
	{
		float params[MAXPARAM];
	};
//EnumList(1,2,4,8,16,32,64,128,256,512,1024,2048)

	void CreateMipMap();

#pragma pack (push,1)
	struct Array_
	{
		pU16 count;
	};
#pragma pack (pop)

	int ArrayCount;

	void Array()
	{
		Array_* in = (Array_*)(currentPos + 1);
		ArrayCount = in->count;
	}

#pragma pack (push,1)
	struct NullDrawer_
	{
		pU8 shader;
		pEnum staging; EnumList(off, lod0, mipmaps)
		pU16 instance;
		pU16 gridX; DefValue(0, 32, 2048)
		pU16 gridY; DefValue(0, 32, 2048)
		pS16 p0;
		pS16 p1;
		pS16 p2;
		pS16 p3;
		pS16 p4;
		pS16 p5;
		pS16 p6;
		pS16 p7;
		pS16 p8;
		pS16 p9;
		pS16 p10;
		pS16 p11;
		pS16 p12;
		pS16 p13;
		pS16 p14;
		pS16 p15;
		
		CMD_DATA
		int currentGX, currentGY, lod;
		float pX,pY;
	};
#pragma pack (pop)

	void ProcessStage(int staging, bool stageUsed)
	{
		if (staging >= 1 && stageUsed == false)
		{
			if (staging == 2)
			{
				CreateMipMap();
			}

			CreateStage();

			int tn = 0;
			if (dx::currentRTn == -1)
			{
				tn = 7;
			}
			else
			{
				tn = dx::RTStageColorIndex[dx::currentRTn];
			}

			dx::g_pImmediateContext->PSSetShaderResources(0, 1, &dx::TempTextureResView[tn]);
			dx::g_pImmediateContext->PSSetSamplers(0, 1, &dx::pSampler[prevStageSampler]);

			int tnd = dx::RTStageDepthIndex[dx::currentRTn];
			dx::g_pImmediateContext->PSSetShaderResources(1, 1, &dx::TempDepthResView[tnd]);
			dx::g_pImmediateContext->PSSetSamplers(1, 1, &dx::pSampler[prevStageSampler]);
		}
	}

	void NullDrawer()
	{
		lodUsed = false;
		ArrayCount = 0;
		BYTE* cp = currentPos;
		stageUsed = false;

		ExecuteChildren();
		

		//

		if (!pConstantBufferParams)
		{
			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = roundUp(sizeof(ConstantBufferParams), 16);
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.StructureByteStride = 16;

			HRESULT hr = dx::g_pd3dDevice->CreateBuffer(&bd, NULL, &pConstantBufferParams);
		}

		//

		NullDrawer_* in = (NullDrawer_ *)(cp + 1);

		if (in->gridX == 0 || in->gridY == 0) return;

		ProcessStage(in->staging, stageUsed);

		INT32 n = in->shader;

		UINT stride = sizeof(dx::Vertex);
		UINT offset = 0;

		dx::g_pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		dx::ConstantBuffer cb;
		cb.View = XMMatrixTranspose(dx::CameraView);
		cb.iView = XMMatrixTranspose(XMMatrixInverse(NULL,dx::CameraView));
		cb.Proj = XMMatrixTranspose(dx::CameraProjection);
		cb.iProj = XMMatrixTranspose(XMMatrixInverse(NULL, dx::CameraProjection));
		XMMATRIX vp = XMMatrixMultiply(dx::CameraView, dx::CameraProjection);
		cb.iVP = XMMatrixTranspose(XMMatrixInverse(NULL, vp));
		cb.DepthView = XMMatrixTranspose(dx::DepthView);
		cb.DepthProj = XMMatrixTranspose(dx::DepthProjection);
		cb.iDepthVP = XMMatrixTranspose(XMMatrixInverse(NULL, XMMatrixMultiply(dx::DepthView, dx::DepthProjection)));
		XMVECTOR one = XMVectorSet(0., 0., 1., 1.);
		XMVECTOR dv = XMVector4Transform(one, XMMatrixTranspose(dx::DepthView));
		cb.DepthViewVector.x = XMVectorGetX(dv);
		cb.DepthViewVector.y = XMVectorGetY(dv);
		cb.DepthViewVector.z = XMVectorGetZ(dv);
		cb.DepthViewVector.w = 1.;

		if (sceneCamera == 1)//draw depth
		{
			cb.View = XMMatrixTranspose(dx::DepthView);
			cb.iView = XMMatrixTranspose(XMMatrixInverse(NULL, dx::DepthView));
			cb.Proj = XMMatrixTranspose(dx::DepthProjection);
			cb.iProj = XMMatrixTranspose(XMMatrixInverse(NULL, dx::DepthProjection));
			XMMATRIX vp2 = XMMatrixTranspose(XMMatrixMultiply(dx::DepthView, dx::DepthProjection));
			cb.iVP = XMMatrixTranspose(XMMatrixInverse(NULL, vp2));
		}

		cb.time.x = (float)cursor;
		cb.time.y = timer::currentTime;
		cb.time.z = 0;
		cb.time.w = 0;
		cb.aspectRatio.x = dx::aspect;
		cb.aspectRatio.y = dx::iaspect;
		cb.aspectRatio.z = 0;
		cb.aspectRatio.w = 0;

		bool selStatus = RT_GetSelStatus(cp)||isAnyElementSelected;

		cb.selected = XMFLOAT4(selStatus, 0, 0, 0);

		INT32 gx = in->gridX*subDiv;
		INT32 gy = in->gridY*subDiv;

		cb.lodLevel = XMFLOAT4(0,0,0,0);

		dx::g_pImmediateContext->UpdateSubresource(dx::pConstantBufferV, 0, NULL, &cb, 0, 0);
		dx::g_pImmediateContext->UpdateSubresource(dx::pConstantBufferP, 0, NULL, &cb, 0, 0);
		dx::g_pImmediateContext->VSSetConstantBuffers(0, 1, &dx::pConstantBufferV);
		dx::g_pImmediateContext->PSSetConstantBuffers(0, 1, &dx::pConstantBufferP);

		ConstantBufferParams cbP;

		cbP.params[0] = gx;
		cbP.params[1] = gy;

		for (int x = 0; x < 15; x++)
		{
			cbP.params[x+2] = *(short int*)(cp + 9+x*2);
		}

		dx::g_pImmediateContext->UpdateSubresource(pConstantBufferParams, 0, NULL, &cbP, 0, 0);
		dx::g_pImmediateContext->UpdateSubresource(pConstantBufferParams, 0, NULL, &cbP, 0, 0);
		dx::g_pImmediateContext->VSSetConstantBuffers(1, 1, &pConstantBufferParams);
		dx::g_pImmediateContext->PSSetConstantBuffers(1, 1, &pConstantBufferParams);

		if (dx::Shader[n].pVs&&dx::Shader[n].pPs)
		{
			dx::g_pImmediateContext->VSSetShader(dx::Shader[n].pVs, NULL, 0);

			if (overridePS == 0)
			{
				dx::g_pImmediateContext->PSSetShader(dx::Shader[n].pPs, NULL, 0);
			}
			
			if (overridePS == 1)
			{
				dx::g_pImmediateContext->PSSetShader(NULL, NULL, 0);
			}

			if (overridePS > 1)
			{
				dx::g_pImmediateContext->PSSetShader(dx::Shader[overridePS-2].pPs, NULL, 0);
			}

		}


		dx::g_pImmediateContext->IASetInputLayout(NULL);
		dx::g_pImmediateContext->IASetVertexBuffers(0, 0, NULL, NULL, NULL);

		//obj info
		dx::cbObjectInfo ObjectInfo;

		if (!pConstantBufferObjectInfo)
		{
			D3D11_BUFFER_DESC bd;
			ZeroMemory(&bd, sizeof(bd));
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.ByteWidth = roundUp(sizeof(ObjectInfo), 16);
			bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
			bd.CPUAccessFlags = 0;
			bd.StructureByteStride = 16;

			HRESULT hr = dx::g_pd3dDevice->CreateBuffer(&bd, NULL, &pConstantBufferObjectInfo);
		}

		ArrayCount = max(ArrayCount, 1);
		
		ObjectInfo.Pos = XMFLOAT4(0, 0, 0, 0);

		for (int index = 0; index < ArrayCount* ArrayCount; index++)
		{
			gx = in->gridX * subDiv;
			gy = in->gridY * subDiv;
			//get position
			double xG = index % ArrayCount - (ArrayCount-1)/2.;
			double yG = floor(index / ArrayCount) - (ArrayCount-1) / 2.;
			double xGs = xG* transformSizeX/1000.;
			double yGs = yG* transformSizeZ/1000.;

			double xGo = xG* transformSizeX;
			double yGo = yG* transformSizeX;

			XMVECTOR pt = XMVectorSet(0, 0, 0, 1);
			XMMATRIX mt = XMMatrixTranslation(xGs, 0, yGs);
			pt = XMVector4Transform(pt, perObjectConst.modelTransform);
			pt = XMVector4Transform(pt, mt);
			pt = XMVector4Transform(pt, dx::CameraView);
			pt = XMVector4Transform(pt, dx::CameraProjection);
			float screenX = dx::iaspect * XMVectorGetX(pt) / XMVectorGetW(pt);
			float screenY = XMVectorGetY(pt) / XMVectorGetW(pt);

//			XMVECTOR pos = XMVectorSet(transformPosX/1000. + xGs, transformPosY/ 1000., transformPosZ +yGs, 1);
			ObjectInfo.Pos = XMFLOAT4(xGs, 0, yGs, 1);
//			XMVECTOR posTrasformed = XMVector4Transform(pos, vp);
			XMVECTOR posTrasformed = pt;
			float distance = XMVectorGetW(posTrasformed);

			float dd = distance;
			int ld =0;

			if (lodUsed)
			{

				float xMargin = 1 +max(max(transformSizeX / 1000., transformSizeY / 1000.), transformSizeZ / 1000.) * 1.4;//replace by transformed size
				XMFLOAT4X4 fView;
				XMStoreFloat4x4(&fView, dx::Projection);
				float f= fView._11;
				//xMargin /= f;

				//if (distance < -xMargin) continue;

				distance = max(distance, 1);
				float x = XMVectorGetX(posTrasformed)/distance ;
				float y = XMVectorGetY(posTrasformed)/distance ;

				//if (x > xMargin || x < -xMargin) continue;
				//if (y > xMargin || y < -xMargin) continue;

				distance *= lodScale;
				distance += lodOffset;
				distance = clamp(distance, minLod, maxLod);
				int lod = floor(distance);

				gx = gx >> lod;
				gy = gy >> lod;

				gx = max(gx, 2);
				gy = max(gy, 2);

				int src = textureDimensions[0];//geometry must be in slot 0
				int dst = max(gx, gy);
				int ratio = src / dst;
				ld = log2(ratio);

				cb.lodLevel = XMFLOAT4(0, 0, frac(distance) + ld, 0);//lod in float format for soft lod-to-lod transition

				in->currentGX = gx;
				in->currentGY = gy;

			}
			
			INT32 pCount = gx * gy;
			ObjectInfo.info = XMFLOAT4(frac(distance) + ld, index, gx, gy);

			dx::g_pImmediateContext->UpdateSubresource(pConstantBufferObjectInfo, 0, NULL, &ObjectInfo, 0, 0);
			dx::g_pImmediateContext->VSSetConstantBuffers(4, 1, &pConstantBufferObjectInfo);
			dx::g_pImmediateContext->PSSetConstantBuffers(4, 1, &pConstantBufferObjectInfo);

			dx::g_pImmediateContext->DrawInstanced(pCount * 6, max(in->instance,1), 0, 0);

			#ifdef EditMode
				objDrawCallCouter++;
				totalPolyCount += pCount * 2;
			#endif
		}

	}

	/*		if (in->instance == 0)
		{
			dx::g_pImmediateContext->Draw(pCount * 6, 0);
		}
*/

#pragma pack (push,1)
	struct LoadTexture_
	{
		pName
	};
#pragma pack (pop)


	void LoadTexture()
	{

	}

	void Render();

	#pragma pack (push,1)
	struct CreateShader_
	{
		pName
		pEnum type; EnumList(exec, lib)
		pText data[64000];
	};
	#pragma pack (pop)

	void CompileShader(int n, char* source_vs, char* source_ps);

	void CreateShader()
	{
		if (resCreationFlag == false) return;
		//if (loopPoint != stack::data) return;

		CreateShader_* in = (CreateShader_*)(currentPos + 1);
		BYTE* shaderText = (BYTE*)in->data;
		BYTE slot = shaderSlotCounter;

//#ifdef EditMode
//		resourceCreationPtr[0][slot] = currentPos+1;
//		resourceCreationPtr[0][slot+1] = NULL;
//		strcpy(resourceCreationName[0][slot], in->Name);
//#endif

		if (loopPoint != stack::data)
		{
			if (in->type == 1 || (dx::Shader[slot].pPs && dx::Shader[slot].pVs))
			{
				shaderSlotCounter++;
				return;
			}
		}
		shaderSlotCounter++;

		

		CompileShader(slot, (char*)shaderText, (char*)shaderText);

	}

	char* ConsolidatedIncludeText = NULL;
	char* FullShaderVS = NULL;
	char* FullShaderPS = NULL;

	void CompileShader(int n, char* source_vs, char* source_ps)
	{
		HRESULT hr = S_OK;

		//search for includes
		if (!ConsolidatedIncludeText)
		{
			ConsolidatedIncludeText = new char[64000];
			FullShaderVS = new char[64000];
			FullShaderPS = new char[64000];
		}

		strcpy(ConsolidatedIncludeText, " ");

		char* ptr = source_vs;
		bool done = false;

		while (!done)
		{
			char* icmd = strstr(ptr, "//include");
			if (icmd)
			{
				icmd += 9;
				while (*icmd == ' ') icmd++;
				char* namestart = icmd;
				char name[64];
				int count = 0;
				while (isalnum(*(icmd + count))) count++;

				strncpy(name, icmd, count);
				name[count] = 0;

				int c = 0;
				bool found = false;
				while (Commands::resourceCreationPtr[0][c] != NULL&&!found)
				{
					Commands::CreateShader_* in = (Commands::CreateShader_*)(resourceCreationPtr[0][c]);

					if (!strncmp(in->Name, name, 32))
					{
						strcat(ConsolidatedIncludeText, in->data);
						found = true;
					}

					c++;
				}


				ptr += 9+count;

			}
			else
			{
				done = true;
			}
		}

		strcpy(FullShaderVS, " ");
		strcat(FullShaderVS, ConsolidatedIncludeText);
		strcat(FullShaderVS, source_vs);

		strcpy(FullShaderPS, " ");
		strcat(FullShaderPS, ConsolidatedIncludeText);
		strcat(FullShaderPS, source_ps);

		//

		dx::Shader[n].pVSBlob = NULL;
		//if (Shader[n].pVSBlob) Shader[n].pVSBlob->Release();
		hr = D3DCompile(FullShaderVS, strlen(FullShaderVS), NULL, NULL, NULL, "VS", "vs_4_1", D3DCOMPILE_PACK_MATRIX_COLUMN_MAJOR, NULL, &dx::Shader[n].pVSBlob, &dx::pErrorBlob);
		if (hr != S_OK)
		{
			//if (Shader[n].pVSBlob) Shader[n].pVSBlob->Release();
			/* SetWindowText(hWnd, (char*)pErrorBlob->GetBufferPointer());*/
			return;
		}


		if (dx::Shader[n].pVs) dx::Shader[n].pVs->Release();
		hr = dx::g_pd3dDevice->CreateVertexShader(dx::Shader[n].pVSBlob->GetBufferPointer(), dx::Shader[n].pVSBlob->GetBufferSize(), NULL, &dx::Shader[n].pVs);
		if (FAILED(hr))
		{
			if (dx::Shader[n].pVs) dx::Shader[n].pVs->Release();
			SetWindowText(hWnd, "vs fail");
			return;
		}

		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD",1, DXGI_FORMAT_R32G32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		UINT numElements = ARRAYSIZE(layout);

		if (dx::pVertexLayout[n] != NULL)
		{
			dx::pVertexLayout[n]->Release();
		}

		hr = dx::g_pd3dDevice->CreateInputLayout(layout, numElements, dx::Shader[n].pVSBlob->GetBufferPointer(), dx::Shader[n].pVSBlob->GetBufferSize(), &dx::pVertexLayout[n]);
		if (FAILED(hr))
		{
			SetWindowText(hWnd, "input layout fail");  return;
		}

		//if (Shader[n].pPSBlob) Shader[n].pPSBlob->Release();
		dx::Shader[n].pPSBlob = NULL;
		hr = D3DCompile(FullShaderVS, strlen(FullShaderVS), NULL, NULL, NULL, "PS", "ps_4_1", D3DCOMPILE_PACK_MATRIX_COLUMN_MAJOR, NULL, &dx::Shader[n].pPSBlob, &dx::pErrorBlob);
		if (hr != S_OK)
		{
			//if (Shader[n].pPSBlob) Shader[n].pPSBlob->Release();
			/* SetWindowText(hWnd, (char*)pErrorBlob->GetBufferPointer()); */
			return;
		}





		if (dx::Shader[n].pPs) dx::Shader[n].pPs->Release();
		hr = dx::g_pd3dDevice->CreatePixelShader(dx::Shader[n].pPSBlob->GetBufferPointer(), dx::Shader[n].pPSBlob->GetBufferSize(), NULL, &dx::Shader[n].pPs);
		if (FAILED(hr))
		{
			if (dx::Shader[n].pPs) dx::Shader[n].pPs->Release();
			SetWindowText(hWnd, "ps fail");
			return;
		}



	}

	#pragma pack (push,1)
	struct CreateVB_
	{
		pName
		pU8 targetSlot;
		pEnum type; EnumList(type1, type2)
		pS32 size;
	};
	#pragma pack (pop)

	void CreateVB()
	{
		CreateVB_* in = (CreateVB_ *)(currentPos + 1);
		BYTE slot = in->targetSlot;
		INT32 size = in->size;
		dx::CreateVB(slot,size);
	}

	#pragma pack(push, 1)
	struct SetRT_
	{
		pU8 texture;
		pU8 level; DefValue(0, 0, 16)
	};
	#pragma pack(pop)

	void SetRT()
	{
		SetRT_* in = (SetRT_*)(currentPos + 1);
		BYTE n= in->texture;

		dx::RenderTargetViewSlice[dx::currentRTn] = in->level;
		
		int cn = 1;
		dx::currentRTn = n;
		if (dx::pTextureType[n] == 1)
		{
			cn = 6;
			dx::g_pImmediateContext->OMSetRenderTargets(cn, &dx::RenderTargetView[n][in->level][0], 0);
		}
		else
		{
			dx::g_pImmediateContext->OMSetRenderTargets(1, &dx::RenderTargetView[dx::currentRTn][0][0], dx::DepthStencilView[dx::currentRTn][0]);
		}
		D3D11_VIEWPORT vp;
		float denom = pow(2, in->level);
		vp.Width = (FLOAT)dx::Tsize[n].x/ denom;
		vp.Height = (FLOAT)dx::Tsize[n].y/ denom;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		dx::g_pImmediateContext->RSSetViewports(1, &vp);
	}


	void CreateMipMap()
	{
		if (dx::currentRTn >= 0)
		{
			dx::g_pImmediateContext->GenerateMips(dx::TextureResView[dx::currentRTn]);
		}
	}

	void ResetRT()
	{
		D3D11_VIEWPORT vp;
		vp.Width = (FLOAT)dx::winW;
		vp.Height = (FLOAT)dx::winH;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		dx::g_pImmediateContext->RSSetViewports(1, &vp);
		dx::g_pImmediateContext->OMSetRenderTargets(1, &dx::g_pRenderTargetView, dx::g_pDepthStencilView);


		dx::currentRTn = -1;

	}

	#pragma pack(push, 1)
	struct SetZBuffer_
	{
		pEnum mode; EnumList(off, read&write,readonly,writeonly)
	};
	#pragma pack(pop)

	void SetZBuffer()
	{
		SetZBuffer_* in = (SetZBuffer_*)(currentPos+1);
		BYTE mode = in->mode;
		if (dx::currentRTn == -1)
		{	
//			dx::g_pImmediateContext->OMSetRenderTargets(1, &dx::g_pRenderTargetView, dx::g_pDepthStencilView);
			dx::g_pImmediateContext->OMSetDepthStencilState(dx::pDSState[mode], 1);
		}
		else
		{
			int slice = dx::RenderTargetViewSlice[dx::currentRTn];
//			dx::g_pImmediateContext->OMSetRenderTargets(1, &dx::RenderTargetView[dx::currentRTn][slice][0], dx::DepthStencilView[dx::currentRTn][slice]);
			dx::g_pImmediateContext->OMSetDepthStencilState(dx::pDSState[mode], 1);
		}
	}

//#define ExternalTextures
	
	//----
#ifdef ExternalTextures
	struct TargaHeader
	{
		unsigned char data1[12];
		unsigned short width;
		unsigned short height;
		unsigned char bpp;
		unsigned char data2;
	};

	unsigned char* m_targaData;

	bool LoadTarga(char* filename, int& height, int& width)
	{
		int error, bpp, imageSize, index, i, j, k;
		FILE* filePtr;
		unsigned int count;
		TargaHeader targaFileHeader;
		unsigned char* targaImage;


		// Open the targa file for reading in binary.
		error = fopen_s(&filePtr, filename, "rb");
		if (error != 0)
		{
			return false;
		}

		// Read in the file header.
		count = (unsigned int)fread(&targaFileHeader, sizeof(TargaHeader), 1, filePtr);
		if (count != 1)
		{
			return false;
		}

		// Get the important information from the header.
		height = (int)targaFileHeader.height;
		width = (int)targaFileHeader.width;
		bpp = (int)targaFileHeader.bpp;

		// Check that it is 32 bit and not 24 bit.
		if (bpp != 32)
		{
			return false;
		}

		// Calculate the size of the 32 bit image data.
		imageSize = width * height * 4;

		// Allocate memory for the targa image data.
		targaImage = new unsigned char[imageSize];
		if (!targaImage)
		{
			return false;
		}

		// Read in the targa image data.
		count = (unsigned int)fread(targaImage, 1, imageSize, filePtr);
		if (count != imageSize)
		{
			return false;
		}

		// Close the file.
		error = fclose(filePtr);
		if (error != 0)
		{
			return false;
		}

		// Allocate memory for the targa destination data.
		m_targaData = new unsigned char[imageSize];
		if (!m_targaData)
		{
			return false;
		}

		// Initialize the index into the targa destination data array.
		index = 0;

		// Initialize the index into the targa image data.
		k = (width * height * 4) - (width * 4);

		// Now copy the targa image data into the targa destination array in the correct order since the targa format is stored upside down.
		for (j = 0; j < height; j++)
		{
			for (i = 0; i < width; i++)
			{
				m_targaData[index + 0] = targaImage[k + 2];  // Red.
				m_targaData[index + 1] = targaImage[k + 1];  // Green.
				m_targaData[index + 2] = targaImage[k + 0];  // Blue
				m_targaData[index + 3] = targaImage[k + 3];  // Alpha

				// Increment the indexes into the targa data.
				k += 4;
				index += 4;
			}

			// Set the targa image data index back to the preceding row at the beginning of the column since its reading it in upside down.
			k -= (width * 8);
		}

		// Release the targa image data now that it was copied into the destination array.
		delete[] targaImage;
		targaImage = 0;

		return true;
	}

	char homeDir[1000];

	void rtSelfLocate()
	{
		char *i;
		char tmpstr[1000];

		strcpy(tmpstr, GetCommandLine());
		i = strstr(tmpstr, ".exe"); while (*(i) != '\\') i--; *(i + 1) = 0;//cut tail
		strcpy(homeDir, tmpstr + 1);

		SetCurrentDirectory(homeDir);
	}

#endif
	//-----

	int mrtRTcount = 0;

#pragma pack(push, 1)
	struct CreateMRT_
	{
		pName
	};
#pragma pack(pop)

	void CreateMRT()
	{
		mrtRTcount = 0;
		ExecuteChildren();
	}

#pragma pack(push, 1)
	struct CreateRTforMRT_
	{
		pU8 texture;
	};
#pragma pack(pop)

	void CreateRTforMRT()
	{
		CreateRTforMRT_* in = (CreateRTforMRT_*)(currentPos + 1);

		D3D11_TEXTURE2D_DESC tdesc;

		dx::pTexture[in->texture]->GetDesc(&tdesc);

		D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;

		renderTargetViewDesc.Format = tdesc.Format;
		renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		renderTargetViewDesc.Texture2D.MipSlice = 0;

		HRESULT result = dx::g_pd3dDevice->CreateRenderTargetView(dx::pTexture[in->texture], &renderTargetViewDesc, &dx::RenderTargetViewCluster[mrtRTcount]);

		if (result != S_OK)
		{
			int a = 0;
		}

		mrtRTcount++;
	}

#pragma pack(push, 1)
	struct SetMRT_
	{
		pU8 mrt;
	};
#pragma pack(pop)

	void SetMRT()
	{
		dx::g_pImmediateContext->OMSetRenderTargets(mrtRTcount, &dx::RenderTargetViewCluster[0], dx::DepthStencilView[dx::currentRTn][0]);
	}


	int tempTexCounter = 0;


#pragma pack(push, 1)
	struct CreateTexture_
	{
		pName
		pEnum size; EnumList(32, 64, 128, 256, 512, 1024, 2048, screen,half,quater)
		pEnum format; EnumList(rgba8,rgba8s,rgba16,rgba32)
		pEnum type; EnumList(flat, cube)
		pEnum ext; EnumList(off, on)
	};
	#pragma pack(pop)

	void CreateTexture()
	{

		if (resCreationFlag == false ) return;
		if (loopPoint != stack::data) return;

		DXGI_FORMAT ft[4] = { DXGI_FORMAT_R8G8B8A8_UNORM ,DXGI_FORMAT_R8G8B8A8_SNORM ,DXGI_FORMAT_R16G16B16A16_FLOAT ,DXGI_FORMAT_R32G32B32A32_FLOAT };

		CreateTexture_* in = (CreateTexture_ *)(currentPos + 1);
		BYTE format = in->format;

		int maxMip = 1;
		BYTE ensize = in->size;
		int sizeX = (int)pow(2, ensize + 5);
		int sizeY = (int)pow(2, ensize + 5);

//			texBitDepth

		if (in->size >= 7)
		{
			int c = 1;
			if (in->size == 8) c = 2;
			if (in->size == 9) c = 4;
			sizeX = dx::winW / c;
			sizeY = dx::winH / c;
		}

		if (in->size < 7)
		{
			float multipler[5] = { 1,.25,.5,2,4 };
			sizeX *= multipler[texQuality];
			sizeY *= multipler[texQuality];
			maxMip = log2(sizeX);

			if (texBitDepth > 0 && format > 1)
			{
				format -= texBitDepth;
				format = max(format, 1);
			}


		}


		if (dx::pTexture[tempTexCounter]!=NULL)
		{
			D3D11_TEXTURE2D_DESC td;
			dx::pTexture[tempTexCounter]->GetDesc(&td);

			bool changed = false;
			if (td.Format != ft[format]) changed = true;
			if (td.Width != sizeX) changed = true;
			if (td.Height != sizeY) changed = true;
			if (td.ArraySize != 1 && in->type == 0) changed = true;
			if (td.ArraySize != 6 && in->type == 1) changed = true;

			if (!changed)
			{
				tempTexCounter++;
				return;
			}
			int a = 0;

		}

		if (dx::pTexture[tempTexCounter]) dx::pTexture[tempTexCounter]->Release();
		if (dx::TextureResView[tempTexCounter]) dx::TextureResView[tempTexCounter]->Release();

		dx::pTextureType[tempTexCounter] = in->type;




#ifdef ExternalTextures
		int th, tw;
		bool Loaded = false;
		if (in->ext == 1)
		{
			rtSelfLocate();
			char n[1255];
			strcpy(n, homeDir);
			strcat(n, in->Name);
			strcat(n, ".tga");
			Loaded = LoadTarga(n, th, tw);
			sizeX = tw;
			sizeY = th;
		}
#endif 

		dx::Tsize[tempTexCounter].x = (float)sizeX;
		dx::Tsize[tempTexCounter].y = (float)sizeY;

		D3D11_TEXTURE2D_DESC tdesc;
		D3D11_SUBRESOURCE_DATA tbsd;

		tbsd.pSysMem = (void *)dx::TextureTempMem;

#ifdef ExternalTextures
		if (Loaded) tbsd.pSysMem = (void *)m_targaData;
#endif
		tbsd.SysMemPitch = sizeX * 4;
		tbsd.SysMemSlicePitch = sizeX * sizeY * 4;

		tdesc.Width = sizeX;
		tdesc.Height = sizeY;
		tdesc.MipLevels = in->size<7 ? 0 : 1;
		tdesc.ArraySize = 1;
		tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
		tdesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

		if (in->type == 1)
		{
			//tdesc.MipLevels = 1;
			tdesc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE | D3D11_RESOURCE_MISC_GENERATE_MIPS;
			tdesc.ArraySize = 6;
		}

		tdesc.CPUAccessFlags = 0;

		tdesc.SampleDesc.Count = 1;
		tdesc.SampleDesc.Quality = 0;
		tdesc.Usage = D3D11_USAGE_DEFAULT;


		
		tdesc.Format = ft[format];
		dx::TFormat[tempTexCounter] = format;

		HRESULT h = dx::g_pd3dDevice->CreateTexture2D(&tdesc, NULL, &dx::pTexture[tempTexCounter]);

#ifdef ExternalTextures
		if (in->ext == 1 && Loaded)
		{
			int rowPitch = (tw * 4) * sizeof(unsigned char);
			dx::g_pImmediateContext->UpdateSubresource(dx::pTexture[tempTexCounter], 0, NULL, m_targaData, rowPitch, 0);
		}
#endif

		D3D11_SHADER_RESOURCE_VIEW_DESC svDesc;
		svDesc.Format = tdesc.Format;

		svDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

		if (in->type == 1)
		{
			svDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
			svDesc.TextureCube.MostDetailedMip = 0;
			svDesc.TextureCube.MipLevels = -1;

		}
		else
		{
			svDesc.Texture2D.MipLevels = -1;
			svDesc.Texture2D.MostDetailedMip = 0;
		}

		h = dx::g_pd3dDevice->CreateShaderResourceView(dx::pTexture[tempTexCounter], &svDesc, &dx::TextureResView[tempTexCounter]);

#ifdef ExternalTextures
		if (in->ext == 1 && Loaded)
		{
			dx::g_pImmediateContext->GenerateMips(dx::TextureResView[tempTexCounter]);
		}
#endif

		if (in->ext == 0)
		{

		D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;

		renderTargetViewDesc.Format = tdesc.Format;
		renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		renderTargetViewDesc.Texture2D.MipSlice = 0;

		if (in->type == 1)
		{
			renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
			renderTargetViewDesc.Texture2DArray.ArraySize = 1;
			renderTargetViewDesc.Texture2DArray.MipSlice = 0;

			for (int j = 0; j < 6; j++)
			{
				renderTargetViewDesc.Texture2DArray.FirstArraySlice = j;
				HRESULT result = dx::g_pd3dDevice->CreateRenderTargetView(dx::pTexture[tempTexCounter], &renderTargetViewDesc, &dx::RenderTargetView[tempTexCounter][0][j]);
				int a = 0;
			}
		}

		else
		{
			for (int m = 0; m < maxMip; m++)
			{
				renderTargetViewDesc.Texture2D.MipSlice = m;
				HRESULT result = dx::g_pd3dDevice->CreateRenderTargetView(dx::pTexture[tempTexCounter], &renderTargetViewDesc, &dx::RenderTargetView[tempTexCounter][m][0]);
			}
		}

		//depth
		HRESULT hr = S_OK;
		D3D11_TEXTURE2D_DESC descDepth;
		ZeroMemory(&descDepth, sizeof(descDepth));
		descDepth.Width = sizeX;
		descDepth.Height = sizeY;
		descDepth.MipLevels = in->size<7 ? 0 : 1;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_R32_TYPELESS;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;
		hr = dx::g_pd3dDevice->CreateTexture2D(&descDepth, NULL, &dx::pDepth[tempTexCounter]);

		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
		ZeroMemory(&descDSV, sizeof(descDSV));
		descDSV.Format = DXGI_FORMAT_D32_FLOAT;
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSV.Flags = 0;

		for (int m = 0; m < maxMip; m++)
		{
			descDSV.Texture2D.MipSlice = m;
			hr = dx::g_pd3dDevice->CreateDepthStencilView(dx::pDepth[tempTexCounter], &descDSV, &dx::DepthStencilView[tempTexCounter][m]);
		}

		svDesc.Format = DXGI_FORMAT_R32_FLOAT;
		svDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		svDesc.Texture2D.MostDetailedMip = 0;
		svDesc.Texture2D.MipLevels = 1;

		hr = dx::g_pd3dDevice->CreateShaderResourceView(dx::pDepth[tempTexCounter], &svDesc, &dx::DepthResView[tempTexCounter]);

		dx::RTStageColorIndex[tempTexCounter] = in->size+in->format*10;
		dx::RTStageDepthIndex[tempTexCounter] = in->size;

	}

//#ifdef EditMode
		//resourceCreationPtr[1][tempTexCounter] = currentPos+1;
		//resourceCreationPtr[1][tempTexCounter + 1] = NULL;
		//strcpy(resourceCreationName[1][tempTexCounter], in->Name);
//#endif

		tempTexCounter++;
	}


#pragma pack(push, 1)
	struct ShowScene_
	{
		pU8 scene;
		pEnum timeline; EnumList(off, on)
		pS16 x;
		pS16 y;
		pS16 length;
		pEnum camera; EnumList(view, depth)
	};
#pragma pack(pop)

	void ShowScene()
	{

		ShowScene_* in = (ShowScene_*)(currentPos + 1);

		sceneCamera = in->camera;

		BYTE* ptr = stack::data;

		overrideCull = 0;
		overridePS = 0;
		overrideBlend = 0;

		ExecuteChildren();

		if (in->timeline)
		{
			if (in->x > Commands::cursorF || in->x + in->length < Commands::cursorF)
			{
				return;
			}
		}

		BYTE scene = in->scene;
		//BYTE scene = *(BYTE*)(currentPos + CmdDesc[*(BYTE*)currentPos].ParamOffset[0]);

		CallStack[CallStackPointer].pointer = currentPos;

		CallStack[CallStackPointer].left = in->x;
		CallStack[CallStackPointer].right = in->x + in->length;

		CallStackPointer++;

		currentPos = ScenePointer[scene];
	}



	//-------------------------------------
	//       execute command list
 
	int ccounter;


	void Render()
	{
		BYTE* ptr = stack::data;
		int cShader = 0;
		int cTexture = 0;
		int cScene = 0;
		int cParam = 0;

		resCreationFlag = true;
		tempTexCounter = 0;
		shaderSlotCounter = 0;
		//if (!SceneLocateFlag)
		{
			while (*ptr != 0)
			{
				if (CmdDesc[*ptr].Routine == Scene)
				{
					ScenePointer[cScene] = ptr;
					//	#ifdef EditMode
					resourceCreationPtr[2][cScene] = ptr + 1;
					resourceCreationPtr[2][cScene + 1] = NULL;
					Scene_* in = (Scene_*)(ptr + 1);
					strcpy(resourceCreationName[2][cScene], in->Name);
					//	#endif	
					cScene++;
				}

				//	#ifdef EditMode
				if (CmdDesc[*ptr].Routine == CreateShader)
				{
					resourceCreationPtr[0][cShader] = ptr + 1;
					resourceCreationPtr[0][cShader + 1] = NULL;
					CreateShader_* in = (CreateShader_*)(ptr + 1);;
					strcpy(resourceCreationName[0][cShader], in->Name);

					currentPos = ptr;
					currentCmd = *currentPos;
					CmdDesc[*ptr].Routine();

					cShader++;
				}

				if (CmdDesc[*ptr].Routine == CreateTexture)
				{
					resourceCreationPtr[1][cTexture] = ptr + 1;
					resourceCreationPtr[1][cTexture + 1] = NULL;
					CreateTexture_* in = (CreateTexture_*)(ptr + 1);;
					strcpy(resourceCreationName[1][cTexture], in->Name);
					
					currentPos = ptr;
					currentCmd = *currentPos;
					CmdDesc[*ptr].Routine();

					cTexture++;
				}

				if (CmdDesc[*ptr].Routine == DefineParams)
				{
					resourceCreationPtr[3][cParam] = ptr + 1;
					resourceCreationPtr[3][cParam + 1] = NULL;
					DefineParams_* in = (DefineParams_*)(ptr + 1);;
					strcpy(resourceCreationName[3][cParam], in->Name);
					cParam++;
				}

				//	#endif	


				ptr += CmdDesc[*ptr].Size;
			}
		}

		resCreationFlag = false;

#ifndef EditMode
		SceneLocateFlag = true;
#endif	

		ccounter = 0; int precmds = 1;

		//todo:: refactor
		if (loopPoint == stack::data)
		{
			//resources
		/*	currentPos = loopPoint;
			while (*currentPos != (BYTE)0)
			{
				currentCmd = *currentPos;
				if (CmdDesc[currentCmd].Routine == CreateShader ||
					CmdDesc[currentCmd].Routine == CreateTexture)
				{
					CmdDesc[currentCmd].Routine();
				}

				currentPos += CmdDesc[*currentPos].Size;
			}*/

			//precalc actions
			currentPos = loopPoint;
			while (*currentPos != (BYTE)0 && CmdDesc[*currentPos].Routine != EndDraw && CmdDesc[*currentPos].Routine != MainLoop)
			{
				currentCmd = *currentPos;
				if (CmdDesc[currentCmd].Routine == ShowScene ||
					CmdDesc[currentCmd].Routine == Scene ||
					CmdDesc[currentCmd].Routine == EndScene)
				{
					CmdDesc[currentCmd].Routine();
				}
					
				currentPos += CmdDesc[*currentPos].Size;
				precmds++;
			}
		}

		dx::currentRTn = -1;
		//tempTexCounter = 0;
		//shaderSlotCounter = 0;

		if (timer::resetStartFlag) 
		{
			timer::StartTime = timer::GetCounter();
			timer::resetStartFlag = false;
		}

		currentChannel = -1;
		master = false;
		cursorF = playmode*(timer::prevtick - timer::StartTime) / (1.f / 60.f * 1000.f) + StartPosition;
		cursor = (int)floor(cursorF);
		cursorM = (int)(cursorF+preCount);

		#ifdef EditMode
			if (playonenoteFlag && cursorM > playingNoteEnd)
			{
				playonenoteFlag = false;
				Commands::playmode = 0;
				Commands::StopWave();
			}
		#endif

		if (timeLoopEnable && playStartPoint < timeLoopStart+timeLoopLen && cursorM>timeLoopStart)
		{
			if (playmode==1||prerender==1)
			{
				if (cursorM > timeLoopLen + timeLoopStart)
				{
					playStartPoint = timeLoopStart;
				}

				cursorM = (cursorM - timeLoopStart) % (timeLoopLen)+timeLoopStart;
				cursorF = (int(cursorF) - timeLoopStart) % (timeLoopLen)+timeLoopStart;
				cursor = (int)floor(cursorF);
			}
		}

		currentPos = loopPoint;

		while (*currentPos != (BYTE)0 &&CmdDesc[*currentPos].Routine!=EndDraw)
		{
			currentCmd = *currentPos;
 			CmdDesc[currentCmd].Routine();
			currentPos += CmdDesc[*currentPos].Size;
			
			//precalcing progress bar;
			if (loopPoint == stack::data)
			{
				ccounter++;
				char s_progress[65];
				char status[65];
				strcpy(status, "Precalcing ");
				_itoa(100*ccounter / precmds, s_progress, 10);
				strcat(s_progress, "%");
				strcat(status, s_progress);
				SetWindowText(hWnd, status);
			}
		}

		ResetRT();

		dx::g_pImmediateContext->OMSetRenderTargets(1, &dx::g_pRenderTargetView, dx::g_pDepthStencilView);
	}

}