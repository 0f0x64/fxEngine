

#define RTMIDI_DLL_PUBLIC

#include <vector>

class MidiApi;

class RTMIDI_DLL_PUBLIC RtMidi
{
 public:
  virtual void openPort( unsigned int portNumber = 0 ) = 0;
  virtual void closePort( void ) = 0;
  
 protected:
  RtMidi();
  virtual ~RtMidi();
  MidiApi *rtapi_;
};

class RTMIDI_DLL_PUBLIC RtMidiIn : public RtMidi
{
 public:

  //! User callback function type definition.
  typedef void (*RtMidiCallback)( double timeStamp, std::vector<unsigned char> *message, void *userData );

  RtMidiIn();

  //! If a MIDI connection is still open, it will be closed by the destructor.
  ~RtMidiIn ( void ) throw();

  void openPort( unsigned int portNumber = 0);
  void setCallback( RtMidiCallback callback, void *userData = 0 );
  void closePort( void );
  void ignoreTypes( bool midiSysex = true, bool midiTime = true, bool midiSense = true );
  double getMessage( std::vector<unsigned char> *message );

 protected:

};


class RTMIDI_DLL_PUBLIC MidiApi
{
 public:

  MidiApi();
  virtual ~MidiApi();
  virtual void openPort( unsigned int portNumber) = 0;
  virtual void closePort( void ) = 0;
     
protected:
  virtual void initialize() = 0;

  void *apiData_;
  bool connected_;
};

class RTMIDI_DLL_PUBLIC MidiInApi : public MidiApi
{
 public:

  MidiInApi();
  virtual ~MidiInApi( void );
  void setCallback( RtMidiIn::RtMidiCallback callback, void *userData );
  virtual void ignoreTypes( bool midiSysex, bool midiTime, bool midiSense );
  double getMessage( std::vector<unsigned char> *message );

  struct MidiMessage {
    std::vector<unsigned char> bytes;
    double timeStamp;
    MidiMessage()
      : bytes(0), timeStamp(0.0) {}
  };

  struct MidiQueue {
    unsigned int front;
    unsigned int back;
    unsigned int ringSize;
    MidiMessage *ring;

    MidiQueue()
      : front(0), back(0), ringSize(0), ring(0) {}
    bool push( const MidiMessage& );
    bool pop( std::vector<unsigned char>*, double* );
    unsigned int size( unsigned int *back=0, unsigned int *front=0 );
  };

  struct RtMidiInData {
    MidiQueue queue;
    MidiMessage message;
    unsigned char ignoreFlags;
    bool doInput;
    bool firstMessage;
    void *apiData;
    bool usingCallback;
    RtMidiIn::RtMidiCallback userCallback;
    void *userData;
    bool continueSysex;

    // Default constructor.
    RtMidiInData()
      : ignoreFlags(7), doInput(false), firstMessage(true), apiData(0), usingCallback(false),
        userCallback(0), userData(0), continueSysex(false) {}
  };

 protected:
  RtMidiInData inputData_;
};

inline void RtMidiIn :: openPort( unsigned int portNumber) { rtapi_->openPort( portNumber ); }
inline void RtMidiIn :: closePort( void ) { rtapi_->closePort(); }
inline void RtMidiIn :: setCallback( RtMidiCallback callback, void *userData ) { static_cast<MidiInApi *>(rtapi_)->setCallback( callback, userData ); }
inline void RtMidiIn :: ignoreTypes( bool midiSysex, bool midiTime, bool midiSense ) { static_cast<MidiInApi *>(rtapi_)->ignoreTypes( midiSysex, midiTime, midiSense ); }
inline double RtMidiIn :: getMessage( std::vector<unsigned char> *message ) { return static_cast<MidiInApi *>(rtapi_)->getMessage( message ); }


class MidiInWinMM : public MidiInApi
{
public:
	MidiInWinMM();
	~MidiInWinMM(void);
	void openPort(unsigned int portNumber);
	void closePort(void);

protected:
	void initialize();
};

RtMidi::RtMidi()
	: rtapi_(0)
{
}

RtMidi :: ~RtMidi()
{
	delete rtapi_;
	rtapi_ = 0;
}


RTMIDI_DLL_PUBLIC RtMidiIn::RtMidiIn()
	: RtMidi()
{
	delete rtapi_;
	rtapi_ = 0;
	rtapi_ = new MidiInWinMM();
}

RtMidiIn :: ~RtMidiIn() throw()
{
}

MidiApi::MidiApi(void)
	: apiData_(0), connected_(false)
{
}

MidiApi :: ~MidiApi(void)
{
}

MidiInApi::MidiInApi()
	: MidiApi()
{
	// Allocate the MIDI queue.
	inputData_.queue.ringSize = 100;
	if (inputData_.queue.ringSize > 0)
		inputData_.queue.ring = new MidiMessage[inputData_.queue.ringSize];
}

MidiInApi :: ~MidiInApi(void)
{
	// Delete the MIDI queue.
	if (inputData_.queue.ringSize > 0) delete[] inputData_.queue.ring;
}

void MidiInApi::setCallback(RtMidiIn::RtMidiCallback callback, void *userData)
{
	inputData_.userCallback = callback;
	inputData_.userData = userData;
	inputData_.usingCallback = true;
}

void MidiInApi::ignoreTypes(bool midiSysex, bool midiTime, bool midiSense)
{
	inputData_.ignoreFlags = 0;
	if (midiSysex) inputData_.ignoreFlags = 0x01;
	if (midiTime) inputData_.ignoreFlags |= 0x02;
	if (midiSense) inputData_.ignoreFlags |= 0x04;
}

double MidiInApi::getMessage(std::vector<unsigned char> *message)
{
	message->clear();

	double timeStamp;
	if (!inputData_.queue.pop(message, &timeStamp))
		return 0.0;

	return timeStamp;
}

unsigned int MidiInApi::MidiQueue::size(unsigned int *__back,
	unsigned int *__front)
{
	// Access back/front members exactly once and make stack copies for
	// size calculation
	unsigned int _back = back, _front = front, _size;
	if (_back >= _front)
		_size = _back - _front;
	else
		_size = ringSize - _front + _back;

	// Return copies of back/front so no new and unsynchronized accesses
	// to member variables are needed.
	if (__back) *__back = _back;
	if (__front) *__front = _front;
	return _size;
}

// As long as we haven't reached our queue size limit, push the message.
bool MidiInApi::MidiQueue::push(const MidiInApi::MidiMessage& msg)
{
	// Local stack copies of front/back
	unsigned int _back, _front, _size;

	// Get back/front indexes exactly once and calculate current size
	_size = size(&_back, &_front);

	if (_size < ringSize - 1)
	{
		ring[_back] = msg;
		back = (back + 1) % ringSize;
		return true;
	}

	return false;
}

bool MidiInApi::MidiQueue::pop(std::vector<unsigned char> *msg, double* timeStamp)
{
	// Local stack copies of front/back
	unsigned int _back, _front, _size;

	// Get back/front indexes exactly once and calculate current size
	_size = size(&_back, &_front);

	if (_size == 0)
		return false;

	// Copy queued message to the vector pointer argument and then "pop" it.
	msg->assign(ring[_front].bytes.begin(), ring[_front].bytes.end());
	*timeStamp = ring[_front].timeStamp;

	// Update front
	front = (front + 1) % ringSize;
	return true;
}

//*********************************************************************//
//  Common MidiOutApi Definitions
//*********************************************************************//

#include <mmsystem.h>

#define  RT_SYSEX_BUFFER_SIZE 1024
#define  RT_SYSEX_BUFFER_COUNT 4

// A structure to hold variables related to the CoreMIDI API
// implementation.
struct WinMidiData {
	HMIDIIN inHandle;    // Handle to Midi Input Device
	HMIDIOUT outHandle;  // Handle to Midi Output Device
	DWORD lastTime;
	MidiInApi::MidiMessage message;
	LPMIDIHDR sysexBuffer[RT_SYSEX_BUFFER_COUNT];
	CRITICAL_SECTION _mutex; // [Patrice] see https://groups.google.com/forum/#!topic/mididev/6OUjHutMpEo
};

//*********************************************************************//
//  API: Windows MM
//  Class Definitions: MidiInWinMM
//*********************************************************************//

static void CALLBACK midiInputCallback(HMIDIIN /*hmin*/,
	UINT inputStatus,
	DWORD_PTR instancePtr,
	DWORD_PTR midiMessage,
	DWORD timestamp)
{
	if (inputStatus != MIM_DATA && inputStatus != MIM_LONGDATA && inputStatus != MIM_LONGERROR) return;

	MidiInApi::RtMidiInData *data = (MidiInApi::RtMidiInData *)instancePtr;
	WinMidiData *apiData = static_cast<WinMidiData *> (data->apiData);

	// Calculate time stamp.
	if (data->firstMessage == true) {
		apiData->message.timeStamp = 0.0;
		data->firstMessage = false;
	}
	else apiData->message.timeStamp = (double)(timestamp - apiData->lastTime) * 0.001;

	if (inputStatus == MIM_DATA) { // Channel or system message

	  // Make sure the first byte is a status byte.
		unsigned char status = (unsigned char)(midiMessage & 0x000000FF);
		if (!(status & 0x80)) return;

		// Determine the number of bytes in the MIDI message.
		unsigned short nBytes = 1;
		if (status < 0xC0) nBytes = 3;
		else if (status < 0xE0) nBytes = 2;
		else if (status < 0xF0) nBytes = 3;
		else if (status == 0xF1) {
			if (data->ignoreFlags & 0x02) return;
			else nBytes = 2;
		}
		else if (status == 0xF2) nBytes = 3;
		else if (status == 0xF3) nBytes = 2;
		else if (status == 0xF8 && (data->ignoreFlags & 0x02)) {
			// A MIDI timing tick message and we're ignoring it.
			return;
		}
		else if (status == 0xFE && (data->ignoreFlags & 0x04)) {
			// A MIDI active sensing message and we're ignoring it.
			return;
		}

		// Copy bytes to our MIDI message.
		unsigned char *ptr = (unsigned char *)&midiMessage;
		for (int i = 0; i < nBytes; ++i) apiData->message.bytes.push_back(*ptr++);
	}
	else { // Sysex message ( MIM_LONGDATA or MIM_LONGERROR )
		MIDIHDR *sysex = (MIDIHDR *)midiMessage;
		if (!(data->ignoreFlags & 0x01) && inputStatus != MIM_LONGERROR) {
			// Sysex message and we're not ignoring it
			for (int i = 0; i < (int)sysex->dwBytesRecorded; ++i)
				apiData->message.bytes.push_back(sysex->lpData[i]);
		}

		// The WinMM API requires that the sysex buffer be requeued after
		// input of each sysex message.  Even if we are ignoring sysex
		// messages, we still need to requeue the buffer in case the user
		// decides to not ignore sysex messages in the future.  However,
		// it seems that WinMM calls this function with an empty sysex
		// buffer when an application closes and in this case, we should
		// avoid requeueing it, else the computer suddenly reboots after
		// one or two minutes.
		if (apiData->sysexBuffer[sysex->dwUser]->dwBytesRecorded > 0)
		{
			EnterCriticalSection(&(apiData->_mutex));
			MMRESULT result = midiInAddBuffer(apiData->inHandle, apiData->sysexBuffer[sysex->dwUser], sizeof(MIDIHDR));
			LeaveCriticalSection(&(apiData->_mutex));

			if (data->ignoreFlags & 0x01) return;
		}
		else return;
	}

	// Save the time of the last non-filtered message
	apiData->lastTime = timestamp;

	if (data->usingCallback) {
		RtMidiIn::RtMidiCallback callback = (RtMidiIn::RtMidiCallback) data->userCallback;
		callback(apiData->message.timeStamp, &apiData->message.bytes, data->userData);
	}
	else {
		// As long as we haven't reached our queue size limit, push the message.
		data->queue.push(apiData->message);
	}

	// Clear the vector for the next input message.
	apiData->message.bytes.clear();
}

MidiInWinMM::MidiInWinMM()
	: MidiInApi()
{
	MidiInWinMM::initialize();
}

MidiInWinMM :: ~MidiInWinMM()
{
	// Close a connection if it exists.
	MidiInWinMM::closePort();

	WinMidiData *data = static_cast<WinMidiData *> (apiData_);
	DeleteCriticalSection(&(data->_mutex));
	delete data;
}

void MidiInWinMM::initialize()
{
	unsigned int nDevices = midiInGetNumDevs();

	// Save our api-specific connection information.
	WinMidiData *data = (WinMidiData *) new WinMidiData;
	apiData_ = (void *)data;
	inputData_.apiData = (void *)data;
	data->message.bytes.clear();  // needs to be empty for first input message

	InitializeCriticalSectionAndSpinCount(&(data->_mutex), 0x00000400);
}

void MidiInWinMM::openPort(unsigned int portNumber)
{

	unsigned int nDevices = midiInGetNumDevs();

	WinMidiData *data = static_cast<WinMidiData *> (apiData_);
	MMRESULT result = midiInOpen(&data->inHandle,
		portNumber,
		(DWORD_PTR)&midiInputCallback,
		(DWORD_PTR)&inputData_,
		CALLBACK_FUNCTION);


	// Allocate and init the sysex buffers.
	for (int i = 0; i < RT_SYSEX_BUFFER_COUNT; ++i) {
		data->sysexBuffer[i] = (MIDIHDR*) new char[sizeof(MIDIHDR)];
		data->sysexBuffer[i]->lpData = new char[RT_SYSEX_BUFFER_SIZE];
		data->sysexBuffer[i]->dwBufferLength = RT_SYSEX_BUFFER_SIZE;
		data->sysexBuffer[i]->dwUser = i; // We use the dwUser parameter as buffer indicator
		data->sysexBuffer[i]->dwFlags = 0;

		result = midiInPrepareHeader(data->inHandle, data->sysexBuffer[i], sizeof(MIDIHDR));

		// Register the buffer.
		result = midiInAddBuffer(data->inHandle, data->sysexBuffer[i], sizeof(MIDIHDR));
	}

	result = midiInStart(data->inHandle);


	connected_ = true;
}

void MidiInWinMM::closePort(void)
{
	if (connected_) {
		WinMidiData *data = static_cast<WinMidiData *> (apiData_);
		EnterCriticalSection(&(data->_mutex));
		midiInReset(data->inHandle);
		midiInStop(data->inHandle);

		for (int i = 0; i < RT_SYSEX_BUFFER_COUNT; ++i)
		{
			int result = midiInUnprepareHeader(data->inHandle, data->sysexBuffer[i], sizeof(MIDIHDR));
			delete[] data->sysexBuffer[i]->lpData;
			delete[] data->sysexBuffer[i];
		}

		midiInClose(data->inHandle);
		data->inHandle = 0;
		connected_ = false;
		LeaveCriticalSection(&(data->_mutex));
	}
}







