

	bool midiNotePressed = false;
	int midiNoteCode = 0;
	int midiNoteVelocity = 0;
	int midiNoteVelocityMapping = 0;

	void midiCallBack(double deltatime, std::vector< unsigned char > *message, void *userData)
	{
		if (message->size()>2)
		{
			if ((int)message->at(0) == 144)
			{
				midiNotePressed = true;
				midiNoteCode = (int)message->at(1);
				midiNoteCode = clamp(midiNoteCode - 12*2, 1, 12 * 8 - 1);//c3
				midiNoteVelocity = (int)message->at(2)*2;
			}
			else
			{
				midiNotePressed = false;
			}

		}
	}

	std::vector<unsigned char> message;

	void midiSetup()
	{
		midiin = new RtMidiIn();

		midiPortCount = midiInGetNumDevs();

		MIDIINCAPS deviceCaps;
		midiInGetDevCaps(midiPort, &deviceCaps, sizeof(MIDIINCAPS));
		strncpy(midiPortName, deviceCaps.szPname,31);

		if (midiPortCount > 0)
		{
			midiin->openPort(midiPort);
			midiin->setCallback(&midiCallBack);
			midiin->ignoreTypes(false, false, false);
		}
	}

