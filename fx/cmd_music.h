//TODO: separate to classes
void Render();

//common
#define PI 3.14159265359
#define PIf 3.14159265359f
#define M_PI 3.14159265358979323846f
#define M_LN2 0.69314718055994530942f

#define SAMPLERATE 44100
int framelen = SAMPLERATE / 60;
#define FRAMELEN 735
#define FRAMELENF 735.f
#define PREBUFFER 4
#define maxEnvPoints 1900
#define MAXNOTES 900

bool master = false;
signed short MasterBPM = 0;

//player
void ClearAllTemporalData();
int playmode = 0;
int prerender = 0;
int cursorM = 0;
double StartPosition = 0;
bool timeLoopEnable = false;
signed short playStartPoint = 0;
signed short timeLoopStart = 0;
signed short timeLoopLen = 0;
int preCount = 0;

//channel
BYTE channel_amp[MAXCHANNELS];
int currentChannel = 0;
bool channelOn[MAXCHANNELS];
BYTE channelVolume[MAXCHANNELS];
signed char channelPan[MAXCHANNELS];
bool channelSolo[MAXCHANNELS];
float sendTable[MAXCHANNELS];

//clip
bool inClip[MAXCHANNELS];
double clipLeft[MAXCHANNELS];
double clipRight[MAXCHANNELS];
double clipLen = 0;

//note
bool activeNoteFound[MAXCHANNELS];
float NoteSize, CurrentNoteSize, NextNoteSize;//for processing swing

//envelope
typedef struct
{
	float x;
	float y;
	float slide;
	bool r;
	bool off;
	float send[4];
	int var;

} EnvPoint;

typedef struct
{
	EnvPoint position[MAXNOTES*2];
	int CollectedPoints;
	float loopStart;
	float loopEnd;
	int loopStartI;
	int loopEndI;
	pEnum link;
	pEnum loop;
	pEnum retrigger;
	bool Used;
	float loopStartY;
	float range;
	float(*opFunc)(float value, float env);
	float(*mixFunc)(float value, float env);

} EnvPack;


#define NoteIndex MAXPARAM
#define NoteSendIndex0 MAXPARAM+1
#define NoteSendIndex1 MAXPARAM+2
#define NoteSendIndex2 MAXPARAM+3
#define NoteSendIndex3 MAXPARAM+4
#define NoteVariatonIndex MAXPARAM+5

EnvPack env[MAXPARAM*2+1];
//EnvPack envClip[MAXPARAM + EXTRAENV];
float envValue[MAXPARAM*2+1][FRAMELEN];
//float envValueClip[MAXPARAM][FRAMELEN];

void PlayWave(int start);
void ProcessRetriggerAndNoteOff();

double fracRange(double a, double range)
{
	return a - range * floor(a / range);
}

float fracRangef(float a, float range)
{
	return a - range * floorf(a / range);
}

int fracRanged(int a, int range)
{
	return a - range * (int)floor(a / (float)range);
}


void StopWave()
{
	sound::pSourceVoice->Stop(0, 0);
}

#pragma pack (push, 1)
struct TimeLoop_
{
	pEnum active; EnumList(off,on)
	pS32 x; DefValue(0, 0, 2147483647)
	pS32 length; DefValue(0, 4410000, 2147483647)
};
#pragma pack (pop)

void TimeLoop()
{
#ifdef EditMode
	
	TimeLoop_* in = (TimeLoop_*)(currentPos + 1);

	bool _timeLoopEnable = in->active;
	int  _timeLoopStart = in->x/ FRAMELEN;
	int _timeLoopLen = max(in->length/ FRAMELEN,1);

	//restart sound if loop settings changed
	if ((_timeLoopEnable != timeLoopEnable ||
		_timeLoopStart != timeLoopStart ||
		_timeLoopLen != timeLoopLen)
		&& (playmode == 1 || prerender == 1))
	{
		timeLoopEnable = _timeLoopEnable;
		timeLoopStart = _timeLoopStart;
		timeLoopLen = _timeLoopLen;

		StartPosition = timeLoopStart;
		PlayWave(timeLoopStart);
	}

	timeLoopEnable = _timeLoopEnable;
	timeLoopStart = _timeLoopStart;
	timeLoopLen = _timeLoopLen;

	if (!timeLoopEnable) return;

	if (playmode == 1 || prerender == 1)
	{
		for (int x = 0; x < MAXCHANNELS - 1; x++) activeNoteFound[x] = false;
	}

#endif
}

void EndMusic()
{

}

#ifdef EditMode
bool playonenoteFlag = false;
BYTE* musicPtr = NULL;
#endif

#pragma pack(push, 1)
struct Music_
{
	pS16 masterBPM; DefValue(30, 120, 240)
};
#pragma pack(pop)

void Music()
{
	Music_* in = (Music_*)(currentPos + 1);
	MasterBPM = in->masterBPM;

#ifdef EditMode

	musicPtr = currentPos;

	if (playonenoteFlag == false && playmode == 0)
	{
		//skip all music
		while (*currentPos != 0 && CmdDesc[*currentPos].Routine != EndMusic)
			currentPos += CmdDesc[*currentPos].Size;

	}
#endif

}


int prevNote[MAXCHANNELS];
int curNote[MAXCHANNELS];
int nextNote[MAXCHANNELS];
bool firstClip = false;

#pragma pack (push,1)
struct Channel_
{
	pName
	pU8 volume; EnvOn DefValue(0, 255, 255)
	pS8 pan; EnvOn
	pU8 send;
	pEnum mute; EnumList(off, on)
	pEnum solo; EnumList(off, on)
	CMD_DATA
	BYTE highestNote;
	BYTE lowestNote;
};
#pragma pack (pop)

int activeChannelCounter = 0;

void Channel()
{
	firstClip = false;
	Channel_* in = (Channel_*)(currentPos + 1);

	currentChannel++;
	sendTable[currentChannel] = in->send/255.f;

	bool solomode = false;

	//if some channel in solo mode set flag
	for (int x = 0; x < activeChannelCounter; x++)
	{
		if (channelSolo[x] == true) solomode = true;
	}

	if (currentChannel >= 0 && currentChannel < activeChannelCounter)
	{
		channelOn[currentChannel] = false;
		channelVolume[currentChannel] = in->volume;
		channelPan[currentChannel] = in->pan;
		auto mute = in->mute;
		auto solo = in->solo;
		channelSolo[currentChannel] = solo;

		if (solomode)
		{
			channelVolume[currentChannel] *= solo;
		}
		else
		{
			channelVolume[currentChannel] *= (1 - mute);
		}

		//clean buffer
		ZeroMemory(sound::channel[currentChannel], framelen * 2 * 2);
		/*for (int i = 0; i < framelen * 2; i += 2)
		{
			*(signed short*)(sound::channel[currentChannel] + (i) * 2) = 0;
			*(signed short*)(sound::channel[currentChannel] + (i) * 2 + 2) = 0;
		}*/
	}

	env[NoteIndex].Used = false;

	for (int t = 0; t < MAXPARAM*2; t++)
	{
		env[t].Used = false;
	}

}


#pragma pack(push, 1)
struct Note_
{
		pU8 Note;
		pU8 Retrigger;
		pU8 Slide;
		pU8 Variation;
		pU8 s0;
		pU8 s1;
		pU8 s2;
		pU8 s3;
};
#pragma pack(pop)

void Note()
{

}

void CreateNoteEnv(BYTE* i);

#pragma pack(push, 1)
struct Clip_ 
{
		pS32 x; DefValue(0, 0, 2147483647)
		pS16 length; DefValue(1, 32, 256)
		pS16 repeat; DefValue(1, 1, 480)
		pU8 volume; DefValue(0,255,255)
		pS8 bpmScale; DefValue(1, 1, 16)
		pS8 swing; DefValue(-127, 0, 127)
		pU8 grid; DefValue(0, 4, 32)
};
#pragma pack(pop)

Clip_* CurrentClip;

void SkipClipChildren();
void ExecuteChildren();

float currentClipBPM;

float SilencePoint;

int GetFirstNoteindex(Clip_* in)
{
	BYTE* i = currentPos;
	i+= CmdDesc[*i].Size;
	int ns = CmdDesc[*i].Size;

	int startIndex = 0;
	float start = playStartPoint *FRAMELEN;
	float offset = start - in->x;

	if (offset >= 0)
	{
		float noteLen = SAMPLERATE * 60.f / (Commands::MasterBPM*in->bpmScale);
		startIndex = offset / noteLen;
	}

	for (int count= 0; count < in->length; count++)
	{
		int c = fracRanged(count + startIndex, in->length);
		Note_* inN = (Note_*)(i +c*ns + 1);

		if (inN->Note != 0 && inN->Note != 255)
		{
			return count + startIndex;
		}
	}

	return INT_MAX;
}

void Clip()
{
	if (!firstClip)
	{
		firstClip = true;
		CurrentClip = NULL;
	}

	if (currentChannel < 0)
	{
		SkipClipChildren();
		return;
	}

	Clip_* in = (Clip_*)(currentPos+1);

	auto left = in->x;
	auto repeat = in->repeat;

	//todo: process scale
	float scale = 1; 
	if (in->bpmScale < 0) scale = in->bpmScale;
	if (in->bpmScale > 0) scale = 1./(float)in->bpmScale;

	if (Commands::MasterBPM*in->bpmScale == 0) return;

	//!!!TODO: calc swing
	auto len = in->length*SAMPLERATE*60. / (Commands::MasterBPM*in->bpmScale);

	double right = left + repeat * len;

	if (len == 0 || cursorM * FRAMELEN > right || (cursorM * FRAMELEN < left))
	{
		SkipClipChildren();
		return;
	}

	if (cursorM * FRAMELEN < left + GetFirstNoteindex(in)*SAMPLERATE*60. / (Commands::MasterBPM*in->bpmScale))
	{
		SkipClipChildren();
		return;
	}

	currentClipBPM = Commands::MasterBPM*in->bpmScale;
	CurrentClip = in;
	clipLen = len;
	inClip[currentChannel] = true;
	clipLeft[currentChannel] = left;
	clipRight[currentChannel] = right;
	channelOn[currentChannel] = true;
	NoteSize = SAMPLERATE * 60.f / currentClipBPM;

	for (int t = 0; t < MAXPARAM*2+1; t++)
	{
		env[t].Used = false;
	}

	CreateNoteEnv(currentPos + CmdDesc[currentCmd].Size);

	ExecuteChildren();
}

float EnvOpPlus(float value, float env)
{
	return value + env;
}

float EnvOpMinus(float value, float env)
{
	return value - env;
}

float EnvOpCpy(float value, float env)
{
	return env;
}

float EnvOpMul(float value, float env)
{
	return value * env;
}

float EnvOpDiv(float value, float env)
{
	return value / env;
}

enum{ envOp_plus, envOp_minus, envOp_cpy, envOp_mul, envOp_div};

float(*EnvOpArray[5])(float value, float env) = { EnvOpCpy, EnvOpPlus,EnvOpMinus, EnvOpMul, EnvOpDiv };

void ResetEnv()
{
	for (int x = 0; x < MAXPARAM; x++)
	{
		env[x].CollectedPoints = 0;
		env[x].Used = false;
	}
}

#pragma pack (push,1)
struct Point_
{
	pS32 x; DefValue(0, 0, 25000000);
	pS16 y;
};
#pragma pack (pop)

void Point()
{

}

#pragma pack (push,1)
struct Envelope_
{
	pS32 x; DefValue(0, 0, 25000000);
	pS16 y; 
	pU8 variation; DefValue(0, 0, 9);
	pEnum mixOp; EnumList(=, +, -)
	pEnumAssign assignTo; 
	pEnum assignOp; EnumList(=, +,-)
	pS16 min;
	pS16 max; DefValue(-32767, 255, 32767)
	pEnum link; EnumList(off, on)
	pEnum loop; EnumList(off, on)
	pU8 loopStart;
	pU8 loopEnd;
	
	CMD_DATA
	float prev;
};
#pragma pack (pop)

void Envelope();

typedef struct
{ 
	float y; 
	float send[4]; 
	BYTE var;
}
envRS;

envRS GetEnvAmp(float pos, int slot)
{
	bool nType = slot == NoteIndex;
	float envpos = nType ? fracRangef(pos, clipLen) : pos;

	int l = 0;
	int r = 0;

	//search retrigger
	int rI = -1;
	int offI = 0;
	int i = 0;
	float frEnvPos = fracRangef(pos, clipLen);
	int nI = NoteIndex;
	float rOfs = 0;
	float offPos = 0;

	if (!nType && env[slot].link)
	{
		while (i < env[nI].CollectedPoints && env[nI].position[i].x <= frEnvPos)
		{
			if (env[nI].position[i].r) rI = i;
			if (env[nI].position[i].off) offI = i;
			i++;
		}
		rOfs = env[nI].position[rI].x;

		//if not found, maybe in loop
		if (rI < 0)
		{
			while (i < env[nI].CollectedPoints)
			{
				if (env[nI].position[i].r) rI = i;
				//if (env[nI].position[i].off) offI = i; //???
				i++;
			}
			rOfs = env[nI].position[rI].x-clipLen;
		}

		envpos = frEnvPos - rOfs;

		if (env[slot].loop)
		{
			offPos = env[nI].position[offI].x;
			int loopStartI = env[slot].loopStartI;
			int loopEndI = env[slot].loopEndI;
			float loopStartPos = env[slot].position[loopStartI].x;
			float loopEndPos = env[slot].position[loopEndI].x;
			
			if (offI > rI)
			{
				float loopLen = loopEndPos - loopStartPos;
				float envTailPos = frEnvPos - offPos + loopEndPos;
				float loopTail = loopLen - fracRange(offPos - rOfs - loopEndPos, loopLen);
				envpos = envTailPos-loopTail;
			}
			else if (envpos >= loopEndPos)
			{
				envpos = fracRange(envpos- loopEndPos, loopEndPos-loopStartPos)+loopStartPos;
			}
		}

	}

	//search left
	while (l < env[slot].CollectedPoints && env[slot].position[l].x < envpos) l++;
	l = max(l-1, 0);
	
	float lPos = env[slot].position[l].x;
	float rPos = 0;

	if (nType)
	{
		r = fracRanged(l + 1, env[slot].CollectedPoints);
		rPos = env[slot].position[r].x;
		if (lPos >= rPos) rPos += clipLen;
	}
	else
	{
		//return left
		if (envpos < env[slot].position[0].x)
		{
			envRS ret;
			ret.y = env[slot].position[0].y;
			ret.var = env[slot].position[l].var;
			ret.send[0] = env[slot].position[l].send[0];
			ret.send[1] = env[slot].position[l].send[1];
			ret.send[2] = env[slot].position[l].send[2];
			ret.send[3] = env[slot].position[l].send[3];
			return ret;
		}
		//return right
		if (l + 1 >= env[slot].CollectedPoints)
		{
			envRS ret;
			ret.y = env[slot].position[l].y;
			ret.var = env[slot].position[l].var;
			ret.send[0] = env[slot].position[l].send[0];
			ret.send[1] = env[slot].position[l].send[1];
			ret.send[2] = env[slot].position[l].send[2];
			ret.send[3] = env[slot].position[l].send[3];
			return ret;
		}
		r = l + 1;
		rPos = env[slot].position[r].x;
	}

	float coef = (envpos-lPos)/(rPos - lPos);

	envRS ret;
	ret.y = lerp(env[slot].position[l].y, env[slot].position[r].y, coef);
	ret.var = env[slot].position[l].var;
	ret.send[0] = env[slot].position[l].send[0];
	ret.send[1] = env[slot].position[l].send[1];
	ret.send[2] = env[slot].position[l].send[2];
	ret.send[3] = env[slot].position[l].send[3];

	return ret;
	
}

void CreateNoteEnv(BYTE* i)
{
	float _noteSize = SAMPLERATE*60.f / currentClipBPM;
	float pos = 0;
	int index = 0;
	Note_* in = (Note_*)(i + 1);
	env[NoteIndex].Used = true;

	float swing = CurrentClip->swing*_noteSize / 100.f;
	int notecount = 0;
	float swingtable[2] = { -1,1 };
	float _noteSizeSwing;

	bool silence = false;
	float firstPos = _noteSize * CurrentClip->length;
	float slide = 0;
	int slideNoteCounter = 1;

	while (CmdDesc[*i].Routine == Note)
	{
		in = (Note_*)(i + 1);

		float s = swingtable[int(notecount % 2)];
		_noteSizeSwing = _noteSize + swing * s;

		//reserve for loop
		if (index == 0 && (in->Note == 0 || in->Note == 255))
		{
			index+=2;
			silence = true;
		}

		if (in->Note != 0)
		{
			if (in->Note != 255)
			{
				firstPos = min(firstPos, pos);

				slide = (in->Slide / 255.f) * _noteSize;
				slideNoteCounter = 1;

				env[NoteIndex].position[index].var = in->Variation;
				env[NoteIndex].position[index].send[0] = in->s0;
				env[NoteIndex].position[index].send[1] = in->s1;
				env[NoteIndex].position[index].send[2] = in->s2;
				env[NoteIndex].position[index].send[3] = in->s3;
				env[NoteIndex].position[index].off = false;
				env[NoteIndex].position[index].r = in->Retrigger == 0 ? true : false;
				env[NoteIndex].position[index].x = pos;
				env[NoteIndex].position[index].y = in->Note - 1.f;
				index++;

				env[NoteIndex].position[index].var = in->Variation;
				env[NoteIndex].position[index].send[0] = in->s0;
				env[NoteIndex].position[index].send[1] = in->s1;
				env[NoteIndex].position[index].send[2] = in->s2;
				env[NoteIndex].position[index].send[3] = in->s3;
				env[NoteIndex].position[index].off = false;
				env[NoteIndex].position[index].r = false;
				env[NoteIndex].position[index].x = pos + _noteSizeSwing - 1.f - slide;
				env[NoteIndex].position[index].y = in->Note - 1.f;
				index++;
			}
			else //note off processing
			{
				env[NoteIndex].position[index].var = env[NoteIndex].position[max(index - 1, 0)].var;
				env[NoteIndex].position[index].send[0] = env[NoteIndex].position[max(index - 1, 0)].send[0];
				env[NoteIndex].position[index].send[1] = env[NoteIndex].position[max(index - 1, 0)].send[1];
				env[NoteIndex].position[index].send[2] = env[NoteIndex].position[max(index - 1, 0)].send[2];
				env[NoteIndex].position[index].send[3] = env[NoteIndex].position[max(index - 1, 0)].send[3];
				env[NoteIndex].position[index].off = true;
				env[NoteIndex].position[index].r = false;
				env[NoteIndex].position[index].x = pos;
				env[NoteIndex].position[index].y = env[NoteIndex].position[max(index-1,0)].y;
				index++;
				env[NoteIndex].position[index].var = env[NoteIndex].position[max(index - 1, 0)].var;
				env[NoteIndex].position[index].send[0] = env[NoteIndex].position[max(index - 1, 0)].send[0];
				env[NoteIndex].position[index].send[1] = env[NoteIndex].position[max(index - 1, 0)].send[1];
				env[NoteIndex].position[index].send[2] = env[NoteIndex].position[max(index - 1, 0)].send[2];
				env[NoteIndex].position[index].send[3] = env[NoteIndex].position[max(index - 1, 0)].send[3];
				env[NoteIndex].position[index].off = false;
				env[NoteIndex].position[index].r = false;
				env[NoteIndex].position[index].x = pos;
				env[NoteIndex].position[index].y = env[NoteIndex].position[max(index - 1, 0)].y;
				index++;
				slideNoteCounter++;
			}
		}
		else //empty note processing - shift freq right
		{
			slideNoteCounter++;
			env[NoteIndex].position[max(index - 1, 0)].x = pos + _noteSizeSwing - 1.f - slide* slideNoteCounter;
		}

		pos += _noteSize +swing * s;
		notecount++;
		i += CmdDesc[*i].Size;
	}

	if (silence && index>0)
	{
		env[NoteIndex].position[0].off = false;
		env[NoteIndex].position[0].r = false;
		env[NoteIndex].position[0].x = 0;
		env[NoteIndex].position[0].y = env[NoteIndex].position[index - 1].y;

		env[NoteIndex].position[1].off = false;
		env[NoteIndex].position[1].r = false;
		env[NoteIndex].position[1].x = firstPos;
		env[NoteIndex].position[1].y = env[NoteIndex].position[index - 1].y;
	}

	env[NoteIndex].CollectedPoints = index;
	env[NoteIndex].opFunc = EnvOpCpy;

	for (int x = 0; x < FRAMELEN; x++)
	{
		envRS rs = GetEnvAmp((float)(cursorM * FRAMELEN - CurrentClip->x + x), NoteIndex);
		envValue[NoteIndex][x] = rs.y;
		envValue[NoteVariatonIndex][x] = rs.var;
		envValue[NoteSendIndex0][x] = rs.send[0];
		envValue[NoteSendIndex1][x] = rs.send[1];
		envValue[NoteSendIndex2][x] = rs.send[2];
		envValue[NoteSendIndex3][x] = rs.send[3];
	}

}

void Envelope()
{
	if (currentChannel < 0) return;
	if (inClip[currentChannel] == false) return;

	Envelope_* in = (Envelope_*)(currentPos + 1);
	
	auto assign = in->assignTo;
	auto assignOp = in->assignOp;

	env[assign].opFunc = EnvOpArray[in->assignOp];
	env[assign].mixFunc = EnvOpArray[in->mixOp];
	env[assign].Used = true;
	env[assign].loop = in->loop;
	env[assign].link = in->link;
	env[assign].CollectedPoints = 0;
	env[assign].range = float( in->max - in->min);

	auto link = in->link;
	
	double firstX=0;

	//if (cursorM*FRAMELEN < firstX) return;//left margin

	//search max x, period, loopedcursor
	BYTE* i = currentPos + CmdDesc[currentCmd].Size;
	double maxpoint = firstX;

	Point_* pIn;

	//env[assign].position[0].x = (float)firstX;
	env[assign].position[0].x = in->x;
	env[assign].position[0].y = in->y;

	env[assign].loopStartI = in->loopStart;
	env[assign].loopEndI = in->loopEnd;

	int pCounter = 1;
	float _noteSize = SAMPLERATE * 60.f / currentClipBPM;

	while (*i != 0 && CmdDesc[*i].Routine == Point)
	{
		pIn = (Point_*)(i+1);
		double pointX = pIn->x + firstX;

		maxpoint = max(pointX, maxpoint);
		i += CmdDesc[*i].Size;

		env[assign].position[pCounter].x = float(pointX);
		env[assign].position[pCounter].y = float(pIn->y);

		if (in->loop == 1)
		{
			if (in->loopStart == pCounter)
			{
				env[assign].loopStart = float(pointX);
				env[assign].loopStartY = float(pIn->y);
			}

			if (in->loopEnd == pCounter)
			{
				env[assign].loopEnd = float(pointX);
			}
		}

		pCounter++;
	}

	env[assign].CollectedPoints=pCounter;

	if (CurrentClip)
	{
		float ep = cursorM * FRAMELEN - CurrentClip->x*in->link;
		
		for (int x = 0; x < FRAMELEN; x++)
		{
			BYTE var = envValue[NoteVariatonIndex][x];

			if (var == in->variation ||  in->variation ==0)
			{
				envRS r = GetEnvAmp(ep+x, assign);
				float a = lerp(in->prev, r.y, .01);
				envValue[assign][x] = env[assign].mixFunc(envValue[assign][x],a);
				in->prev = a;
			}

		}

		//TODO: smooth var transition

	}
}

void ClearAllChannels()
{
	for (int x = 0; x < MAXCHANNELS - 1; x++)
	{
		ZeroMemory(sound::channel[x], sound::channelLen * 2);
	}

	ZeroMemory(sound::channel[MAXCHANNELS - 1], sound::len * 2);
}

void PlayWave(int start)
{
	sound::pSourceVoice->Stop(0, 0);

	if (StartPosition < 0) StartPosition = 0;
	if (start < 0) start = 0;

	for (int x = 0; x < MAXCHANNELS; x++)
	{
		channel_amp[x] = *(currentPos + x + 1);
		activeNoteFound[x] = false;
	}

	ClearAllChannels();
	ResetEnv();

	#ifdef EditMode
		ClearAllTemporalData();
	#endif

	Commands::playmode = 0;
	Commands::prerender = 1;

	for (int z = 0; z < PREBUFFER; z++)
	{
		preCount = z;
		Render();
	}

	Commands::prerender = 0;
	Commands::playmode = 1;
	   
	int startS = start * FRAMELEN;
	int x = MAXCHANNELS - 1;

	sound::buffer.AudioBytes = 2 * 2 * sound::len;
	sound::buffer.Flags = XAUDIO2_END_OF_STREAM;
	sound::buffer.PlayBegin = start * FRAMELEN;

	playStartPoint = start;

	if (timeLoopEnable && start < timeLoopStart+timeLoopLen)
	{
		sound::buffer.LoopBegin = timeLoopStart * FRAMELEN;
		sound::buffer.LoopCount = XAUDIO2_LOOP_INFINITE;
		sound::buffer.LoopLength = timeLoopLen * FRAMELEN;
	}
	else
	{
		sound::buffer.LoopBegin = 0;
		sound::buffer.LoopLength = sound::len;
		sound::buffer.LoopCount = 1;
	}

	sound::pSourceVoice->FlushSourceBuffers();
	sound::pSourceVoice->SubmitSourceBuffer(&sound::buffer);
	sound::pSourceVoice->Start();

	timer::resetStartFlag = true;
}

#ifdef EditMode

INT32 playingNoteEnd;
int playingnoteChannel = 0;
float  SelectedNoteX = 0;
float  SelectedNoteClipXWithRepeat = 0;
float  SelectedNoteClipRepeat = 0;
int clickInstance = 0;

void PlayOneNote(BYTE* cp)
{
	ClearAllTemporalData();

	//search position
	BYTE* i = stack::data;
	bool found = false;

	Clip_ tc;
	tc.x = 0;
	tc.bpmScale = 1;
	auto x = tc.x;
	auto npm = (MasterBPM);

	int notecounter = 0;
	int notesinloop = 1;
	playingnoteChannel = -1;
	int l = 0;

	while (*i != 0 && found == false)
	{
		if (CmdDesc[*i].Routine == Channel)
		{
			playingnoteChannel++;
		}

		if (CmdDesc[*i].Routine == Clip)
		{
			Clip_* in = (Clip_*)(i + 1);
			x = in->x;
			npm = (MasterBPM*in->bpmScale);
			l = in->length;
			notecounter = -1;
		}

		if (CmdDesc[*i].Routine == Note)
		{
			notecounter++;
		}

		if (i >= cp)
		{
			found = true;

			BYTE* j = i+CmdDesc[*i].Size;
			while(*j!=0&&CmdDesc[*j].Routine==Note&&
				(((Note_*)(j+1))->Note==0 || ((Note_*)(j + 1))->Note == 255))
			{
				notesinloop++;
				j+= CmdDesc[*j].Size;
			}
			
		}

		i += CmdDesc[*i].Size;
	}

	if (npm == 0) npm = 1;
	float _noteSize = SAMPLERATE*60.f / npm;

	int start = (x + notecounter * _noteSize + _noteSize*clickInstance *l)/ FRAMELENF;

	//int start = (int)(SelectedNoteX/FRAMELENF);
	playingNoteEnd = (INT32)(start+notesinloop * _noteSize/FRAMELENF);
	StartPosition =start;
	playStartPoint = Commands::StartPosition;

	sound::pSourceVoice->Stop(0, 0);

	if (StartPosition < 0) StartPosition = 0;
	if (start < 0) start = 0;

	for (int x = 0; x < MAXCHANNELS; x++)
	{
		channel_amp[x] = *(currentPos + x + 1);
		activeNoteFound[x] = false;
	}

	ClearAllChannels();
	ResetEnv();

	Commands::playmode = 0;
	prerender = 1;

	for (int z = 0; z < PREBUFFER; z++)
	{
		preCount = z;
		Render();
	}

	prerender = 0;
	int ch = MAXCHANNELS - 1;
	
	sound::buffer.AudioBytes = 2 * 2 * sound::len;	
	sound::buffer.PlayBegin = start*FRAMELEN;
	sound::pSourceVoice->FlushSourceBuffers();
	sound::pSourceVoice->SubmitSourceBuffer(&sound::buffer);
	sound::pSourceVoice->Start();
	timer::StartTime = timer::GetCounter();

	Commands::playmode = 1;

}
#endif


float ApplyEnvValue(float value, float denominator, int index, int pos)
{
	if (env[index].Used)
	{
		float envValue_ = envValue[index][pos] / denominator;
		return env[index].opFunc(value / denominator, envValue_);
	}
	else
	{
		return value / denominator;
	}
}

float foldback(float in, float threshold)
{
	if (in > threshold || in < -threshold)
	{
		in = fabsf(fabsf(fmodf(in - threshold, threshold * 4)) - threshold * 2) - threshold;
	}
	return in * (32767.f / threshold);
}

float waveshape_distort(float in) {
	return 1.5f * in - 0.5f * in *in * in;
}

float saturator(float a)
{
	float x = a / 32767.f;
	return sinhf(x) / coshf(x)*32767.f;
}

float saturatorSin(float a)
{
	float x = a / 32767.f;
	return 32767.f*sinf(x*x) / x;
}

float saturatorSoftSaw(float a)
{
	float x = a / 32767.f;
	return 32767.f*(atan2f(sinf(x * x / 6.f), cosf(x * x / 6.f) + 1.51f) + 2.f * sinhf(x) / coshf(x)) / 4.f;
}

float saturatorRndSaw(float a)
{
	float x = a / 32767.f;
	return 32767.f*(atan2f(sinf(x*x*1.811f), cosf(x*x) + 1.9001f) / x * .8f + 2.f * sinhf(x) / coshf(x)) / 2.7f;
}

float limit(float a, int type)
{
	switch (type)
	{
	case 0:
		a = clamp(a, -32767.f, 32767.f);
		break;
	case 1:
		a = foldback(a, 32767.f);
		break;
	case 2:
		a = saturator(a);
		break;
	case 3:
		a = saturatorSin(a);
		break;
	case 4:
		a = saturatorSoftSaw(a);
		break;
	case 5:
		a = saturatorRndSaw(a);
		break;

	}
	return a;

}

#pragma pack (push,1)
struct Oscillator_
{
	pU8 volume; EnvOn DefValue(0, 255, 255)
	pEnum waveType; EnumList(sin,noise,fastTri,fastSaw,fastSquare)
	pU8 harmonicsCount; EnvOn DefValue(0, 0, 255)
	pS8 pulseWidth; EnvOn DefValue(-128, 0, 127)
	pU8 harmonicsAmp; EnvOn
	pU8 vibratoAmp; EnvOn
	pU8 vibratoSpeed; EnvOn
	pS16 freqShift; EnvOn
	pU8 hFade;
	pU8 hFadeStart;
	pEnum mixType; EnumList(=, +, -)
	pS8 noteShift; EnvOn
	CMD_DATA
	double phase;
	double phaseV;
};
#pragma pack (pop)

enum osc {
	osc_volume,
	osc_waveType,
	osc_harmonicsCount,
	osc_pulseWidth,
	osc_harmonicsAmp,
	osc_vibratoAmp,
	osc_vibratoSpeed,
	osc_freqShift,
	osc_hFade,
	osc_hFadeStart,
	osc_mixType,
	osc_noteShift
};

void FlushEnvCashe()
{
	for (int x = 0; x < MAXPARAM; x++)
	{
		env[x].Used = false;
	}
}

void FlushClipEnvCashe()
{
	for (int x = 0; x < MAXPARAM; x++)
	{
		env[x+MAXPARAM].Used = false;
	}
}



bool CheckRange()
{
	if (currentChannel < 0 ||
		cursorM < 0 ||
		inClip[currentChannel] == false)
	{
		return false;
	}

	return true;
}

float(*oscMixFunc)(float value, float env);

void Oscillator()
{
	if (!CurrentClip) return;
	if (!CheckRange()) return;

	Oscillator_* in = (Oscillator_*)(currentPos + 1);

	FlushEnvCashe();
	ExecuteChildren();

	auto _freqShift = in->freqShift;
	auto _amp = in->volume;
	int startI = 0;
	int start = cursorM * FRAMELEN;
	auto wavetype = in->waveType;
	double fStart = in->phase;
	double f = fStart;
	double fv = in->phaseV;

	float noteSize = SAMPLERATE * 60.f / (MasterBPM*CurrentClip->bpmScale);
	float ClipLoop = CurrentClip->length*noteSize;

	oscMixFunc = EnvOpArray[in->mixType];

	for (int i = 0; i < framelen * 2; i += 2)
	{
		float h_detuneI = ApplyEnvValue(in->pulseWidth, 255.f, osc::osc_pulseWidth,i/2);
		float h_countF = ApplyEnvValue(in->harmonicsCount,1.,osc::osc_harmonicsCount,i/2); 
		float h_count = ceilf(h_countF);

		float NoteCode = ApplyEnvValue(0,1.,NoteIndex,i / 2);
		NoteCode += .1*ApplyEnvValue(in->noteShift, 1., osc::osc_noteShift, i / 2);
		float Freq = 16.3515987f *(float)pow(2.0f, NoteCode / 12.0f);
		float fhrFreq=0.;

		if (i/2 + start == CurrentClip->x)
		{
			fv = 0;
			f = 0;
			in->phase = 0;
			in->phaseV = 0;

			srand(0);
		}

		float vFreq = ApplyEnvValue(in->vibratoSpeed, 16.f, osc::osc_vibratoSpeed,i/2);		
		float vd =(float)pow(2.f, (NoteCode - 1.f) / 12.f) - (float)pow(2.f, (NoteCode + 1.f) / 12.f);
		float vAmp = ApplyEnvValue(in->vibratoAmp, 1.f, osc::osc_vibratoAmp,i/2);
		Freq += .2f*vd*vAmp*(float)sin(fv);
		Freq = ApplyEnvValue(Freq, 1.f, osc::osc_freqShift,i/2);

		float la = 0;
		float ha = ApplyEnvValue(in->harmonicsAmp, 1.f, osc::osc_harmonicsAmp,i/2);

		float hAdj = max(NoteCode-in->hFadeStart,0);
		float hFade = 1.f+ hAdj*in->hFade/255.f;
		ha /= hFade;
		h_countF /= hFade;
		h_count /= hFade;

		float ff = (float)f;

		switch (wavetype)
		{
			case 0://sin
			{
				la += sinf(ff);
				break;
			}

			case 1://noise
			{
				la += (float)(((rand() % 32767)-16383.5) / 16383.5f);
				break;
			}

			case 2://fast triangle
			{	
				float hc = h_countF;
				hc = 1.f-1.f/max(hc*hc,2.f);
				float f2 = asinf(cosf(ff)*hc) / hc;				
				la += f2 * (.5f - (ha / 255.f)*cosf(ff * h_count));
				break;
			}

			case 3://fast saw
			{
				float f2 = 2.f*lerp(frac(-ff / ((float)PI*2.f)), .5f, powf(1.f - fabsf(sinf(-ff / 2.f)), max(h_countF, 0.f))) - 1.f;
				la += f2 * (.5f - ha / 255.f*cosf(-h_count * ff));
				break;
			}

			case 4://fast square with pulse
			{
				float f1 = atanf((cosf(ff) + sinf(ff)- h_detuneI*4.f) * h_countF);
				float hamp = fminf(fabsf(1.f / (atanf((sinf(ff) + cosf(ff) - h_detuneI*4.f))) / 8.f), 1.f);
				float f2 = f1 + (ha / 255.f)*hamp * sinf(ff * h_count);
				la += f2;
				break;
			}
		}

		float newAmp = ApplyEnvValue(in->volume, 255.f, osc::osc_volume,i/2);
		la = clamp(la * 32767.0f * newAmp, -32767.f, 32767.f);

		signed short lb = *(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2);
		signed short rb  = *(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + 2);

		lb = clamp_int(oscMixFunc(lb, la),-32767,32767);
		rb = clamp_int(oscMixFunc(rb, la),-32767, 32767);

		*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2) = (signed short)lb;
		*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + 2) = (signed short)rb;

		f += 2.0 * PI * (double)Freq / (double)SAMPLERATE;
		fv += 2.0 * PI * (double)vFreq / (double)SAMPLERATE;
	}

	if (playmode == 1 || prerender == 1)
	{
		in->phase = f;
		in->phaseV = fv;
	}
}

#pragma pack (push,1)
struct SendToEnv_
{
	pEnum source; EnumList(channel,send,freq,freqAsDelay)
	pU8 inID;
	pEnumAssign assignTo;
	pEnum assignOp; EnumList(=, +, -)
	pS16 range;
	pS16 offset;
};
#pragma pack (pop)

void SendToEnv()
{
	SendToEnv_* in = (SendToEnv_*)(currentPos + 1);

	int target = in->assignTo;
	float scale = in->range;

	//channel send
	if (in->source == 0) 
	{
		scale /= 32767.f;

		for (int x = 0; x < FRAMELEN; x++)
		{
			envValue[target][x] = *(signed short*)(sound::channel[in->inID] + x * 4);
			envValue[target][x] *= scale;
			envValue[target][x] += in->offset;
		}
	}

	//env send
	if (in->source == 1)
	{
		//if (!env[in->inID + MAXPARAM].Used) return;
		scale /= 255.f;

		for (int x = 0; x < FRAMELEN; x++)
		{
			envValue[target][x] = envValue[in->inID + MAXPARAM+1][x];
			envValue[target][x] *= scale;
			envValue[target][x] += in->offset;
		}

	}

	//note freq send
	if (in->source == 2)
	{
		scale /= 255.f;

		for (int x = 0; x < FRAMELEN; x++)
		{
			envValue[target][x] = 44100.f / (16.3515987f *(float)pow(2.0f, envValue[NoteIndex][x] / 12.0f));
			envValue[target][x] *= 1.f + scale;
			envValue[target][x] += in->offset;
		}
	}

	//note freq as delay send
	if (in->source == 3)
	{
		scale /= 255.f;

		for (int x = 0; x < FRAMELEN; x++)
		{
			float f = 44100.f / (16.3515987f *(float)pow(2.0f, envValue[NoteIndex][x] / 12.0f));
			float d = 1. / f;
			envValue[target][x] = f;
			envValue[target][x] *= 1.f + scale;
			envValue[target][x] += in->offset;
		}
	}

	env[target].Used = true;
	env[target].opFunc = EnvOpArray[in->assignOp];

}

#define Chorus2BufSize 44100
#pragma pack (push,1)
struct DelayLine_
{
	pU16 delay; EnvOn DefValue(0, 0, 32767)
	pU16 delayNotes; DefValue(0, 0, 16)
	pS8 gain; EnvOn DefValue(-127, 32, 127)
	pU16 random; EnvOn DefValue(0, 0, 32767)
	pU16 modAmp; EnvOn DefValue(0, 0, 32767)
	pU16 modFreq; EnvOn DefValue(0, 0, 32767)
	pU16 Stereo; EnvOn DefValue(0, 0, 32767)
	pEnum bypass; EnumList(off, on) 
	CMD_DATA
	float left[Chorus2BufSize];
	float right[Chorus2BufSize];
	int bufPos;
	double counter;
};
#pragma pack (pop)

enum chr2 {
	chr2_delay,
	chr2_delayNotes,
	chr2_gain,
	chr2_random,
	chr2_modAmp,
	chr2_modFreq,
	chr2_stereo
};

void DelayLine()
{
	if (!CurrentClip) return;

	DelayLine_* in = (DelayLine_*)(currentPos + 1);

	if (in->bypass) return;

	if (currentChannel < 0) return;
	if (inClip[currentChannel] == false) return;
	if (cursorM < 0) return;

	FlushEnvCashe();
	ExecuteChildren();

	int startofs = 0;
	if (master == true) startofs = FRAMELEN * cursorM * 4;

	for (int x = 0; x < FRAMELEN; x++)
	{
		int pos = fracRanged(in->bufPos + x, Chorus2BufSize);
		in->left[pos] = *(signed short*)(sound::channel[currentChannel] + startofs + x * 4);
		in->right[pos] = *(signed short*)(sound::channel[currentChannel] + startofs + x * 4 + 2);
	}

	srand(cursorM);
	float noteSize = SAMPLERATE * 60.f / (MasterBPM*CurrentClip->bpmScale);

	for (int x = 0; x < FRAMELEN; x++)
	{
		if (CurrentClip && x + cursorM == CurrentClip->x)
		{
			in->counter = 0;
		}

		float delay = ApplyEnvValue(in->delay, 1.f, chr2::chr2_delay, x);

		delay += noteSize * in->delayNotes;

		delay+= ((rand()%32767)/32767.f)*ApplyEnvValue(in->random, 1.f, chr2::chr2_random, x);

		float modAmp = ApplyEnvValue(in->modAmp, 1.f, chr2::chr2_modAmp, x);
		float modFreq = ApplyEnvValue(in->modFreq, 1.f, chr2::chr2_modFreq, x);
		float modF = (FRAMELENF*in->counter + x);
		float modOfs = .5+.25*sin(modF*modFreq/10000.f);
		delay += modOfs*modAmp;

		float stereo = ApplyEnvValue(in->Stereo, 1., chr2::chr2_stereo, x);

		float gain = ApplyEnvValue(in->gain, 64.f, chr2::chr2_gain, x);

		int srcPos = fracRanged(in->bufPos + x, Chorus2BufSize);
		int delayedPosL = fracRanged(in->bufPos + x - (int)(delay+stereo), Chorus2BufSize);
		
		float L = in->left[srcPos];
		L += in->left[delayedPosL]*gain;
		L = limit(L, 0);
		in->left[srcPos] = L;

		int delayedPosR = fracRanged(in->bufPos + x - (int)(max(delay-stereo,0)), Chorus2BufSize);

		float R = in->right[srcPos];
		R += in->right[delayedPosR] * gain;
		R = limit(R, 0);
		in->right[srcPos] = R;

		*(signed short*)(sound::channel[currentChannel] + startofs + x * 4) = (signed short)(L);
		*(signed short*)(sound::channel[currentChannel] + startofs + x * 4 + 2) = (signed short)(R);
	}

	in->bufPos += FRAMELEN;
	in->bufPos = fracRanged(in->bufPos, Chorus2BufSize);
	in->counter++;

}

#pragma pack (push,1)
struct Crunch_
{
	pU8 preamp; EnvOn
	pU8 offset; EnvOn
	pS16 scale; EnvOn
	pU8 outVolume; EnvOn DefValue(0, 255, 255)
	pU8 mix; EnvOn
	pEnum bypass; EnumList(off, on)
};
#pragma pack (pop)

enum dn {
	dn_preamp,
	dn_offset,
	dn_scale,
	dn_outVolume,
	dn_mix,
	dn_bypass
};

void Crunch()
{
	Crunch_* in = (Crunch_*)(currentPos + 1);

	if (in->bypass) return;

	FlushEnvCashe();
	ExecuteChildren();

	if (currentChannel < 0) return;
	if (inClip[currentChannel] == false) return;

	if (cursorM < 0) return;
	int startofs = 0;
	if (master == true) startofs = framelen * cursorM * 4;

	int start = cursorM * FRAMELEN;
	int startI = 0;

	auto preamp = in->preamp;
	auto offset = in->offset;
	auto hardness = in->scale;
	auto out = in->outVolume;
	auto mix = in->mix;

	for (int i = 0; i < framelen * 2; i += 2)
	{
		float preampI = ApplyEnvValue(preamp, 255.f, dn::dn_preamp, i / 2);
		float offsetI = ApplyEnvValue(offset, 255.f, dn::dn_offset, i / 2);
		float hardnessI = ApplyEnvValue(hardness, 255.f, dn::dn_scale, i / 2);
		float outI = ApplyEnvValue(out, 255.f, dn::dn_outVolume, i / 2);
		float mixI = ApplyEnvValue(mix, 255.f, dn::dn_mix,i/2);

		signed short left = *(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + startofs);
		signed short right = *(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + 2 + startofs);

		float _left = (float)(left*(float)(1.f + 8.f*preampI)) / 32767.0f;
		_left += offsetI;
		float lp = (float)pow(fabsf(_left), 1.0f + hardnessI);
		if (left < 0) lp *= -1.0f;
		_left -= offsetI;
		_left = lerp((float)sin(lp*3.14f / 2.0f), lp, mixI);
		_left = clamp(_left, -1, 1);
		_left *= 32767.0f*outI;

		float _right = (float)(right*(float)(1. + 8.*preampI)) / 32767.0f;
		_right += offsetI;
		lp = (float)pow(fabs(_right), 1.0 + hardnessI);
		if (right < 0) lp *= -1.0;
		_right -= offsetI;
		_right = lerp((float)sin(lp*3.14f / 2.0f), lp, mixI);
		_right = clamp(_right, -1, 1);
		_right *= 32767.0f*outI;

		left = (signed short)_left;
		right = (signed short)_right;

		*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + startofs) = left;
		*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + 2 + startofs) = right;
	}
}



#pragma pack (push,1)
struct Waveshaper_
{
		pEnum type; EnumList(hard, foldback, soft, sin, softsaw, sinhsin)
		pU16 preamp; EnvOn DefValue(0, 0, 32767)
		pU8 outVolume; EnvOn DefValue(0, 255, 255)
		pEnum bypass; EnumList(off, on)
};
#pragma pack (pop)

enum wsh {
	wsh_type,
	wsh_preamp,
	wsh_outVolume,
	wsh_bypass
};

void Waveshaper()
{
	Waveshaper_* in = (Waveshaper_*)(currentPos + 1);

	if (in->bypass) return;

	FlushEnvCashe();
	ExecuteChildren();

	if (currentChannel < 0) return;
	if (inClip[currentChannel] == false) return;

	if (cursorM < 0) return;
	int startofs = 0;
	if (master == true) startofs = framelen * cursorM * 4;

	int start = cursorM * FRAMELEN;
	int startI = 0;

	for (int i = 0; i < framelen * 2; i += 2)
	{
		signed short left = *(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + startofs);
		signed short right = *(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + 2 + startofs);

		float preamp = ApplyEnvValue(in->preamp, 32.f, wsh::wsh_preamp,i/2)+1.f;
		float postamp = ApplyEnvValue(in->outVolume, 255.f, wsh::wsh_outVolume,i/2);

		float _left = postamp*limit(left*preamp, in->type);
		float _right = postamp*limit(right*preamp,in->type);

		*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + startofs) = (signed short)_left;
		*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + 2 + startofs) = (signed short)_right;
	}

}

#define ChorusBufLen 735
#pragma pack (push,1)
struct Chorus_
{
	pU8 voices; EnvOn DefValue(0, 1, 255)
	pU16 delay; EnvOn DefValue(0, 0, 32767)
	pS8 mix; EnvOn DefValue(-127, 0, 127)
	pEnum bypass; EnumList(off, on)
CMD_DATA
	signed short lBuf[ChorusBufLen];//buffer
	signed short rBuf[ChorusBufLen];//buffer
};
#pragma pack (pop)

enum chorus {
	ch_voices,
	ch_delay,
	ch_mix,
	ch_bypass
};

void Chorus()
{
	Chorus_* in = (Chorus_*)(currentPos + 1);

	if (in->bypass) return;

	FlushEnvCashe();
	ExecuteChildren();

	if (currentChannel < 0) return;
	if (inClip[currentChannel] == false) return;

	if (cursorM < 0) return;
	int startofs = 0;
	if (master == true) startofs = framelen * cursorM * 4;

	int start = cursorM * FRAMELEN;
	int startI = 0;

	signed short leftc[ChorusBufLen *2];
	signed short rightc[ChorusBufLen *2];

	for (int i = 0; i < ChorusBufLen; i ++)
	{
		leftc[i] = in->lBuf[i];
		rightc[i] = in->rBuf[i];
	}

	for (int i = 0; i < framelen * 2; i += 2)
	{
		leftc[i/2+ ChorusBufLen] = *(signed short*)(sound::channel[currentChannel] + (i ) * 2 + startofs);
		rightc[i/2+ ChorusBufLen] = *(signed short*)(sound::channel[currentChannel] + (i ) * 2 + 2 + startofs);

		in->lBuf[i / 2] = leftc[i / 2 + ChorusBufLen];
		in->rBuf[i / 2] = rightc[i / 2 + ChorusBufLen];

	}

	for (int i = 0; i < framelen * 2; i += 2)
	{
		float voices = ApplyEnvValue(in->voices, 1, chorus::ch_voices,i/2);
		float delay = ApplyEnvValue(in->delay, 1, chorus::ch_delay, i / 2);
		float mix = ApplyEnvValue(in->mix, 64., chorus::ch_mix, i / 2);


		float left_ = 0;  float right_=0;

		for (int j = 0; j < voices; j++)
		{
			int variable = (int)((sinf((float)j)*.5f + .5f)*fmin(delay, (float)ChorusBufLen));
			int ofs = ChorusBufLen - variable;
			left_ += leftc[i/2+ofs];
			right_ += rightc[i / 2 + ofs];
		}

		left_ /= voices;
		right_ /= voices;

		left_ *= 1+mix;
		right_ *= 1+mix;
		left_ = limit(left_, 0);
		right_ = limit(right_, 0); 

		*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + startofs) = (signed short)left_;
		*(signed short*)(sound::channel[currentChannel] + (i + startI * 2) * 2 + 2 + startofs) = (signed short)right_;
	}
}

#pragma pack (push,1)
struct Return_
{
	pU8 mixLevel; DefValue(0, 255, 255)
};
#pragma pack (pop)

void Return()
{
	Return_* in = (Return_*)(currentPos + 1);
	float mixLevel = in->mixLevel/255.f;
	if (currentChannel < 0) return;

	double cl = 9*60*SAMPLERATE;
	double cr = 0;
	for (int j = 0; j < currentChannel; j++)
	{
		if (sendTable[j] > 0)
		{
			cl = min(clipLeft[j], cl);
			cr = max(clipRight[j], cr);
		}
	}

	if (cl > cr) return;
	clipLeft[currentChannel] = cl;
	clipRight[currentChannel] = cr;

	if (cursorM * FRAMELEN < cl || cursorM * FRAMELEN > cr)
	{
		return;
	}

	inClip[currentChannel] = true;

	for (int i = 0; i < framelen * 4; i += 4)
	{
		float left_ = 0;
		float right_ = 0;

		for (int j = 0; j < currentChannel; j++)
		{
			if (sendTable[j] > 0)
			{
				signed short left = *(signed short*)(sound::channel[j] + i);
				signed short right = *(signed short*)(sound::channel[j] + i + 2);
				left_ += left * sendTable[j]* mixLevel;
				right_ += right * sendTable[j]* mixLevel;
			}
		}

		*(signed short*)(sound::channel[currentChannel] + i) = (signed short)left_;
		*(signed short*)(sound::channel[currentChannel] + i + 2) = (signed short)right_;

	}

	for (int j = 0; j < currentChannel; j++)
	{
		sendTable[j] = 0;
	}
}

enum {
	LPF, /* low pass filter */
	HPF, /* High pass filter */
	BPF, /* band pass filter */
	NOTCH, /* Notch Filter */
	PEQ, /* Peaking band EQ filter */
	LSH, /* Low shelf filter */
	HSH /* High shelf filter */
};

#pragma pack (push,1)
struct Equalizer_
{
	pEnum type; EnumList(lowpass, highpass, bandpass, notch, eq)
		pS16 freq; EnvOn
		pS16 gain; EnvOn
		pS16 bandwidth; EnvOn
		pS8 Amp; EnvOn
		pEnum limitType; EnumList(hard, foldback, soft, sin, softsaw, sinhsin)
		pEnum bypass; EnumList(off, on)
		pU8 DryWet; EnvOn DefValue(0, 0, 255)
CMD_DATA
	signed short lBuf[3];//buffer
	signed short rBuf[3];//buffer
	float PrevSampleL[3];
	float PrevSampleR[3];
};
#pragma pack (pop)

enum eq{
	eq_type,
	eq_freq,
	eq_gain,
	eq_bandwidth,
	eq_Amp,
	eq_limitType,
	eq_bypass,
	eq_drywet
};

void Equalizer()
{
	Equalizer_* in = (Equalizer_*)(currentPos + 1);

	if (in->bypass) return;

	FlushEnvCashe();
	ExecuteChildren();

	if (currentChannel < 0) return;
	if (inClip[currentChannel] == false) return;
	if (cursorM < 0) return;

	int startofs = 0;
	if (master == true) startofs = framelen * cursorM * 4;

	auto type = in->type;
	auto freq = in->freq;
	auto gain = in->gain;
	auto width = in->bandwidth;
	int fl = framelen * 4;// +freq * 4;

	float BufferL[FRAMELEN + 4 * 2];
	float BufferR[FRAMELEN + 4 * 2];

	int pre = 2;

	BufferL[0] = in->lBuf[0];
	BufferL[1] = in->lBuf[1];

	BufferR[0] = in->rBuf[0];
	BufferR[1] = in->rBuf[1];

	if (isnan(in->PrevSampleL[0]))
	{
		in->PrevSampleL[0] = 0;
		in->PrevSampleL[1] = 0;
		in->PrevSampleL[2] = 0;
		in->PrevSampleR[0] = 0;
		in->PrevSampleR[1] = 0;
		in->PrevSampleR[2] = 0;
	}

	//copy 
	for (int i = 0; i < fl; i += 4)
	{
		float Amp = ApplyEnvValue(in->Amp, in->Amp >= 0.0 ? 16.f : 127.f, eq::eq_Amp,i/4) + 1.f;

		signed short left = *(signed short*)(sound::channel[currentChannel] + i + startofs);
		BufferL[i / 4 + pre] = left* Amp;
		signed short right = *(signed short*)(sound::channel[currentChannel] + i + 2 + startofs);
		BufferR[i / 4 + pre] = right* Amp;
	}

	int i = pre;
	int n = FRAMELEN + pre;

	int limitType = in->limitType;

	bool anyEnvUsed = false;
	for (int t=0;t<6;t++)
	{
		if (env[t].Used) anyEnvUsed = true;
	}

	bool filterParamsDone = false;
	float a0, a1, a2, b0, b1, b2;

	int m = 0;

	while (i < n)
	{
		float freqI = ApplyEnvValue(freq, 1.f, eq::eq_freq,m);
		
		float gainI = ApplyEnvValue(in->gain, 1.f, eq::eq_gain,m);
		float widthI = ApplyEnvValue(in->bandwidth, 1., eq::eq_bandwidth,m);


		float _width = (widthI + 255.0f) / 255.0f;
		if (_width < .01f) _width = .01f;  

		if (!filterParamsDone)
		{
			float Fs = SAMPLERATE;
			float A = (float)pow(10.0, gainI / 40.0);
			float omega = 2 * M_PI * freqI / Fs;
			float sn = (float)sin(omega);
			float cs = (float)cos(omega);
			float alpha = (float)(sn * sinh(M_LN2 / 2.f * _width * omega / sn));
			float beta = (float)sqrt(A + A);

			switch (type) {
			case LPF:
				b0 = (1 - cs) / 2;
				b1 = 1 - cs;
				b2 = (1 - cs) / 2;
				a0 = 1 + alpha;
				a1 = -2 * cs;
				a2 = 1 - alpha;
				break;
			case HPF:
				b0 = (1 + cs) / 2;
				b1 = -(1 + cs);
				b2 = (1 + cs) / 2;
				a0 = 1 + alpha;
				a1 = -2 * cs;
				a2 = 1 - alpha;
				break;
			case BPF:
				b0 = alpha;
				b1 = 0;
				b2 = -alpha;
				a0 = 1 + alpha;
				a1 = -2 * cs;
				a2 = 1 - alpha;
				break;
			case NOTCH:
				b0 = 1;
				b1 = -2 * cs;
				b2 = 1;
				a0 = 1 + alpha;
				a1 = -2 * cs;
				a2 = 1 - alpha;
				break;
			case PEQ:
				b0 = 1 + (alpha * A);
				b1 = -2 * cs;
				b2 = 1 - (alpha * A);
				a0 = 1 + (alpha / A);
				a1 = -2 * cs;
				a2 = 1 - (alpha / A);
				break;
			}

			if (!anyEnvUsed)
			{
				filterParamsDone = true;
			}
		}

		in->PrevSampleL[2] = in->PrevSampleL[1];
		in->PrevSampleL[1] = in->PrevSampleL[0];
		in->PrevSampleL[0] = BufferL[i];

		BufferL[i] = limit((b0 / a0 * in->PrevSampleL[0]) +
			(b1 / a0 * in->PrevSampleL[1]) +
			(b2 / a0 * in->PrevSampleL[2]) -
			(a1 / a0 * BufferL[i - 1]) -
			(a2 / a0 * BufferL[i - 2]),limitType);

		in->PrevSampleR[2] = in->PrevSampleR[1];
		in->PrevSampleR[1] = in->PrevSampleR[0];
		in->PrevSampleR[0] = BufferR[i];

		BufferR[i] = limit((b0 / a0 * in->PrevSampleR[0]) +
			(b1 / a0 * in->PrevSampleR[1]) +
			(b2 / a0 * in->PrevSampleR[2]) -
			(a1 / a0 * BufferR[i - 1]) -
			(a2 / a0 * BufferR[i - 2]),limitType);
		i++;
		m++;
	}

	//paste 
	{
		for (int i = 0; i < fl; i += 4)
		{
			signed short left = (signed short)BufferL[i / 4 + pre];
			signed short right = (signed short)BufferR[i / 4 + pre];

			signed short s_left = *(signed short*)(sound::channel[currentChannel] + i + startofs);
			signed short s_right = *(signed short*)(sound::channel[currentChannel] + i + 2 + startofs);

			float DryWet = ApplyEnvValue(in->DryWet, 255., eq::eq_drywet,i/4);
			
			left = (signed short)lerp(left, s_left, DryWet);
			right = (signed short)lerp( right, s_right, DryWet);

			*(signed short*)(sound::channel[currentChannel] + i + startofs) = left;
			*(signed short*)(sound::channel[currentChannel] + i + 2 + startofs) = right;
		}
	}

	in->lBuf[0] = (signed short)BufferL[FRAMELEN];
	in->lBuf[1] = (signed short)BufferL[FRAMELEN + 1];
	in->rBuf[0] = (signed short)BufferR[FRAMELEN];
	in->rBuf[1] = (signed short)BufferR[FRAMELEN + 1];
}

float volumeL[MAXCHANNELS];
float volumeR[MAXCHANNELS];

#pragma pack (push,1)
struct MasterChannel_
{
		pS8 preAmp;
};
#pragma pack (pop)

void MasterChannel()
{
	MasterChannel_* in = (MasterChannel_*)(currentPos + 1);

	float preamp = 1.f+in->preAmp/64.f;

	for (int c = 0; c < MAXCHANNELS; c++)
	{
		volumeL[c] = 0;
		volumeR[c] = 0;
	}

	int start = cursorM * FRAMELEN;
	int startI = 0;

	if (start < 0) return;

	master = true;

	int channelcount = currentChannel + 1;

	activeChannelCounter = currentChannel+1;

	int outchannel = MAXCHANNELS - 1;
	currentChannel = outchannel;
	inClip[currentChannel] = true;

	float sum[2];
	sum[0] = 0;
	sum[1] = 0;

	for (int i = 0; i < framelen * 2; i += 2)
	{
		float a = 0;
		float b = 0;
		for (int x = 0; x < min(channelcount, MAXCHANNELS - 1); x++)
		{
			signed short _a = *(signed short*)(sound::channel[x] + (i + startI * 2) * 2);
			signed short _b = *(signed short*)(sound::channel[x] + (i + startI * 2) * 2 + 2);

			float scf = (-channelPan[x] + 127.0f) / 255.0f;
			float vlm = channelVolume[x] / 255.0f;

			if (start +i/4> clipRight[x]) vlm = 0;

		#ifdef EditMode
			if (Commands::playonenoteFlag)
			{
				if (x != playingnoteChannel)
				{
					vlm = 0;
				}
			}
			
			if (Commands::playonenoteFlag || playmode == 1)
			{
				volumeL[x] = max(fabsf(_a / 32767.f * scf*vlm), volumeL[x]);
				volumeR[x] = max(fabsf(_b / 32767.f * (1.f - scf)*vlm), volumeR[x]);
			}
		#endif

			if (!inClip[x]) vlm = 0;

			a += (float)_a * scf*vlm;
			b += (float)_b * (1.f - scf)*vlm;
		}
		a *= preamp;
		b *= preamp;
		a = clamp(a, -32767.f, 32767.f);
		b = clamp(b, -32767.f, 32767.f);
		*(signed short*)(sound::channel[outchannel] + (i + start * 2) * 2) = (signed short)a;
		*(signed short*)(sound::channel[outchannel] + (i + start * 2) * 2 + 2) = (signed short)b;

		sum[0] += a;
		sum[1] += b;

		#ifdef EditMode
			if (Commands::playonenoteFlag || playmode == 1)
			{
				volumeL[outchannel] = max(fabsf(a / 32767.f), volumeL[outchannel]);
				volumeR[outchannel] = max(fabsf(b / 32767.f), volumeR[outchannel]);
			}
			else
			{
				volumeL[outchannel] = 0;
				volumeR[outchannel] = 0;
			}

		#endif
	}

}

void SkipClipChildren()
{

	int sz = CmdDesc[*currentPos].Size;
	currentPos += sz;

	while (
		CmdDesc[*currentPos].Routine == Note ||
		CmdDesc[*currentPos].Routine == Envelope ||
		CmdDesc[*currentPos].Routine == Point)
	{
		sz = CmdDesc[*currentPos].Size;
		currentPos += sz;
	}

	currentPos -= sz;

	return;

}

#ifdef EditMode

void ClearAllTemporalData()
{

	BYTE* i = stack::data;
	while (*i != 0)
	{
		BYTE cmd = *i;

		if (CmdDesc[cmd].Routine == Commands::Oscillator)
		{
			Commands::Oscillator_* in = (Commands::Oscillator_*)(i + 1);
			in->phase = 0;
			in->phaseV = 0;
		}

		if (CmdDesc[cmd].Routine == Commands::Envelope)
		{
			Commands::Envelope_* in = (Commands::Envelope_*)(i + 1);
			in->prev = 0;
		}

		if (CmdDesc[cmd].Routine == Commands::Chorus)
		{
			Commands::Chorus_* in = (Commands::Chorus_*)(i + 1);

			for (int x = 0; x < ChorusBufLen; x++)
			{
				in->lBuf[x] = 0;
				in->rBuf[x] = 0;
			}
		}

		if (CmdDesc[cmd].Routine == Commands::DelayLine)
		{
			Commands::DelayLine_* in = (Commands::DelayLine_*)(i + 1);

			for (int x = 0; x < Chorus2BufSize; x++)
			{
				in->left[x] = 0;
				in->right[x] = 0;
			}

			in->bufPos = 0;
			in->counter = 0;
		}

		if (CmdDesc[cmd].Routine == Commands::Equalizer)
		{
			Commands::Equalizer_* in = (Commands::Equalizer_*)(i + 1);

			for (int x = 0; x < 3; x++)
			{
				in->lBuf[x] = 0;
				in->rBuf[x] = 0;
				in->PrevSampleL[x] = 0;
				in->PrevSampleR[x] = 0;
			}
			
		}
	
		i += CmdDesc[cmd].Size;
	}
}
#endif
