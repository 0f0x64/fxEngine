namespace ui
{

#define maxTimelineScale 2000.0f
#define	minTimelineScale 1.0f

	XMFLOAT4 menuBoxColor = XMFLOAT4(.2, .2, .2, 1.);
	XMFLOAT4 menuTextColor = XMFLOAT4(1, 1, 1, 1.);
	int textCursor = 0;
	float textCursorX = 0;
	char editBoxText[64];

	bool stackViewMode = true;

	BYTE* selectedElementPtr = NULL;
	int selectedParamID = -1;
	int prevSelectedParamID = -1;
	bool selectedElementOrientation;
	dxui::rect2d selectedElementRect;
	void ClickNone() {};
	void Click();
	void ClickMainMenu();
	void ClickViewController();
	void ClickProjectMenu();
	void ClickInsertCmd();
	void ClickParam();
	void ClickParamContextMenu();
	void ClickEnv();
	void ClickPoint();
	void ClickAutomationContext();
	void ClickAddAutomation();
	void ClickSelectEnv();
	void ClickPointWidget();
	void ClickSendToEnv();
	void ClickNum();
	void ClickSlider();
	
	void SendToEnvWidget(float x, float y, BYTE* i);

	int SelectedEnv =-1;

	//int dxui::cursorID = 0;
	BYTE overCmd = -1;

	float lineStep = .04 * 1.1*1.3;//param line step

	//float  SelectedNoteX = 0;
	BYTE isSelected;
	int cursorpos = 0;
	double cursorposF = 0;
	int cOctave = 1;
	int cVolume = 255;
	bool VolumeOverwrite = true;
	int autoNextNote = 0;

	typedef struct { float x; float y; float z; } point3;
	typedef struct { float x; float y; } point2d;
	point2d fParamPos;
	point2d viewPos, oldviewPos, oldCursor;
	point2d timelinePos, oldtimelinePos, oldtimelineCursor;

	point2d itemViewPos, itemOldCursor;
	point2d* itemOldviewPos;
	point3* itemOldviewPos3;

	point2d stackItemViewPos, stackItemOldCursor;
	point2d* stackItemOldviewPos;

	point2d cameraPos, oldcameraPos, oldcameraCursor;

	float bx, by, bh, bw;

	float timelineScale = 50;
	int dragFlag = 0;

	int itemDragFlag = 0;
	int stackItemDragFlag = 0;

	int timelinedragFlag = 0;

	int cameradragFlag = 0;

	bool LButtonDown = false;

	HWND PEditor;
	HWND TEditor;
	int peName = 2000;
	int peButton = 3000;
	int peEditBox = 4000;

	int teEditBox = 5000;
	int teOk = 5001;
	int teCancel = 5002;

	BYTE* TextEditorActivePtr = NULL;
	int TextEditorActiveMaxSize = 0;
	BYTE TextEditorActiveCmd = 0;
	BYTE TextEditorActiveShader = 0;
	BYTE ActiveShaderType = 0;
	char* ActiveShaderName = NULL;
	enum ShaderType {exec,lib};
	

	HWND TE_edit, TE_ok, TE_cancel;

#define PESTEP 22

	int CurrentPos = 0;

	int lastSelectedI = 0;

	typedef struct {
		HWND Text;
		HWND Edit;
		HWND Button;
	} paramElements;

	paramElements paramH[MAXPARAM];

	double logTimer;

	char* GetResourceName(BYTE id, PVFN CreationRoutine, char* name);

	bool isKeyDown(int key)
	{
		return GetAsyncKeyState(key) & 0x8000;
	}


	void Log(char* text)
	{
		logTimer = timer::GetCounter();
		SetWindowText(hWnd, text);
	}

	void NLog(int n)
	{
		logTimer = timer::GetCounter();
		char sss[22];
		_itoa(n, sss, 10);
		SetWindowText(hWnd, sss);
	}

	bool isElementSelected(BYTE* ptr)
	{
		return *(ptr + CmdDesc[*ptr].Size - EditorInfoOffset + EditorInfoOffsetSelected)==1;
	}

	void SetElementSelection(BYTE* ptr,int i)
	{
		*(ptr + CmdDesc[*ptr].Size - EditorInfoOffset + EditorInfoOffsetSelected) = i;
	}


	BYTE GetCmdLevel(BYTE cmd)
	{
		return CmdDesc[cmd].Level;
	}

	BYTE GetCmdLevel(BYTE* i)
	{
		return CmdDesc[*i].Level;
	}

	void SetCurSelect()
	{
		*(stack::data + CurrentPos + CmdDesc[*(stack::data + CurrentPos)].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
	}
	void removeSelection();
	void Select(int mode);
	void WriteValue(int pNum, int value);

	//special drawers
	Commands::Clip_* CurrentDrawingClip;
	float ClipPosX;
	float ClipPosX_untransformed;
	float ClipPosX_untransformed_inFocus[256];
	float NotePosX_untransformed;
	float ClipW;
	int ClipNoteCount;
	int ClipRepeat;
	BYTE ClipGrid = 0;
	int LocalNoteCounter;
	Commands::Envelope_* CurrentDrawingEnvelope;
	float EnvelopePosX;
	float EnvelopeRange;
	float EnvelopeMin;
	int EnvelopeLoopStartIndex;
	int EnvelopeLoopEndIndex;
	float EnvelopeHeight = .52;
	float EnvelopeLink;
	float EnvelopeRetrigger;
	float EnvelopePointSize = .05f / 2.75f;
	BYTE Open;
	bool HiddenFlag;
	float NoteWidthUntransformed = 0;
	float MusicTimelineLeftMargin = -1.09;

	float tx, ty, w;
	BYTE* i;
	BYTE cmd;
	BYTE nextCmd;
	BYTE prevLevelCmd[32];
	BYTE* prevLevelCmdPtr[32];
	XMFLOAT4 Color;
	XMFLOAT4 TextColor;
	float stepY;
	float LevelStepRight;
	int envelopeLineCount;
	float elementHeight = .04f;
	float letterWidth = .032f;
	int counter;

	float ClipPosY;
	int InClipNoteCounter;

	float ScreenToTimeS16(float screenX)
	{
		return (screenX - (float)timelinePos.x) / ((.01f / 60.f)*timelineScale);
	}

	float TimeToScreenS16(float timeSigned16bit)
	{
		float x = timeSigned16bit;
		x *= .01f / 60.f;
		x *= timelineScale;
		x += (float)timelinePos.x;
		return x;
	}

	float SampleToScreen(float timeSigned16bit)
	{
		float x = timeSigned16bit;
		x *= .01f / (60.f*(44100 / 60.f));
		x *= timelineScale;
		x += (float)timelinePos.x;
		return x;
	}

	float SampleToScreenNoOffset(float timeSigned16bit)
	{
		float x = timeSigned16bit;
		x *= .01f / (60.f*(44100 / 60.f));
		x *= timelineScale;
		return x;
	}

	float WidthToScreenS16(float timeSigned16bit)
	{
		float x = timeSigned16bit;
		x *= .01f / 60.f;
		x *= timelineScale;
		//x += (float)timelinePos.x;
		return x;
	}


	XMFLOAT2 getCusorPos()
	{
		POINT p;
		GetCursorPos(&p);
		ScreenToClient(hWnd, &p);

		RECT r;
		GetClientRect(hWnd, &r);
		float c = (r.bottom - r.top) / float(dx::winH);
		float c2 = (r.right - r.left) / float(dx::winW);
		p.y /= c;
		p.x /= c2;

		float x, y;
		x = p.x / float(dx::width) * 2 - 1;
		y = -(p.y / float(dx::height) * 2 - 1);
		x *= 1.f / dx::aspect;

		XMFLOAT2 xp(x, y);
		return xp;
	}


	float clipUIx;
	float clipUIy;



	float currentDrawedClipW = 0;
	float prevPointX;
	float prevPointY;
	float prevPointSrcX;
	float prevPointSrcY;
	float envelopeYoffset;
	int pointCounter;
	BYTE* prevPointPtr;


	bool clipEnv = false;

	bool GetElementOpenStatus(BYTE* ptr)
	{
		return *(BYTE*)(ptr + CmdDesc[*ptr].Size - EditorInfoOffset + EditorInfoOffsetMode);
	}

	void SetElementOpenStatus(BYTE* ptr, BYTE v)
	{
		*(BYTE*)(ptr + CmdDesc[*ptr].Size - EditorInfoOffset + EditorInfoOffsetMode) = v;
	}

	int copiedBytes = 0;

	void copySelected()
	{
		BYTE* i = stack::data;
		BYTE* j = stack::data2;
		int counter = 0;

		while (*i != 0)
		{
			BYTE cmd = *i;

			BYTE nextcmd = *(i + CmdDesc[cmd].Size);

			BYTE selFlag = *(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected);

			if (CmdDesc[nextcmd].Level > CmdDesc[cmd].Level && selFlag == 1)
			{
				BYTE* j = i + CmdDesc[cmd].Size;
				while (*j != 0 && CmdDesc[*j].Level > CmdDesc[cmd].Level)
				{
					*(j + CmdDesc[*j].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
					j += CmdDesc[*j].Size;
				}
			}

			if (selFlag == 1)
			{
				memcpy(j, i, CmdDesc[cmd].Size);
				j += CmdDesc[cmd].Size;
			}

			i += CmdDesc[cmd].Size;
		}
		copiedBytes = j - stack::data2;
	}

	void RestoreLinks();

	bool paste()
	{
		if (copiedBytes < 1) return false;

		Commands::loopPoint = stack::data;

		BYTE* i = stack::data;
		BYTE* j = stack::data2;
		bool pflag = false;
		bool nflag = false;

		if (CmdDesc[*(BYTE*)(i + CurrentPos)].Routine == Point)
		{
			int n = 0;
			while (n < copiedBytes)
			{
				if (CmdDesc[*(BYTE*)(j + n)].Routine != Point) pflag = true;

				n += CmdDesc[*(BYTE*)(j + n)].Size;
			}
		}

		if (CmdDesc[*(BYTE*)(i + CurrentPos)].Routine == Note)
		{
			int n = 0;
			while (n < copiedBytes)
			{
				if (CmdDesc[*(BYTE*)(j + n)].Routine != Note) nflag = true;

				n += CmdDesc[*(BYTE*)(j + n)].Size;
			}
		}


		if (pflag || nflag)
		{
			return false;
		}

		memmove(i + copiedBytes + CurrentPos, i + CurrentPos, MAXDATASIZE - CurrentPos - copiedBytes);
		memcpy(i + CurrentPos, j, copiedBytes);

		RestoreLinks();
		Commands::loopPoint = stack::data;
		Commands::SceneLocateFlag = false;
		Commands::precalcIsDone = false;

		return true;
	}

	void removeSelection()
	{
		BYTE* i = stack::data;
		while (*i != 0)
		{
			BYTE cmd = *i;
			*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)0;
			i += CmdDesc[cmd].Size;
		}

	}

	

	void removeCmd(BYTE* i, bool linksUpdate = true)
	{
	/*	BYTE n = *i;
		if (CmdDesc[n].Routine == Commands::CreateShader)
		{
			int c = 0;
			while (!(Commands::resourceCreationPtr[0][c] == NULL || Commands::resourceCreationPtr[0][c] == i + CmdDesc[*i].Size + 1))
			{
				c++;
			}

			if (Commands::resourceCreationPtr[0][c] != NULL)
			{
				BYTE* ptr = stack::data;
				while (*ptr != 0)
				{
					if (CmdDesc[*ptr].Routine == Commands::NullDrawer)
					{
						Commands::NullDrawer_* in = (Commands::NullDrawer_*)(ptr + 1);
						if (in->shader >= c)
						{
							in->shader--;
						}
					}

					ptr += CmdDesc[*ptr].Size;
				}
			}

			Commands::loopPoint = stack::data;
		}*/


		memmove(i, i + CmdDesc[*i].Size, MAXDATASIZE + stack::data - i - CmdDesc[*i].Size);

		if (i < Commands::loopPoint)
		{
			Commands::loopPoint = stack::data;
		}

		if (linksUpdate) RestoreLinks();
	}

	void removeSelected()
	{
		Commands::loopPoint = stack::data;
		BYTE* i = stack::data;
		int counter = 0;

		while (*i != 0)
		{
			BYTE cmd = *i;

			BYTE nextcmd = *(i + CmdDesc[cmd].Size);

			BYTE selFlag = *(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected);

			if (CmdDesc[nextcmd].Level > CmdDesc[cmd].Level && selFlag == 1)
			{
				BYTE* j = i + CmdDesc[cmd].Size;
				while (*j != 0 && CmdDesc[*j].Level > CmdDesc[cmd].Level)
				{
					*(j + CmdDesc[*j].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
					j += CmdDesc[*j].Size;
				}
			}

			if (selFlag == 1)
			{
				removeCmd(i,false);
				CurrentPos = min(i - stack::data, CurrentPos);
				lastSelectedI = min(counter, lastSelectedI);
			}
			else
			{
				i += CmdDesc[cmd].Size;
			}

			counter++;
		}

		*(stack::data + CurrentPos + CmdDesc[*(stack::data + CurrentPos)].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;

		RestoreLinks();
	}

	void SelectRange(int i1, int i2)
	{
		int start = min(i1, i2);
		int end = max(i1, i2);

		BYTE* i = stack::data; int level = -1;
		int counter = 0;
		while (*i != 0)
		{
			BYTE cmd = *i;
			if (counter >= start && counter <= end)
			{
				if (level == -1) level = GetCmdLevel(cmd);
				*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
			}
			i += CmdDesc[cmd].Size;
			counter++;
		}

		//remove sublevels
		i = stack::data;
		while (*i != 0)
		{
			BYTE cmd = *i;

			if (*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) == (BYTE)1 && GetCmdLevel(*i) != level)
			{
				*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)0;
			}
			i += CmdDesc[cmd].Size;
		}
	}

	int GetScenePosById(int id)
	{
		BYTE* ptr = stack::data;
		int c = 0;

		while (*ptr != 0)
		{
			if (CmdDesc[*ptr].Routine == Scene)
			{
				if (c == id)
				{
					return ptr - stack::data;
				}

				c++;
			}

			ptr += CmdDesc[*ptr].Size;
		}

		return ui::CurrentPos;
	}

	float GetNoteSize(BYTE* i)
	{
		BYTE* c = stack::data;
		int noteCounter = 0;
		int activeNoteCounter = 0;
		float noteSize = 0;
		float clipX = 0;
		Commands::Clip_* in;

		while (*c != 0 && c <= i)
		{
			if (CmdDesc[*c].Routine == Clip)
			{
				noteCounter = 0;
				in = (Commands::Clip_*)(c + 1);
				clipX = (float)in->x;
				noteSize = 44100.f*60.f / (MasterBPM*in->bpmScale);
			}

			c += CmdDesc[*c].Size;
		}

		return noteSize;
	}

	void SelectByPos(int mode,float x,float y)
	{
		BYTE* i = stack::data;
		int counter = 0;

		if (!selectedElementPtr) return;

		while (*i != (BYTE)0 && i < selectedElementPtr)
		{
			i += CmdDesc[*i].Size;
			counter++;
		}

		cmd = *i;

		BYTE selectFlag = *(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected);

			if (mode == 0)
				{
					removeSelection();
					selectFlag = 1;
					lastSelectedI = counter;
					CurrentPos = i - stack::data;
					*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)selectFlag;

					//SelectedNoteX = 0;

					if (CmdDesc[cmd].Routine == Note)
					{
						BYTE* c = stack::data;
						int noteCounter = 0;
						int activeNoteCounter = 0;
						float noteSize = 0;
						float clipX = 0;
						Commands::Clip_* in;

						while (*c != 0 && c <= i)
						{
							if (CmdDesc[*c].Routine == Clip)
							{
								noteCounter = 0;
								in = (Commands::Clip_*)(c + 1);
								clipX = (float)in->x;
								noteSize = 44100.f*60.f / (MasterBPM*in->bpmScale);
							}

							if (CmdDesc[*c].Routine == Note)
							{
								Commands::Note_* n = (Note_*)(c + 1);
								//if (n->Slide ==0 && (n->Note!= 255 && n->Note != 0))
								activeNoteCounter = noteCounter;
								noteCounter++;
							}

							c += CmdDesc[*c].Size;
						}

						float delta = 9 * 60 * 60;
						float rOfs = 0;

						SelectedNoteClipRepeat = 0;

						if (in)
						{
							for (int j = 0; j < in->repeat; j++)
							{
								float tx = ScreenToTimeS16(x);
								float rnoteX = (clipX + noteSize * activeNoteCounter + noteSize * in->length*j) / (44100 / 60);
								float newdelta = (float)fabs(tx - rnoteX);
								//float newdelta = (float)fabs(x - TimeToScreenS16(clipX + noteSize * activeNoteCounter + noteSize * in->length*j));
								if (newdelta < delta)
								{
									delta = newdelta;
									rOfs = noteSize * in->length*j;
								}
							}
						}

						SelectedNoteX = clipX + noteSize * activeNoteCounter + rOfs;
						//cursorF = ScreenToTimeS16(SelectedNoteX);
						//cursorM = ScreenToTimeS16(SelectedNoteX);
						//Commands::StartPosition = ScreenToTimeS16(SelectedNoteX);

						SelectedNoteClipXWithRepeat = clipX + rOfs;
						SelectedNoteClipRepeat = rOfs;


						in = (Commands::Clip_*)(i + 1);
						int chnl = 0;

						c = stack::data;
						while (*c != 0 && c <= i)
						{
							if (CmdDesc[*c].Routine == Channel)
							{
								chnl++;
							}
							c += CmdDesc[*c].Size;
						}

						if (chnl > 0) ClipPosX_untransformed_inFocus[chnl - 1] = clipX;
						//ClipPosX_untransformed_inFocus = clipX;
					}

					if (CmdDesc[cmd].Routine == Clip)
					{
						Commands::Clip_* in = (Commands::Clip_*)(i + 1);
						int chnl = 0;

						BYTE* c = stack::data;
						while (*c != 0 && c <= i)
						{
							if (CmdDesc[*c].Routine == Channel)
							{
								chnl++;
							}
							c += CmdDesc[*c].Size;
						}

						if (chnl > 0) ClipPosX_untransformed_inFocus[chnl - 1] = (float)in->x;
					}

					//	return;
					/*	BYTE ggg = *(stack::data + CurrentPos);
						int ggg2 = CmdDesc[ggg].Category;
						int a = 0;*/
				}

			if (mode == 1)//toggle
				{
					selectFlag = 1 - selectFlag;
					lastSelectedI = counter;
					CurrentPos = i - stack::data;
				}

			if (mode == 2)//add
				{
					selectFlag = 1;
					SelectRange(counter, lastSelectedI);
					lastSelectedI = counter;
					CurrentPos = i - stack::data;
				}

			*(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)selectFlag;
		

	}

	void Select(int mode)
	{
		SelectByPos(mode, dxui::mousePos.x, dxui::mousePos.y);
	}

	int SearchCmdByRoutine(PVFN r);
	bool orderValidation = true;

	void RestoreLinks()
	{
		//resourses table
		int cScene = 0;
		int cShader = 0;
		int cTexture = 0;
		int cParam = 0;
		byte* ptr = stack::data;
		while (*ptr != 0)
		{
			if (CmdDesc[*ptr].Routine == Scene)
			{
				Scene_* in = (Scene_*)(ptr+1);
				strcpy(resourceCreationName2[2][cScene], in->Name);
				cScene++;
			}

			if (CmdDesc[*ptr].Routine == CreateShader)
			{
				CreateShader_* in = (CreateShader_*)(ptr+1);
				strcpy(resourceCreationName2[0][cShader], in->Name);
				cShader++;
			}

			if (CmdDesc[*ptr].Routine == CreateTexture)
			{
				CreateTexture_* in = (CreateTexture_*)(ptr + 1);
				strcpy(resourceCreationName2[1][cTexture], in->Name);
				cTexture++;
			}

			if (CmdDesc[*ptr].Routine == DefineParams)
			{
				DefineParams_* in = (DefineParams_*)(ptr + 1);
				strcpy(resourceCreationName2[3][cParam], in->Name);
				cParam++;
			}

			ptr += CmdDesc[*ptr].Size;
		}

		ptr = stack::data;
		while (*ptr != 0)
		{
			if (CmdDesc[*ptr].Routine == NullDrawer)
			{
				NullDrawer_* in = (NullDrawer_*)(ptr + 1);
				char* name = resourceCreationName[0][in->shader];
				int x = 0;
				while (x < cShader && strcmp(name, resourceCreationName2[0][x])) x++;
				in->shader = x;
			}

			if (CmdDesc[*ptr].Routine == DefineParams)
			{
				DefineParams_* in = (DefineParams_*)(ptr + 1);
				char* name = resourceCreationName[0][in->shader];
				int x = 0;
				while (x < cShader && strcmp(name, resourceCreationName2[0][x])) x++;
				in->shader = x;
			}

			if (CmdDesc[*ptr].Routine == ShowScene)
			{
				ShowScene_* in = (ShowScene_*)(ptr + 1);
				char* name = resourceCreationName[2][in->scene];
				int x = 0;
				while (x < cShader && strcmp(name, resourceCreationName2[2][x])) x++;
				in->scene = x;
			}

			if (CmdDesc[*ptr].Routine == SetRT)
			{
				SetRT_* in = (SetRT_*)(ptr + 1);
				char* name = resourceCreationName[1][in->texture];
				int x = 0;
				while (x < cShader && strcmp(name, resourceCreationName2[1][x])) x++;
				in->texture = x;
			}

			if (CmdDesc[*ptr].Routine == SetTexture)
			{
				SetTexture_* in = (SetTexture_*)(ptr + 1);
				char* name = resourceCreationName[1][in->texture];
				int x = 0;
				while (x < cShader && strcmp(name, resourceCreationName2[1][x])) x++;
				in->texture = x;
			}


			ptr += CmdDesc[*ptr].Size;
		}
	}

	void InsertCmd(int n)
	{
		BYTE CurCmd = *(stack::data + CurrentPos);
		BYTE* CurCmdPtr = stack::data + CurrentPos;
		//base logic
		int cursorLevel = CmdDesc[CurCmd].Level;
		int insertLevel = CmdDesc[n].Level;
		int nextCmd = *(stack::data + CurrentPos + CmdDesc[CurCmd].Size);
		int nextCmdLevel = CmdDesc[nextCmd].Level;

		if (insertLevel > cursorLevel + 1)
		{
			Log("Wrong Level");
			return;
		}

		//skip children
		if (insertLevel == cursorLevel)
		{
			if (nextCmdLevel > cursorLevel)
			{
				BYTE* j = stack::data + CurrentPos + CmdDesc[CurCmd].Size;
				int sz = 0;
				while (*j != 0 && CmdDesc[*j].Level > cursorLevel)
				{
					sz = CmdDesc[*j].Size;
					j += sz;
				}
				CurrentPos = j - stack::data - sz;
				CurCmd = *(stack::data + CurrentPos);
				//Log(CmdDesc[*j].Name);
			}
		}

		if (insertLevel < cursorLevel)
		{
			BYTE* j = stack::data + CurrentPos + CmdDesc[CurCmd].Size;
			int sz = CmdDesc[CurCmd].Size;
			while (*j != 0 && CmdDesc[*j].Level >= cursorLevel)
			{
				sz = CmdDesc[*j].Size;
				j += sz;
			}
			CurrentPos = j - stack::data - sz;
			CurCmd = *(stack::data + CurrentPos);

		}

		//custom logic


		BYTE Parent = -1;

		if (CmdDesc[n].Category == CAT_SOUND)
		{		
		//bla
		//track
		//  channel
		//    clip
		//      note
		//      env
		//        point
		//    osc/filter
		//      env
		//        point
		//	mixer
		//	  filter
		//      env
		//        point
		//	return
		//    filter
		//      env
		//        point
		//endtrack
		//bla

			//check track area
			BYTE* ptr = stack::data;
			BYTE* beginTrack = NULL;
			BYTE* endTrack = NULL;

			while (*ptr != 0 && CmdDesc[*ptr].Routine != EndDraw)
			{
				if (CmdDesc[*ptr].Routine == Commands::Music)
				{
					beginTrack = ptr;
				}

				if (CmdDesc[*ptr].Routine == Commands::EndMusic)
				{
					endTrack = ptr;
				}

				ptr += CmdDesc[*ptr].Size;
			}

			if (orderValidation)
			{
				if (beginTrack && endTrack)
				{
					//ok, between markers
					if (CurCmdPtr >= beginTrack &&
						CurCmdPtr < endTrack)
					{
						//get parent level
						BYTE* j = stack::data;
						BYTE level = 0;


						while (*j != 0 && CmdDesc[*j].Routine != EndDraw && j <= CurCmdPtr)
						{
							if (CmdDesc[*j].Level < CmdDesc[n].Level)
							{
								Parent = *j;
							}

							j += CmdDesc[*j].Size;
						}

						if (Parent == -1) return;

						//point only if parent is env
						if (CmdDesc[n].Routine == Point && CmdDesc[Parent].Routine != Envelope)
						{
							Log("point can be inserted only to envelope");
							return;
						}

						//env only if parent is clip(after notes/other env!) or osc/filter
						if (CmdDesc[n].Routine == Envelope)
						{
							BYTE* j = CurrentPos + stack::data;
							if (CmdDesc[*j].Routine == Clip) j += CmdDesc[*j].Size;
							int sz = 0; int cnt = 0; int notes = 0;
							while (*j != 0 &&
								CmdDesc[*j].Routine == Note)
							{
								sz = CmdDesc[*j].Size;
								j += sz;
							}

							CurrentPos = j - sz - stack::data;
							CurCmd = *(stack::data + CurrentPos);
						}

						//point autoinsert after env

						//note only if parent is clip
						if (CmdDesc[n].Routine == Note && CmdDesc[Parent].Routine != Clip)
						{
							Log("note can be inserted only to clip");
							return;
						}

						//clip only if parent is channel
						if (CmdDesc[n].Routine == Clip
							&& CmdDesc[Parent].Routine != Channel)
						{
							Log("clip can be inserted only to channel");
							return;
						}

						//osc/filter only if parent is channel or mixer
						if ((CmdDesc[n].Routine == Oscillator ||
							CmdDesc[n].Routine == Crunch ||
							CmdDesc[n].Routine == Chorus ||
							CmdDesc[n].Routine == DelayLine ||
							CmdDesc[n].Routine == Waveshaper ||
							CmdDesc[n].Routine == Equalizer)
							&& (CmdDesc[Parent].Routine != Channel && CmdDesc[Parent].Routine != MasterChannel))
						{
							Log("osc/filter can be inserted only to channel");
							return;
						}

						//osc/filter only after clip/his env
						if (CmdDesc[n].Routine == Oscillator ||
							CmdDesc[n].Routine == Crunch ||
							CmdDesc[n].Routine == Chorus ||
							CmdDesc[n].Routine == DelayLine ||
							CmdDesc[n].Routine == Waveshaper ||
							CmdDesc[n].Routine == Equalizer)
						{
							/*BYTE* j = CurrentPos + stack::data;
							if (CmdDesc[*j].Routine == Channel) j += CmdDesc[*j].Size;
							int sz = 0;
							while (*j != 0 &&
								(
									CmdDesc[*j].Routine == Clip ||
									CmdDesc[*j].Routine == Note ||
									CmdDesc[*j].Routine == Envelope ||
									CmdDesc[*j].Routine == Point))
							{
								sz = CmdDesc[*j].Size;
								j += sz;
							}

							CurrentPos = j - sz - stack::data;
							CurCmd = *(stack::data + CurrentPos);*/
						}

						//todo:: filter only after osc except return channel
					}
					else
						//outside markers
					{
						Log("please insert sound command between begin/end track markers only");
						return;
					}
				}
				else
				{
					if (CmdDesc[n].Routine != Commands::Music && CmdDesc[n].Routine != Commands::EndMusic)
					{
						Log("please insert begin/end track markers first");
						return;
					}

					if (CmdDesc[n].Routine == Commands::Music)
					{
						if (beginTrack)
						{
							Log("track already exist");
							return;
						}

						if (!endTrack)
						{
							//todo: place track+endtrack
						}

					}

					if (CmdDesc[n].Routine == Commands::EndMusic)
					{
						if (endTrack)
						{
							Log("endtrack already exist");
							return;
						}

					}



				}
			}
		}

		if (CmdDesc[n].Routine == Commands::CreateShader)
		{
			/*int c = 0;
			while (!(Commands::resourceCreationPtr[0][c] == NULL || Commands::resourceCreationPtr[0][c] == CurCmdPtr+CmdDesc[*CurCmdPtr].Size+1))
			{
				c++;
			}

			if (Commands::resourceCreationPtr[0][c] != NULL)
			{
				BYTE* ptr = stack::data;
				while (*ptr != 0)
				{
					if (CmdDesc[*ptr].Routine == Commands::NullDrawer)
					{
						Commands::NullDrawer_* in = (Commands::NullDrawer_*)(ptr + 1);
						if (in->shader >= c)
						{
							in->shader++;
						}
					}

					ptr += CmdDesc[*ptr].Size;
				}
			}

			Commands::loopPoint = stack::data;*/
		}

		if (CmdDesc[n].Routine == Commands::Scene)//update links
		{
			//BYTE* ptr = stack::data;

			//search current scene num
			/*int scenenum = -1;
			while (*ptr != 0 && ptr<CurCmdPtr)
			{
				if (CmdDesc[*ptr].Routine == Commands::Scene)
				{
					scenenum++;
				}
				ptr += CmdDesc[*ptr].Size;
			}

			//
			ptr = stack::data;
			while (*ptr != 0)
			{
				if (CmdDesc[*ptr].Routine == Commands::ShowScene)
				{
					Commands::ShowScene_* in = (Commands::ShowScene_*)(ptr + 1);

					if (in->scene > scenenum)
					{
						in->scene++;
					}

				}
				ptr += CmdDesc[*ptr].Size;
			}

			Commands::loopPoint = stack::data;
			Commands::SceneLocateFlag = false;
			Commands::precalcIsDone = false;
			*/
		}

		removeSelection();


		*(BYTE*)(stack::data + CurrentPos + CmdDesc[CurCmd].Size - EditorInfoOffset + EditorInfoOffsetMode) = 1;

		int LastPos = CurrentPos;
		BYTE LastCmd = *(BYTE*)(stack::data + CurrentPos);
		int LastSize = CmdDesc[LastCmd].Size;

		int CurCmdSize = CmdDesc[CurCmd].Size;
		CurrentPos += CurCmdSize;
		int sz = CmdDesc[n].Size;
		memmove(stack::data + CurrentPos + sz, stack::data + CurrentPos, MAXDATASIZE - CurrentPos - sz);
		ZeroMemory(stack::data + CurrentPos, sz);
		*(stack::data + CurrentPos) = (BYTE)n;

		int p = 0;
		while (*(BYTE*)CmdDesc[n].ParamName[p] != 0)
		{
			if (CmdDesc[n].Routine == Envelope && Parent >= 0 && CmdDesc[Parent].Routine == Commands::Clip && !strcmp(CmdDesc[n].ParamName[p], "assignTo"))
			{
				WriteValue(p, CmdDesc[n].ParamDefault[p]+MAXPARAM);
			}
			else
			{
				WriteValue(p, CmdDesc[n].ParamDefault[p]);
			}
			p++;
		}

		if (CmdDesc[n].Routine == Envelope)
		{
			*(BYTE*)(stack::data + CurrentPos + CmdDesc[n].Size - EditorInfoOffset + EditorInfoOffsetMode) = (BYTE)1;
		}


		if (CmdDesc[n].Routine == Point)
		{
			Commands::Point_* in = (Commands::Point_*)(stack::data + LastPos + 1);
			//signed short pX = (signed short)( *(signed short*)(stack::data + LastPos + 1)+200.0f/ui::timelineScale);
			auto pX = in->x;
			//signed short pY = 0;
			auto pY = 0;

			if (CmdDesc[*(BYTE*)(stack::data + CurrentPos + CmdDesc[n].Size)].Routine == Point)
			{
				/*signed short x = *(signed short*)(stack::data + LastPos + 1);
				signed short y = *(signed short*)(stack::data + LastPos + 3);
				signed short x1 = *(signed short*)(stack::data + CurrentPos + CmdDesc[n].Size + 1);
				signed short y1 = *(signed short*)(stack::data + CurrentPos + CmdDesc[n].Size + 3);
				*/
				in = (Commands::Point_*)(stack::data + LastPos + 1);
				auto x = in->x;
				auto y = in->y;

				in = (Commands::Point_*)(stack::data + CurrentPos + CmdDesc[n].Size + 1);
				auto x1 = in->x;
				auto y1 = in->y;

				pX = (signed short)((x + x1)*.5f);
				pY = (signed short)((y + y1)*.5f);
			}
			in = (Commands::Point_*)(stack::data + CurrentPos + 1);
			in->x = pX;
			in->y = pY;
			//*(signed short*)(stack::data + CurrentPos + 1)=pX;
			//*(signed short*)(stack::data + CurrentPos + 3) = pY;

		}


		lastSelectedI++;
		SetCurSelect();

		ui::Select(0);

		ui::LButtonDown = true;

		if (CmdDesc[n].Routine == Commands::Music)
		{
			orderValidation = false;
			InsertCmd(SearchCmdByRoutine(Channel));
			InsertCmd(SearchCmdByRoutine(Clip));
			InsertCmd(SearchCmdByRoutine(Oscillator));
			InsertCmd(SearchCmdByRoutine(MasterChannel));
			InsertCmd(SearchCmdByRoutine(EndMusic));
			orderValidation = true;
		}


		if (CmdDesc[n].Routine == Commands::Scene||
			CmdDesc[n].Routine == Commands::CreateTexture||
			CmdDesc[n].Routine == Commands::CreateShader||
			CmdDesc[n].Routine == Commands::DefineParams
		)
		{
			RestoreLinks();
			Commands::loopPoint = stack::data;
			Commands::SceneLocateFlag = false;
			Commands::precalcIsDone = false;
		}

	}


	int EnumMenu(int cmd, int pNum)
	{

		RECT rect; POINT pt;
		GetWindowRect(paramH[pNum].Button, &rect);
		pt.x = rect.left + 2;
		pt.y = rect.top + PESTEP - 2;

		HMENU hMenu;
		hMenu = CreatePopupMenu();

		int i = 0;

		while ((BYTE*)*CmdDesc[cmd].ParamEnum[pNum][i] != 0)
		{
			AppendMenu(hMenu, MF_STRING, 2000 + i, CmdDesc[cmd].ParamEnum[pNum][i]);
			i++;
		}

		return TrackPopupMenu(hMenu, TPM_RETURNCMD | TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, 0, PEditor, NULL) - 2000;
	}

	int EnumMenuAssign(int cmd, int pNum)
	{

		RECT rect; POINT pt;
		GetWindowRect(paramH[pNum].Button, &rect);
		pt.x = rect.left + 2;
		pt.y = rect.top + PESTEP - 2;

		HMENU hMenu;
		hMenu = CreatePopupMenu();

		int i = 0;

		if (CmdDesc[cmd].Routine == Commands::Clip)
		{
			for (int i=0;i<MAXPARAM;i++)
			{
				char complex[255];
				_itoa(i, complex, 10);
				AppendMenu(hMenu, MF_STRING, 2000 + i+MAXPARAM, complex);
			}
		}
		else
		{
			while ((BYTE*)*CmdDesc[cmd].ParamName[i] != 0)
			{
				char complex[255];

				strcpy(complex, CmdDesc[cmd].Name);
				strcat(complex, ": ");
				strcat(complex, CmdDesc[cmd].ParamName[i]);
				if (CmdDesc[cmd].ParamEnvControlled[i] == true)
				{
					AppendMenu(hMenu, MF_STRING, 2000 + i, complex);
				}
				else
				{
					AppendMenu(hMenu, MF_STRING | MF_DISABLED, 2000 + i, complex);
				}
				i++;
			}
		}

		return TrackPopupMenu(hMenu, TPM_RETURNCMD | TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, 0, PEditor, NULL) - 2000;
	}

	void Migrate()
	{
		POINT pt;
		pt.x = 0;
		pt.y = 0;

		HMENU hMenu;
		hMenu = CreatePopupMenu();
		int j = 0;
		for (int x = 0; x < stack::CommandsCount; x++)
		{
			AppendMenu(hMenu, MF_STRING, 2000 + j++, CmdDesc[x].Name);
		}

		BYTE c = TrackPopupMenu(hMenu, TPM_RETURNCMD | TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x + 32, pt.y, 0, hWnd, NULL)-2000;

		DestroyMenu(hMenu);

		removeSelection();
		SetCurSelect();
		copySelected();
		BYTE cmdToChange = *(stack::data2);
		*(stack::data2) = c;
		int oldcopiedBytes = copiedBytes;
		copiedBytes = CmdDesc[c].Size;

		BYTE* n = stack::data;

		while (*n != 0)
		{
			if (*n == cmdToChange)
			{
				char* s1 = CmdDesc[*n].Name;
				char* s2 = CmdDesc[cmdToChange].Name;
				CurrentPos = n - stack::data;
				removeCmd(n);
				paste();
			}
			n += CmdDesc[*n].Size;
		}

	}

	void CleanUiPtr()
	{
		dxui::mousePtr = NULL;
		dxui::mouseParam =-1;
		dxui::ClickRoutine = NULL;
		dxui::LastClickedRoutine = NULL;
		dxui::cursorID = 0;
		dxui::anyElementOver = false;
		selectedElementPtr = NULL;
		selectedParamID = -1;
		prevSelectedParamID = -1;

	}

	void ProcessProjectMenu(int c)
	{
		switch (c)
		{
		case 0: {CleanUiPtr(); Commands::loopPoint = stack::data; io::LoadTemplate(); ui::logTimer = 0; break;	}
		case 1: {CleanUiPtr(); Commands::loopPoint = stack::data; io::Load(); ui::logTimer = 0; break; }
		case 2: {io::Save(); ui::Log("saved"); break; }
		case 3: {io::SaveAs(); ui::Log("saved with new filename"); break; }
		case 4: {Commands::loopPoint = stack::data; break; }
		case 5: {io::SaveUsedCmdList(); ui::Log("saved"); break; }
		case 6: {Migrate(); break; }
		case 7: {io::SaveAsText(); ui::Log("exported"); break; }
		case 8: {CleanUiPtr(); io::ImportFromText();  break; }
		}
	}



	void AdaptiveTESize()
	{
		RECT r;
		GetWindowRect(TEditor, &r);
		SetWindowPos(TE_ok, HWND_TOP, 0, r.bottom - r.top - 65-32, (r.right - r.left - 15) / 1, 25+32, SWP_SHOWWINDOW);
		//SetWindowPos(TE_cancel, HWND_TOP, (r.right - r.left - 5) / 2, r.bottom - r.top - 65, (r.right - r.left - 15) / 2, 25, SWP_SHOWWINDOW);
		SetWindowPos(TE_edit, HWND_TOP, 5, 5, r.right - r.left - 20, r.bottom - r.top - 75-32, SWP_SHOWWINDOW);
	}

	bool changeWhenCompile = false;

	HANDLE shaderCompileInProgressMutex;
	HANDLE ShaderCompilerThread;

	//DWORD WINAPI CompileShaderInBackground(CONST LPVOID lpParam)
	void CompileShaderInBackground()
	{
		//shaderCompileInProgressMutex = CreateMutex(NULL, TRUE, NULL);

		if (ActiveShaderType == lib)
		{
			BYTE* i = stack::data;
			int cnt = 0;
			//while (*i != 0 && CmdDesc[*i].Routine != MainLoop)
			int t = 0;
			while (resourceCreationPtr[0][t])
			{
				i = resourceCreationPtr[0][t];
//				if (CmdDesc[*i].Routine == Commands::CreateShader)
				{
					Commands::CreateShader_* in = (Commands::CreateShader_*)(i);
					char* ptr = in->data;
					bool done = false;

					while (!done)
					{
						ptr = strstr(ptr, "//include");
						if (ptr)
						{
							char* endstr = strstr(ptr, "\n");
							char buf[500];
							memcpy(buf, ptr, endstr - ptr);
							buf[endstr - ptr] = 0;

							if (strstr(buf, ActiveShaderName))
							{
								Commands::CompileShader(cnt, (char*)in->data, (char*)in->data);
							}

							ptr = strstr(ptr, "\n");
						}
						else
						{
							done = true;
						}
					}

					cnt++;
				}
			//	i += CmdDesc[*i].Size;
				t++;
			}


		}
		else
		{
			Commands::CompileShader(TextEditorActiveShader, (char*)TextEditorActivePtr, (char*)TextEditorActivePtr);

			if (dx::pErrorBlob != NULL)
			{
				char* s = strstr((char*)dx::pErrorBlob->GetBufferPointer(), "error");
				if (s) SetWindowText(TE_ok, s); else
					SetWindowText(TE_ok, "Shader compiled");
			}
			else
			{
				SetWindowText(TE_ok, "Shader compiled");
			}
		}
	//	ReleaseMutex(shaderCompileInProgressMutex);
	//	ExitThread(0);
	}

	BOOL CALLBACK EditBoxProc(HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch (msg)
		{
		case WM_COMMAND:
			{
				int control = LOWORD(wParam);

				if (control == teOk)
				{
					if (dx::pErrorBlob != NULL)
					{
						char* p1 = NULL;
						p1 = strstr((char*)dx::pErrorBlob->GetBufferPointer(), "(");
						if (p1)
						{
							p1++;
							int s = atoi(p1);
							char* p2 = (char*)TextEditorActivePtr;
							int i = 1;
							while (i < s)
							{
								p2 = strstr(p2, "\n"); p2 += 2;
								i++;
							}
							
							char* p3= strstr(p2, "\n");

							SendDlgItemMessage(hDlg, teEditBox, EM_SETSEL, p2 - (char*)TextEditorActivePtr-1, p3 - (char*)TextEditorActivePtr);;
							SendDlgItemMessage(hDlg, teEditBox, EM_SCROLLCARET, 0, 0);
						}
					
					}
				}

				if (control == teEditBox || changeWhenCompile)
				{
					if (HIWORD(wParam) == EN_CHANGE || changeWhenCompile)
					{
						
						if (CmdDesc[TextEditorActiveCmd].Routine == CreateShader)
						{

							//DWORD signal = WaitForSingleObject(shaderCompileInProgressMutex, 0);
							//if (signal == WAIT_OBJECT_0 || signal == WAIT_FAILED)
							{
								changeWhenCompile = false;

								ZeroMemory(TextEditorActivePtr, TextEditorActiveMaxSize);
								GetDlgItemText(hDlg, control, (LPSTR)TextEditorActivePtr, TextEditorActiveMaxSize - 1);

								//ShaderCompilerThread = CreateThread(NULL, 0, &CompileShaderInBackground, NULL, 0, NULL);
								CompileShaderInBackground();
							}
							//else
							{
							//	changeWhenCompile = true;
							}

						}
					}
				}
				break;
			}

			case WM_SIZING:
			{
				AdaptiveTESize();
				break; 
			}

			case WM_CLOSE:
			{
				ShowWindow(TEditor, SW_HIDE);
			break;
			}
		}

		return 0;
	}

	int LastShownCmd = -1;

	char* GetShaderName(BYTE shaderID);
	char* GetShaderPtr(BYTE shaderID);

	char noteText[7] = "      ";

	void CreateNoteText(BYTE p)
	{
		//char* table = "C-0C#0D-0D#0E-0F-0F#0G-0G#0A-0A#0B-0";
		char* table = "C 0C#0D 0D#0E 0F 0F#0G 0G#0A 0A#0B 0";
		char* vtable = "0123456789";
		strcpy(noteText, " - ");
		//strcpy(noteText, "  ");
		if (p == 0) return;

		if (p == 255)
		{
			strcpy(noteText, "off");
			return;
		}

		if (p < 12 * 8 + 1) //in range
		{
			p--;//skip nonote
			int octave = p / 12;
			char _octave[10];
			_itoa(octave, _octave, 10);
			int note = p - octave * 12;

			noteText[0] = table[note * 3];
			noteText[1] = table[note * 3 + 1];
			noteText[2] = _octave[0];
			noteText[3] = 0;
		}

	}

	/*
	void ShowParam()
	{

		HWND pf = GetFocus();

		RECT r;
		GetWindowRect(hWnd, &r);

		BYTE* pP = stack::data + CurrentPos;

		BYTE cmd = *pP;
		SetWindowText(PEditor, CmdDesc[cmd].Name);
		int x = 0;

		//change name for replace it from shader constant buffer
		if (CmdDesc[cmd].Routine == Quad)
		{
			BYTE shaderID = *(BYTE*)(pP + CmdDesc[cmd].ParamOffset[0]);
			char* text = GetShaderPtr(shaderID);
			char* ptr1 = text;
			char* ptr2 = text;
			char* end = text;
			//strstr(text, "float4 _");
			if (text)
			{
				text = strstr(text, "{");
				end = strstr(text, "}");

				int pc = 1;
				while (text < end&&text != NULL && ptr2 != NULL)
				{
					strcpy(CmdDesc[cmd].ParamName[pc], "");
					text = strstr(text, "float4 _");
					if (text)
					{
						text += 8;
						ptr2 = strstr(text, ";");
						if (ptr2 != NULL)
						{
							if (text != NULL && ptr2 != NULL)
							{
								strncpy(CmdDesc[cmd].ParamName[pc], text, min(ptr2 - text, MAXPARAMNAMELEN - 1));
								CmdDesc[cmd].ParamName[pc][min(ptr2 - text, MAXPARAMNAMELEN - 1)] = 0;
								CmdDesc[cmd].ParamType[pc] = PT_SWORD;
								CmdDesc[cmd].ParamMin[pc] = -32768;
								CmdDesc[cmd].ParamMax[pc] = 32768;
							}
						}

						//enum
						char* cr = strstr(ptr2, "\n");
						ptr1 = ptr2;
						int enumN = 0;

						for (int t = 0; t < MAXPARAMENUM; t++)CmdDesc[cmd].ParamEnum[pc][t][0] = 0;

						while (ptr1 != NULL && ptr1 < cr)
						{
							ptr1 = strstr(ptr1, "//");
							if (ptr1 != NULL && ptr1 + 2 < cr)
							{
								ptr1 += 2;
								char* ptr3 = strstr(ptr1, "//");
								if (ptr3 == NULL || (ptr3 != NULL && ptr3 > cr))
								{
									strncpy(CmdDesc[cmd].ParamEnum[pc][enumN], ptr1, min(cr - ptr1, MAXPARAMNAMELEN - 1));
									CmdDesc[cmd].ParamEnum[pc][enumN][min(cr - ptr1, MAXPARAMNAMELEN - 1)] = 0;
									ptr1 = cr;
								}
								else
								{
									strncpy(CmdDesc[cmd].ParamEnum[pc][enumN], ptr1, min(ptr3 - ptr1, MAXPARAMNAMELEN - 1));
									CmdDesc[cmd].ParamEnum[pc][enumN][min(ptr3 - ptr1, MAXPARAMNAMELEN - 1)] = 0;

								}

								CmdDesc[cmd].ParamType[pc] = PT_ENUM;
								enumN++;
							}
						}
					}

					pc++;
				}
				//strncpy(CmdDesc[cmd].ParamName[1], "ppp", 32);
			}
		}
		//


		while ((BYTE*)*CmdDesc[*pP].ParamName[x] != 0)
		{
			int offset = CmdDesc[cmd].ParamOffset[x];
			char* name = CmdDesc[cmd].ParamName[x];
			BYTE type = CmdDesc[cmd].ParamType[x];
			//

			//custom enum
			if (!strcmp(name, "shader"))
			{
				type = PT_ENUM;
				CmdDesc[cmd].ParamType[x] = type;
				
				BYTE* sp = stack::data;
				int shaderCounter = 0;
				while (*sp != 0 )//check shader creation area
				{
					if (CmdDesc[*sp].Routine == CreateShader && shaderCounter<MAXPARAMENUM)
					{
							for (int _x = 0; _x<MAXPARAM; _x++)
							{
								if (!strcmp(CmdDesc[*sp].ParamName[_x], "Name"))
								{
									strcpy(CmdDesc[cmd].ParamEnum[x][shaderCounter], (char*)(sp + CmdDesc[*sp].ParamOffset[_x]));
								}
							}
						

						shaderCounter++;
					}

					sp += CmdDesc[*sp].Size;
				}

			}

			if (!strcmp(name, "texture"))
			{
				type = PT_ENUM;
				CmdDesc[cmd].ParamType[x] = type;

				BYTE* sp = stack::data;
				int textureCounter = 0;
				while (*sp != 0 )//check texture creation area
				{
					if (CmdDesc[*sp].Routine == CreateTexture&& textureCounter<MAXPARAMENUM)
					{
						for (int _x = 0; _x<MAXPARAM; _x++)
						{
							if (!strcmp(CmdDesc[*sp].ParamName[_x], "Name"))
							{
								char* tn = (char*)(sp + CmdDesc[*sp].ParamOffset[_x]);
								strcpy(CmdDesc[cmd].ParamEnum[x][textureCounter], tn);
							}
						}

						textureCounter++;
					}
					sp += CmdDesc[*sp].Size;
				}

			}

			if (!strcmp(name, "scene"))
			{
				type = PT_ENUM;
				CmdDesc[cmd].ParamType[x] = type;

				BYTE* sp = stack::data;
				int sceneCounter = 0;
				while (*sp != 0 )//check scene creation area
				{
					if (CmdDesc[*sp].Routine == Scene && sceneCounter<MAXPARAMENUM)
					{
						for (int _x = 0; _x<MAXPARAM; _x++)
						{
							if (!strcmp(CmdDesc[*sp].ParamName[_x], "Name"))
							{
								char* tn = (char*)(sp + CmdDesc[*sp].ParamOffset[_x]);
								strcpy(CmdDesc[cmd].ParamEnum[x][sceneCounter], tn);
							}
						}

						sceneCounter++;
					}
					sp += CmdDesc[*sp].Size;
				}

			}

			if (!strcmp(name, "vb"))
			{
			}

			if (name)
			{
				SetWindowText(paramH[x].Text, name);
				switch (type)
				{
				case PT_BYTE:
				{
					BYTE v;
					v = *(pP + offset);
					SetDlgItemInt(PEditor, peEditBox + x, v, false);
					EnableWindow(paramH[x].Edit, true);
					ShowWindow(paramH[x].Button, SW_HIDE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140, step * x, 55, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_SBYTE:
				{
					signed char v;
					v = *(signed char*)(pP + offset);
					SetDlgItemInt(PEditor, peEditBox + x, v, true);
					EnableWindow(paramH[x].Edit, true);
					ShowWindow(paramH[x].Button, SW_HIDE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140, step * x, 55, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_SWORD:
				{
					signed short v;
					signed short* s=(signed short*)(pP + offset);
					//v = *(pP + offset);
					v = *s;
					SetDlgItemInt(PEditor, peEditBox + x, v, true);
					EnableWindow(paramH[x].Edit, true);
					ShowWindow(paramH[x].Button, SW_HIDE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140, step * x, 55, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_WORD:
				{
					unsigned short v;
					unsigned short* s = (unsigned short*)(pP + offset);
					//v = *(pP + offset);
					v = *s;
					SetDlgItemInt(PEditor, peEditBox + x, v, true);
					EnableWindow(paramH[x].Edit, true);
					ShowWindow(paramH[x].Button, SW_HIDE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140, step * x, 55, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_INT:
				{
					INT32 v;
					INT32* s = (INT32*)(pP + offset);
					//v = *(pP + offset);
					v = *s;
					SetDlgItemInt(PEditor, peEditBox + x, v, true);
					EnableWindow(paramH[x].Edit, true);
					ShowWindow(paramH[x].Button, SW_HIDE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140, step * x, 55, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_ENUMASSIGN:
				{
					//search target

					BYTE* i = stack::data;
					BYTE* start = stack::data;
					BYTE* end = stack::data + CurrentPos;
					while (*start != 0 && start < end)
					{
						if (CmdDesc[*start].Level < CmdDesc[*end].Level)
						{
							i = start;
						}
						start += CmdDesc[*start].Size;
					}

					EnableWindow(paramH[x].Edit, false);
					BYTE v = *(pP + offset);

					char* enumText; char buf[10];
					if (CmdDesc[*i].Routine == Commands::Clip)
					{
						_itoa(v-MAXPARAM,buf,10);
						enumText = buf;
					}
					else
					{
						enumText = CmdDesc[*i].ParamName[v];
					}

					SetWindowText(paramH[x].Edit, enumText);

					ShowWindow(paramH[x].Button, SW_SHOW | SW_SHOWNOACTIVATE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Button, 0, 120 - 66, step * x, h, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140-66, step * x, 55+66, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110-66, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_ENUM:
				{
					EnableWindow(paramH[x].Edit, false);
					BYTE i = *(pP + offset);

					char* enumText = CmdDesc[cmd].ParamEnum[x][i];

					SetWindowText(paramH[x].Edit, enumText);

					ShowWindow(paramH[x].Button, SW_SHOW|SW_SHOWNOACTIVATE);
					int step = PESTEP; int h = 20;
					int offset = 0;
					if (!strcmp(CmdDesc[cmd].ParamName[x], "shader")|| 
						!strcmp(CmdDesc[cmd].ParamName[x], "scene")||
						!strcmp(CmdDesc[cmd].ParamName[x], "texture") ) offset = 50;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Button, 0, 120-offset, step * x, h, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140-offset, step * x, 55+offset, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110-offset, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);

					break;
				}
				case PT_LABEL:
				{
					EnableWindow(paramH[x].Edit, true);
					BYTE* Text = pP + offset;
					SetWindowText(paramH[x].Edit, (char*)Text);
					ShowWindow(paramH[x].Button, SW_HIDE );

					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140-100, step * x, 55+100, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 25, h, SWP_SHOWWINDOW | SWP_NOACTIVATE);
					
					break;
				}
				case PT_TEXT:
					EnableWindow(paramH[x].Edit, false);
					ShowWindow(paramH[x].Button, SW_SHOW|SW_SHOWNOACTIVATE);
					int step = PESTEP; int h = 20;
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Edit, 0, 140, step * x, 55, h, SWP_HIDEWINDOW );
					if (cmd != LastShownCmd) SetWindowPos(paramH[x].Text, 0, 5, x*step + 2, 110, h, SWP_SHOWWINDOW );

					break;
				}
			}
			x++;
		}

		if (x == 0)
		{
			ShowWindow(PEditor, SW_HIDE ); return;
			SetFocus(pf);
		}
		int headerh = GetSystemMetrics(SM_CYCAPTION) - 7;
		if (cmd != LastShownCmd)
		{
			SetWindowPos(PEditor, HWND_TOP, r.right - 210, 32, 210, (x + 1)*PESTEP + headerh, SWP_SHOWWINDOW | SWP_NOMOVE );
		}
		else
		{
			SetWindowPos(PEditor, HWND_TOP, r.right - 210, 32, 210, (x + 1)*PESTEP + headerh, SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOACTIVATE);
		}

		SetFocus(pf);

		LastShownCmd = cmd;
	}*/

	bool isTypeNumeric(int type)
	{
		if (type == PT_BYTE) return true;
		if (type == PT_SBYTE) return true;
		if (type == PT_SWORD) return true;
		if (type == PT_WORD) return true;
		if (type == PT_INT) return true;
		return false;
	}

	bool isTypeSigned(int type)
	{
		if (type == PT_SBYTE) return true;
		if (type == PT_SWORD) return true;
		if (type == PT_INT) return true;
		return false;
	}

	void ReadValue(BYTE* pP, int t, char* ptr)
	{
		BYTE cmd = *pP;
		int offset = CmdDesc[cmd].ParamOffset[t];
		char* name = CmdDesc[cmd].ParamName[t];
		BYTE type = CmdDesc[cmd].ParamType[t];
		pP += offset;

		switch (type)
		{
			case PT_BYTE:
			{
				BYTE v = *(pP);
				_itoa(v, ptr, 10);
				break;
			}
			case PT_SBYTE:
			{
				signed char v = *(signed char*)(pP);
				_itoa(v, ptr, 10);
				break;
			}
			case PT_SWORD:
			{
				signed short v = *(signed short*)(pP);
				_itoa(v, ptr, 10);
				break;
			}
			case PT_WORD:
			{
				unsigned short v = *(unsigned short*)(pP);
				_itoa(v, ptr, 10);
				break;
			}
			case PT_INT:
			{
				INT32 v = *(INT32*)(pP);
				_itoa(v, ptr, 10);
				break;
			}

			case PT_ENUMASSIGN:
			{
				BYTE* i = stack::data;
				BYTE* start = stack::data;
				BYTE* end = stack::data + CurrentPos;
				while (*start != 0 && start < end)
				{
					if (CmdDesc[*start].Level < CmdDesc[*end].Level)
					{
						i = start;
					}
					start += CmdDesc[*start].Size;
				}

				BYTE v = *(pP);

				char* enumText; char buf[64];
				if (CmdDesc[*i].Routine == Commands::Clip)
				{
					_itoa(v - MAXPARAM, buf, 10);
					enumText = buf;
				}
				else
				{
					enumText = CmdDesc[*i].ParamName[v];
				}
				strcpy(ptr, enumText);
				break;
			}
			case PT_ENUM:
			{
				BYTE i = *(pP);
				strcpy(ptr,CmdDesc[cmd].ParamEnum[t][i]);
				break;
			}
			case PT_LABEL:
			{
				strcpy(ptr, (char*)pP);
				break;
			}
			case PT_TEXT:
			{
				break;
			}
		}
	}

	int GetCmdType(int pNum)
	{
		BYTE cmd = *(stack::data + CurrentPos);
		return CmdDesc[cmd].ParamType[pNum];
	}

	int GetCmdType(BYTE* ptr, int pNum)
	{
		return CmdDesc[*ptr].ParamType[pNum];
	}

	char* GetCmdName(BYTE* ptr)
	{
		return CmdDesc[*ptr].Name;
	}

	char* GetParamName(BYTE* ptr,int pNum)
	{
		return CmdDesc[*ptr].ParamName[pNum];
	}


	INT32 ReadNumericValue(BYTE* ptr,int pNum)
	{
		BYTE cmd = *ptr;
		ptr+=CmdDesc[cmd].ParamOffset[pNum];

		switch (CmdDesc[cmd].ParamType[pNum])
		{
			case PT_BYTE:
			{
				BYTE v = *(ptr);
				return v;
			}
			case PT_SBYTE:
			{
				signed char v = *(signed char*)(ptr);
				return v;
			}
			case PT_SWORD:
			{
				signed short v = *(signed short*)(ptr);
				return v;
			}
			case PT_WORD:
			{
				unsigned short v = *(unsigned short*)(ptr);
				return v;
			}
			case PT_INT:
			{
				INT32 v = *(INT32*)(ptr);
				return v;
			}
		}

		return 0;
	}

	INT32 ReadNumericValue (int pNum)
	{
		return ReadNumericValue (stack::data + CurrentPos, pNum);
	}


	void WriteValue(BYTE* ptr,int pNum,INT32 value)
	{
		BYTE cmd = *(ptr);
		int type = CmdDesc[cmd].ParamType[pNum];
		int offset = CmdDesc[cmd].ParamOffset[pNum];
		value = clamp_int32(value, CmdDesc[cmd].ParamMin[pNum], CmdDesc[cmd].ParamMax[pNum]);
		ptr += offset;

		switch (type)
		{
		case PT_BYTE:
			*(ptr) = (BYTE)value;
			break;
		case PT_SBYTE:
			*(signed char*)(ptr) = (signed char)value;
			break;

		case PT_SWORD:
		{
			signed short* p = (signed short*)(ptr);
			*p = (signed short)value;
			break;
		}
		case PT_WORD:
		{
			unsigned short* p = (unsigned short*)(ptr);
			*p = (unsigned short)value;
			break;
		}
		case PT_INT:
		{
			INT32* p = (INT32*)(ptr);
			*p = (INT32)value;
			break;
		}
		case PT_TEXT:
			//todo*(stack::data + CurrentPos) = (BYTE)value;
			break;
		case PT_ENUM:
			*(ptr) = (BYTE)value;
			break;
		case PT_ENUMASSIGN:
			*(ptr) = (BYTE)value;
			break;
		}
	}

	void WriteValue(int pNum, int value, int position)
	{
		WriteValue(stack::data + position,pNum, value);
	}

	void WriteValue(int pNum, int value)
	{
		WriteValue(stack::data + CurrentPos, pNum, value);
	}
	

	int lastControl = 0;

	void EditShader()
	{		
		Commands::CreateShader_* in = (Commands::CreateShader_*)(stack::data + CurrentPos+1);

		TextEditorActivePtr = (BYTE*)in->data;
		TextEditorActiveMaxSize = TypeSizeTable[PT_TEXT] - 1;
		TextEditorActiveCmd = *(stack::data + CurrentPos);;

		BYTE* sptr = stack::data; int shCounter = 0;
		while (*sptr != 0 && sptr < stack::data + CurrentPos)
		{
			if (CmdDesc[*sptr].Routine == Commands::CreateShader)
			{
				shCounter++;
			}
			sptr += CmdDesc[*sptr].Size;
		}

		SetWindowText(TEditor, in->Name);
		ActiveShaderName = in->Name;
		TextEditorActiveShader = shCounter;
		ActiveShaderType = in->type;

		ShowWindow(TEditor, SW_SHOW);
		SetWindowText(TE_edit, (LPCSTR)TextEditorActivePtr);
	}
	
	LPDLGTEMPLATE lpdt;
	void DialogTemplate()
	{
		HGLOBAL hgbl;
		LPWORD lpw;
		hgbl = GlobalAlloc(GMEM_ZEROINIT, 1024);
		lpdt = (LPDLGTEMPLATE)GlobalLock(hgbl);
		lpdt->style = WS_CAPTION | WS_POPUP;
		lpdt->cdit = 0;         // Number of controls
		lpdt->x = 0;  lpdt->y = 0;
		lpdt->cx = 0; lpdt->cy = 0;
		lpw = (LPWORD)(lpdt + 1);
		*lpw++ = 0;             // No menu
		*lpw++ = 0;
	}

	void initTextEditor()
	{
		static DLGPROC lpfnDlgTextEditor;
		lpfnDlgTextEditor = (DLGPROC)MakeProcInstance((FARPROC)EditBoxProc, hInst);
		lpdt->style = WS_CAPTION | WS_SYSMENU | WS_SIZEBOX;
		TEditor = CreateDialogIndirect(hInst, lpdt, hWnd, lpfnDlgTextEditor);

		TE_edit = CreateWindow("EDIT", "", WS_VISIBLE | WS_CHILD | ES_MULTILINE | WS_VSCROLL | ES_NOHIDESEL, 0,0, 0, 0, TEditor, (HMENU)(teEditBox), (HINSTANCE)GetWindowLong(TEditor, GWL_HINSTANCE), NULL);
		SendMessage(TE_edit, WM_SETFONT, (LPARAM)GetStockObject(DEFAULT_GUI_FONT), true);

		HFONT hFont = CreateFont(16, 6, 0, 0, FW_LIGHT, 0, 0, 0, 0, 0, 0, 0, 0, TEXT("Tahoma"));
		SendMessage(TE_edit, WM_SETFONT, (WPARAM)hFont, 0);

		TE_ok = CreateWindow("BUTTON", "", WS_VISIBLE | WS_CHILD| BS_MULTILINE, 0, 0, 0, 0, TEditor, (HMENU)(teOk), (HINSTANCE)GetWindowLong(TEditor, GWL_HINSTANCE), NULL);
		SendMessage(TE_ok, WM_SETFONT, (LPARAM)GetStockObject(DEFAULT_GUI_FONT), true);

	//	TE_cancel = CreateWindow("BUTTON", "CANCEL", WS_VISIBLE | WS_CHILD, 0, 0, 0, 0, TEditor, (HMENU)(teCancel), (HINSTANCE)GetWindowLong(TEditor, GWL_HINSTANCE), NULL);
	//	SendMessage(TE_cancel, WM_SETFONT, (LPARAM)GetStockObject(DEFAULT_GUI_FONT), true);

		SetWindowPos(TEditor, HWND_TOP, dx::winW- dx::winW/3, 75, dx::winW/3, dx::winH*.95, SWP_HIDEWINDOW);

		AdaptiveTESize();

		ShowWindow(TEditor, SW_HIDE);
		UpdateWindow(TEditor);
	}
	
	void AddCmdAfter()
	{

	}

	void AddChild()
	{

	}

	void ContextInsertCmd()
	{
		if (CmdDesc[*i].Category != CAT_SOUND) return;

		if (isSelected)
		{
			if (CmdDesc[*i].Routine != Envelope && CmdDesc[*i].Routine != Point && CmdDesc[*i].Routine != SendToEnv && 
				CmdDesc[*i].Routine != Music && CmdDesc[*i].Routine != EndMusic)
			{
				dxui::Box(tx + w + letterWidth * .1, ty, elementHeight*.9, elementHeight, Color, .1, true, true, i, -1, AddCmdAfter);
				dxui::String(tx + letterWidth * .125 + w + letterWidth * .1, ty - .004, letterWidth*.9, elementHeight*.8, TextColor, "...");
			}

			if (CmdDesc[*i].Routine != Clip && CmdDesc[*i].Routine != Point && CmdDesc[*i].Routine != SendToEnv && CmdDesc[*i].Routine != EndMusic)
			{
				dxui::Box(tx + w + letterWidth * .1 + elementHeight, ty, elementHeight*.9, elementHeight, Color, .1, true, true, i, -1, AddChild);
				dxui::String(tx + letterWidth * .125 + w + letterWidth * .1 + elementHeight, ty - .004, letterWidth*.9, elementHeight*.8, TextColor, "+");
			}

			//show context menu
			if (dxui::LastClickedRoutine == AddCmdAfter)
			{
				int cat = CmdDesc[*i].Category;

				{
					XMFLOAT4 boxColor = menuBoxColor;
					XMFLOAT4 textColor = menuTextColor;

					w = .2;
					float x = -1.35;
					float y = ty;


					int cnt = 0;
					for (int t = 0; t < stack::CommandsCount; t++)
					{
						float _x = x;
						float _y = y - cnt * stepY;

						//if (CmdDesc[t].Category == cat)
						if (
							(CmdDesc[*i].Routine == Channel && CmdDesc[t].Routine == Channel) ||
							(CmdDesc[*i].Routine == Channel && CmdDesc[t].Routine == MasterChannel) ||
							(CmdDesc[*i].Routine == Clip && CmdDesc[t].Routine == Clip) ||
							(CmdDesc[*i].Routine == Clip && CmdDesc[t].Routine == Oscillator) ||
							
								(
									(
										CmdDesc[*i].Routine == Oscillator ||
										CmdDesc[*i].Routine == Crunch ||
										CmdDesc[*i].Routine == DelayLine ||
										CmdDesc[*i].Routine == Chorus ||
										CmdDesc[*i].Routine == Equalizer ||
										CmdDesc[*i].Routine == Waveshaper
									)
									&& 
									(
										CmdDesc[t].Routine == Oscillator ||
										CmdDesc[t].Routine == Crunch ||
										CmdDesc[t].Routine == DelayLine ||
										CmdDesc[t].Routine == Chorus ||
										CmdDesc[t].Routine == Equalizer ||
										CmdDesc[t].Routine == Waveshaper
									)
								)
							)
						{
							dxui::Box(_x, _y, w, elementHeight, boxColor, .1, false, true, NULL, t, ClickInsertCmd);
							dxui::String(_x, _y - elementHeight * .05, letterWidth*.9, elementHeight*.8, textColor, CmdDesc[t].Name);
							cnt++;
						}
					}
				}
			}

			//show context menu - incert child
			if (dxui::LastClickedRoutine == AddChild)
			{
				int cat = CmdDesc[*i].Category;
				{
					XMFLOAT4 boxColor = menuBoxColor;
					XMFLOAT4 textColor = menuTextColor;

					float x = x = -1.35;
					float y = ty;
					w = .2;

					int cnt = 0;
					for (int t = 0; t < stack::CommandsCount; t++)
					{
						float _x = x;
						float _y = y - cnt * stepY;

						if ((CmdDesc[*i].Routine == Music && CmdDesc[t].Routine == Channel)||

							(CmdDesc[*i].Routine == MasterChannel && 
								(
									CmdDesc[t].Routine == Crunch ||
									CmdDesc[t].Routine == DelayLine ||
									CmdDesc[t].Routine == Chorus ||
									CmdDesc[t].Routine == Equalizer ||
									CmdDesc[t].Routine == Waveshaper 
								)
							) ||

						    (CmdDesc[*i].Routine == Channel && CmdDesc[t].Routine == Clip)||
							(CmdDesc[*i].Routine == Channel && CmdDesc[t].Routine == Return) ||

							(CmdDesc[*i].Routine == Oscillator && CmdDesc[t].Routine == Envelope) ||
							(CmdDesc[*i].Routine == Oscillator && CmdDesc[t].Routine == SendToEnv) ||

							(CmdDesc[*i].Routine == Crunch && CmdDesc[t].Routine == Envelope) ||
							(CmdDesc[*i].Routine == Crunch && CmdDesc[t].Routine == SendToEnv) ||

							(CmdDesc[*i].Routine == DelayLine && CmdDesc[t].Routine == Envelope) ||
							(CmdDesc[*i].Routine == DelayLine && CmdDesc[t].Routine == SendToEnv) ||

							(CmdDesc[*i].Routine == Chorus && CmdDesc[t].Routine == Envelope) ||
							(CmdDesc[*i].Routine == Chorus && CmdDesc[t].Routine == SendToEnv) ||

							(CmdDesc[*i].Routine == Equalizer && CmdDesc[t].Routine == Envelope) ||
							(CmdDesc[*i].Routine == Equalizer && CmdDesc[t].Routine == SendToEnv) ||

							(CmdDesc[*i].Routine == Waveshaper && CmdDesc[t].Routine == Envelope) ||
							(CmdDesc[*i].Routine == Waveshaper && CmdDesc[t].Routine == SendToEnv) ||

							(CmdDesc[*i].Routine == Envelope && CmdDesc[t].Routine == Point)

						   )
						{
							dxui::Box(_x, _y, w, elementHeight, boxColor, .1, false, true, NULL, t, ClickInsertCmd);
							dxui::String(_x, _y - elementHeight * .05, letterWidth*.9, elementHeight*.8, textColor, CmdDesc[t].Name);
							cnt++;
						}
					}
				}
			}

		}
	}

	XMFLOAT2 StandartDrawer(char* Name = NULL)
	{
		if (HiddenFlag) return XMFLOAT2 (tx,ty);

		if (CmdDesc[cmd].Level < CmdDesc[nextCmd].Level)
		{
			dxui::String(tx- letterWidth, ty, letterWidth, elementHeight*.9, XMFLOAT4(1,1,1,1), GetElementOpenStatus(i) ? "|" :"{");
			bx -= letterWidth;
			bw += elementHeight;
		}

		if (isSelected)
		{
			Color = colorScheme.selectedbox;
			TextColor = colorScheme.selectedfont;
		}

		dxui::Box(tx, ty, w, elementHeight, Color, .1, false, true, i);

		char* n = CmdDesc[cmd].Name;
		if (Name)
		{ 
			n = Name;
		}

		dxui::String(tx + letterWidth * .125, ty - .004, letterWidth*.9, elementHeight*.8, TextColor, n);

		if (!strcmp(CmdDesc[cmd].ParamName[0], "Name"))
		{
			dxui::String(tx+ w+letterWidth*.21, ty-.004, letterWidth*.9, elementHeight*.8, colorScheme.font, (char*)(i+1));
		}

		ContextInsertCmd();

		float tyR = ty;

		ty -= stepY;

		return XMFLOAT2(tx+w+letterWidth*.21, tyR);
	}

	BYTE GetNextShownCmd(BYTE* i)
	{
		BYTE* j = i + CmdDesc[*i].Size;
		BYTE Level = CmdDesc[*i].Level;

		while (*j != (BYTE)0 && CmdDesc[*j].Level > Level)
		{
			j += CmdDesc[*j].Size;
		}

		return *j;
	}

	float ChannelPosX;
	float ChannelPosY;
	float EnvelopePosY;
	float ChannelH;
	float EnvelopeH;
	int channelCounter;
	int CurrentDrawingChannel;

	float channelY;
	bool anyClipEnv = false;

	BYTE TrackOpen = false;

	float SynthCmdPos = 0;
	int SynthCmdCounter = 0;
	float SynthCmdY = 0;

	float teY = 0;
	float teYSize=0;
	float SynthY = 0;
	int clipCounter = 0;

	float GetSomeColor(float v)
	{
		return .5f + .125f*sinf(v) + .25f*isSelected;
	}

	float GetSomeColor(float v, float amp)
	{
		return .5f + amp*sinf(v);
	}

	XMFLOAT4 ColorForEnvelope(float t)
	{
		t += .4;
		XMFLOAT4 c = XMFLOAT4(GetSomeColor(t*5.9, .5), GetSomeColor(t * 4.2, .5), GetSomeColor(t * 3.6, .5), 1);
		float m = 1.f / max(max(c.x, c.y), c.z);
		m *= .75;
		c.x *= m; c.y *= m; c.z *= m;
		return c;
	}

	void ToggleBypass()
	{
		BYTE* i = ui::selectedElementPtr;
		BYTE cmd = *i;
		if (CmdDesc[cmd].Routine == Commands::Equalizer)
		{
			Commands::Equalizer_* eqIn = (Commands::Equalizer_*)(i + 1);
			eqIn->bypass = !eqIn->bypass;
		}

		if (CmdDesc[cmd].Routine == Commands::Chorus)
		{
			Commands::Chorus_* eqIn = (Commands::Chorus_*)(i + 1);
			eqIn->bypass = !eqIn->bypass;
		}

		if (CmdDesc[cmd].Routine == Commands::DelayLine)
		{
			Commands::DelayLine_* eqIn = (Commands::DelayLine_*)(i + 1);
			eqIn->bypass = !eqIn->bypass;
		}

		if (CmdDesc[cmd].Routine == Commands::Crunch)
		{
			Commands::Crunch_* eqIn = (Commands::Crunch_*)(i + 1);
			eqIn->bypass = !eqIn->bypass;
		}

		if (CmdDesc[cmd].Routine == Commands::Waveshaper)
		{
			Commands::Waveshaper_* eqIn = (Commands::Waveshaper_*)(i + 1);
			eqIn->bypass = !eqIn->bypass;
		}
	}

	void SynthCommandDrawer()
	{

		//return;

		if (HiddenFlag) return;

		//teY -= teYSize;
		//ChannelPosY = teY;
		//SynthY = teY;
		//teYSize = elementHeight * 1.2f;
		//SynthCmdY = ChannelPosY;

		//bx = tx;
		//by = ChannelPosY;
		//bw = w * 2.f;
		//bh = elementHeight;

		//return;

		if (CmdDesc[cmd].Routine== Commands::Equalizer)
		{ 
			Commands::Equalizer_* eqIn = (Commands::Equalizer_*)(i + 1);
			if (eqIn->bypass) Color.w *= .5;
		}

		if (CmdDesc[cmd].Routine == Commands::Chorus)
		{
			Commands::Chorus_* eqIn = (Commands::Chorus_*)(i + 1);
			if (eqIn->bypass) Color.w *= .5;
		}

		if (CmdDesc[cmd].Routine == Commands::Crunch)
		{
			Commands::Crunch_* eqIn = (Commands::Crunch_*)(i + 1);
			if (eqIn->bypass) Color.w *= .5;
		}

		if (CmdDesc[cmd].Routine == Commands::Waveshaper)
		{
			Commands::Waveshaper_* eqIn = (Commands::Waveshaper_*)(i + 1);
			if (eqIn->bypass) Color.w *= .5;
		}

		if (CmdDesc[cmd].Routine == Commands::DelayLine)
		{
			Commands::DelayLine_* eqIn = (Commands::DelayLine_*)(i + 1);
			if (eqIn->bypass) Color.w *= .5;
		}


//		if (CmdDesc[cmd].Level < CmdDesc[nextCmd].Level)
	//	{
		//	dxui::String(bx - elementHeight, by, letterWidth, elementHeight, Color, GetElementOpenStatus(i) ? "|" : "{");
		//}


		StandartDrawer();

		//if (CmdDesc[cmd].Level < CmdDesc[nextCmd].Level) dxui::Box(bx - elementHeight / 2., by - elementHeight / 2. + .007, .015, .015, Color, .5);

		//dxui::Box(bx, by, bw, bh, Color, .1, NULL, counter);
		//dxui::Box(bx, by, .12, bh, Color, .1, false, true, i);
		//dxui::String(bx, by, letterWidth*.9, bh*.9, TextColor, CmdDesc[cmd].Name);

		//BYTE j = *(i+CmdDesc[*i].Size);

		//SynthCmdPos = MusicTimelineLeftMargin + dxui::StringLen(elementHeight, CmdDesc[cmd].Name)*1.2f;
		//SynthCmdCounter++;
	}


	int focusedChannel = 0;

	float channelSoloTable[255];

	float channelXofs = .0;

	int currentSelectedNote = 0;
	BYTE* currentChannelPtr = NULL;
	BYTE* currentClipPtr = NULL;
	BYTE* currentNotePtr = NULL;
	int highestNote;
	int lowestNote;

	void ClickNote()
	{
		int i = 0;
		if (isKeyDown(VK_CONTROL)) i = 1;
		if (isKeyDown(VK_SHIFT)) i = 2;

		//currentSelectedNote = ui::selectedParamID;
		//currentClipPtr = ui::selectedElementPtr;

		currentNotePtr = ui::selectedElementPtr;
		char* a = CmdDesc[*ui::currentNotePtr].Name;
		ui::Select(i);
	}

	void ChannelWidget()
	{
		bool ext = true;

		Commands::Channel_* in = (Commands::Channel_*)(i + 1);
		XMFLOAT4 col = XMFLOAT4(.2, .2, .2, 2);
		XMFLOAT4 tex = XMFLOAT4(1, 1, 1, 1);
		XMFLOAT4 off = XMFLOAT4(0, 0, 0, 1);
		XMFLOAT4 on = XMFLOAT4(1, .5, 0, 1);

		float h = elementHeight * 2.1f;
		float y = ChannelPosY;
		bx -= .05;
		dxui::Box(bx - letterWidth / 2. - .01, y, ext ? .3 : .16, teYSize - .005, isSelected ? XMFLOAT4(.35, .35, .35, 1) : col, 0.07f, false, true, i);
		y -= .01;
		bool selNameBox = (0 == selectedParamID && i == ui::selectedElementPtr);
		if (dxui::LastClickedRoutine == ui::ClickNote) selNameBox = false;
		XMFLOAT4 c = selNameBox ? tex : col;
		XMFLOAT4 c2 = selNameBox ? col : tex;

		dxui::Box(bx, y, ext ? .17 : .12, elementHeight, c, 0.1f, false, false, i, 0, ClickParam);
		dxui::String(bx, y, letterWidth, elementHeight*.9,c2, in->Name);

		if (selNameBox)
		{
			dxui::Box(bx + .005 + textCursorX, y - 0.012 / 2, .002, ui::elementHeight*.8, XMFLOAT4(0, 0, 0, .5 + .5*sin(.01*timer::GetCounter())), 0.001, NULL, -1, false);
		}

		y -= .005;
		dxui::Box(bx, y - elementHeight, letterWidth*.9, elementHeight*.8, in->mute ? on : off, 0.1f, false, false, i, 4, ClickParam);
		dxui::String(bx + .005, y - elementHeight, letterWidth, elementHeight*.8, tex, "M");

		dxui::Box(bx + letterWidth, y - elementHeight, letterWidth*.9, elementHeight*.8, in->solo ? on : off, 0.1f, false, false, i, 5, ClickParam);
		dxui::String(bx + .005 + letterWidth, y - elementHeight, letterWidth, elementHeight*.8, tex, "S");
		y += .01;

		if (ext)
		{
			float eh = h - .01;

			//volume
			float vol = in->volume / 255.;
			dxui::Box(bx + .18, y-.005, .03, eh, XMFLOAT4(0.1, 0.1, 0.1, 1), 0.015f, false, false, i, 1, ClickSlider, XMFLOAT4(0, vol, 0, 0), XMFLOAT4(.8, .4, .2, 1));

			//send
			float snd = in->send / 255.;
			dxui::Box(bx + .18 +.035, y-.005, .03, eh, XMFLOAT4(0.1, 0.1, 0.1, 1), 0.015f, false, false, i, 3, ClickSlider, XMFLOAT4(0, snd, 0, 0), XMFLOAT4(.2, .8, .2, 1));

			//volumemeter
			float vl = volumeL[CurrentDrawingChannel];
			float vr = volumeR[CurrentDrawingChannel];
			XMFLOAT4 peakl = XMFLOAT4(1, 1, 0, 1);
			XMFLOAT4 peakr = XMFLOAT4(1, 1, 0, 1);

			dxui::Box(bx + .1825,		 y - eh + vl * eh, .01, vl * eh, peakl, 0.01f, false, false);
			dxui::Box(bx + .1825 + .015, y - eh + vr * eh, .01, vr * eh, peakr, 0.01f, false, false);
			
			//helpers
			dxui::String(bx + .18 + .006, y, letterWidth*.75, elementHeight*.75, XMFLOAT4(.5, .5, .5, 1), "V");
			dxui::String(bx + .18 + .035 + .006, y, letterWidth*.75, elementHeight*.75, XMFLOAT4(.5, .5, .5, 1), "S");

			//pan
			y -= .01;
			
			float pan = .5*letterWidth*2.5 * in->pan / 127.;
			dxui::Box(bx + letterWidth * 2.1, y - elementHeight * 1.25, letterWidth * 2.5, elementHeight / 2, XMFLOAT4(0.1, 0.1, 0.1, 1), 0.015f, false, false, i, 2, ClickSlider);
			dxui::Box(bx + letterWidth * 2.1+pan+ letterWidth * 2.5/2.- .0025, y - elementHeight * 1.25, .005, elementHeight / 2, XMFLOAT4(1., 0.5, 0.5, 1), 0.015f, false, false);

			char chn[22];
			_itoa(CurrentDrawingChannel, chn, 10);

			XMFLOAT4 cc = TextColor;
			cc.w *= 2;
			//dxui::String(bx - .01, y - elementHeight * .1, letterWidth*.75, elementHeight*.9, TextColor, "chn");
			dxui::String(bx - .027, y - elementHeight * .45, letterWidth*.8, elementHeight*.8, cc, chn);
		}
	}

	int noteViewMode = 0;

	void ShowNotes()
	{
		Clip_* in = (Clip_*)(i + 1);

		float npm = Commands::MasterBPM * in->bpmScale;
		float noteTimeWidthInFrames = 60.f*60.f / npm;
		float noteSize = WidthToScreenS16(noteTimeWidthInFrames);//on-screen size

		if (noteSize <= .025)
		{
			return;
		}

		float left = (ScreenToTimeS16(MusicTimelineLeftMargin + noteSize)-in->x/FRAMELEN) / noteTimeWidthInFrames;
		float right = (ScreenToTimeS16(2)) / noteTimeWidthInFrames;
		int rm = min(in->length*in->repeat - 1, 9 * 60 * 60 / noteTimeWidthInFrames);
		left = max(left, 0);
		right = min(right, rm);

		XMFLOAT4 back = XMFLOAT4(.85, .82, .80, 1);
		XMFLOAT4 front = XMFLOAT4(.85, .82, .80, 1);
		float rollHeight = .25;

							/*dxui::SetupDrawerline();
							dxui::lineVBSet();
							for (int t = 0; t < 8 * 12/2; t+=2)
							{

								dxui::lineVertices[0].Pos.x = ui::MusicTimelineLeftMargin;
								dxui::lineVertices[0].Pos.y = ChannelPosY- t * elementHeight*rollHeight*2;
								dxui::lineVertices[0].Pos.z = 0;
								dxui::lineVertices[1].Pos.x = 2;
								dxui::lineVertices[1].Pos.y = ChannelPosY - t * elementHeight*rollHeight*2;
								dxui::lineVertices[1].Pos.z = 0;

								dxui::UpdateLineVb(2 * 2);

								dxui::Line(0.0, 0.f, 1.f, 1.f, XMFLOAT4(1,1,1,.3),1);
							}

							dxui::SetupDrawerBox();*/

		int highestNote = 1;
		int lowestNote = 8 * 12 + 1;

		BYTE* notePtr = (BYTE*)(i + CmdDesc[*i].Size);

		int noteCmdSize = CmdDesc[*notePtr].Size;

		for (int c = 0 ; c < in->length; c++)
		{
			Note_* noteIn = (Note_*)(notePtr + 1);
			if (noteIn->Note != 0 && noteIn->Note != 255)
			{
				highestNote = max(highestNote, noteIn->Note);
				lowestNote = min(lowestNote, noteIn->Note);
			}

			notePtr += noteCmdSize;
		}

		int range = highestNote - lowestNote;
		int lN = lowestNote;
		float lowestY = ChannelPosY - elementHeight*2 - (range * elementHeight) * rollHeight;

		if (Open)
		{
			teYSize = max(range * elementHeight * rollHeight + elementHeight, teYSize);
			ChannelH = max(ChannelH, range * elementHeight * rollHeight + elementHeight * 2+ elementHeight*.95);

		}
		else
		{
			teYSize = max(elementHeight * 2, teYSize);
		}

		notePtr = (BYTE*)(i + CmdDesc[*i].Size);

		for (int t = (int)left; t <= (int)right; t++)
		{
			float swingtable[2] = { -1,1 };
			float s = swingtable[int(t % 2)];
			float adj = s*noteTimeWidthInFrames * in->swing / 127.;
			float noteSizeSwing = WidthToScreenS16(noteTimeWidthInFrames+adj);
			float swingOfs = -(noteSizeSwing - noteSize)/2.;

			int tFR = fracRanged(t, in->length);
			notePtr = (BYTE*)(i + CmdDesc[*i].Size) + tFR * noteCmdSize;

			Note_* noteIn = (Note_*)(notePtr + 1);
			float x, y;
			x = SampleToScreen(in->x)- WidthToScreenS16(noteTimeWidthInFrames * in->swing / 127.)/2.;
			y = ChannelPosY- elementHeight;

			if (Open)
			{
				if (noteIn->Note > 0 && noteIn->Note < 255)
				{
					y -= (range * elementHeight - (noteIn->Note - lN) * elementHeight) * rollHeight;
				}
			}

			//bool sel = notePtr == currentNotePtr;
			bool sel = *(notePtr + noteCmdSize - EditorInfoOffset + EditorInfoOffsetSelected);
			//XMFLOAT4 c = sel ? front : back;
			XMFLOAT4 c = sel ? XMFLOAT4(.5,.3,.1,1) : XMFLOAT4(.3, .3, .3, 1);

			if (noteIn->Note == 0)
			{
				c.w*= .862;
			}

			//note 
			if (noteViewMode > 3)
			{
				y= ChannelPosY - elementHeight;
			}

			XMFLOAT4 c2 = c;
			c2.x *= .5;c2.y *= .5;c2.z *= .5;
			dxui::Box(t*noteSize + x+ swingOfs, y, noteSizeSwing*.98, elementHeight, 
				c2, .1, false, false, notePtr, 2, ClickNote, XMFLOAT4(1. - noteIn->Slide / 255., 0, 0, 0), c,floor(t/in->length));

			if (noteIn->Retrigger == 1)
			{
				dxui::Box(t*noteSize + x + swingOfs, y+ elementHeight*.1, noteSizeSwing*.98, elementHeight*.075,
					XMFLOAT4(1,0,0,1), .05, false, false, notePtr, 2, NULL, XMFLOAT4(0, 0, 0, 0), c, floor(t / in->length));
			}

			if (noteViewMode == 0 && noteIn->Variation>0)
			{
				XMFLOAT4 vc[9];
				vc[0] = XMFLOAT4(1, 0, 0, 1);
				vc[1] = XMFLOAT4(0, 1, 0, 1);
				vc[2] = XMFLOAT4(0, 0, 1, 1);
				vc[3] = XMFLOAT4(1, 1, 1, 1);
				vc[4] = XMFLOAT4(.5, .5, .5, 1);
				vc[5] = XMFLOAT4(1, 1, 0, 1);
				vc[6] = XMFLOAT4(0, 1, 1, 1);
				vc[7] = XMFLOAT4(1, 0, 1, 1);
				vc[8] = XMFLOAT4(1, .5, .0, 1);

				dxui::Box(t*noteSize + x + swingOfs, y -elementHeight+elementHeight*.2, elementHeight*.3, elementHeight*.3,
					vc[clamp_int(noteIn->Variation-1,0,8)], .24, false, false, notePtr, 2, NULL, XMFLOAT4(0, 0, 0, 0), c, floor(t / in->length));
			}


			if (noteViewMode == 0 || noteViewMode > 3)
			{
				CreateNoteText(noteIn->Note);
			}

			if (noteViewMode == 1)
			{
				strcpy(noteText, noteIn->Retrigger == 0 ? "S" : "L");
			}

			if (noteViewMode == 2)
			{
				_itoa(noteIn->Slide,noteText, 10);
			}

			if (noteViewMode == 3)
			{
				char var[11] = "0123456789";
				noteText[0] = var[noteIn->Variation];
				noteText[1] = 0;
			}
			
			if (noteSize <= .05 && noteSize > .025 &&noteIn->Note > 0)
			{
				noteText[1] = 0;
				float sl = dxui::StringLen(letterWidth, noteText);
				float so = noteSize / 2. - sl / 2.;
				dxui::String(t*noteSize + x + so, y, fmin(noteSize / 1.5, letterWidth), elementHeight, sel ? back : front, noteText);
			}

			if (noteSize > .05&&noteIn->Note > 0)
			{
				noteText[3] = 0;
				float sl = dxui::StringLen(letterWidth, noteText);
				float so = noteSize / 2. - sl / 2.;
				dxui::String(t*noteSize + x + so, y, fmin(noteSize / 1.5, letterWidth), elementHeight, sel ? back : front, noteText);
			}

			if (noteViewMode > 3)
			{
				//sends
				if (Open)
				{
					int si = 0;
					float sendH = elementHeight * 3;
					//float sy = lowestY - sendH * si - elementHeight;
					float sy = ChannelPosY - sendH * si - elementHeight*2;
					float val = 0;
					switch (noteViewMode)
					{
					case 4: val = noteIn->s0; break;
					case 5: val = noteIn->s1; break;
					case 6: val = noteIn->s2; break;
					case 7: val = noteIn->s3; break;
					}
					XMFLOAT4 bar = XMFLOAT4(.35, .35, .35, 1);
					XMFLOAT4 progress = XMFLOAT4(.75, .75, .35, 1);

					dxui::Box(t*noteSize + x, sy, noteSize / 3, sendH, bar, 0.01, false, true, notePtr, noteViewMode, ClickSlider, XMFLOAT4(0, val / 255., 0, 0), progress);
					
					//if (noteSize > .05)
					if (selectedElementPtr&&ui::LButtonDown)
					{
						if (notePtr == selectedElementPtr)
						{
							char ss[22];
							_itoa(val, ss, 10);
							float sl = dxui::StringLen(letterWidth, ss);
							float so = noteSize / 2. - sl / 2.;
							dxui::String(t*noteSize + x + so, y + elementHeight, fmin(noteSize / 1.5, letterWidth), elementHeight *.8, front, ss);
						}
					}
				}
			}
			
	
		}

	}

	void ChannelDrawer()
	{
		clipCounter = 0;
		SynthCmdCounter = 0;
		SynthCmdPos = MusicTimelineLeftMargin;

		teY -= teYSize;
		ChannelPosY -= ChannelH+EnvelopeH;
		ty = min(ChannelPosY, ty);
		ChannelPosY = min(ty, ChannelPosY);

		//StandartDrawer();

		teYSize = stepY*2.;
		anyClipEnv = false;
		ChannelH = elementHeight * 2.+ elementHeight*.5;
		EnvelopeH = 0;
		//CurrentDrawingChannel++;

		//return;
		/*
		float channelColorAmp = 1;

		if (!HiddenFlag)
		{
			Commands::Channel_* in = (Commands::Channel_*)(i + 1);

			auto mute = in->mute;
			auto solo = in->solo;

			bool solomode = false;
			for (int x = 0; x < MAXCHANNELS; x++)
			{
				if (Commands::channelSolo[x] == true) solomode = true;
			}

			float coloramp = 1;
			if (solomode)
			{
				coloramp *= solo;
				channelColorAmp *= solo;
			}
			else
			{
				coloramp *= (1 - mute);
				channelColorAmp *= (1 - mute);
			}

			channelSoloTable[CurrentDrawingChannel] = coloramp;

			coloramp = coloramp*.25f+.75f;
			
			Color.x *= coloramp;
			Color.y *= coloramp;
			Color.z *= coloramp;

		//	bx = tx;
		//	by = ChannelPosY + .007f;
		//	bw = MusicTimelineLeftMargin-bx+4;
		//	bh = elementHeight*2;

		}
		*/
		ChannelWidget();

		tx += .12;
		ContextInsertCmd();
		tx -= .12;

		ty -= stepY * 2.;
		CurrentDrawingChannel++;

	}

	void TimeLoopDrawer()
	{
		StandartDrawer();

		if (!HiddenFlag)
		{
			TimeLoop_* in = (TimeLoop_*)(i + 1);
			float x = SampleToScreen((float)in->x);
			bx = x;

			by = -1. + 2.*(dx::height - dx::winH) / (float)(dx::height) + elementHeight*3.;
			bw = WidthToScreenS16(in->length/FRAMELENF);
			bh = elementHeight*.7;
			Color.w *= .5;
			XMFLOAT4 c = in->active ? XMFLOAT4(1,.5,.0,1) : Color;

			dxui::Box(bx, by, bw, bh, c, .1, false, true, i);

			if (in->active == 1)
			{
				Color.w *= .5f;
				dxui::Box(bx, 1.1f, 0.0025f, 2.2f, Color, .01f);
				dxui::Box(bx + bw, 1.1f, 0.0025f, 2.2f, Color, .01f);
			}
		}
	}

	int ClipEnvelopeCount = 0;

	void ClipConsistencyUpdate()
	{
		BYTE* firstNote = i + CmdDesc[*i].Size;
		BYTE* j = firstNote;
		int notesCollected = 0;
		while (CmdDesc[*j].Routine == Note)
		{
			notesCollected++;
			j+= CmdDesc[*j].Size;
		}
		BYTE* firstNoNote = j;

		Commands::Clip_* in = (Commands::Clip_*)(i + 1);

		//LESS NOTES THEN NEEDED
		if (in->length > notesCollected)
		{
			int nc = in->length-notesCollected;
			int sz = sizeof(Note_) + 1 + EditorInfoOffset;
			BYTE* afterClip = firstNote + sz * in->length;
			int tSize = MAXDATASIZE - (afterClip - stack::data);
			memmove(afterClip, firstNoNote, tSize);
			ZeroMemory(firstNoNote, sz*nc);

			BYTE cm = 0;
			while (cm < stack::CommandsCount && CmdDesc[cm].Routine != Note) cm++;

			for (int x = 0; x < nc; x++)
			{
				*j = cm;
				int p = 0;
				while (*(BYTE*)CmdDesc[cm].ParamName[p] != 0)
				{
					WriteValue(p, CmdDesc[cm].ParamDefault[p],j-stack::data);
					p++;
				}
				j += sizeof(Note_) + 1 + EditorInfoOffset;
			}
		}

		//MORE NOTES THEN NEEEDED
		if (in->length < notesCollected)
		{
			int nc = notesCollected- in->length;
			int sz = sizeof(Note_) + 1 + EditorInfoOffset;
			BYTE* firstNote = i + CmdDesc[*i].Size;
			int tSize = MAXDATASIZE - (firstNoNote - stack::data);
			BYTE* afterClip = firstNote + sz * in->length;
			memmove(afterClip, firstNoNote, tSize);
		}
	}

	float GetSomeColor(float v);

	void ClipDrawer()
	{
		CurrentDrawingClip = (Commands::Clip_*)(i + 1);
		ClipConsistencyUpdate();

		Color = ColorForEnvelope(clipCounter);
		Color.x *= .35;
		Color.y *= .35;
		Color.z *= .35;
		StandartDrawer();

		if (ui::isElementSelected(i))
		{
			Color = colorScheme.selectedbox;
			TextColor = colorScheme.selectedfont;
		}

	//	Color.w*=(channelSoloTable[max(CurrentDrawingChannel-1,0)] * .85f + .15f);

		ClipEnvelopeCount = 0;

		clipUIx = tx;
		clipUIy = ty;
		clipEnv = false;

		Commands::Clip_* in = (Commands::Clip_*)(i+1);

		auto _posX = in->x;
		auto _noteCount = in->length;
		auto repeat = in->repeat;
		auto npm = Commands::MasterBPM*in->bpmScale;

		ClipGrid = in->grid;

		float width= WidthToScreenS16(_noteCount*60.f*60.f / npm);
		currentDrawedClipW = width;
		float posX = SampleToScreen((float)_posX);

		SynthCmdPos = posX+.1f;

		NoteWidthUntransformed = _noteCount * 60.f*60.f / npm;

		float curs = (float)(Commands::cursorM * SAMPLERATE / 60);

		ClipPosX = posX;
		ClipPosX_untransformed =(float)_posX;
		ClipNoteCount = _noteCount;
		ClipW = width ;
		ClipRepeat = repeat;
		LocalNoteCounter = 0;

		char cname[64];
		_itoa(clipCounter, cname, 10);
		//strcpy(cname, CmdDesc[cmd].Name);
		float xc = 0;
		float mw = width;
		float lastX = posX;
		for (int x = 0; x < repeat; x++)
		{
			xc = posX + width * x;
			if (xc + width > MusicTimelineLeftMargin)
			{
				mw = width;

				if (xc < MusicTimelineLeftMargin)
				{
					mw -=  MusicTimelineLeftMargin-xc;
				}

				xc = max(xc, MusicTimelineLeftMargin);

				dxui::Box(xc, ChannelPosY, mw, elementHeight*.8, Color, .1, false, true, i,-1,NULL,XMFLOAT4(0,0,0,0), XMFLOAT4(0, 0, 0, 0),x);
				
			}

			lastX += mw;
		}
		
		if (posX + width*repeat > MusicTimelineLeftMargin+ dxui::StringLen(elementHeight, cname)*1.15f)
		{
			dxui::String(max(posX, MusicTimelineLeftMargin), ChannelPosY, letterWidth, elementHeight, TextColor, cname);
		}


		ShowNotes();


		bx = max(posX,MusicTimelineLeftMargin);
		//by = ChannelPosY;
		bw = lastX-posX;

		BYTE Scmd = GetNextShownCmd(i);

		clipCounter++;
	}



	void EnvelopeContextDrawer()
	{
		if (HiddenFlag) return;

		Commands::Envelope_* eIn = (Commands::Envelope_*)(i + 1);
		auto v = eIn->assignTo;

		Color = XMFLOAT4(GetSomeColor((float)v), GetSomeColor(v+1.f), GetSomeColor(v+2.f),.9f);

		char buf[10];
		_itoa(v, buf, 10);
		char* enumText;

		if (CmdDesc[prevLevelCmd[CmdDesc[*i].Level]].Routine == Commands::Clip)
		{
			char tx[30];
			strcpy(tx, "esend ");
			_itoa(v-MAXPARAM, buf, 10);
			strcat(tx, buf);
			enumText = tx;

			float sw = dxui::StringLen(elementHeight, enumText)*1.5f;
			float clipXscreen = TimeToScreenS16(CurrentDrawingClip->x / FRAMELEN);			
			if (sw + SynthCmdPos > clipXscreen+currentDrawedClipW) return;
		}
		else
		{			
			enumText = CmdDesc[prevLevelCmd[CmdDesc[cmd].Level]].ParamName[v];
		}

		char nt[128];
		strcpy(nt, enumText);

		bx = MusicTimelineLeftMargin-elementHeight;
		by = SynthY;
		bw = dxui::StringLen(elementHeight,nt) * 1.1f;
		bh = elementHeight;

		by -= eIn->assignTo*elementHeight*1.1*1.4;
		//dxui::Box(bx, by, bh, bh, Color, .25, false, true);

//		dxui::Box(bx, by, bw, bh, Color, .1, NULL, counter);
//		dxui::String(bx, by, letterWidth, elementHeight, TextColor, nt);

		Commands::Envelope_* in = (Commands::Envelope_*)(i + 1);
		XMFLOAT4 col = XMFLOAT4(.2, .2, .2, 2);
		XMFLOAT4 tex = XMFLOAT4(1, 1, 1, 1);
		XMFLOAT4 off = XMFLOAT4(0, 0, 0, 1);
		XMFLOAT4 on = XMFLOAT4(1, .5, 0, 1);

		float h = elementHeight;
		float y = by;
		bx -= elementHeight;

		//assign op
		bx += .01;
		dxui::Box(bx+letterWidth, y, h, h, col, 0.25f, false, true, i, 5,ClickParam);
		dxui::String(bx+letterWidth+.01, y, letterWidth, h, tex, CmdDesc[*i].ParamEnum[5][in->assignOp]);

		if (i == ui::selectedElementPtr && dxui::LastClickedRoutine == ClickParam)
		{
			for (int n = 0; n < 5; n++)
			{
				dxui::Box(bx+letterWidth, y+h*(n+1)*1.2, h, h, col, 0.25f, false, true, i, n,ClickParamContextMenu);
				dxui::String(bx+letterWidth+.01, y+h*(n+1)*1.2, letterWidth, h, tex, CmdDesc[*i].ParamEnum[5][n]);
			}
		}

		//notelink
		//
	}

	void SomeToEnvDrawer()
	{
		StandartDrawer();
	}

	void EnvelopeDrawer()
	{
		Commands::Envelope_* eIn = (Commands::Envelope_*)(i + 1);
		auto v = eIn->assignTo;

		CurrentDrawingEnvelope = (Commands::Envelope_*)(i + 1);

		XMFLOAT4 eCol = ColorForEnvelope(CurrentDrawingEnvelope->assignTo);
		Color = eCol;
		StandartDrawer();

		EnvelopePosY = ChannelPosY - ChannelH;

		if (GetElementOpenStatus(i) && !HiddenFlag)
		{
			EnvelopeH = EnvelopeHeight +elementHeight*.5;
			ClipEnvelopeCount++;
		}

		pointCounter = 0;

		if (SelectedEnv == eIn->assignTo && CurrentPos == prevLevelCmdPtr[CmdDesc[*i].Level] - stack::data)
		{
			ui::SetElementOpenStatus(i, 1 - ui::GetElementOpenStatus(i));
			ui::SelectedEnv = -1;
		}

		//target name
		float toofs = dxui::StringLen(.05f, CmdDesc[cmd].Name);
		

		if (Open&&!HiddenFlag)
		{
			auto x = eIn->x;
			auto y = eIn->y;

			int eMin = eIn->min;
			EnvelopeMin = (float)eMin;
			int eMax = eIn->max;

			if (eIn->loop)
			{
				EnvelopeLoopStartIndex = eIn->loopStart;
				EnvelopeLoopEndIndex = eIn->loopEnd;
			}
			else
			{
				EnvelopeLoopStartIndex = 0;
				EnvelopeLoopEndIndex = 0;
			}

			EnvelopeRange = (float)(eMax - eMin);
			auto link = eIn->link;
			EnvelopeLink = link;

			float offset = ClipPosX_untransformed;

			by = EnvelopePosY + ((y - EnvelopeMin) / EnvelopeRange)*EnvelopeHeight - EnvelopeHeight;

			if (EnvelopeLink == 1)
			{
				bx = MusicTimelineLeftMargin + SampleToScreenNoOffset(x);
			}
			else
			{
				bx = SampleToScreen(x);
			}

			bw = bh = EnvelopePointSize;

			float posX = (float)timelinePos.x;
			float StartPos = max(posX, MusicTimelineLeftMargin);
			float boxw = TimeToScreenS16(9 * 60 * 60) - StartPos;

			

			bx -= bw / 2.f;
			by += bw / 2.f;

			float zeroY = EnvelopePosY + ((0 - eMin) / EnvelopeRange)*EnvelopeHeight - EnvelopeHeight;

			if (sign(eMin*eMax) < 0)
			{
				dxui::Box(MusicTimelineLeftMargin, zeroY, 4, .003, XMFLOAT4(0.62, 0.62, 0.62, .41), .001, false, false);
			}
			dxui::Box(MusicTimelineLeftMargin, EnvelopePosY, 4, .003, XMFLOAT4(0.62, 0.62, 0.62, .41), .001, false, false);
			dxui::Box(MusicTimelineLeftMargin, EnvelopePosY - EnvelopeHeight, 4, .003, XMFLOAT4(0.62, 0.62, 0.62, .41), .001, false, false);

			if (bx + bw/2. >= MusicTimelineLeftMargin)
			{
				dxui::Box(bx, by, bw, bh, isSelected ? Color : eCol, .1, false, true, i, -1, ClickEnv);
			}
				
			prevPointX = bx;
			prevPointY = by;
			prevPointPtr = i;
			prevPointSrcX = eIn->x;
			prevPointSrcY = eIn->y;
		}
	}

	int point3dCounter = 0;

	void CurveDrawer()
	{
		StandartDrawer();
		point3dCounter = 0;
	}

	void Point3dDrawer()
	{
		StandartDrawer();

		Commands::Point3d_* in = (Commands::Point3d_*)(i + 1);
		float d = 2500.;
		float pts = .5*elementHeight;
		bx = in->x / d;
		by = in->y / d;
		bw = pts;
		bh = pts;

		XMVECTOR p = XMVectorSet(bx, by, in->z / d, 1);
		p = XMVector4Transform(p, dx::View2*dx::Projection);

		bx = XMVectorGetX(p)/dx::aspect;
		by = XMVectorGetY(p);

		dxui::Box(bx, by, bw, bh, Color, .1, false, true, i);

		point3dCounter++;
	}

	point2d currentLinePoint;
	BYTE* insertPointPtr = NULL;

	bool isOnline(point2d t, point2d p1, point2d p2)
	{
		double a = p2.y - p1.y;
		double b = p1.x - p2.x;
		double c = -a * p1.x - b * p1.y;
		double eps = .01*sqrt(a*a + b * b);
		if (fabs(a * t.x + b * t.y + c) > eps) return false;

		if (t.x<fmin(p1.x, p2.x)-.01 || t.x > .01+fmax(p1.x, p2.x)) return false;
		//if (t.y<fmin(p1.y, p2.y) || t.y > .01+ fmax(p1.y, p2.y)) return false;

		return true;
	}

	void CoordsWidget(float x, float y, BYTE* i)
	{
		x += letterWidth;
		y += elementHeight;
		Point_* in = (Point_*)(i + 1);

		XMFLOAT4 boxColor = XMFLOAT4(.2, .2, .2, 1);
		XMFLOAT4 selColor = XMFLOAT4(1, 1, 1, 1);
		char value[11];

		if (ui::selectedElementPtr == i)
		{
			bool selStatus = ui::selectedParamID == 0;
			dxui::Box(x, y, letterWidth * 2.9, elementHeight*.9, selStatus ? selColor : boxColor, 0.1,
				false, true, i, 0, ClickPointWidget);
			_itoa(in->x, value, 10);
			if (selStatus) strcpy(value, ui::editBoxText);
			dxui::String(x + .005, y, letterWidth, elementHeight*.9, selStatus ? boxColor : selColor, value);

			selStatus = ui::selectedParamID == 1;
			dxui::Box(x, y - elementHeight, letterWidth * 2.9, elementHeight*.9, selStatus ? selColor : boxColor, 0.1,
				false, true, i, 1, ClickPointWidget);
			_itoa(in->y, value, 10);
			if (selStatus) strcpy(value, ui::editBoxText);
			dxui::String(x + .005, y - elementHeight, letterWidth, elementHeight*.9, selStatus ? boxColor : selColor, value);
		}

		if (i == ui::selectedElementPtr && 0 <= selectedParamID && selectedParamID <=1)
		{
			dxui::Box(x + .005 + textCursorX, y - elementHeight* selectedParamID, .003, ui::elementHeight*.8, XMFLOAT4(0, 0, 0, .5 + .5*sin(.01*timer::GetCounter())), 0.001, NULL, -1, false);
		}
	}

	void NumField(int id, float x, float y, int v)
	{
		XMFLOAT4 boxColor = XMFLOAT4(.2, .2, .2, 1);
		XMFLOAT4 selColor = XMFLOAT4(1, 1, 1, 1);
		char value[11];
		bool selStatus = ui::selectedParamID == id;
		float m = elementHeight * .1;
		dxui::String(x, y, letterWidth, elementHeight*.9, selColor, GetParamName(i, id));
		x += .15;
		dxui::Box(x, y, letterWidth * 3.5, elementHeight+m, selStatus ? selColor : boxColor, 0.1,
			false, true, i, id, ClickNum);
		_itoa(v, value, 10);
		if (selStatus) strcpy(value, ui::editBoxText);
		dxui::String(x + .005, y, letterWidth, elementHeight*.9, selStatus ? boxColor : selColor, value);
	}

	void ENumField(int id, float x, float y, int v)
	{
		XMFLOAT4 boxColor = XMFLOAT4(.2, .2, .2, 1);
		XMFLOAT4 selColor = XMFLOAT4(1, 1, 1, 1);
		bool selStatus = ui::selectedParamID == id;
		float m = elementHeight * .1;
		dxui::String(x, y, letterWidth, elementHeight*.9, selColor, GetParamName(i, id));
		x += .15;
		dxui::Box(x, y, letterWidth * 3.5, elementHeight+m, selStatus ? selColor : boxColor, 0.1,
			false, true, i, id, ClickSendToEnv);
		char* s = (char*)CmdDesc[*i].ParamEnum[id][v];
		
		dxui::String(x + .005, y, letterWidth, elementHeight*.9, selStatus ? boxColor : selColor, s);
	}


	void SendToEnvWidget(float x, float y, BYTE* i)
	{
		if (!(dxui::LastClickedRoutine == ClickSelectEnv || 
			dxui::LastClickedRoutine == ClickSendToEnv ||
			dxui::LastClickedRoutine == ClickNum ||
			dxui::LastClickedRoutine == ClickParamContextMenu
			)) return;

		x = ui::MusicTimelineLeftMargin;
		y = ui::fParamPos.y-ui::SelectedEnv*lineStep;

		SendToEnv_* in = (SendToEnv_*)(i + 1);

		XMFLOAT4 boxColor = XMFLOAT4(.2, .2, .2, 1);
		XMFLOAT4 selColor = XMFLOAT4(1, 1, 1, 1);

		if (ui::SelectedEnv == in->assignTo)
		{
			ENumField(0, x, y, in->source);
			y -= lineStep;
			NumField(1, x, y, in->inID);
			y -= lineStep;
			ENumField(3, x, y, in->assignOp);
			y -= lineStep;
			NumField(4, x, y, in->range);
			y -= lineStep;
			NumField(5, x, y, in->offset);
		}

		if (dxui::LastClickedRoutine == ClickSendToEnv)
		{
			int pID = ui::selectedParamID;
			y -= pID * lineStep;

			int t = 0;
			BYTE cmd = *i;

			x += .3;
			y = ui::fParamPos.y - ui::SelectedEnv*lineStep;
			float m = elementHeight * .1;

			while (*(CmdDesc[cmd].ParamEnum[pID][t]) != 0)
			{
				XMFLOAT4 boxColor = menuBoxColor;
				XMFLOAT4 textColor = menuTextColor;

				dxui::Box(x, y - t * lineStep, .12, elementHeight+m, boxColor, .1, true, true, i, t, ClickParamContextMenu);
				dxui::String(x, y - t * lineStep, letterWidth, elementHeight*.9, textColor, CmdDesc[cmd].ParamEnum[pID][t]);
				t++;				
			}
		}


		if (i == ui::selectedElementPtr && 
			(1 == selectedParamID ||
			 4 == selectedParamID ||
			 5 == selectedParamID ))
		{
			dxui::Box(x + .005 + textCursorX, y - elementHeight * selectedParamID, .003, ui::elementHeight*.8, XMFLOAT4(0, 0, 0, .5 + .5*sin(.01*timer::GetCounter())), 0.001, NULL, -1, false);
		}
	}

	void PointDrawer()
	{
		StandartDrawer();

		int a = CurrentDrawingEnvelope->assignTo;
		XMFLOAT4 eCol = ColorForEnvelope(a);

		float noteSize = SAMPLERATE * 60.f / float(Commands::MasterBPM*CurrentDrawingClip->bpmScale);

		Commands::Point_* pIn = (Commands::Point_*)(i + 1);
		XMFLOAT4 c = Color;
		
		if (!HiddenFlag) 
		{
		auto x = pIn->x;
		auto y = pIn->y;

		if (EnvelopeLink == 1)
		{
			bx = MusicTimelineLeftMargin + SampleToScreenNoOffset(x);
		}
		else
		{
			bx = SampleToScreen(x);
		}

		by = EnvelopePosY + ((y - EnvelopeMin) / EnvelopeRange)*EnvelopeHeight - EnvelopeHeight;
		bw = bh = EnvelopePointSize;

		bx -= bw / 2.f;
		by += bw / 2.f;

		if (bx +bw/2. >= MusicTimelineLeftMargin)
		{
			dxui::lineVertices[0].Pos.x = bx + bw / 2;
			dxui::lineVertices[0].Pos.y = by - bw / 2;
			dxui::lineVertices[0].Pos.z = 0;

			dxui::lineVertices[1].Pos.x = prevPointX + bw / 2;
			dxui::lineVertices[1].Pos.y = prevPointY - bw / 2;
			dxui::lineVertices[1].Pos.z = 0;

			if (prevPointX + bw / 2 < MusicTimelineLeftMargin)
			{
				float x1 = prevPointX + bw / 2.f;
				float y1 = prevPointY - bw / 2.f;

				float x2 = bx + bw / 2;
				float y2 = by - bw / 2;

				float deltaX = x2 - x1;
				float deltaY = y2 - y1;
				float coefR = deltaY / deltaX;

				float left = MusicTimelineLeftMargin - x1;

				dxui::lineVertices[1].Pos.x = MusicTimelineLeftMargin;
				dxui::lineVertices[1].Pos.y += left * coefR;
			}

			pIn->y = clamp_int(pIn->y, CurrentDrawingEnvelope->min, CurrentDrawingEnvelope->max);

				
			dxui::lineVBSet();
			dxui::SetupDrawerline();
			dxui::UpdateLineVb(2 * 2);

			float mx = ((2.*dxui::mousePos.x) / dx::width - 1) / dx::aspect;
			float my = -((2.*dxui::mousePos.y) / dx::height - 1);
			point2d t; t.x = mx; t.y = my;
			point2d p1; p1.x = dxui::lineVertices[0].Pos.x; p1.y = dxui::lineVertices[0].Pos.y;
			point2d p2; p2.x = dxui::lineVertices[1].Pos.x; p2.y = dxui::lineVertices[1].Pos.y;

			bool is = isOnline(t, p1, p2);
			if (is&&!dxui::mousePtr|| is&&dxui::mousePtr&&CmdDesc[*dxui::mousePtr].Routine!=Point&& CmdDesc[*dxui::mousePtr].Routine != Envelope ) 
			{
				float cx = (t.x-min(p1.x,p2.x)) / (fabs(p2.x - p1.x)+.001);
				float cy = (t.y - min(p1.y, p2.y)) / (fabs(p2.y - p1.y) + .001);
				if (p1.x < p2.x) cx = 1 - cx;
				if (p1.y < p2.y) cy = 1 - cy;
				currentLinePoint.x= lerp(prevPointSrcX,pIn->x,cx);
				currentLinePoint.y = lerp(prevPointSrcY, pIn->y, cy);
				insertPointPtr = prevPointPtr;
			}

			dxui::Line(0.0, 0.f, 1.f, 1.f, is ? XMFLOAT4(.7,.7,.7,1) : eCol, 1);

			if (EnvelopeLoopEndIndex != 0)
			{
				if (EnvelopeLoopStartIndex == pointCounter+1 || EnvelopeLoopEndIndex == pointCounter + 1)
				{
					dxui::lineVertices[0].Pos.x = bx + bw / 2;
					dxui::lineVertices[0].Pos.y = EnvelopePosY;
					dxui::lineVertices[0].Pos.z = 0;

					dxui::lineVertices[1].Pos.x = bx + bw / 2;
					dxui::lineVertices[1].Pos.y = EnvelopePosY - EnvelopeHeight;
					dxui::lineVertices[1].Pos.z = 0;
					dxui::UpdateLineVb(2 * 2);
					dxui::Line(0.0, 0.f, 1.f, 1.f, is ? XMFLOAT4(.7, .7, .7, 1) : eCol, 1);

				}

			}

			dxui::SetupDrawerBox();

			dxui::Box(bx, by, bw, bh, isSelected ? Color : eCol, .25f, false, true, i, -1, ClickPoint);
			CoordsWidget(bx,by,i);

			}

			prevPointX = bx;
			prevPointY = by;
			prevPointPtr = i;
			prevPointSrcX = pIn->x;
			prevPointSrcY = pIn->y;
		}

		pointCounter++;
	}

	void NoteDrawer()
	{
	
	}

	float trackStartY = 0;
	float trackEndY = 0;

	void TrackDrawer()
	{
		if (!HiddenFlag) TrackOpen = *(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode);
		
		StandartDrawer();
		int ptc = 0;
		float gridH = .9;
		if (!GetElementOpenStatus(i)) return;

		if (MasterBPM > 0 && timelineScale / float(MasterBPM) > .5f / 120.f)
		{
			double step = 4*60.f*60.f / float(MasterBPM);

			float left = ScreenToTimeS16(MusicTimelineLeftMargin) + step;
			left = max(left, 0.f);
			left = step * int(left / step);

			float right = 9. * 60. * 60.;
			right = min(ScreenToTimeS16(-MusicTimelineLeftMargin * 2.f), right);

			//minor
			int t = 0;
			for (float x = left; x < right; x += step)
			{
				XMFLOAT4 c = colorScheme.notestrong;
				float f = frac(x / (step * 4.f));
				c.w = 1.f - f * .85f;
				c.w *= .8f;
				if (f < .1f) c.y *= 1.5f;

				if (timelineScale / (MasterBPM) > 10.f / 120.f)
				{
					float xp = TimeToScreenS16(x) - .0025f*.5f;

					dxui::lineVertices[ptc].Pos.x = xp;
					dxui::lineVertices[ptc].Pos.y = gridH;
					dxui::lineVertices[ptc].Pos.z = 0.0f;
					ptc++;
					dxui::lineVertices[ptc].Pos.x = xp + .0001;
					dxui::lineVertices[ptc].Pos.y = -gridH;
					dxui::lineVertices[ptc].Pos.z = 0.0f;
					ptc++;

				}

				t++;
			}

			dxui::lineVBSet();
			dxui::SetupDrawerline();
			dxui::UpdateLineVb(ptc);

			dxui::Line(0.0, 0.f, 1, 1.f, XMFLOAT4(1,1,1,.4), ptc/2);

			//major
			t = 0; ptc = 0;
			int cn = 0;
			for (float xx = left; xx < right; xx += step)
			{
				double x = step * cn;
				cn++;
				XMFLOAT4 c = colorScheme.notestrong;
				float f = frac(x / (step * 4.f));
				c.w = 1.f - f * .85f;
				c.w *= .8f;
				if (f < .1f) c.y *= 1.5f;

				if (f < .1)
				{
					float xp = TimeToScreenS16(x) - .0025f*.5f;

					dxui::lineVertices[ptc].Pos.x = xp;;
					dxui::lineVertices[ptc].Pos.y = gridH;
					dxui::lineVertices[ptc].Pos.z = 0.0f;
					ptc++;
					dxui::lineVertices[ptc].Pos.x = xp + .0001;;
					dxui::lineVertices[ptc].Pos.y = -gridH;
					dxui::lineVertices[ptc].Pos.z = 0.0f;
					ptc++;

				}

				t++;
			}

			dxui::lineVBSet();
			dxui::SetupDrawerline();
			dxui::UpdateLineVb(ptc);

			dxui::Line(0.0, 0.f, 1, 1.f, XMFLOAT4(.7, .4, .2, 1), ptc / 2);



			/*				if (timelineScale / MasterBPM > 10.f / 120.f)
				{
					float xp = TimeToScreenS16(x) - .0025f*.5f;

					dxui::lineVertices[ptc].Pos.x = xp;
					dxui::lineVertices[ptc].Pos.y = 1;
					dxui::lineVertices[ptc].Pos.z = 0.0f;
					ptc++;
					dxui::lineVertices[ptc].Pos.x = xp+.0001;
					dxui::lineVertices[ptc].Pos.y = -1;
					dxui::lineVertices[ptc].Pos.z = 0.0f;
					ptc++;

				}
							else
				{

				}
*/
			dxui::SetupDrawerBox();

		}
		//focusedChannel = -1;
	}

	
	void MasterChannelDrawer()
	{
		teY -= teYSize;

		teYSize = stepY * 2.;

		ChannelPosY -= ChannelH + EnvelopeH;
		ty = min(ChannelPosY, ty);
		ChannelPosY = min(ty, ChannelPosY);

		bool ext = true;

		Commands::MasterChannel_* in = (Commands::MasterChannel_*)(i + 1);
		XMFLOAT4 col = XMFLOAT4(.2, .2, .2, 2);
		XMFLOAT4 tex = XMFLOAT4(1, 1, 1, 1);
		XMFLOAT4 off = XMFLOAT4(0, 0, 0, 1);
		XMFLOAT4 on = XMFLOAT4(1, .5, 0, 1);

		float h = elementHeight * 2.1f;
		float y = ChannelPosY;
		bx -= .05;
		dxui::Box(bx - letterWidth / 2. - .01, y, ext ? .3 : .16, teYSize - .005, isSelected ? XMFLOAT4(.35, .35, .35, 1) : col, 0.07f, false, true, i);
		y -= .01;

		y -= .005;
		y += .01;

		if (ext)
		{
			float eh = h - .01;

			//volume
			float vol = in->preAmp / 127.+.5;
			dxui::Box(bx + .18, y - .005, .03, eh, XMFLOAT4(0.1, 0.1, 0.1, 1), 0.015f, false, false, i, 0, ClickSlider, XMFLOAT4(0, vol, 0, 0), vol >= .5 ? XMFLOAT4(.5, .25, 0, 1) : XMFLOAT4(0, .3, .6, 1));

			//volumemeter
			float vl = volumeL[CurrentDrawingChannel];
			float vr = volumeR[CurrentDrawingChannel];
			XMFLOAT4 peakl = XMFLOAT4(1, 1, 0, 1);
			XMFLOAT4 peakr = XMFLOAT4(1, 1, 0, 1);

			dxui::Box(bx + .1825, y - eh + vl * eh, .01, vl * eh, peakl, 0.01f, false, false);
			dxui::Box(bx + .1825 + .015, y - eh + vr * eh, .01, vr * eh, peakr, 0.01f, false, false);

			//helpers
			dxui::String(bx + .18 + .006, y, letterWidth*.75, elementHeight*.75, XMFLOAT4(.5, .5, .5, 1), "V");

			dxui::String(bx -.01, y-elementHeight*.5, letterWidth*.9, elementHeight*.9, TextColor, "MasterChannel");

		}

		tx = -1.74f + 3*LevelStepRight;
		ContextInsertCmd();

//----------------------

		ty -= ChannelH;

		int outchannel = MAXCHANNELS - 1;
		float amp = 1.f / 10.f;
		float ofs = .015f;
		float width = dx::iaspect*2.f;

		float waveformHeight = amp*2.f;

		teY -= teYSize;

		float cx = MusicTimelineLeftMargin - dxui::StringLen(elementHeight, CmdDesc[cmd].Name)*1.15f - elementHeight * 2.5f;
		cx = MusicTimelineLeftMargin - cx;

		XMFLOAT4 channelColor = XMFLOAT4(.2f, .2f, .2f, .5f*(.5f+.5f*isSelected));

		by = ChannelPosY;
		bw = MusicTimelineLeftMargin - bx;
		bh = elementHeight*2.1f;

		if (Open) bh = waveformHeight*2.f;

		CurrentDrawingChannel = MAXCHANNELS - 1;

		float bh_ = bh;// *.95f;

		if (!Open) return;

		ChannelPosY = -1. + 2.*(dx::height - dx::winH) / (float)(dx::height) + .01 + waveformHeight * 2. + elementHeight*4.;

		int StartSample = 0;
		float StartPos = 0;

		float posX = (float)timelinePos.x;
		StartPos = max(posX, MusicTimelineLeftMargin);

		dxui::SetupDrawerBox();
		float boxw = width;
		boxw = TimeToScreenS16(9 * 60 * 60) - StartPos;

		float ySpace = elementHeight * .5;

		dxui::Box(StartPos, ChannelPosY , boxw, 2.f * amp, colorScheme.masterwave, .001f,false,false);
		dxui::Box(StartPos, ChannelPosY  - waveformHeight-ySpace, boxw, 2.f * amp, colorScheme.masterwave, .001f, false, false);

		float tf = (timelineScale - minTimelineScale) / (maxTimelineScale - minTimelineScale);

		float WaveFormQuality = max(8.f - tf * 7.f,.1f);

		int points = (int)(734 * WaveFormQuality);

		float WaveformScale = (width * 100.f * 60.f / timelineScale) / WaveFormQuality;

		StartSample =(int)( -min(posX - MusicTimelineLeftMargin, 0.f) * 60.f * 100.f * 735.f / timelineScale);

		StartSample =(int)(WaveformScale * int(StartSample / WaveformScale));

		StartSample *= 2 * 2;

		int _framelen = (int)(WaveFormQuality * 2 * 2 * 44100 / 60);
		float wc = 1.f;

		int lod = 1;

		for (int w = 0; w < points; w++)
		{
			int leftOfs = StartSample + (int(WaveformScale*float(w))) * 2 * 2;
			int rightOfs = StartSample + (int(WaveformScale*float(w)+ WaveformScale)) * 2 * 2;

			leftOfs = clamp_int(leftOfs, 0, sound::len);
			rightOfs = clamp_int(rightOfs, 0, sound::len);
			signed short smp1 = *(short*)(sound::channel[outchannel] +leftOfs);
			signed short smp2 = *(short*)(sound::channel[outchannel] + rightOfs);

			dxui::lineVertices[w * 2].Pos.x = width * (w / float(points - 1) ) + StartPos;
			dxui::lineVertices[w * 2].Pos.y = smp1 / 32767.0f * amp*wc + ChannelPosY- waveformHeight*.5f;
			dxui::lineVertices[w * 2].Pos.z = 0.0f;

			dxui::lineVertices[w * 2 + 1].Pos.x = width * ((w + 1) / float(points - 1) ) + StartPos;
			dxui::lineVertices[w * 2 + 1].Pos.y = smp2 / 32767.0f * amp*wc + ChannelPosY- waveformHeight*.5f;
			dxui::lineVertices[w * 2 + 1].Pos.z = 0.0f;
		}

		for (int w = 0; w < points; w++)
		{
			int leftOfs = StartSample + (int(WaveformScale*float(w))) * 2 * 2;
			int rightOfs = StartSample + (int(WaveformScale*float(w) + WaveformScale)) * 2 * 2;
			leftOfs = clamp_int(leftOfs, 0, sound::len);
			rightOfs = clamp_int(rightOfs, 0, sound::len);
			signed short smp1 = *(short*)(sound::channel[outchannel] + leftOfs+2);
			signed short smp2 = *(short*)(sound::channel[outchannel] + rightOfs+2);

			dxui::lineVertices[w * 2 + points * 2].Pos.x = width * (w / float(points - 1)) + StartPos;
			dxui::lineVertices[w * 2 + points * 2].Pos.y = smp1 / 32767.0f * amp*wc + ChannelPosY- waveformHeight*1.5f - ySpace;
			dxui::lineVertices[w * 2 + points * 2].Pos.z = 0.0f;

			dxui::lineVertices[w * 2 + 1 + points * 2].Pos.x = width * ((w + 1) / float(points - 1)) + StartPos;
			dxui::lineVertices[w * 2 + 1 + points * 2].Pos.y = smp2 / 32767.0f * amp*wc + ChannelPosY- waveformHeight*1.5f - ySpace;
			dxui::lineVertices[w * 2 + 1 + points * 2].Pos.z = 0.0f;
		}

		dxui::lineVBSet();
		dxui::SetupDrawerline();
		dxui::UpdateLineVb(points * 2);

		dxui::Line(0.0, 0.f, 1, 1.f, colorScheme.masterwaveline, points * 2);

		dxui::SetupDrawerBox();
	}

	void ShowSceneDrawer()
	{
		if (HiddenFlag) return;

		XMFLOAT2 pos = StandartDrawer();

		if (!isSelected)
		{
			char* Label = GetResourceName(*(BYTE*)(i + CmdDesc[cmd].ParamOffset[0]), Scene, "Name");
			dxui::String(pos.x, pos.y - .004, letterWidth*.9, elementHeight*.8, colorScheme.font, Label);
		}

		//show called scene
		/*char* Label = GetResourceName(*(BYTE*)(i + CmdDesc[cmd].ParamOffset[0]), Scene, "Name");
		//dxui::String(tx + w * 2.3f, ty, elementHeight, elementHeight, TextColor, Label);

		Commands::ShowScene_* in = (Commands::ShowScene_*)(i + 1);

		if (in->timeline)
		{
			bx = ui::TimeToScreenS16(in->x);
			bw = ui::WidthToScreenS16(in->length);
			dxui::Box(bx, by, bw, elementHeight, Color);
			//dxui::String(bx, by, elementHeight, elementHeight, TextColor, CmdDesc[cmd].Name);
		}*/

		ContextInsertCmd();

		//ty -= stepY;
	}

	void CamDrawer()
	{
		if (HiddenFlag)
		{
			return;
		}

		dxui::Box(tx, ty, w*2.2f, elementHeight, Color);
		dxui::String(tx, ty, elementHeight, elementHeight, TextColor, CmdDesc[cmd].Name);

		//show called scene
		dxui::String(tx + w * 2.3f, ty, elementHeight, elementHeight, TextColor, "Cam");

		Commands::CameraYPR_* in = (Commands::CameraYPR_*)(i + 1);

			bx = ui::TimeToScreenS16(in->x);
			bw = elementHeight*6;
			dxui::Box(bx, by, bw, elementHeight, Color);
			//dxui::String(bx, by, elementHeight, elementHeight, TextColor, CmdDesc[cmd].Name);

			ContextInsertCmd();

		ty -= stepY;
	}

	//

	enum {
		LB_DOWN,
		LB_UP,
		LB_DBLCLICK
	};

	void StandartTimelineElementClick()
	{

	}
	//
	BYTE* FindCommandInStack(BYTE* ptr,PVFN routine)
	{
		while (*ptr != 0)
		{
			if (CmdDesc[*ptr].Routine == routine)
			{
				return ptr;
			}
			ptr += CmdDesc[*ptr].Size;
		}

		return NULL;
	}

	int SearchCmdByRoutine(PVFN r)
	{
		for (int i = 0;i<stack::CommandsCount;i++)
		{
			if (r == CmdDesc[i].Routine)
			{
				return i;
			}
		}

		return -1;
	}

	void RegisterDrawer(int i, PVFN r)
	{
		if (i>=0) CmdDesc[i].Draw = r;
	}

	void RegisterParamDrawer(int i, PVFN r)
	{
		if (i >= 0) CmdDesc[i].ParamDraw = r;
	}


	void RegisterClick(int i, PVFN r)
	{
		if (i >= 0) CmdDesc[i].Click = r;
	}

	void EmptyParamDrawer()
	{}

	float ePos = 0;
	
	bool scroll = false;
	bool camerafly = false;

	void ClickMove()
	{
		ui::removeSelection();
		ui::Select(0);
		
		//open parents
		BYTE* ptrSelected = selectedElementPtr;
		BYTE* ptr = stack::data;
		BYTE* ptrParent = NULL;
		BYTE* ptrGrandParent = NULL;

		while (*ptr != 0 && ptr < ptrSelected)
		{
			if (CmdDesc[*ptr].Routine == Scene)
			{
				ptrGrandParent = ptr;
			}

			if (CmdDesc[*ptr].Routine == NullDrawer)
			{
				ptrParent = ptr;
			}
			ptr += CmdDesc[*ptr].Size;
		}
			
		if (ptrParent&&ptrGrandParent)
		{
			ui::SetElementOpenStatus(ptrParent, 1);
			ui::SetElementOpenStatus(ptrGrandParent, 1);
		}
		


		scroll = true;
		camerafly = true;
	}
	
	BYTE* lastNullDrawer = NULL;

	void TransformDrawer()
	{
		Transform_* in = (Transform_*)(i + 1);

			float mx = ((2. * dxui::mousePos.x) / dx::winW - 1) / dx::aspect;
			float my = -((2. * dxui::mousePos.y) / dx::winH - 1);
			float d = fmin(fabs(mx - in->pX), fabs(my - in->pY));
			d = clamp(1 - d * 10, 0, 1);
			dxui::Box(in->pX, in->pY, elementHeight * .7, elementHeight * .7, XMFLOAT4(.5, .5, .5, d), .24, false, true, i, -1, ClickMove);

			if (isSelected)
			{
				float aOfs = elementHeight;
				dxui::Box(in->pX + aOfs, in->pY, elementHeight * .7, elementHeight * .7, XMFLOAT4(.5, 0, 0, 1), .24, false, true, i, 0, ClickSlider);
				dxui::String(in->pX + aOfs + letterWidth * .2, in->pY + letterWidth * .2, letterWidth * .8, elementHeight * .89, XMFLOAT4(1, 1, 1, 1), "x");

				dxui::Box(in->pX, in->pY + aOfs, elementHeight * .7, elementHeight * .7, XMFLOAT4(0, .5, 0, 1), .24, false, true, i, 1, ClickSlider);
				dxui::String(in->pX + letterWidth * .2, in->pY + aOfs + letterWidth * .2, letterWidth * .8, elementHeight * .89, XMFLOAT4(1, 1, 1, 1), "y");

				dxui::Box(in->pX + aOfs, in->pY + aOfs, elementHeight * .7, elementHeight * .7, XMFLOAT4(0, 0, .5, 1), .24, false, true, i, 2, ClickSlider);
				dxui::String(in->pX + aOfs + letterWidth * .2, in->pY + aOfs + letterWidth * .2, letterWidth * .8, elementHeight * .89, XMFLOAT4(1, 1, 1, 1), "z");

				ePos = ty;

				if (scroll)
				{
					ui::viewPos.y = lerp(ui::viewPos.y, ui::viewPos.y - ePos, .1);
					if (fabs(ePos) < elementHeight * .1) scroll = false;
				}

				//TODO:: doesnt't work((((
				/*
				if (camerafly)
				{
					XMVECTOR s, q, t;
					XMMATRIX aView = dx::View2;
					XMMATRIX pofs = XMMatrixTranslation(0, 0, -1);
					//aView = XMMatrixMultiply(aView, pofs);

					XMMatrixDecompose(&s, &q, &t, aView);
					XMVECTOR newT = XMVectorSet(-in->x / 1000., -in->y / 1000., -in->z / 1000., 0);
					newT = t-newT;
					XMMATRIX tm = XMMatrixTranslationFromVector(newT);
					dx::View2 = XMMatrixMultiply(dx::View2 , tm);

					XMMATRIX ofs = XMMatrixTranslation(0, 0, 1);
					//dx::View2 = XMMatrixMultiply(dx::View2, ofs);
					
				}*/
			}


			//

			//search geometry

			char* gName = NULL;
			BYTE* ptr = lastNullDrawer + CmdDesc[*lastNullDrawer].Size;
			while (*ptr != 0 && CmdDesc[*ptr].Level > CmdDesc[*lastNullDrawer].Level)
			{
				if (CmdDesc[*ptr].Routine == Commands::SetTexture)
				{
					SetTexture_* tIn = (SetTexture_*)(ptr + 1);
					if (tIn->slot == 0)
					{
						int t = tIn->texture;
						gName = resourceCreationName[1][t];
						dxui::String(in->pX, in->pY - elementHeight, letterWidth * .8, elementHeight * .89, isSelected ? XMFLOAT4(1, 1, 1, 1) : XMFLOAT4(1, 1, 1, .5), gName);
					}
				}
				ptr += CmdDesc[*ptr].Size;
			}
		
		//
			StandartDrawer();
	}

	void NullDrawerDrawer()
	{
		lastNullDrawer = i;

		Commands::NullDrawer_* in = (Commands::NullDrawer_*)(i + 1);
		int id = in->shader;
		char* sn = (char*)Commands::resourceCreationPtr[0][id];
		if (!sn) strcpy(sn, "resCrPtrErr");
		
		w= dxui::StringLen(letterWidth, sn);

		//lod info
		if (!HiddenFlag && (isSelected|| GetElementOpenStatus(i)) &&in->currentGX>0 && in->currentGY > 0 )
		{
			char lod[40];
			char x[8];
			_itoa(in->currentGX, x, 10);
			strcpy(lod, x);
			strcat(lod, "x");
			_itoa(in->currentGY, x, 10);
			strcat(lod, x);
			dxui::String(tx + w + letterWidth, ty, letterWidth*.8, elementHeight * .89, XMFLOAT4(1, 1, 1, 1), lod);
		}
		//

		

		StandartDrawer(sn);

	}

	void TextureDrawer()
	{
		Commands::CreateTexture_* in = (Commands::CreateTexture_*)(i + 1);
		w = dxui::StringLen(letterWidth, "T: ");
		StandartDrawer("T: ");


	}

	void ShaderDrawer()
	{
		Commands::CreateShader_* in = (Commands::CreateShader_*)(i + 1);
		w = dxui::StringLen(letterWidth, "S: ");
		StandartDrawer("S: ");
		

	}
		

	void RegisterSpecificDrawers()
	{
		//return;
		RegisterDrawer(SearchCmdByRoutine(ShowScene),ShowSceneDrawer);
		//RegisterDrawer(SearchCmdByRoutine(CameraYPR), CamDrawer);
		RegisterDrawer(SearchCmdByRoutine(Channel), ChannelDrawer);
		RegisterDrawer(SearchCmdByRoutine(Clip), ClipDrawer);
		RegisterDrawer(SearchCmdByRoutine(Note), NoteDrawer);
		RegisterDrawer(SearchCmdByRoutine(Point), PointDrawer);
		RegisterDrawer(SearchCmdByRoutine(Envelope), EnvelopeDrawer);
		RegisterDrawer(SearchCmdByRoutine(MasterChannel), MasterChannelDrawer);
		RegisterDrawer(SearchCmdByRoutine(Music), TrackDrawer);
		RegisterDrawer(SearchCmdByRoutine(TimeLoop), TimeLoopDrawer);

		RegisterDrawer(SearchCmdByRoutine(Oscillator), SynthCommandDrawer);
		RegisterDrawer(SearchCmdByRoutine(Equalizer), SynthCommandDrawer);
		RegisterDrawer(SearchCmdByRoutine(Chorus), SynthCommandDrawer);
		RegisterDrawer(SearchCmdByRoutine(Crunch), SynthCommandDrawer);
		RegisterDrawer(SearchCmdByRoutine(Waveshaper), SynthCommandDrawer);
		RegisterDrawer(SearchCmdByRoutine(Return), SynthCommandDrawer);
		RegisterDrawer(SearchCmdByRoutine(SendToEnv), SomeToEnvDrawer);
		RegisterDrawer(SearchCmdByRoutine(DelayLine), SynthCommandDrawer);

		RegisterDrawer(SearchCmdByRoutine(Point3d), Point3dDrawer);
		RegisterDrawer(SearchCmdByRoutine(Curve), CurveDrawer);

		RegisterDrawer(SearchCmdByRoutine(NullDrawer), NullDrawerDrawer);
		RegisterDrawer(SearchCmdByRoutine(Transform), TransformDrawer);
		RegisterDrawer(SearchCmdByRoutine(CreateTexture), TextureDrawer);
		RegisterDrawer(SearchCmdByRoutine(CreateShader), ShaderDrawer);

		//param drawers
		RegisterParamDrawer(SearchCmdByRoutine(Channel), EmptyParamDrawer);
		RegisterParamDrawer(SearchCmdByRoutine(MasterChannel), EmptyParamDrawer);
		RegisterParamDrawer(SearchCmdByRoutine(Note), EmptyParamDrawer);
	}

	void RegisterClicks()
	{
		RegisterClick(SearchCmdByRoutine(ShowScene), StandartTimelineElementClick);
		//RegisterClick(SearchCmdByRoutine(CameraYPR), StandartTimelineElementClick);
		RegisterClick(SearchCmdByRoutine(Clip), StandartTimelineElementClick);
		RegisterClick(SearchCmdByRoutine(Point), StandartTimelineElementClick);
		RegisterClick(SearchCmdByRoutine(Envelope), StandartTimelineElementClick);

	}

	void init()
	{
		itemOldviewPos = new point2d[MAXDATASIZE];
		itemOldviewPos3 = new point3[MAXDATASIZE];
		stackItemOldviewPos = new point2d[MAXDATASIZE];

		DialogTemplate();

		initTextEditor();//changes template!

		dxui::LoadFont();
		dxui::SetTextureOptions();
		dxui::SetBlendStates();
		dxui::ShadersInit();

		dxui::CreateConstBuf();
		dxui::VBInit();

		dxui::CreateConstBufTL();
		dxui::TimelineVBInit();

		dxui::CreateConstBufL();
		dxui::lineVBInit();

		RegisterSpecificDrawers();
		RegisterClicks();

		viewPos.x = 3;
		timelinePos.x = ui::MusicTimelineLeftMargin+0.001;

	}

	int notePosX=0;
	float notePosY=0;
	int npm=0;
	float clipw = 0;
	int cliprep = 0;
	float envelopePosX = 0;
	float cDistance = 1;
	float cFov = 45;

	void CameraMovement()
	{

		POINT p;
		GetCursorPos(&p);
		ScreenToClient(hWnd, &p);

		RECT r;
		GetClientRect(hWnd, &r);
		float c = (r.bottom - r.top) / float(dx::winH);
		float c2 = (r.right - r.left) / float(dx::winW);
		p.y /= c;
		p.x /= c2;

		float x, y;
		x = p.x / float(dx::width) * 2 - 1;
		y = -(p.y / float(dx::height) * 2 - 1);
		x *= 1 / dx::aspect;
		
		Projection = XMMatrixPerspectiveFovLH(cFov*PI/180., width / (FLOAT)height, 0.01f, 100.0f);


		if ((isKeyDown(VK_CONTROL)|| isKeyDown(VK_SHIFT))&&(isKeyDown(VK_LBUTTON)||isKeyDown(VK_RBUTTON)))
		{

			if (ui::cameradragFlag == 0)
			{
				ui::cameradragFlag = 1;
				dx::tempView = dx::View2;
				
				if (isKeyDown(VK_LBUTTON)|| isKeyDown(VK_RBUTTON))
				{
					ui::oldcameraCursor.x = x;
					ui::oldcameraCursor.y = y;
				}
			}

			float xangle = 0;
			float yangle = 0;
			float zangle = 0;

			float xtrans = 0;
			float ytrans = 0;
			float ztrans = 0;
			

			if (isKeyDown(VK_CONTROL))
			{
				if (isKeyDown(VK_LBUTTON))
				{
					xangle = x - ui::oldcameraCursor.x;
					yangle = y - ui::oldcameraCursor.y;
				}

				if (isKeyDown(VK_RBUTTON)) zangle = x - ui::oldcameraCursor.x;
			}

			if (isKeyDown(VK_SHIFT))
			{
				//if (!isKeyDown('Z'))
				{
					if (isKeyDown(VK_LBUTTON))
					{
						xtrans = x - ui::oldcameraCursor.x;
						ytrans = y - ui::oldcameraCursor.y;
					}

				}
				//else
				{
					if (isKeyDown(VK_RBUTTON)) 
						ztrans = y - ui::oldcameraCursor.y;
				}
			}

				XMMATRIX rotationZ = XMMatrixRotationZ(zangle);
				XMMATRIX rotationY = XMMatrixRotationY(xangle);
				XMMATRIX rotationX = XMMatrixRotationX(yangle);
				XMMATRIX t = XMMatrixTranslation(0, 0, -cDistance);
				XMMATRIX t2 = XMMatrixTranslation(0, 0, cDistance);
				XMMATRIX trans = XMMatrixTranslation(xtrans, ytrans, ztrans);
				XMMATRIX temp = XMMatrixMultiply(dx::tempView, t);


				XMMATRIX temp2 = XMMatrixMultiply(temp, rotationZ);
				XMMATRIX temp3 = XMMatrixMultiply(temp2, rotationY);
				XMMATRIX temp4 = XMMatrixMultiply(temp3, rotationX);
				XMMATRIX temp5 = XMMatrixMultiply(temp4, trans);


			dx::View2 = XMMatrixMultiply(temp5, t2);

		}
		else
		{
			ui::cameradragFlag = 0;

		}

	}

	void StackMovement()
	{
		float tw = WidthToScreenS16(9*60*60);
		ui::timelinePos.x = min(ui::timelinePos.x, ui::MusicTimelineLeftMargin+.001);
		ui::timelinePos.x = max(ui::timelinePos.x, -tw);

		POINT p;
		GetCursorPos(&p);

		dxui::mousePos = p;
		ScreenToClient(hWnd, &dxui::mousePos);

		RECT r;
		GetClientRect(hWnd, &r);
		float c = (r.bottom - r.top) / float(dx::winH);
		float c2 = (r.right - r.left) / float(dx::winW);
		dxui::mousePos.y /= c;
		dxui::mousePos.x /= c2;

		float x, y;
		x = dxui::mousePos.x / float(dx::width) * 2 - 1;
		y = -(dxui::mousePos.y / float(dx::height) * 2 - 1);


		if (isKeyDown(VK_CONTROL)|| isKeyDown(VK_SHIFT)) return;

		if (isKeyDown(VK_LBUTTON)&& isKeyDown(VK_MENU))
		{
			Commands::StartPosition = -playmode * (timer::GetCounter() - timer::StartTime) / (1.f / 60.f * 1000.f)
				+ ScreenToTimeS16(x*dx::iaspect);

			if (Commands::playmode == 1)
			{
				Commands::StopWave();
				Commands::PlayWave((int)Commands::StartPosition);
				timer::StartTime = timer::GetCounter();
			}

			if (!ui::dragFlag)
			{
				float tlc = (float)((Commands::StartPosition)*ui::timelineScale) / (-60.f*100.f) - timelinePos.x;
				if (tlc < -1.9) timelinePos.x += tlc + 1.9;
				if (tlc > 1.9) timelinePos.x -= -tlc + 1.9;
			}
		}

		if (isKeyDown(VK_RBUTTON))
		{
			if (isKeyDown(VK_MENU))
			{
				
				if (ui::timelinedragFlag == 0)
				{
					ui::timelinedragFlag = 1;
					ui::oldtimelinePos.x = ui::timelinePos.x;
					ui::oldtimelinePos.y = ui::timelinePos.y;
					ui::oldtimelineCursor.x = x;
					ui::oldtimelineCursor.y = y;
				}
				ui::dragFlag = 0;
				ui::oldCursor.x = x;
				ui::oldCursor.y = y;
				ui::oldviewPos.x = ui::viewPos.x;
				ui::oldviewPos.y = ui::viewPos.y;

				ui::timelinePos.x = ui::oldtimelinePos.x + x - ui::oldtimelineCursor.x;
				ui::timelinePos.y = ui::oldtimelinePos.y + y - ui::oldtimelineCursor.y;
				
			}
			else
			{
				if (ui::dragFlag == 0)
				{
					ui::dragFlag = 1;
					ui::oldviewPos.x = ui::viewPos.x;
					ui::oldviewPos.y = ui::viewPos.y;
					ui::oldCursor.x = x;
					ui::oldCursor.y = y;
				}
				ui::timelinedragFlag = 1;
				ui::oldtimelineCursor.x = x;
				ui::oldtimelineCursor.y = y;
				ui::oldtimelinePos.x = ui::timelinePos.x;
				ui::oldtimelinePos.y = ui::timelinePos.y;

				ui::viewPos.x = ui::oldviewPos.x + x - ui::oldCursor.x;
				ui::viewPos.y = ui::oldviewPos.y + y - ui::oldCursor.y;

			}

		}
		else
		{
			ui::timelinedragFlag = 0;
			ui::dragFlag = 0;
		}
	}
	
	float currentEnvRange = 0;

	float p3dDx = 1.656 * 1.f / 10.f;
	float p3dDy = 3. * 1.f / 10.f;

	bool getItemCoords3d(BYTE* i, point3& p)
	{
		if (CmdDesc[*i].Routine == Commands::Point3d)
		{
			Commands::Point3d_* in = (Commands::Point3d_*)(i + 1);

			p.x = in->x;
			p.y = in->y;
			p.z = in->z;

			return true;
		}

	}

	bool getItemCoords(BYTE* i,POINT& p)
	{

		if (CmdDesc[*i].Routine == Commands::Point)
		{
			Commands::Point_* in = (Commands::Point_*)(i + 1);
			p.x = in->x;
			p.y = in->y;
			return true;
		}
		
		if (CmdDesc[*i].Routine == Commands::Envelope)
		{
			Commands::Envelope_* in = (Commands::Envelope_*)(i + 1);
			p.x = in->x;
			p.y = in->y;
			return true;
		}

		if (CmdDesc[*i].Routine == Commands::Clip)
		{
			Commands::Clip_* in = (Commands::Clip_*)(i + 1);
			if (dxui::cursorID == 0)
			{
				p.x = in->x;
			}

			if (dxui::cursorID == 1)
			{
				auto npm = Commands::MasterBPM;// *in->bpmScale;
				float width = in->length *60.f*60.f / float(MasterBPM*in->bpmScale);
				p.x = (LONG)(in->repeat*width*FRAMELENF);
			}

			return true;
		}

		if (CmdDesc[*i].Routine == Commands::TimeLoop)
		{
			Commands::TimeLoop_* in = (Commands::TimeLoop_*)(i + 1);
			if (dxui::cursorID == 0)
			{
				p.x = in->x;
			}

			if (dxui::cursorID == 1)
			{
				p.x = in->length;
			}

			if (dxui::cursorID == 2)
			{
				p.x = in->x;
			}


			return true;
		}

		if (CmdDesc[*i].Routine == Commands::ShowScene)
		{
			Commands::ShowScene_* in = (Commands::ShowScene_*)(i + 1);
			if (dxui::cursorID == 0)
			{
				p.x = in->x*735.f;
			}

			if (dxui::cursorID == 1)
			{
				p.x = in->length*735.f;
			}

			if (dxui::cursorID == 2)
			{
				p.x = in->x*735.f;
			}

			return true;
		}

		return false;
	}

	float Quantize(float x, float step)
	{
		return step * roundf(x / step);
	}

	void setItemCoords3d(BYTE* i, point3 p)
	{
		if (CmdDesc[*i].Routine == Commands::Point3d)
		{
			Commands::Point3d_* in = (Commands::Point3d_*)(i + 1);

			in->x = p.x;
			in->y = p.y;
			in->z = p.z;
		}

	}

	void setItemCoords(BYTE* i,POINT p)
	{

		if (CmdDesc[*i].Routine == Commands::Point)
		{
			Commands::Point_* in = (Commands::Point_*)(i + 1);
			in->x = p.x;
			in->y = (signed short)p.y;
		}

		if (CmdDesc[*i].Routine == Commands::Envelope)
		{
			Commands::Envelope_* in = (Commands::Envelope_*)(i + 1);
			in->x = in->link ? 0: p.x;
			in->y = (signed short)p.y;
		}

		if (CmdDesc[*i].Routine == Commands::Clip)
		{
			Commands::Clip_* in = (Commands::Clip_*)(i + 1);
			float step = FRAMELENF*60.f*60.f / float(MasterBPM*in->bpmScale);			
			float noteSize = SAMPLERATE*60.f / float(MasterBPM*in->bpmScale);

			if (dxui::cursorID == 0)
			{
				if (in->x !=p.x)
				{
					float x = isKeyDown(VK_SHIFT) ? Quantize((float)p.x, step) : Quantize((float)p.x, noteSize);
					in->x = (INT32) max(x,1);
				}
			}
			if (dxui::cursorID == 1)
			{
				auto npm = MasterBPM;
				float width = in->length *60.f*60.f / float(MasterBPM*in->bpmScale);
				in->repeat = (signed short)(p.x/width/FRAMELENF);
			}

		}

		if (CmdDesc[*i].Routine == Commands::TimeLoop)
		{
			Commands::TimeLoop_* in = (Commands::TimeLoop_*)(i + 1);
			//float step = 60.f*60.f / float(MasterBPM);
			//p.x = isKeyDown(VK_SHIFT) ? Quantize(p.x, step*256) : Quantize(p.x, step*100);
			if (dxui::cursorID == 0)
			{
				in->x = p.x;
			}
			if (dxui::cursorID == 1)
			{
				in->length = max(p.x,735*1);
			}

			if (dxui::cursorID == 2)
			{
				in->length+= in->x - p.x;
				in->x = p.x;
				in->length = max(in->length, 735*1);
			}
		}

		if (CmdDesc[*i].Routine == Commands::ShowScene)
		{
			Commands::ShowScene_* in = (Commands::ShowScene_*)(i + 1);
			//float step = 60.f*60.f / float(MasterBPM);
			//p.x = isKeyDown(VK_SHIFT) ? Quantize(p.x, step*256) : Quantize(p.x, step*100);
			if (dxui::cursorID == 0)
			{
				in->x = p.x/735.f;
			}
			if (dxui::cursorID == 1)
			{
				in->length = p.x/735.f;
			}

			if (dxui::cursorID == 2)
			{
				in->length += in->x - p.x/735.f;
				in->x = p.x/735.f;
			}
		}

	}

	void ItemMovement()
	{
		if (isKeyDown(VK_MENU)) return;

		POINT p;
		GetCursorPos(&p);

		ScreenToClient(hWnd, &p);
		RECT r;
		GetClientRect(hWnd, &r);
		float c = (r.bottom - r.top) / float(dx::winH);
		float c2 = (r.right - r.left) / float(dx::winW);
		p.y /= c;
		p.x /= c2;

		float x, y;
		x = p.x / float(dx::width) * 2.f - 1.f;
		y = -(p.y / float(dx::height) * 2.f - 1.f);
	//	x *= dx::invWinAspect;

		if (isKeyDown(VK_LBUTTON) && ui::LButtonDown==true)
		{
			if (ui::itemDragFlag == 0)
			{
				ui::itemDragFlag = 1;

				BYTE* j = stack::data; int m = 0;

				if (ui::selectedParamID >= 0)
				{
					if (ui::selectedElementPtr&&
						ui::isTypeNumeric(ui::GetCmdType(ui::selectedElementPtr, selectedParamID))&&
						(dxui::LastClickedRoutine == ClickSlider||dxui::LastClickedRoutine == ClickNote))
						{
							if (ui::selectedElementOrientation)
							{
								ui::itemOldviewPos[0].x = ReadNumericValue(ui::selectedElementPtr, ui::selectedParamID);
								ui::itemOldCursor.x = x;
							}
							else
							{
								ui::itemOldviewPos[0].y = ReadNumericValue(ui::selectedElementPtr, ui::selectedParamID);
								ui::itemOldCursor.y = y;
							}
						}
				}
				else
				{
					while (*(j) != 0)
					{
						if (*(BYTE*)(j + CmdDesc[*j].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
						{
							POINT p;
							if (getItemCoords(j, p))
							{
								ui::itemOldviewPos[m].x = (float)p.x;
								ui::itemOldviewPos[m].y = (float)p.y;

								ui::itemOldCursor.x = x;
								ui::itemOldCursor.y = y;
							}

							point3 p3;
							if (getItemCoords3d(j, p3))
							{
								ui::itemOldviewPos3[m].x = (float)p3.x;
								ui::itemOldviewPos3[m].y = (float)p3.y;
								ui::itemOldviewPos3[m].z = (float)p3.z;

								ui::itemOldCursor.x = x;
								ui::itemOldCursor.y = y;
							}

						}

						j += CmdDesc[*j].Size; m++;
					}
				}

			}
				//ui::NLog(ui::itemOldviewPos.x);

				int i = 0;
	

			{
				BYTE* i = stack::data; int m = 0;

				float _min = 0;
				float _max = 0;

				if (ui::selectedParamID >= 0)
				{
					if (ui::selectedElementPtr&&
						ui::isTypeNumeric(ui::GetCmdType(ui::selectedElementPtr, selectedParamID))&&
						(dxui::LastClickedRoutine == ClickSlider || dxui::LastClickedRoutine == ClickNote))
					{

						int v = ui::ReadNumericValue(ui::selectedElementPtr, ui::selectedParamID);
						_itoa(v, ui::editBoxText, 10);
						ui::textCursor = strlen(ui::editBoxText);
						ui::textCursorX = dxui::StringLen(letterWidth, ui::editBoxText);
						float scale10 = isKeyDown(VK_CONTROL) ? 10 : 1;
						float scale100 = isKeyDown(VK_SHIFT) ? 100 : 1;
						//WriteValue(ui::selectedElementID - PARAMSID, ui::itemOldviewPos[0].x + scale10 * scale100*dx::width*(x - ui::itemOldCursor.x) / 4.0);
						BYTE cmd = *(ui::selectedElementPtr);
						float range = CmdDesc[cmd].ParamMax[ui::selectedParamID] - CmdDesc[cmd].ParamMin[ui::selectedParamID];

						if (ui::selectedElementOrientation)
						{
							float dir = dxui::LastClickedRoutine == ClickNote ? -1. : 1.;

							WriteValue(ui::selectedElementPtr, ui::selectedParamID, ui::itemOldviewPos[0].x + dx::iaspect*dir*range * dx::width*(x - ui::itemOldCursor.x) / 400.);
						}
						else
						{
							WriteValue(ui::selectedElementPtr, ui::selectedParamID, ui::itemOldviewPos[0].y + range * dx::height*(y - ui::itemOldCursor.y) / 100.);
						}
					}
				}
				else
				{
					while (*i != 0)
					{
						if (CmdDesc[*i].Routine == Envelope)
						{
							Commands::Envelope_* eIn = (Commands::Envelope_*)(i + 1);

							_min = eIn->min;
							_max = eIn->max;

							currentEnvRange = (float)(_max - _min);
						}

						if (*(BYTE*)(i + CmdDesc[*i].Size - EditorInfoOffset + EditorInfoOffsetSelected) == 1)
						{
							if (CmdDesc[*i].Routine == Envelope || CmdDesc[*i].Routine == Point ||
								CmdDesc[*i].Routine == Clip || CmdDesc[*i].Routine == TimeLoop)//check this
							{
								POINT p;
								p.x = (INT32)(ui::itemOldviewPos[m].x + 1020.f*(x - ui::itemOldCursor.x)*4.0f *float(dx::width) / ui::timelineScale);
								p.y = (signed short)(ui::itemOldviewPos[m].y + (y - ui::itemOldCursor.y)*currentEnvRange * 2.f);
								p.y = (LONG)clamp((float)p.y, _min, _max);
								setItemCoords(i, p);
							}

							if (CmdDesc[*i].Routine == ShowScene)
							{
								POINT p;
								p.x = (INT32)(ui::itemOldviewPos[m].x + 1000.f*(x - ui::itemOldCursor.x)*4.0f *float(dx::width) / ui::timelineScale);
								//p.y = (signed short)(ui::itemOldviewPos[m].y + (y - ui::itemOldCursor.y)*currentEnvRange * 2.f);
								//p.y = (LONG)clamp((float)p.y, _min, _max);
								setItemCoords(i, p);
							}

							if (CmdDesc[*i].Routine == Point3d)
							{
								point3 p;
								float dx = 4.0f *float(dx::width)*(x - ui::itemOldCursor.x);
								float dy = 4.0f *float(dx::height)*(y - ui::itemOldCursor.y);

								XMVECTOR pt = XMVectorSet(dx, dy, 0, 0);
								XMVECTOR pt1 = XMVectorSet(ui::itemOldviewPos3[m].x, ui::itemOldviewPos3[m].y, ui::itemOldviewPos3[m].z, 0);

								pt = XMVectorAdd(pt1, XMVector3Transform(pt, XMMatrixInverse(NULL, dx::View2)));

								p.x = XMVectorGetX(pt);
								p.y = XMVectorGetY(pt);
								p.z = XMVectorGetZ(pt);

								setItemCoords3d(i, p);
							}
						}
						i += CmdDesc[*i].Size; m++;
					}
				}
			}

		}
		else
		{
			if (ui::itemDragFlag == 1 && ui::itemOldCursor.x != x && ui::itemOldCursor.y != y)
			{
				io::WriteUndoStage(ui::CurrentPos);
			}
			ui::itemDragFlag = 0;
		}

		
	}



	float ui_clipleft = 0;
	int clipgrid = 0;
	float channelPeak[MAXCHANNELS];

	dxui::gradient grad1, grad2, grad3;
	int colorpointnum;
	XMFLOAT4 ContorCurveScreenPos;

	char* GetShaderName(BYTE shaderID)
	{
		int shaderCounter = 0;
		BYTE* p = stack::data;

		while (*p != 0 )
		{
			if (CmdDesc[*p].Routine == CreateShader)
			{
				if (shaderID == shaderCounter)
				{
					for (int x=0;x<MAXPARAM;x++)
					{
						if (!strcmp(CmdDesc[*p].ParamName[x], "name"))
						{
							return (char*)(p + CmdDesc[*p].ParamOffset[x]);
						}
					}
				}

				shaderCounter++;
			}

			p += CmdDesc[*p].Size;
		}

		return NULL;
	}


	char* GetResourceName(BYTE id,PVFN CreationRoutine,char* name)
	{
		int counter = 0;
		BYTE* p = stack::data;

		while (*p != 0 )
		{
			if (CmdDesc[*p].Routine == CreationRoutine)
			{
				if (id == counter)
				{
					for (int x = 0; x<MAXPARAM; x++)
					{
						if (!strcmp(CmdDesc[*p].ParamName[x], name))
						{
							return (char*)(p + CmdDesc[*p].ParamOffset[x]);
						}
					}
				}

				counter++;
			}

			p += CmdDesc[*p].Size;
		}

		return NULL;
	}

	char* GetShaderPtr(BYTE shaderID)
	{
		int shaderCounter = 0;
		BYTE* p = stack::data;

		while (*p != 0 )
		{
			if (CmdDesc[*p].Routine == CreateShader)
			{
				if (shaderID == shaderCounter)
				{
					for (int x = 0; x<MAXPARAM; x++)
					{
						if (!strcmp(CmdDesc[*p].ParamName[x], "data"))
						{
							return (char*)(p + CmdDesc[*p].ParamOffset[x]);
						}
					}
				}

				shaderCounter++;
			}

			p += CmdDesc[*p].Size;
		}

		return NULL;
	}





	void DrawTimestamps()
	{
		


		dxui::SetupDrawerTimeline();
		float y = -1. + 2.*(dx::height-dx::winH)/(float)(dx::height)+.01;
		dxui::Linelist(timelinePos.x, y, timelineScale, 1.f, colorScheme.timelinelines);
		
		dxui::SetupDrawerBox();
		
		int firstLine = (int)((-1.f / dx::aspect - ((float)timelinePos.x - .015f)) / (.01f*timelineScale));
		int lastLine = (int)((1.f / dx::aspect - ((float)timelinePos.x - .015f)) / (.01f*timelineScale));
		lastLine = min(lastLine, timedividercount);
		firstLine = max(firstLine, 0);

		y += elementHeight*1.5;
		for (int j = firstLine; j < lastLine; j += 10)
		{
			int j10 = 10*(int) (j / 10);

			char t[12]; char t2[12];
			_itoa(j10 % 60, t, 10);
			_itoa(j10 / 60, t2, 10);
			strcat(t2, ":");
			strcat(t2, t);
			if (j10 % 60 == 0) strcat(t2, "0");
			if (j10 % 60 == 0 || timelineScale > .75)
			{
				dxui::String(j10*.01f*timelineScale + (float)timelinePos.x - .015f,y, ui::elementHeight , ui::elementHeight, colorScheme.timelinefont, t2);
			}
		}

	}

	void DrawTimelineCursor()
	{
		dxui::SetupDrawerBox();

		float tlc = (float)((Commands::cursorF)*ui::timelineScale) / (-60.f*100.f) - timelinePos.x;

		if (isKeyDown(VK_MENU) || playmode == 1)
			dxui::Box(0.f - tlc, 1.f, .005f, 2.f, colorScheme.cursor, .001f);
		else
			dxui::Box(0.f - tlc, -.95f, .005f, .15f, colorScheme.cursor, .001f);
	}

	void DrawTimeline()
	{
		DrawTimestamps();
		DrawTimelineCursor();
	}

	int Level;
	
	int GetParamCount(BYTE cmd)
	{
		int pCount = 0;
		while (*CmdDesc[cmd].ParamName[pCount] != 0) pCount++;
		return pCount;
	}

	

	void DoCmdDraw()
	{
		cmd = *i;
		PVFN Routine = CmdDesc[cmd].Routine;
		int Category = CmdDesc[cmd].Category;
		int Size = CmdDesc[cmd].Size;
		nextCmd = *(i + Size);
		isSelected = *(i + Size - EditorInfoOffset + EditorInfoOffsetSelected);
		Level = CmdDesc[cmd].Level;
		Open = *(BYTE*)(i + CmdDesc[cmd].Size - EditorInfoOffset + EditorInfoOffsetMode);

		tx = -1.74f + CmdDesc[cmd].Level*LevelStepRight;
		
		if (CmdDesc[cmd].Category == CAT_SOUND && CmdDesc[cmd].Level > 2)
		{
			tx = -1.74f + (CmdDesc[cmd].Level-2)*LevelStepRight;
		}

		w = dxui::StringLen(letterWidth, CmdDesc[cmd].Name)+letterWidth/4;
		Color = CmdDesc[cmd].Color;
		TextColor = colorScheme.font;;

		bx = tx - elementHeight / 3.f / 4.f;
		by = ty;
		bw = w * 2.f + elementHeight / 3.f;
		bh = elementHeight;

		if (CmdDesc[cmd].Draw == NULL || !stackViewMode)
		{
			StandartDrawer();
		}
		else
		{
			CmdDesc[cmd].Draw();
		}

		if (CmdDesc[nextCmd].Level > CmdDesc[cmd].Level)
		{
			prevLevelCmd[CmdDesc[nextCmd].Level] = cmd;
			prevLevelCmdPtr[CmdDesc[nextCmd].Level] = i;
		}

		if (i == stack::data + ui::CurrentPos)
		{
			fParamPos.x = tx - elementHeight / 3.f / 4.f +bw+.001;
			fParamPos.y = ty + stepY;
		}

		i += Size;
		counter++;
	}
	
	char pressedKey[100];
	char pressedMKey[100];

	bool trackOpen;
	BYTE* lastShaderPtr = NULL;

	char* searchCommaOrSpace(char* ptr, char* end)
	{
		while (*(ptr) != '\r' && *ptr != ',' && *ptr != ' ' && ptr < end) ptr++;
		if (ptr == end) return NULL;
		return ptr;
	}

	char* searchAnyLetter(char* ptr, char* end)
	{
		while (!isalpha(*ptr) && ptr < end) ptr++;
		if (ptr == end) return NULL;
		return ptr;
	}

	char* searchAnyNumber(char* ptr, char* end)
	{
		while (!isdigit(*ptr) && ptr < end && *ptr!='-') ptr++;
		if (ptr == end) return NULL;
		return ptr;
	}

	BYTE* lastProcessedShader = NULL;
	
	void ShaderIntro()
	{
		BYTE* pP = stack::data + CurrentPos;
		BYTE cmd = *pP;

		//change name for replace it from shader constant buffer
		if (CmdDesc[cmd].Routine == NullDrawer || CmdDesc[cmd].Routine == DefineParams || CmdDesc[cmd].Routine == SetParams)
		{
			int pc;
			BYTE shaderID = 0;
			char cbLabel[100];

			if (CmdDesc[cmd].Routine == NullDrawer) {
				if (lastProcessedShader != pP)
				{
					SetWindowText(PEditor, CmdDesc[cmd].Name);
					lastProcessedShader = pP;
				}
				int x = 0;

				shaderID = *(BYTE*)(pP + CmdDesc[cmd].ParamOffset[0]);
				strcpy(cbLabel, "cbuffer ConstantBuffer : register(b1)");
				pc = 3;//param offset
			}

			if (CmdDesc[cmd].Routine == DefineParams)
			{
				DefineParams_* in = (DefineParams_*)(pP + 1);
				shaderID = in->shader;
				strcpy(cbLabel, "cbuffer ConstantBuffer2 : register(b2)");
				pc = 2;
			}

			if (CmdDesc[cmd].Routine == SetParams)
			{
				byte* ndPtr = stack::data;
				byte* hdPtrF = NULL;
				while (*ndPtr!=0 && ndPtr < pP)
				{
					if (CmdDesc[*ndPtr].Routine == NullDrawer)
					{
						hdPtrF = ndPtr;
					}
					ndPtr += CmdDesc[*ndPtr].Size;
				}
				NullDrawer_* ndIn = (NullDrawer_*)(hdPtrF + 1);
				shaderID = ndIn->shader;
				strcpy(cbLabel, "cbuffer ConstantBuffer3 : register(b3)");
				pc = 0;
			}

			char* shaderPtr = GetShaderPtr(shaderID);
			char* shaderEnd = shaderPtr;

			while (*shaderEnd != 0) shaderEnd++;

			int shaderLen = shaderEnd - shaderPtr;

			char* text = shaderPtr;
			char* ptr1 = text;
			char* ptr2 = text;
			char* end = text;

			if (text)
			{
				text = strstr(text, cbLabel);

				if (!text) return;

				text = strstr(text, "{");

				if (!text) return;

				end = strstr(text, "}")+1;

				if (!end) return;
				if (end > shaderEnd) return;

				
				#define bufSize 100*16
				char buffer[bufSize];
				if (end - text >= bufSize) MessageBox(hWnd, "shader intro buffer too small","error", MB_OK);

				memcpy(buffer, text, min(bufSize, end - text));
				buffer[min(bufSize - 1, end - text)] = 0;

				end = buffer;
				int len = end - text;
				end += len;
				text = buffer;

				while (text && ptr2 && text < end )
				{
					CmdDesc[cmd].ParamName[pc][0] = 0;

					text = strstr(text, "float ");
					if (text && text < end)
					{
						text += 6;
						ptr2 = strstr(text, ";");
						if (ptr2 != NULL)
						{
							if (text != NULL && ptr2 != NULL)
							{
								strncpy(CmdDesc[cmd].ParamName[pc], text, min(ptr2 - text, MAXPARAMNAMELEN - 1));
								CmdDesc[cmd].ParamName[pc][min(ptr2 - text, MAXPARAMNAMELEN - 1)] = 0;
								CmdDesc[cmd].ParamType[pc] = PT_SWORD;
								CmdDesc[cmd].ParamMin[pc] = -32767;
								CmdDesc[cmd].ParamMax[pc] = 32767;
							}
						}
						else break;

						char* cr = strstr(ptr2, "\n");
						ptr1 = ptr2;

						//range
						ptr1 = strstr(ptr1, "Range ");
						if (ptr1 && ptr1<cr)
						{
							ptr1 += 5;
							char* left = searchAnyNumber(ptr1, cr);
							if (left)
							{
								CmdDesc[cmd].ParamMin[pc] = atoi(left);
								char* right = NULL;
								right = searchCommaOrSpace(left,cr);
								if (right)
								{
									right = searchAnyNumber(right, cr);
									CmdDesc[cmd].ParamMax[pc] = atoi(right);
								}
							}
						}
						else
						{
							ptr1 = ptr2;
						}

						//enum
						int enumN = 0;

						for (int t = 0; t < MAXPARAMENUM; t++)CmdDesc[cmd].ParamEnum[pc][t][0] = 0;

						ptr1 = strstr(ptr1, "EnumList ");
						if (ptr1 && ptr1<cr)
						{
							ptr1 += 8;
							char* left = searchAnyLetter(ptr1, cr);
							while (left)
							{
								char* right = NULL;
								right = searchCommaOrSpace(left, cr);
								if (right)
								{
									strncpy(CmdDesc[cmd].ParamEnum[pc][enumN], left, right - left);
									CmdDesc[cmd].ParamType[pc] = PT_ENUM;
									enumN++;
									left = searchAnyLetter(right, cr);
								}
								else
								{
									left = NULL;
								}
							}
						}
						else
						{
							ptr1 = ptr2;
						}

					}

					pc++;
				}
				//strncpy(CmdDesc[cmd].ParamName[1], "ppp", 32);
			}
		}
		//
	}

	BYTE* cCmdPtr = NULL;

	void FillResourceList(int resType, int cmd, int param, int offset=0)
	{
		int k=0;
		while (Commands::resourceCreationPtr[resType][k] != NULL)
		{
			BYTE* ptr = Commands::resourceCreationPtr[resType][k];
		
			char* name = (char*)(ptr);

			char nk[22];
			if (*name == 0)
			{
				_itoa(k, nk, 10);
				name = nk;
			}

			strcpy(CmdDesc[cmd].ParamEnum[param][k+offset], name);
			CmdDesc[cmd].ParamEnum[param][k +offset + 1][0] = 0;

			k++;
		}
		CmdDesc[cmd].ParamType[param] = PT_ENUM;
	}

	void ShowCmdParams()
	{
		if (dxui::LastClickedRoutine == AddCmdAfter || dxui::LastClickedRoutine == AddChild) return;
	
		ShaderIntro();

		float y = fParamPos.y-elementHeight;
		float x = -1.86;

		float h = .5;
		float w = .12;
		float drag = 0;// .05;
		float inside = .01;
		float scale = 1.4;
		float width = letterWidth*.9;

		y = fParamPos.y;
		float yT = y-.012/2.;
		x = -1.7+.21+.035;
		dxui::SetupDrawer();

		BYTE* pP = stack::data + CurrentPos;
		BYTE cmd = *pP;

		int pCount = GetParamCount(cmd);
		if (pCount == 0) return;

		float winW = .32;
		float paramNameW = .15;
		float selX, selY;

		for (int t = 0; t < pCount; t++)
		{
			float paramValueW = .1;
			int offset = CmdDesc[cmd].ParamOffset[t];
			char* name = CmdDesc[cmd].ParamName[t];
			BYTE type = CmdDesc[cmd].ParamType[t];

			if (*name != 0)
			{
				XMFLOAT4 boxColor = menuBoxColor;
				XMFLOAT4 textColor = menuTextColor;
				float ofs = .21;

				if (!strcmp(name, "Name"))
				{
					//ofs = dxui::StringLen(width,"Name  ")*1.1;
					ofs = tx-x+ dxui::StringLen(letterWidth, CmdDesc[cmd].Name)+letterWidth;
					paramValueW = winW - ofs-.01;
				}

				if (!strcmp(name, "shader"))
				{
					FillResourceList(0,cmd,t);
				}

				if (!strcmp(name, "texture"))
				{
					FillResourceList(1, cmd, t);
				}

				if (!strcmp(name, "scene"))
				{
					FillResourceList(2, cmd, t);
				}

				if (!strcmp(name, "paramSet"))
				{
					FillResourceList(3, cmd, t);
				}

				if (CmdDesc[cmd].Routine == Override && !strcmp(name, "ps"))
				{
					FillResourceList(0, cmd, t,2);
				}

				if (pP == ui::selectedElementPtr && t == selectedParamID)
				{
					boxColor = menuTextColor;
					textColor = menuBoxColor;
					selX = x + winW;
					selY = y - drag - t * elementHeight*scale;
				}

				if (type == PT_TEXT)
				{
					dxui::Box(x + ofs , y - drag - t * lineStep, paramValueW-.01, elementHeight*scale*.8, menuBoxColor, 0.1, false,false, ui::selectedElementPtr,t,ClickParam);
					dxui::String(x + ofs, yT - drag - t * lineStep , width, elementHeight*scale - inside * 2.4, menuTextColor, ">edit");
				}
				else
				{
					char value[64];
					ReadValue(pP, t, value);

					if (*(CmdDesc[cmd].ParamEnum[t][2]) == 0)
					{
						if (type == PT_ENUM )						{
							BYTE v = *(pP + offset);
							if (v == 1)
							{
								boxColor = XMFLOAT4(.75, .35, 0, 1);
								textColor = menuTextColor;
							}
						}
					}

					//value holder
					dxui::Box(x + ofs, y - drag - t * lineStep, paramValueW-.01, elementHeight*scale*.8, boxColor, 0.1,
						(t == selectedParamID && pP == ui::selectedElementPtr )? false : true,true,pP,t, ClickParam);

					if (isTypeNumeric(type))
					{
						float pW = ofs;
						float range = CmdDesc[cmd].ParamMax[t] - CmdDesc[cmd].ParamMin[t];
						float n = ReadNumericValue(t);
						float percent = (-CmdDesc[cmd].ParamMin[t]+n) / range;// *pW;

						dxui::Box(x  - .01, y - drag - t * lineStep, pW, elementHeight*scale*.8, 
							XMFLOAT4(0.2, 0.2, 0.2, 1), 0.25001, false,false,pP,t,ClickSlider,
							XMFLOAT4(fabs(percent),0,0,0), n > 0 ? XMFLOAT4(.5, .25, 0, 1) : XMFLOAT4(0, .3, .6, 1));
					}

					if (pP == ui::selectedElementPtr && t == selectedParamID && CmdDesc[cmd].ParamType[selectedParamID] != PT_ENUM && CmdDesc[cmd].ParamType[selectedParamID] != PT_ENUMASSIGN)
					{
						strcpy(value, editBoxText);
						textColor.w = 5;
					}
					dxui::String(x + ofs+.005, yT - drag - t * lineStep , width, elementHeight*scale - inside * 2.4, textColor, value);

					if (pP == ui::selectedElementPtr && t == selectedParamID && CmdDesc[cmd].ParamType[selectedParamID] != PT_ENUM && CmdDesc[cmd].ParamType[selectedParamID] != PT_ENUMASSIGN)
					{
						dxui::Box(x + ofs +.005+ textCursorX, yT - drag - t * lineStep , .003, ui::elementHeight*.8, XMFLOAT4(0, 0, 0, .5 + .5*sin(.01*timer::GetCounter())), 0.001, NULL, -1, false);
					}
				}

				if (strcmp(name, "Name"))
				{
					dxui::String(x + 0.001, yT - drag - t * lineStep, width, elementHeight*scale - inside * 2.4, XMFLOAT4(1, 1, 1, 1), CmdDesc[cmd].ParamName[t]);
				}
			}
		}

	
		//envelopes add buttons
		{
			int envOn[MAXPARAM];
			int envLinkType[MAXPARAM];
			for (int t = 0; t < MAXPARAM; t++)
			{
				envOn[t] = 0;
				envLinkType[t] = 0;
			}
			BYTE* ptr = pP;
			int lvl = CmdDesc[*ptr].Level;
			ptr += CmdDesc[*ptr].Size;

			BYTE* envPtr [MAXPARAM];
			for (int e = 0; e < MAXPARAM; e++) envPtr[e] = NULL;

			while (CmdDesc[*ptr].Level > lvl)
			{
				if (CmdDesc[*ptr].Routine == Envelope)
				{
					Envelope_* in = (Envelope_*)(ptr + 1);
					envOn[clamp_int(in->assignTo, 0, MAXPARAM - 1)] = 1;
					envLinkType[clamp_int(in->assignTo, 0, MAXPARAM - 1)] = in->link;
					envPtr[clamp_int(in->assignTo, 0, MAXPARAM - 1)] = ptr;
				}

				if (CmdDesc[*ptr].Routine == SendToEnv)
				{
					SendToEnv_* in = (SendToEnv_*)(ptr + 1);
					envOn[clamp_int(in->assignTo, 0, MAXPARAM - 1)] = 2;
				}

				ptr += CmdDesc[*ptr].Size;
			}

			for (int t = 0; t < pCount; t++)
			{
				XMFLOAT4 eCol = ColorForEnvelope(t);

				if (CmdDesc[cmd].ParamEnvControlled[t])
				{
					float y = fParamPos.y - t * lineStep;

					if (envOn[t] == 0)
					{
						dxui::Box(x + winW, y, elementHeight, elementHeight, XMFLOAT4(0.2, 0.2, 0.2, 1), .25, true, true, pP, t, ClickAutomationContext);
					}

					if (envOn[t] == 1)
					{
						dxui::Box(x + winW, y, elementHeight, elementHeight, eCol, .25, false, true, envPtr[t], t, ClickSelectEnv);

						if (envLinkType[t] == 0)
						{
							dxui::String(x + winW+.0095, y, letterWidth, elementHeight*.9, XMFLOAT4(0, 0, 0, 1), "T");
						}

						if (envLinkType[t] == 1)
						{
							dxui::String(x + winW+.0075, y, letterWidth, elementHeight*.9, XMFLOAT4(0, 0, 0, 1), "N");
						}
					}

					if (envOn[t] == 2)
					{
						dxui::Box(x + winW, y, elementHeight, elementHeight, eCol, .25, false, true, NULL, t, NULL);
						dxui::String(x + winW + .0075, y, letterWidth, elementHeight*.9, XMFLOAT4(0, 0, 0, 1), "S");
					}

				}
			}

			if (dxui::LastClickedRoutine == ClickAutomationContext)
			{
				float y = fParamPos.y - ui::selectedParamID * lineStep;
				float cw = .13;
				dxui::Box(x + winW+letterWidth, y, cw, elementHeight, XMFLOAT4(0.2, 0.2, 0.2, 1), .25, true, true, pP, 0, ClickAddAutomation);
				dxui::Box(x + winW + letterWidth, y - lineStep, cw, elementHeight, XMFLOAT4(0.2, 0.2, 0.2, 1), .25, true, true, pP, 1, ClickAddAutomation);
				dxui::Box(x + winW + letterWidth, y - lineStep*2, cw, elementHeight, XMFLOAT4(0.2, 0.2, 0.2, 1), .25, true, true, pP, 2, ClickAddAutomation);
				dxui::String(x + winW + letterWidth, y, letterWidth, elementHeight*.9, XMFLOAT4(1, 1, 1, 1), "NoteEnv");
				dxui::String(x + winW + letterWidth, y - lineStep, letterWidth, elementHeight*.9, XMFLOAT4(1, 1, 1, 1), "TrackEnv");
				dxui::String(x + winW + letterWidth, y - lineStep*2, letterWidth, elementHeight*.9, XMFLOAT4(1, 1, 1, 1), "SendEnv");
			}

		}


		if (dxui::LastClickedRoutine == ClickParam && pP == ui::selectedElementPtr)
		{
			int pID = ui::selectedParamID;
			y -= pID * lineStep;

			if (CmdDesc[cmd].ParamType[pID] == PT_ENUM)
			{
				int t = 0;

				while (*(CmdDesc[cmd].ParamEnum[pID][t]) != 0) t++;
				if (t != 2)
				{
					t = 0;
					float ofs = -.25;
					while (*(CmdDesc[cmd].ParamEnum[pID][t]) != 0)
					{
						XMFLOAT4 boxColor = menuBoxColor;
						XMFLOAT4 textColor = menuTextColor;

						dxui::Box(selX, selY - t * lineStep, .2, elementHeight*scale*.9, boxColor, .1, true, true, pP, t, ClickParamContextMenu);
						dxui::String(selX, selY - t * lineStep - .012, width, elementHeight*scale - inside * 2.4, textColor, CmdDesc[cmd].ParamEnum[pID][t]);
						t++;
					}
				}
			}
			if (CmdDesc[cmd].ParamType[pID] == PT_ENUMASSIGN)
			{
				int t = 0;
				float ofs = -.25;

				BYTE* i = stack::data;
				BYTE* start = stack::data;
				BYTE* end = stack::data + CurrentPos;

				//get parent
				while (*start != 0 && start < end)
				{
					if (CmdDesc[*start].Level < CmdDesc[*end].Level)
					{
						i = start;
					}
					start += CmdDesc[*start].Size;
				}

				BYTE cmdAssign = *i;

				while (*(CmdDesc[cmdAssign].ParamName[t]) != 0)
				{
					XMFLOAT4 boxColor = menuBoxColor;
					XMFLOAT4 textColor = menuTextColor;
					if (!CmdDesc[cmdAssign].ParamEnvControlled[t])
					{
						textColor = XMFLOAT4(.5, .5, .5, 1);
					}
					dxui::Box(selX, selY - t * lineStep, .2, elementHeight*scale*.9, boxColor, .1, true, false, pP, t, ClickParamContextMenu);
					dxui::String(selX, selY - t * lineStep - .012, width, elementHeight*scale - inside * 2.4, textColor, CmdDesc[cmdAssign].ParamName[t]);
					t++;
				}
			}


		}

}

void ClickOct()
{

}

void ClickMidiDevice()
{
	midiPort++;
	if (midiPort >= midiPortCount) midiPort = 0;
	delete midiin;
	midiSetup();
}

void ClickVelocityMapping()
{
	midiNoteVelocityMapping++;
	if (midiNoteVelocityMapping > 4) midiNoteVelocityMapping = 0;
}


bool anyKeyPressed = false;

double clickCounter = 0;

	void showViewController()
	{
		if (!Commands::musicPtr || !GetElementOpenStatus(Commands::musicPtr)) return;

		dxui::SetupDrawer();

		float y = 1.02;
		float h = .45;
		float w = .05;
		float drag = .05;
		float inside = .01;
		float scale = 1.3;
		float width = .032;

		char* mainMenu[] = { "N","R","S","V","0","1","2","3" };

		float x = -1.*w * _countof(mainMenu)-1.38;

		float selX = 0, selY = 0;


		for (int t = 0; t < _countof(mainMenu); t++)
		{
			XMFLOAT4 boxColor = XMFLOAT4(0.5, 0.5, 0.5, 1);

			if (t == noteViewMode)
			{
				boxColor = XMFLOAT4(1, 1, 1, 1);
			}

			XMFLOAT4 textColor = XMFLOAT4(0, 0, 0, 1);
			float _x = x + t * w*.8;
			float _y = y - drag;

			int a = 0;
			if (selectedParamID == t && dxui::LastClickedRoutine == ClickViewController)
			{
				selX = _x;
				selY = _y;
			}

			dxui::Box(_x + inside, _y, w - inside * 2, elementHeight*scale*.9, boxColor, .1, false, true, NULL, t, ClickViewController);
			float offset = .5*(w - dxui::StringLen(width, mainMenu[t]));
			dxui::String(_x + offset, _y - .012, width, elementHeight*scale - inside * 2.4, textColor, mainMenu[t]);
		}

		//octave
		XMFLOAT4 boxColor = XMFLOAT4(0.5, 0.5, 0.5, 1);
		float _y = y - drag - elementHeight * .1;
		float _x = -1.38;
		dxui::Box(_x, _y, elementHeight , elementHeight, boxColor, .25, false, true, NULL, 0, ClickOct);
		dxui::Box(_x + elementHeight*3, _y, elementHeight, elementHeight, boxColor, .25, false, true, NULL, 1, ClickOct);
		//float offset = .5*(w - dxui::StringLen(width, mainMenu[t]));
		_x += letterWidth * .3;
		dxui::String(_x, _y, letterWidth, elementHeight, XMFLOAT4(0,0,0,1), "-");
		dxui::String(_x + elementHeight * 3, _y, letterWidth, elementHeight, XMFLOAT4(0, 0, 0, 1), "+");
		char oct[] = "oct  ";
		oct[4]= '0'+ui::cOctave;
		dxui::String(_x + elementHeight * .9, _y, letterWidth, elementHeight, XMFLOAT4(1, 1, 1, 1), oct);


		double cc = timer::GetCounter();
		if (cc> clickCounter+150 && GetAsyncKeyState(VK_LBUTTON) && dxui::ClickRoutine == ClickOct)
		{
			clickCounter = cc;
			if (ui::selectedParamID == 0 && ui::cOctave > 0) ui::cOctave--;
			if (ui::selectedParamID == 1 && ui::cOctave < 9) ui::cOctave++;
		}

		//midi
		if (midiPortCount > 0)
		{
			_x += elementHeight * 4.5;
			float mw = dxui::StringLen(letterWidth, midiPortName);
			XMFLOAT4 mc = boxColor;
			if (midiNotePressed)
			{
				dxui::Box(_x, _y, elementHeight*.25, elementHeight*.25, XMFLOAT4(1, 0, 0, 1), .25, false, true);


				if (ui::selectedElementPtr && CmdDesc[*ui::selectedElementPtr].Routine == Commands::Note)
				{
					Note_* in = (Note_*)(ui::selectedElementPtr + 1);
					in->Note = midiNoteCode + 1;

					switch (midiNoteVelocityMapping)
					{
						case 1: 	in->s0 = midiNoteVelocity;break;
						case 2: 	in->s1 = midiNoteVelocity; break;
						case 3: 	in->s2 = midiNoteVelocity; break;
						case 4: 	in->s3 = midiNoteVelocity; break;
					}

					BYTE* ptr = stack::data;
					BYTE* ParentClip = NULL;

					while (!(*ptr == 0 || ptr == ui::selectedElementPtr))
					{
						if (CmdDesc[*ptr].Routine == Clip) ParentClip = ptr;
						ptr += CmdDesc[*ptr].Size;
					}

					Clip_* clipIn = (Clip_*)(ParentClip + 1);
					int nCount = clipIn->length;

					BYTE* firstNotePtr = ParentClip + CmdDesc[*ParentClip].Size;
					int noteSize = CmdDesc[*ui::currentNotePtr].Size;
					int noteNum = (ui::currentNotePtr - firstNotePtr) / noteSize;

					ui::removeSelection();

					float ns = ui::GetNoteSize(stack::data + ui::CurrentPos);

					if (Commands::playonenoteFlag == false && playmode == 0)
					{
						Commands::playonenoteFlag = true;
						PlayOneNote(ui::currentNotePtr);
					}

					/*if (noteNum + ui::autoNextNote < nCount)
					{
						ui::currentNotePtr += CmdDesc[*ui::currentNotePtr].Size*ui::autoNextNote;
						ui::selectedElementPtr = ui::currentNotePtr;
					}
					ui::SetElementSelection(ui::currentNotePtr, 1);*/
				}

			}
			else
			{
				if (Commands::playonenoteFlag && !anyKeyPressed)
				{
					Commands::playonenoteFlag = false;
					Commands::playmode = 0;
					Commands::StopWave();


					BYTE* ptr = stack::data;
					BYTE* ParentClip = NULL;

					while (!(*ptr == 0 || ptr == ui::selectedElementPtr))
					{
						if (CmdDesc[*ptr].Routine == Clip) ParentClip = ptr;
						ptr += CmdDesc[*ptr].Size;
					}

					Clip_* clipIn = (Clip_*)(ParentClip + 1);
					int nCount = clipIn->length;

					BYTE* firstNotePtr = ParentClip + CmdDesc[*ParentClip].Size;
					int noteSize = CmdDesc[*ui::currentNotePtr].Size;
					int noteNum = (ui::currentNotePtr - firstNotePtr) / noteSize;

					if (noteNum + ui::autoNextNote < nCount)
					{
						ui::currentNotePtr += CmdDesc[*ui::currentNotePtr].Size*ui::autoNextNote;
						ui::selectedElementPtr = ui::currentNotePtr;
					}
					ui::SetElementSelection(ui::currentNotePtr, 1);
				}
			}

			dxui::Box(_x, _y, mw, elementHeight, mc, .25, false, true, NULL, 0, ClickMidiDevice);
			dxui::String(_x+ letterWidth*.2, _y, letterWidth*.9, elementHeight*.9, XMFLOAT4(1, 1, 1, 1), midiPortName);
			_x += mw + elementHeight * .5;

			dxui::Box(_x, _y, letterWidth * 3, elementHeight, mc, .25, false, true, NULL, 0, ClickVelocityMapping);
			char wmt[4] = "off";
			if (midiNoteVelocityMapping > 0)
			{
				wmt[0] = '0' + midiNoteVelocityMapping - 1;
				wmt[1] = 0;
			}
			float sw = dxui::StringLen(letterWidth, wmt);
			dxui::String(_x + letterWidth *1.5 - sw*.5, _y, letterWidth*.9, elementHeight*.9, XMFLOAT4(1, 1, 1, 1), wmt);
			sw = dxui::StringLen(letterWidth, "velocity to send");
			dxui::String(_x + letterWidth * 1.5 - sw * .5, _y-elementHeight, letterWidth*.9, elementHeight*.9, XMFLOAT4(1, 1, 1, 1), "velocity to send");

		}
		
	}

	void showMainMenu()
	{
		dxui::SetupDrawer();

		float y = 1.02;
		float h = .5;
		float w = .12;
		float drag = .05;
		float inside = .01;
		float scale = 1.4;
		float width = letterWidth*.9;

		char* mainMenu[] = { "Project","Scene","Cmd","->","3D","Particle","Sound" };

		float x = -.5*w * _countof(mainMenu);

		float selX=0,selY=0;

		for (int t = 0; t < _countof(mainMenu); t++)
		{
			XMFLOAT4 boxColor = colorScheme.boxcat[t];
			XMFLOAT4 textColor = XMFLOAT4(1,1,1,1);
			float _x = x + t * w;
			float _y = y - drag;

			int a = 0;
			if (selectedParamID == t && dxui::LastClickedRoutine == ClickMainMenu)
			{
				selX = _x;
				selY = _y;
			}

			dxui::Box(_x+inside, _y, w-inside*2, elementHeight*scale*.9, boxColor, .1,false,true, NULL,t,ClickMainMenu);
			float offset = .5*(w - dxui::StringLen(width, mainMenu[t]));
			dxui::String(_x + offset, _y - .012, width, elementHeight*scale - inside * 2.4, textColor, mainMenu[t]);
		}

		//process submenu - project
		if (dxui::LastClickedRoutine == ClickMainMenu && selectedParamID == 0)
		{
			XMFLOAT4 boxColor = menuBoxColor;
			XMFLOAT4 textColor = menuTextColor;

			x += w; y -= (selectedParamID) * elementHeight*scale+drag+inside;
			char* projectMenu[] = { "New","Load","Save","Save as...","ReCalc","Save used CmdList","Migrate","Export as text","Import from text" };

			float widest = 0;
			for (int t = 0; t < _countof(projectMenu); t++)
			{
				widest = max(widest, dxui::StringLen(width, projectMenu[t]));
			}

			w = widest+inside*2.;

			for (int t = 0; t < _countof(projectMenu); t++)
			{
				float _x = selX;
				float _y = selY - (t+1) * elementHeight*scale;
				dxui::Box(_x, _y, w, elementHeight*scale*.9, boxColor, .1,false,true,NULL, t, ClickProjectMenu);
				dxui::String(_x+inside, _y - .012, width, elementHeight*scale - inside * 2.4, textColor, projectMenu[t]);
			}
		}


		//process cmd submenus
		if (dxui::LastClickedRoutine == ClickMainMenu && selectedParamID > 0)
		{
			XMFLOAT4 boxColor = menuBoxColor;
			XMFLOAT4 textColor = menuTextColor;

			x += w; y -= (selectedParamID) * elementHeight*scale + drag + inside;

			float widest = 0;
			for (int t = 0; t < stack::CommandsCount; t++)
			{
				if (CmdDesc[t].Category == selectedParamID)
				{
					widest = max(widest, dxui::StringLen(width, CmdDesc[t].Name));
				}
			}

			w = widest + inside * 2.;

			int cnt = 0;
			for (int t = 0; t < stack::CommandsCount; t++)
			{
				if (CmdDesc[t].Category == selectedParamID)
				{
					float _x = selX;
					float _y = selY - (cnt + 1) * elementHeight*scale;

					dxui::Box(_x, _y , w, elementHeight*scale*.9,boxColor, .1, false, true, NULL, t, ClickInsertCmd);
					dxui::String(_x + inside, _y  - .012, width, elementHeight*scale - inside * 2.4, textColor, CmdDesc[t].Name);
					cnt++;
				}
			}
		}



	}

	void AutoScroll()
	{

		float tlc = (float)((Commands::cursorF)*ui::timelineScale) / (-60.f*100.f) - timelinePos.x;
		if (Commands::playmode||Commands::playonenoteFlag)
		{
			if (tlc < -1.) timelinePos.x += tlc + 1;
			if (tlc > -ui::MusicTimelineLeftMargin) timelinePos.x -= -tlc- ui::MusicTimelineLeftMargin;
		}

	}

	void LogKeyPressed(WPARAM wParam)
	{
		timer::pressedKeyTimeStamp = (float)timer::GetCounter();
		ui::pressedKey[0] = 0;

		if (ui::isKeyDown(VK_SHIFT)) strcat(ui::pressedKey, "Shift ");
		if (ui::isKeyDown(VK_CONTROL)) strcat(ui::pressedKey, "Ctrl ");
		if (ui::isKeyDown(VK_OEM_4)) strcat(ui::pressedKey, "[");
		if (ui::isKeyDown(VK_OEM_6)) strcat(ui::pressedKey, "]");
		if (ui::isKeyDown(VK_LEFT)) strcat(ui::pressedKey, "left");
		if (ui::isKeyDown(VK_RIGHT)) strcat(ui::pressedKey, "right");
		if (ui::isKeyDown(VK_UP)) strcat(ui::pressedKey, "up");
		if (ui::isKeyDown(VK_DOWN)) strcat(ui::pressedKey, "down");
		if (ui::isKeyDown(VK_RETURN)) strcat(ui::pressedKey, "enter");
		if (ui::isKeyDown(VK_DELETE)) strcat(ui::pressedKey, "del");

		char k[2];
		k[0] = char(wParam);
		k[1] = 0;

		if (IsCharAlphaNumeric(k[0]))
		{
			strcat(ui::pressedKey, k);
		}
	}

	void Axis3D()
	{

		XMVECTOR x = XMVectorSet( .25, 0, 0, 1 );
		XMVECTOR y = XMVectorSet( 0, .25, 0, 1 );
		XMVECTOR z = XMVectorSet( 0, 0, .25 ,1 );
		XMVECTOR zero = XMVectorSet(0, 0, 0, 1);

		XMMATRIX view = dx::View2;
		view.r[3] = XMVectorSet(0, 0, 0, 1);
		XMMATRIX m = XMMatrixMultiply(view, dx::Projection);

		x = XMVector4Transform(x, m);
		y = XMVector4Transform(y, m);
		z = XMVector4Transform(z, m);
		zero = XMVector4Transform(zero, m);

		XMFLOAT2 pX = XMFLOAT2(XMVectorGetX(x), XMVectorGetY(x));
		XMFLOAT2 pY = XMFLOAT2(XMVectorGetX(y), XMVectorGetY(y));
		XMFLOAT2 pZ = XMFLOAT2(XMVectorGetX(z), XMVectorGetY(z));
		XMFLOAT2 pZero = XMFLOAT2(XMVectorGetX(zero), XMVectorGetY(zero));

		pX.x *= dx::iaspect;
		pY.x *= dx::iaspect;
		pZ.x *= dx::iaspect;
		pZero.x *= dx::iaspect;

		dxui::String(pX.x, pX.y, letterWidth, elementHeight, colorScheme.font, "X");
		dxui::String(pY.x, pY.y, letterWidth, elementHeight, colorScheme.font, "Y");
		dxui::String(pZ.x, pZ.y, letterWidth, elementHeight, colorScheme.font, "Z");

		dxui::lineVertices[0].Pos.x = pZero.x;
		dxui::lineVertices[0].Pos.y = pZero.y;
		dxui::lineVertices[0].Pos.z = 0;
		dxui::lineVertices[1].Pos.x = pX.x;
		dxui::lineVertices[1].Pos.y = pX.y;
		dxui::lineVertices[1].Pos.z = 0;

		dxui::lineVertices[2].Pos.x = pZero.x;
		dxui::lineVertices[2].Pos.y = pZero.y;
		dxui::lineVertices[2].Pos.z = 0;
		dxui::lineVertices[3].Pos.x = pY.x;
		dxui::lineVertices[3].Pos.y = pY.y;
		dxui::lineVertices[3].Pos.z = 0;

		dxui::lineVertices[4].Pos.x = pZero.x;
		dxui::lineVertices[4].Pos.y = pZero.y;
		dxui::lineVertices[4].Pos.z = 0;
		dxui::lineVertices[5].Pos.x = pZ.x;
		dxui::lineVertices[5].Pos.y = pZ.y;
		dxui::lineVertices[5].Pos.z = 0;

	dxui::lineVBSet();
	dxui::SetupDrawerline();
	dxui::UpdateLineVb(6);

	dxui::Line(0.f, 0.f, 1, 1.f, colorScheme.font, 6);

	dxui::SetupDrawerBox();

	}

	void ShowStack()
	{

		if (!isKeyDown(VK_LBUTTON))
		{
			dxui::anyElementOver = false;
		}

		dxui::mousePtr = NULL;
		dxui::mouseParam = -1;
		dxui::ClickRoutine = Click;
		ui::insertPointPtr = NULL;

		StackMovement();

		if (timer::GetCounter() > timer::pressedTimer + 50)
		{
			ItemMovement();
		}

		CameraMovement();
		DrawTimeline();	

		Axis3D();

		//AutoScroll();

		channelCounter = 0;

		//
		dxui::SetupDrawerBox();

		float mx = ((2. *dxui:: mousePos.x) / dx::winW - 1) / dx::aspect;
		float m = 4*(mx-ui::MusicTimelineLeftMargin);
		m = max(m, 0);


		dxui::Box(-2, 1,  -(-2 - ui::MusicTimelineLeftMargin), 2, XMFLOAT4(.2, .2, .2, lerp(.9,.0,clamp(m,0,1))), 0.001);

		stepY = elementHeight * 1.25f;
		LevelStepRight = stepY / 2.f;
		
		tx = ui::viewPos.x;
		ty = ui::viewPos.y;
		i = stack::data;
		counter = 0;

		int openLevel = -1;
		prevLevelCmd[0] = *i;

		ChannelPosY = ty;
		//ty = ui::timelinePos.y;
		//ChannelPosY = ui::timelinePos.y;		

		CurrentDrawingChannel = 0;

		TrackOpen = false;

		teY = ui::timelinePos.y;
		teYSize = 0;
		trackOpen = false;
		ChannelH = 0;
		EnvelopeH = 0;

		while (*i != (BYTE)0)
		{
			if (stackViewMode)
			{
				if (CmdDesc[*i].Routine == Music)
				{
					if (GetElementOpenStatus(i))
					{
						teY = ty - elementHeight * 1.2; trackStartY = teY;
						ChannelPosY = teY;
						trackOpen = true;
					}
					else
					{
						teY = ty - elementHeight;
					}
				}

				if (CmdDesc[*i].Routine == EndMusic)
				{
					if (trackOpen)
					{
					//	ty = teY - teYSize;
						trackEndY = teY;
					}
					else
					{
					//	ty = teY;
					}
				}
			}
			HiddenFlag = false;
			DoCmdDraw();

			//skip closed
			int _Level = Level;
			if (Open == 0&& CmdDesc[*i].Level > _Level )
			{
				while (*i != (BYTE)0 && CmdDesc[*i].Level>_Level)
				{
					HiddenFlag = true;

					if (CmdDesc[*i].Category == CAT_SOUND && !TrackOpen)
					{
						i += CmdDesc[*i].Size;
						counter++;
					}
					else
					{
						DoCmdDraw();
					}
				}

			}

		}
		
		if (isKeyDown(VK_MENU))
		{
			strcpy(pressedKey, "Alt ");
			timer::pressedKeyTimeStamp = (float)timer::GetCounter();
		}

		showMainMenu();
		showViewController();

		if (CmdDesc[*(stack::data + ui::CurrentPos)].ParamDraw != NULL)
		{
			CmdDesc[*(stack::data + ui::CurrentPos)].ParamDraw();
			//ShowCmdParams();
		}
		else
		{
			ShowCmdParams();
		}

//		dxui::String(-1.6, .9, ui::elementHeight, ui::elementHeight, XMFLOAT4(1, 1, 1, 1. - clamp(.1*(timer::GetCounter() - timer::pressedKeyTimeStamp),0,1)), st);

		
/*
		if ((timer::GetCounter() - timer::pressedKeyTimeStamp) < 1000)
		{
			float alpha = 1. - clamp(.001*(timer::GetCounter() - timer::pressedKeyTimeStamp), 0, 1);
			dxui::String(-1.9, .95, ui::elementHeight, ui::elementHeight, XMFLOAT4(1, 1, 1, alpha), pressedKey);
		}
		else
		{
			strcpy(pressedKey, " ");
		}

		if ((timer::GetCounter() - timer::pressedMKeyTimeStamp) < 1000)
		{
			float alpha = 1. - clamp(.001*(timer::GetCounter() - timer::pressedMKeyTimeStamp), 0, 1);
			dxui::String(-1.9, .9, ui::elementHeight, ui::elementHeight, XMFLOAT4(1, 1, 1, alpha), pressedMKey);
		}
		*/

		//if ((timer::GetCounter() - timer::pressedKeyTimeStamp) < 1000)
	/*	{
			//float alpha = 1. - clamp(.001 * (timer::GetCounter() - timer::pressedKeyTimeStamp), 0, 1);

		if (logStr)
			{
				char str[] = "Test";
				float mY = .8;
				float len = dxui::StringLen(ui::letterWidth, logStr);
				dxui::Box(-len, mY, len * 2, ui::elementHeight * 3., XMFLOAT4(.2, .2, .2, .9), 0.1);


				dxui::String(-len / 2., mY - ui::elementHeight, ui::letterWidth, ui::letterWidth, XMFLOAT4(1, 1, 1, 1), str);
			}
		}*/


		if (!dxui::anyElementOver&&!isKeyDown(VK_LBUTTON))
		{
			dxui::cursorID = 0;
		}
	}

	void ClickCommand()
	{

	}

	void ClickMainMenu()
	{

	}

	void ClickViewController()
	{
		noteViewMode = ui::selectedParamID;
	}
	
	void ClickProjectMenu()
	{
		ui::ProcessProjectMenu(ui::selectedParamID);
	}

	void ClickInsertCmd()
	{
		ui::InsertCmd(ui::selectedParamID);
		io::WriteUndoStage(ui::CurrentPos);
	}

	void ClickAutomationContext()
	{
		SelectedEnv = -1;
	}

	void ClickSendToEnv()
	{
	}

	void ClickSelectEnv()
	{
		BYTE* ptr = stack::data;
		BYTE* parentPtr = NULL;

		while (*ptr != 0 && ptr < ui::selectedElementPtr)
		{
			if (GetCmdLevel(ptr) == GetCmdLevel(ui::selectedElementPtr) - 1)
			{
				parentPtr = ptr;
			}
			ptr += CmdDesc[*ptr].Size;
		}
		SetElementOpenStatus(parentPtr, 1);
		SetElementOpenStatus(ui::selectedElementPtr, 1-ui::GetElementOpenStatus(ui::selectedElementPtr));

	/*	if (SelectedEnv == ui::selectedParamID)
		{
			SelectedEnv = -1;
		}
		else
		{
			SelectedEnv = ui::selectedParamID;
		}*/
		ui::selectedParamID = -1;

		io::WriteUndoStage(ui::CurrentPos);
	}

	void ClickAddAutomation()
	{
		int t = CurrentPos;

		if (ui::selectedParamID <=1)
		{
			ui::InsertCmd(ui::SearchCmdByRoutine(Commands::Envelope));
			Envelope_* in = (Envelope_*)(stack::data+t + CmdDesc[*(stack::data + t)].Size + 1);
			in->assignTo = ui::prevSelectedParamID;
			BYTE cmd = *(stack::data + t);
			in->max = CmdDesc[cmd].ParamMax[ui::prevSelectedParamID];
			in->min = CmdDesc[cmd].ParamMin[ui::prevSelectedParamID];
			in->link = 1;
			in->x = 0;
			in->y = in->max;
			in->mixOp = 0;
			in->variation = 0;

			if (ui::selectedParamID == 1)
			{
				in->link = 0;
			}

			CurrentPos = t + CmdDesc[*(stack::data + t)].Size;//ptr to Env
			int m = CurrentPos;

			ui::InsertCmd(ui::SearchCmdByRoutine(Commands::Point));
			m+= CmdDesc[*(stack::data + m)].Size;
			Point_* inP = (Point_*)(stack::data + m + 1);
			inP->x = in->x + 10000;
			inP->y = in->min;
			
			CurrentPos = t;
			io::WriteUndoStage(CurrentPos);
		//	SelectedEnv=ui::prevSelectedParamID;
		}

		if (ui::selectedParamID == 2) 
		{
			ui::InsertCmd(ui::SearchCmdByRoutine(Commands::SendToEnv));
			SendToEnv_* in = (SendToEnv_*)(stack::data + t + CmdDesc[*(stack::data + t)].Size + 1);
			in->assignTo = ui::prevSelectedParamID;
			CurrentPos = t;
			io::WriteUndoStage(CurrentPos);
		}

		ui::selectedParamID = -1;
	}

	void ClickPointWidget()
	{
		int v = ui::ReadNumericValue(ui::selectedElementPtr, ui::selectedParamID);
		_itoa(v, ui::editBoxText, 10);
		ui::textCursor = strlen(ui::editBoxText);
		ui::textCursorX = dxui::StringLen(ui::letterWidth, ui::editBoxText);
	}

	void ClickNum()
	{
		int v = ui::ReadNumericValue(ui::selectedElementPtr, ui::selectedParamID);
		_itoa(v, ui::editBoxText, 10);
		ui::textCursor = strlen(ui::editBoxText);
		ui::textCursorX = dxui::StringLen(ui::letterWidth, ui::editBoxText);
	}

	void ClickParam()
	{
		int type = ui::GetCmdType(ui::selectedElementPtr, ui::selectedParamID);

		if (type == PT_BYTE || type == PT_SBYTE || type == PT_WORD || type == PT_SWORD || type == PT_INT)
		{
			int v = ui::ReadNumericValue(ui::selectedElementPtr, ui::selectedParamID);
			_itoa(v, ui::editBoxText, 10);
			ui::textCursor = strlen(ui::editBoxText);
			ui::textCursorX = dxui::StringLen(ui::letterWidth, ui::editBoxText);
		}

		if (type == PT_LABEL)
		{
			BYTE cmd = *ui::selectedElementPtr;
			int offset = CmdDesc[cmd].ParamOffset[ui::selectedParamID];
			strcpy(ui::editBoxText, (char*)(ui::selectedElementPtr + offset));

			ui::textCursor = strlen(ui::editBoxText);
			ui::textCursorX = dxui::StringLen(ui::letterWidth, ui::editBoxText);
		}

		if (type == PT_TEXT)
		{
			BYTE cmd = *ui::selectedElementPtr;
			int type = CmdDesc[cmd].ParamType[ui::selectedParamID];
			int offset = CmdDesc[cmd].ParamOffset[ui::selectedParamID];

			if (CmdDesc[cmd].Routine == Commands::CreateShader)
			{
				ui::EditShader();
			}
			else
			{
				ui::TextEditorActivePtr = ui::selectedElementPtr + CmdDesc[cmd].ParamOffset[ui::selectedParamID];
				ui::TextEditorActiveMaxSize = TypeSizeTable[CmdDesc[cmd].ParamType[ui::selectedParamID]] - 1;
				ui::TextEditorActiveCmd = cmd;
				ShowWindow(ui::TEditor, SW_SHOW);
				SetWindowText(ui::TE_edit, (LPCSTR)ui::TextEditorActivePtr);
			}
		}

		if (type == PT_ENUM)
		{
			BYTE cmd = *ui::selectedElementPtr;
			int p = 0;
			while (*(CmdDesc[cmd].ParamEnum[ui::selectedParamID][p]) != 0)
			{
				p++;
			}
			if (p == 2)
			{
				BYTE v = *(ui::selectedElementPtr + CmdDesc[cmd].ParamOffset[ui::selectedParamID]);
				v++;
				if (v>1) v =0;
				*(ui::selectedElementPtr + CmdDesc[cmd].ParamOffset[ui::selectedParamID]) = v;
			}
		}
	
	}

	void ClickParamContextMenu()
	{
		BYTE newvalue = ui::selectedParamID;
		ui::WriteValue(ui::selectedElementPtr, ui::prevSelectedParamID, newvalue);
		ui::selectedParamID = ui::prevSelectedParamID;
		io::WriteUndoStage(CurrentPos);
	}


	void Click()
	{
		int i = 0;
		
		if (isKeyDown(VK_CONTROL)) i = 1;
		if (isKeyDown(VK_SHIFT)) i = 2;
		ui::Select(i);

		if (ui::selectedParamID < 0)
		{
			ui::SelectedEnv = -1;
		}
	}

	void ClickSlider()
	{
	}

	void ClickPoint()
	{
		int i = 0;
		if (GetAsyncKeyState(VK_CONTROL)) i = 1;
		//if (GetAsyncKeyState(MK_SHIFT)) i = 2;

		if (i==0) removeSelection();

		BYTE selectFlag = *(selectedElementPtr + CmdDesc[*selectedElementPtr].Size - EditorInfoOffset + EditorInfoOffsetSelected);
		selectFlag = 1 - selectFlag;
		*(selectedElementPtr + CmdDesc[*selectedElementPtr].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)selectFlag;
	}

	void ClickEnv()
	{
		int i = 0;
		if (GetAsyncKeyState(VK_CONTROL)) i = 1;
		//if (GetAsyncKeyState(MK_SHIFT)) i = 2;

		if (i == 0) removeSelection();

		BYTE selectFlag = *(selectedElementPtr + CmdDesc[*selectedElementPtr].Size - EditorInfoOffset + EditorInfoOffsetSelected);
		selectFlag = 1 - selectFlag;
		*(selectedElementPtr + CmdDesc[*selectedElementPtr].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)selectFlag;
	}

}