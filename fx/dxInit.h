using namespace DirectX; 

namespace dx
{

	void CreateTempTexture();
	void CreateConstBuf();
	void SetTextureOptions();

	int width; int height;
	int winW, winH;
	float winAspect, invWinAspect;
	float aspect;
	float iaspect;
	char* TextureTempMem;

	D3D_DRIVER_TYPE         g_driverType = D3D_DRIVER_TYPE_NULL;
	ID3D11Device*           g_pd3dDevice = NULL;
	ID3D11DeviceContext*    g_pImmediateContext = NULL;
	IDXGISwapChain*         g_pSwapChain = NULL;
	ID3D11RenderTargetView* g_pRenderTargetView = NULL;
	int currentRTn = -1;

	ID3D11Texture2D*        g_pDepthStencil = NULL;

	ID3D11DepthStencilView* g_pDepthStencilView = NULL;
	ID3D11ShaderResourceView* g_pDepthStencilSRView = NULL;
	ID3D11DepthStencilState * pDSState[4];
	
	ID3D11BlendState* bs[5*4];

	XMMATRIX View2;
	XMMATRIX Projection;

	XMMATRIX DepthView;
	XMMATRIX DepthProjection;

	XMMATRIX CameraView;
	XMMATRIX CameraProjection;

#ifdef EditMode
	XMMATRIX tempView;
#endif

	

	void InitCamera()
	{
		XMVECTOR Eye = XMVectorSet(0.0f, 0.0f, -1.0f, 0.0f);
		XMVECTOR At = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
		XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
		dx::View2 = XMMatrixLookAtLH(Eye, At, Up);
		Projection = XMMatrixPerspectiveFovLH(XM_PIDIV4, width / (FLOAT)height, 0.01f, 100.0f);
		dx::DepthView = View2;
		dx::DepthProjection = Projection;

		XMMATRIX x1 = XMMatrixIdentity();
		XMMATRIX x2 = XMMatrixTranslation(0, 0, 1);
		x1 = XMMatrixMultiply(x1, x2);
		dx::View2 = x1;
	}

	void InitBlend()
	{

	}

	void InitDepth()
	{
		D3D11_TEXTURE2D_DESC descDepth;
		ZeroMemory(&descDepth, sizeof(descDepth));
		descDepth.Width = winW;
		descDepth.Height = winH;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_R32_TYPELESS;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;

		HRESULT hr = g_pd3dDevice->CreateTexture2D(&descDepth, NULL, &g_pDepthStencil);
		if (FAILED(hr))
		{
			MessageBox(hWnd, "depth texture error", "dx11error", MB_OK);
			return;
		}

		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
		ZeroMemory(&descDSV, sizeof(descDSV));
		descDSV.Format = DXGI_FORMAT_D32_FLOAT;
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSV.Texture2D.MipSlice = 0;
		descDSV.Flags = 0;

		hr = g_pd3dDevice->CreateDepthStencilView(g_pDepthStencil, &descDSV, &g_pDepthStencilView);
		if (FAILED(hr))
		{
			MessageBox(hWnd, "depthview error", "dx11error", MB_OK);
			return;
		}

		D3D11_SHADER_RESOURCE_VIEW_DESC sr_desc;
		sr_desc.Format = DXGI_FORMAT_R32_FLOAT;
		sr_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		sr_desc.Texture2D.MostDetailedMip = 0;
		sr_desc.Texture2D.MipLevels = 1;

		hr = g_pd3dDevice->CreateShaderResourceView(g_pDepthStencil, &sr_desc, &g_pDepthStencilSRView);
		if (FAILED(hr))
		{
			MessageBox(hWnd, "depth shader resview error", "dx11error", MB_OK);
			return;
		}

		g_pImmediateContext->OMSetRenderTargets(1, &g_pRenderTargetView, g_pDepthStencilView);


		D3D11_DEPTH_STENCIL_DESC dsDesc;
		// Depth test parameters
		dsDesc.DepthEnable = false;
		dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

		// Stencil test parameters
		dsDesc.StencilEnable = true;
		dsDesc.StencilReadMask = 0xFF;
		dsDesc.StencilWriteMask = 0xFF;

		// Stencil operations if pixel is front-facing
		dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		// Stencil operations if pixel is back-facing 
		dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		// Create depth stencil state
		dsDesc.StencilEnable = false;

		dsDesc.DepthEnable = false;
		g_pd3dDevice->CreateDepthStencilState(&dsDesc, &pDSState[0]);//off

		dsDesc.DepthEnable = true;
		dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		g_pd3dDevice->CreateDepthStencilState(&dsDesc, &pDSState[1]);//read & write

		dsDesc.DepthEnable = true;
		dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		g_pd3dDevice->CreateDepthStencilState(&dsDesc, &pDSState[2]);//read

		dsDesc.DepthEnable = false;
		dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		g_pd3dDevice->CreateDepthStencilState(&dsDesc, &pDSState[3]);//write
	}

	void InitViewport()
	{
		D3D11_VIEWPORT vp;
		vp.Width = (FLOAT)winW;
		vp.Height = (FLOAT)winH;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		g_pImmediateContext->RSSetViewports(1, &vp);
	}

	ID3D11RasterizerState * g_pRasterState[3];

	void InitRasterizer()
	{


		D3D11_RASTERIZER_DESC rasterizerState;
		rasterizerState.FillMode = D3D11_FILL_SOLID;
		rasterizerState.CullMode = D3D11_CULL_NONE;
		rasterizerState.FrontCounterClockwise = true;
		rasterizerState.DepthBias = false;
		rasterizerState.DepthBiasClamp = 0;
		rasterizerState.SlopeScaledDepthBias = 0;
		rasterizerState.DepthClipEnable = false;
		rasterizerState.ScissorEnable = false;
		rasterizerState.MultisampleEnable = false;
		rasterizerState.AntialiasedLineEnable = true;
		g_pd3dDevice->CreateRasterizerState(&rasterizerState, &g_pRasterState[0]);

		rasterizerState.CullMode = D3D11_CULL_FRONT;
		g_pd3dDevice->CreateRasterizerState(&rasterizerState, &g_pRasterState[1]);

		rasterizerState.CullMode = D3D11_CULL_BACK;
		g_pd3dDevice->CreateRasterizerState(&rasterizerState, &g_pRasterState[2]);

		g_pImmediateContext->RSSetState(g_pRasterState[0]);

		D3D11_BLEND_DESC bSDesc;
		ZeroMemory(&bSDesc, sizeof(D3D11_BLEND_DESC));
		bSDesc.RenderTarget[0].BlendEnable = FALSE;
		bSDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
		bSDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		bSDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;


		//all additional rt's without alphablend, as blend=0 mode
		bSDesc.IndependentBlendEnable = true;
		for (int x = 1; x < 8; x++)
		{
			bSDesc.RenderTarget[x] = bSDesc.RenderTarget[0];
			bSDesc.RenderTarget[x].BlendEnable = false;
		}
		
		bSDesc.AlphaToCoverageEnable = false;
		//bSDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		//bSDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		for (int i = 0; i < 5; i++)
		{
			bSDesc.RenderTarget[0].BlendOp = (D3D11_BLEND_OP)(i + 1);
			g_pd3dDevice->CreateBlendState(&bSDesc, &bs[i]);
		}

		bSDesc.RenderTarget[0].BlendEnable = TRUE;
		bSDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		bSDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;

		for (int i = 0; i < 5; i++)
		{
			bSDesc.RenderTarget[0].BlendOp = (D3D11_BLEND_OP)(i + 1);
			g_pd3dDevice->CreateBlendState(&bSDesc, &bs[i + 5]);
		}

		bSDesc.RenderTarget[0].BlendEnable = TRUE;
		bSDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		bSDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		for (int i = 0; i < 5; i++)
		{
			bSDesc.RenderTarget[0].BlendOp = (D3D11_BLEND_OP)(i + 1);
			g_pd3dDevice->CreateBlendState(&bSDesc, &bs[i + 5 * 2]);
		}

		bSDesc.RenderTarget[0].BlendEnable = FALSE;
		bSDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		bSDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
		bSDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
		bSDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		bSDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		for (int i = 0; i < 5; i++)
		{
			bSDesc.RenderTarget[0].BlendOp = (D3D11_BLEND_OP)(i + 1);
			g_pd3dDevice->CreateBlendState(&bSDesc, &bs[i + 5 * 3]);
		}

	}

	HRESULT InitDevice()
	{
		HRESULT hr = S_OK; 

		RECT rc;
		HMONITOR monitor = MonitorFromWindow(hWnd, MONITOR_DEFAULTTONEAREST);
		MONITORINFO info;
		info.cbSize = sizeof(MONITORINFO);
		GetMonitorInfo(monitor, &info);
		int monitor_width = info.rcMonitor.right - info.rcMonitor.left;
		int monitor_height = info.rcMonitor.bottom - info.rcMonitor.top;
		width = monitor_width;
		height = monitor_height;
		aspect = float(height) / float(width);
		iaspect = float(width) / float(height);
#ifdef EditMode
		GetClientRect(hWnd, &rc);
		winW = rc.right - rc.left;
		winH = rc.bottom - rc.top;
#else
		winW = width;
		winH = height;
#endif
		winAspect = float(winH) / float(winW);
		invWinAspect = float(winW) / float(winH);


		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.BufferCount = 1;
		sd.BufferDesc.Width = winW;
		sd.BufferDesc.Height = winH;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = hWnd;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
#ifdef EditMode
		sd.Windowed = TRUE;
#else
		sd.Windowed = FALSE;
#endif
		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_1,
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
		};

		UINT numFeatureLevels = ARRAYSIZE(featureLevels);
		UINT flags = 0;
		//flags = D3D11_CREATE_DEVICE_DEBUG;
		hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, featureLevels, numFeatureLevels, D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, NULL, &g_pImmediateContext);
		if (FAILED(hr)) 
		{
			MessageBox(hWnd, "device not created", "dx11error", MB_OK); 
			return S_FALSE; 
		}

		ID3D11Texture2D* pBackBuffer = NULL;
		hr = g_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
		if (FAILED(hr)) 
		{ 
			MessageBox(hWnd, "swapchain error", "dx11error", MB_OK); 
			return S_FALSE; 
		}

		hr = g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &g_pRenderTargetView);
		if (FAILED(hr)) 
		{ 
			MessageBox(hWnd, "rt not created", "dx11error", MB_OK); 
			return S_FALSE; 
		}

		pBackBuffer->Release();

		InitViewport();
		
		InitRasterizer();

		InitDepth();

		CreateConstBuf();

		SetTextureOptions();
		InitCamera();		

		TextureTempMem = (char *)malloc(2048 * 2048 * 4 * 2);
		CreateTempTexture();

		return S_OK;
	}

	void CleanupDevice()
	{
		if (g_pImmediateContext) g_pImmediateContext->ClearState();
		if (g_pRenderTargetView) g_pRenderTargetView->Release();
		if (g_pSwapChain) g_pSwapChain->Release();
		if (g_pImmediateContext) g_pImmediateContext->Release();
		if (g_pd3dDevice) g_pd3dDevice->Release();
	}

	//-----------------------------------------
	//shaders support

	typedef struct {
		ID3D11VertexShader*     pVs;
		ID3D11PixelShader*      pPs;
		ID3DBlob*				pVSBlob;
		ID3DBlob*				pPSBlob;
	} shaderStruct;

	shaderStruct Shader[255];

	struct Vertex
	{
		XMFLOAT3 Pos;
		XMFLOAT2 Tex;
		XMFLOAT2 Tex1;
	};

	struct cbObjectInfo
	{
		XMFLOAT4 Pos;
		XMFLOAT4 info;//lodLevel, index, gX,gY
	};

	struct ConstantBuffer
	{
		XMMATRIX Model;
		XMMATRIX View;
		XMMATRIX iView;
		XMMATRIX Proj;
		XMFLOAT4 aspectRatio;
		XMFLOAT4 time;
		XMFLOAT4 DepthViewVector;
		XMMATRIX iProj;
		XMMATRIX iVP;
		XMMATRIX DepthView;
		XMMATRIX DepthProj;
		XMMATRIX iDepthVP;
		XMFLOAT4 lodLevel;
		XMFLOAT4 selected;
	};

	struct ParamConstBuffer
	{
		float p[16];
		XMMATRIX Model;
		XMMATRIX Normal;
	};

	ID3D11Buffer*			ppVB;
	ID3D11InputLayout*      ppLayout;

	ID3D11InputLayout*      pVertexLayout[256];
	ID3D11Buffer*			pVertexBuffer[256];

	ID3D11Buffer*           pConstantBufferV;
	ID3D11Buffer*           pConstantBufferP;
	
	ID3D11Texture2D *pTexture[256];
	int pTextureType[256];
	ID3D11Texture2D* pDepth[256]; 

	#define tempTexCount 7*10
	ID3D11Texture2D *temptexture[tempTexCount];
	ID3D11Texture2D *tempdepth[tempTexCount];
	ID3D11ShaderResourceView* TempTextureResView[tempTexCount];
	ID3D11ShaderResourceView* TempDepthResView[tempTexCount];

	ID3D11ShaderResourceView* TextureResView[256];
	ID3D11RenderTargetView* RenderTargetView[256][16][6];
	ID3D11DepthStencilView* DepthStencilView[256][16];
	ID3D11ShaderResourceView* DepthResView[256];

	ID3D11RenderTargetView* RenderTargetViewCluster[8];

	int RenderTargetViewSlice[256];
	XMFLOAT2 Tsize[256];
	BYTE TFormat[256];
	BYTE RTStageColorIndex[256];
	BYTE RTStageDepthIndex[256];
	ID3D11SamplerState*       pSampler[9];
	
	ID3DBlob* pErrorBlob;

	int roundUp(int n, int r)
	{
		return 	n - (n % r) + r;
	}

	void CreateConstBuf()
	{
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = roundUp(sizeof(ConstantBuffer), 16);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.StructureByteStride = 16;

		HRESULT hr = g_pd3dDevice->CreateBuffer(&bd, NULL, &pConstantBufferV);
		if (FAILED(hr)) 
		{
			SetWindowText(hWnd, "constant bufferV fail");  
			return; 
		}

		bd.ByteWidth = roundUp(sizeof(ConstantBuffer), 16);
		hr = g_pd3dDevice->CreateBuffer(&bd, NULL, &pConstantBufferP);
		if (FAILED(hr)) 
		{ 
			SetWindowText(hWnd, "constant bufferP fail");  
			return; 
		}
	}
	
	int VBsize[256];

	void SetTextureOptions()
	{
		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD =  D3D11_FLOAT32_MAX;

		HRESULT h=dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &dx::pSampler[0]);
		
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		h = dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &dx::pSampler[1]);

		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		h = dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &dx::pSampler[2]);

		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		h = dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &dx::pSampler[3]);

		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = 0;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		h = dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &dx::pSampler[4]);

		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		h = dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &dx::pSampler[5]);

		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		h = dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &dx::pSampler[6]);

		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		h = dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &dx::pSampler[7]);

		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
		for (int x=0;x<4;x++) sampDesc.BorderColor[x]=0;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_LESS;
		sampDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
		h = dx::g_pd3dDevice->CreateSamplerState(&sampDesc, &dx::pSampler[8]);

	}

	void CreateVB(int n,int size)
	{
		return;
		
		//todo: data transfer
		/*
		VBsize[n] = size*6;

		HRESULT hr = S_OK;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(Vertex) * p_count * 6;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = vertices;

		if (pVertexBuffer[n] != NULL) pVertexBuffer[n]->Release();
		hr = g_pd3dDevice->CreateBuffer(&bd, &InitData, &pVertexBuffer[n]);
		*/
	}

	// color (format 0-3 (pot 0-7 screen half quater)) 
	// depth (pot 0-7 screen half quater)

	void CreateTempTexture()
	{
		int mip = 0;
		int size = 32;

		D3D11_TEXTURE2D_DESC tdesc;
		D3D11_SUBRESOURCE_DATA tbsd;

		tbsd.pSysMem = (void *)dx::TextureTempMem;
		tbsd.SysMemPitch = size * 4;
		tbsd.SysMemSlicePitch = size * size * 4;

		tdesc.Width = size;
		tdesc.Height = size;

		tdesc.SampleDesc.Count = 1;
		tdesc.SampleDesc.Quality = 0;
		tdesc.Usage = D3D11_USAGE_DEFAULT;

		tdesc.CPUAccessFlags = 0;

		tdesc.MipLevels = 0;
		tdesc.ArraySize = 1;
		tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
		tdesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

		D3D11_SHADER_RESOURCE_VIEW_DESC svDesc;		

		int cnt = 0;
		for (int f = 0; f < 4; f++)
		{
			switch (f)
			{
			case 0:tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; break;
			case 1:tdesc.Format = DXGI_FORMAT_R8G8B8A8_SNORM; break;
			case 2:tdesc.Format = DXGI_FORMAT_R16G16B16A16_FLOAT; break;
			case 3:tdesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT; break;
			}

			size = 32;
			for (int x = 0; x < 10; x++)
			{
				tdesc.MipLevels = 0;
				tdesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;

				svDesc.Texture2D.MipLevels = -1;
				svDesc.Texture2D.MostDetailedMip = 0;
				svDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

				tdesc.Width = size;
				tdesc.Height = size;

				if (x >= 7)
				{
					int den[3] = { 1, 2, 4 };
					int i = x - 7;
					tdesc.Width = winW/den[i];	tdesc.Height = winH/den[i];
					tdesc.MipLevels = 1;
					tdesc.MiscFlags = 0;
					svDesc.Texture2D.MipLevels = 1;
				}

				svDesc.Format = tdesc.Format;

				HRESULT h = dx::g_pd3dDevice->CreateTexture2D(&tdesc, NULL , &dx::temptexture[cnt]);
				h = dx::g_pd3dDevice->CreateShaderResourceView(dx::temptexture[cnt], &svDesc, &dx::TempTextureResView[cnt]);

				size *= 2;
				cnt++;
			}
		}

		size = 32;
		D3D11_TEXTURE2D_DESC descDepth;
		ZeroMemory(&descDepth, sizeof(descDepth));
		descDepth.MipLevels = 0;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_R32_TYPELESS;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;

		D3D11_SHADER_RESOURCE_VIEW_DESC svDescDepth;
		svDescDepth.Format = DXGI_FORMAT_R32_FLOAT;
		svDescDepth.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		svDescDepth.Texture2D.MostDetailedMip = 0;
		svDescDepth.Texture2D.MipLevels = 1;
		
		for (int x = 0; x < 10; x++)
		{
			descDepth.Width = size;
			descDepth.Height = size;
			descDepth.MipLevels = 0;
			if (x >= 7)
			{
				int den[3] = { 1, 2, 4 };
				int i = x - 7;
				descDepth.Width = winW / den[i];	
				descDepth.Height = winH / den[i];
				descDepth.MipLevels = 1;
			}

			HRESULT hr = dx::g_pd3dDevice->CreateTexture2D(&descDepth, NULL, &dx::tempdepth[x]);
			hr = dx::g_pd3dDevice->CreateShaderResourceView(dx::tempdepth[x], &svDescDepth, &dx::TempDepthResView[x]);

			size *= 2;
		}

	}

}