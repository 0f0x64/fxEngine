#include <string.h>

namespace io
{
	char name[1000];	
	char startDir[1000];
	char undopath[1000];

	int UndoNum = -1;
	int UndoCursor = 0;
	int maxUndo = 0;

	#include <shellapi.h>

	void silently_remove_directory(LPCTSTR dir)
	{
		SHFILEOPSTRUCT file_op = {
			NULL,
			FO_DELETE,
			dir,
			"",
			FOF_NOCONFIRMATION |
			FOF_NOERRORUI |
			FOF_SILENT,
			false,
			0,
			"" };
		SHFileOperation(&file_op);
	}

	void SelfLocate()
	{
		char *i;
		char tmpstr[1000];
		
		strcpy(tmpstr, GetCommandLine());
		//strcpy(cdir2, GetCommandLine());//for registry
		i = strstr(tmpstr, ".exe"); while (*(i) != '\\') i--; *(i + 1) = 0;//cut tail
		strcpy(startDir, tmpstr + 1);

		strcpy(undopath, tmpstr + 1);
		strcat(undopath, "undo\\");
		SetCurrentDirectory(startDir);
		silently_remove_directory(undopath);
		CreateDirectory(undopath, NULL);

	}


	void ShowFilename()
	{
		int j;
		for (j = strlen(name) - 1; j >= 0 && name[j] != '\\'; j--) {}
		SetWindowText(hWnd, name + j + 1);
	}

	int readedBytes = 0;

	char cmdName[100];
	char paramName[100];
	char paramValue[100000];

	bool enterDetected = false;
	int readToken(BYTE* in,char &outstr)
	{
		BYTE* j = in;

		if (*j == 0x27)//'
		{
			j++;
			while (*j != 0x27) j++;
			if (j - in > 1)
			{
				memcpy(&outstr, in + 1, j - in - 1);
				int sz = j - in-1;
				*(&outstr + sz) = 0;
				return j - in + 2;
			}
			*(&outstr) = 0;
			return 3;
		}
		
		while (*j != 32) j++;
		memcpy(&outstr, in, j - in);
		int sz = j - in;
		*(&outstr + sz) = 0;
		return j - in + 1;
		
	}

	BYTE SearchCmdByName(char* str)
	{
		BYTE j = 0;
		while (j< stack::CommandsCount && strcmp(str, CmdDesc[j].Name)) j++;

		if (j>=stack::CommandsCount) return 255;

		return j;
	}

	BYTE SearchParamIndexByName(BYTE cmd,char* str)
	{
		BYTE j = 0;
		while (strcmp("", CmdDesc[cmd].ParamName[j])&&strcmp(str, CmdDesc[cmd].ParamName[j])) j++;

		if (!strcmp(CmdDesc[cmd].ParamName[j], ""))
		{
			return 255;
		}

		return j;
	}

	BYTE SearchParamEnumByName(BYTE cmd, char* str, char* enumstr)
	{
		BYTE j = 0;
		while (strcmp("", CmdDesc[cmd].ParamName[j]) && strcmp(str, CmdDesc[cmd].ParamName[j])) j++;

		if (!strcmp(CmdDesc[cmd].ParamName[j], "")) return 255;

		BYTE i = 0;
		while (strcmp("", CmdDesc[cmd].ParamEnum[j][i]) && strcmp(enumstr, CmdDesc[cmd].ParamEnum[j][i])) i++;

		if (!strcmp(CmdDesc[cmd].ParamEnum[j][i], "")) return 255;

		return i;
	}

	void ResetCustomNodes()
	{

		for (int c = 0; c < stack::CommandsCount; c++)
		{
			if (CmdDesc[c].Routine == NullDrawer)
			{
				for (int x = 3; x < MAXPARAM; x++)
				{
					char cnt[10]; _itoa(x, cnt, 10);
					strcpy(CmdDesc[c].ParamName[x], "p");
					strcat(CmdDesc[c].ParamName[x], cnt);
					CmdDesc[c].ParamType[x] = PT_SWORD;
				}
			}
			if (CmdDesc[c].Routine == SetParams)
			{
				for (int x = 0; x < MAXPARAM; x++)
				{
					char cnt[10]; _itoa(x, cnt, 10);
					strcpy(CmdDesc[c].ParamName[x], "p");
					strcat(CmdDesc[c].ParamName[x], cnt);
					CmdDesc[c].ParamType[x] = PT_SWORD;
				}

			}
			if (CmdDesc[c].Routine == DefineParams)
			{
				for (int x = 2; x < MAXPARAM; x++)
				{
					char cnt[10]; _itoa(x, cnt, 10);
					strcpy(CmdDesc[c].ParamName[x], "p");
					strcat(CmdDesc[c].ParamName[x], cnt);
					CmdDesc[c].ParamType[x] = PT_SWORD;
				}
			}


		}
	}

	void ConvertTextToBinary()
	{

		ResetCustomNodes();

		BYTE* in = stack::textdata;
		BYTE* out = stack::data;

		ZeroMemory(stack::data, MAXDATASIZE);

		int inCnt = 0;

		BYTE prevLevelCmd[10] = { 0,0,0,0,0,0,0,0,0,0 };

		while (inCnt < readedBytes)
		{
			in+= readToken(in, *cmdName);

			BYTE cmdCode = SearchCmdByName(cmdName);			
			
			if (cmdCode != 255)
			{
				*out = cmdCode;
				int x = 0;

				while (*in != 0x0D)
				{
					
					in += readToken(in, *paramName);
					in += readToken(in, *paramValue);

					int pIndex = SearchParamIndexByName(cmdCode, paramName);

					if (pIndex != 255)
					{
						int offset = CmdDesc[cmdCode].ParamOffset[pIndex];
						int type = CmdDesc[cmdCode].ParamType[pIndex];

						int value = 0;
						if (type != PT_LABEL && type != PT_ENUMASSIGN && type != PT_ENUM)
						{
							value = atoi(paramValue);
						}

						switch (type)
						{
						case PT_BYTE:
						{
							*(out + offset) = (BYTE)value;
							break;
						}
						case PT_SBYTE:
						{
							*(signed char*)(out + offset) = (signed char)value;
							break;
						}
						case PT_WORD:
						{
							*(unsigned short*)(out + offset) = (unsigned short)value;
							break;
						}
						case PT_SWORD:
						{
							*(signed short*)(out + offset) = (signed short)value;
							break;
						}
						case PT_INT:
						{
							*(int*)(out + offset) = (int)value;
							break;
						}
						case PT_LABEL:
						{
							memcpy(out + offset, paramValue, TypeSizeTable[type]);
							break;
						}
						case PT_TEXT:
						{
							strncpy((char*)(out + offset), paramValue, TypeSizeTable[type]);
							break;
						}
						case PT_ENUM:
						{
							//*(out + offset) = (BYTE)value;
							//char* n = CmdDesc[cmdCode].Name;
							int aIndex = SearchParamEnumByName(cmdCode, paramName, paramValue);
							if (aIndex != 255)
							{
								*(out + offset) = (BYTE)aIndex;
							}
							else
							{
								MessageBox(hWnd, paramName, "unknown enum", 0);
							}
							break;
						}
						case PT_ENUMASSIGN:
						{
							BYTE parent = prevLevelCmd[CmdDesc[cmdCode].Level];
							char* nn = CmdDesc[parent].Name;
							int aIndex = SearchParamIndexByName(parent, paramValue);
							if (aIndex != 255)
							{
								*(out + offset) = (BYTE)aIndex;
							}
							else
							{
								if (isalnum(paramValue[0]))
								{
									aIndex = atoi(paramValue);
									*(out + offset) = (BYTE)aIndex;
								}
								else
								{
									MessageBox(hWnd, paramName, "unknown assign", 0);
								}
							}

							break;
						}
						}
					}
					else
					{
						MessageBox(hWnd, paramName,"parameter not supported", 0);
					}

					x++;
				}
				
				prevLevelCmd[CmdDesc[cmdCode].Level+1] = cmdCode;

				out += CmdDesc[cmdCode].Size;
			}
			else
			{
				MessageBox(hWnd, cmdName, "command not supported", 0);
				in = (BYTE*)strstr((char*)in, "\r\n");
			}


			in += 2;			
			inCnt = in - stack::textdata;
		}

	}

	void ImportFromText()
	{
		stack::PrecalcStatus = 0; stack::LoopPoint = 0;

		FILE *fp;

		OPENFILENAME ofn;
		char szFile[1000] = "\0";
		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = "fx project\0*.fxs\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

		// Display the Open dialog box. 
		if (GetOpenFileName(&ofn) == TRUE)
		{
			ZeroMemory(stack::data, MAXDATASIZE);

			strcpy(name, ofn.lpstrFile);
			fp = fopen(ofn.lpstrFile, "rb");
			readedBytes = fread(stack::textdata, 1, MAXDATASIZE, fp);
			fclose(fp);
			strcpy(name, ofn.lpstrFile);
			ShowFilename();
		}

		ConvertTextToBinary();
		ZeroMemory(stack::textdata, MAXDATASIZE);
		Commands::loopPoint = stack::data;
	}

	void Load()
	{
		stack::PrecalcStatus = 0; stack::LoopPoint = 0;
		
		FILE *fp;

		OPENFILENAME ofn;
		char szFile[1000] = "\0";
		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = "fx binary\0*.fxb\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

		// Display the Open dialog box. 
		if (GetOpenFileName(&ofn) == TRUE)
		{
			ZeroMemory(stack::data, MAXDATASIZE);

			strcpy(name, ofn.lpstrFile);
			fp = fopen(ofn.lpstrFile, "rb");
			fread(stack::data, 1, MAXDATASIZE, fp);
			fclose(fp);
			strcpy(name, ofn.lpstrFile);
			ShowFilename();	
		}

		Commands::loopPoint = stack::data;
		
	}

	void LoadTemplate()
	{
		if (*startDir!=0) SetCurrentDirectory(startDir);
		FILE *fp;
		fp = fopen("template.fxb", "rb");
		if (fp)
		{
			fread(stack::data, 1, MAXDATASIZE, fp);
			fclose(fp);
			strcpy(name, "untitled.fxb");
			ShowFilename();
		}
	}

	int SearchStackEnd(int cnt)
	{
		while (*(stack::data + cnt) != 0) 
		{
			cnt += CmdDesc[*(stack::data + cnt)].Size;
		}
		return cnt;
	}



	void SaveAs()
	{
		Commands::ClearAllTemporalData();

		int cnt = 0;
		cnt = SearchStackEnd(cnt)+1;

		FILE *fp;
		OPENFILENAME ofn;
		char szFile[1000] = "\0";

		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = "fx binary\0*.fxb\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;

		if (GetSaveFileName(&ofn) == TRUE)
		{
			strcpy(name, ofn.lpstrFile);
			if (NULL == strstr(ofn.lpstrFile, ".fxb")) strcat(name, ".fxb");

			fp = fopen(name, "wb");
			fwrite(stack::data, cnt, 1, fp);
			fclose(fp);
			ShowFilename();
		}
	
	}

	void SaveAsText()
	{
		int cnt = 0;
		cnt = SearchStackEnd(cnt) + 1;

		FILE *fp;
		OPENFILENAME ofn;
		char szFile[1000] = "\0";

		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = "fx project\0*.fxs\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;

		if (GetSaveFileName(&ofn) == TRUE)
		{
			char name_[1000];
			strcpy(name_, ofn.lpstrFile);
			if (NULL == strstr(ofn.lpstrFile, ".fxs")) strcat(name_, ".fxs");

			fp = fopen(name_, "w");

			BYTE* j = stack::data;
			BYTE prevLevelCmd[10] = { 0,0,0,0,0,0,0,0,0,0 };


			ResetCustomNodes();

			while (*j != 0)
			{
				fprintf(fp, CmdDesc[*j].Name);
				fprintf(fp, " ");

				int x = 0;
				while (strcmp(CmdDesc[*j].ParamName[x],""))
					{
						fprintf(fp, CmdDesc[*j].ParamName[x]);
						fprintf(fp, " ");

						char buf[100000];
						int offset = CmdDesc[*j].ParamOffset[x];
						int type = CmdDesc[*j].ParamType[x];
						switch (type)
						{
							case PT_BYTE:
							{
								BYTE v = *(j + offset);
								_itoa(v, buf, 10);
								break;
							}
							case PT_SBYTE:
							{
								signed char v = *(signed char*)(j + offset);
								_itoa(v, buf, 10);
								break;
							}
							case PT_WORD:
							{
								unsigned short v = *(unsigned short*)(j + offset);
								_itoa(v, buf, 10);
								break;
							}
							case PT_SWORD:
							{
								signed short v = *(signed short*)(j + offset);
								_itoa(v, buf, 10);
								break;
							}
							case PT_INT:
							{
								int v = *(int*)(j + offset);
								_itoa(v, buf, 10);
								break;
							}
							case PT_LABEL:
							{
								fprintf(fp, "'");
								strncpy(buf, (char*)(j+offset), TypeSizeTable[type]);
								for (int t = 0; t < TypeSizeTable[type]; t++)
								{
									if (*(BYTE*)(buf+t)==0x27) *(BYTE*)(buf+t) = 32;//"'"
								}
								break;
							}

							case PT_TEXT:
							{
								fprintf(fp, "'");
								strncpy(buf, (char*)(j + offset), TypeSizeTable[type]);

								//cleanup shader code
								
								char* tpt = (char*)(j + offset);
								int dst = 0;
								while (*tpt != 0)
								{
									while (*tpt == '\r')
									{
										tpt++;
									}

									while (*tpt == '\n' && *(tpt + 1) == '\n')
									{
										tpt++;
									}


									while (*tpt == '\r' && *(tpt+1) == '\r')
									{
										tpt++;
									}

									while (*tpt == '\r' && *(tpt + 1) == '\n' && *(tpt+2) == '\r' && *(tpt + 3) == '\n')
									{
										tpt+=2;
									}

									buf[dst] = *tpt;

									tpt++;
									dst++;
								}
								buf[dst] = 0;
								
								break;
							}

							case PT_ENUM:
							{
								BYTE v = *(j + offset);
								//_itoa(v, buf, 10);
								fprintf(fp, "'");
								strcpy(buf, (char*)(CmdDesc[*j].ParamEnum[x][v]));
								
								break;
							}
							case PT_ENUMASSIGN:
							{
								BYTE cLevel = CmdDesc[*j].Level;
								BYTE v = *(j + offset);
								//_itoa(v, buf, 10);
								fprintf(fp, "'");
								if (v < MAXPARAM)
								{
									strcpy(buf, (char*)(CmdDesc[prevLevelCmd[cLevel]].ParamName[v]));
								}
								else
								{
									char nts[10];
									_itoa(v, nts, 10);
									strcpy(buf, (char*)nts);
								}

								break;
							}
						}

						fprintf(fp, buf);

						if (type == PT_LABEL|| type == PT_ENUMASSIGN || type == PT_ENUM || type == PT_TEXT )
						{
							fprintf(fp, "'");
						}

						fprintf(fp, " ");

						x++;
					}

				fprintf(fp, "\n");

				prevLevelCmd[CmdDesc[*j].Level+1] = *j;

				j += CmdDesc[*j].Size;
			}

			fclose(fp);
		}

	}

	void SaveUsedCmdList()
	{
		int cnt = 0;
		cnt = SearchStackEnd(cnt) + 1;

		FILE *fp;
		OPENFILENAME ofn;
		char szFile[1000] = "\0";

		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = "header File\0*.h\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;

		if (GetSaveFileName(&ofn) == TRUE)
		{
			char name_[1000];
			strcpy(name_, ofn.lpstrFile);
			if (NULL == strstr(ofn.lpstrFile, ".h")) strcat(name_, ".h");

			fp = fopen(name_, "w");

				int used[255];
				for (int x = 0; x < 255; x++) used[x] = 0;

				BYTE* j = stack::data; 
			
				while (*j!=0)
				{
					used[*j]++;
					j += CmdDesc[*j].Size;
				}

				for (int x=0;x< stack::CommandsCount;x++)
				{
					if (used[x] > 0)
					{
						fprintf(fp, "#define used_");
						fprintf(fp, CmdDesc[x].Name);
						fprintf(fp, "\n");
					}
				}

			fclose(fp);
		}

	}

	/*
	void SaveAsText()
	{
		int cnt = 0;
		cnt = SearchStackEnd(cnt) + 1;

		FILE *fp;
		OPENFILENAME ofn;
		char szFile[1000] = "\0";

		// Initialize OPENFILENAME
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.lpstrFilter = "header File\0*.h\0";
		ofn.nFilterIndex = 1;
		ofn.lpstrFileTitle = NULL;
		ofn.nMaxFileTitle = 0;
		ofn.lpstrInitialDir = NULL;
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;

		if (GetSaveFileName(&ofn) == TRUE)
		{
			strcpy(name, ofn.lpstrFile);
			if (NULL == strstr(ofn.lpstrFile, ".h")) strcat(name, ".h");

			fp = fopen(name, "w");

			char str[10];
			fprintf(fp, "#define sourceDataSize ");
			_itoa(cnt, str, 10);
			fprintf(fp, str);
			fprintf(fp, "\n");
			fprintf(fp, "\n");
			fprintf(fp, "BYTE sourceData[sourceDataSize] = {");

			BYTE* j = stack::data; int i = 0;
			while (i < cnt)
			{
				BYTE a = *(j + i);
				_itoa(a, str, 10);
				fprintf(fp, str);
				if (i != cnt - 1)
				{
					fprintf(fp, ",");
				}

				if (i%20==0) fprintf(fp, "\n");

				i++;
			}

			fprintf(fp, "};");

			//fwrite(stack::data, cnt, 1, fp);
			fclose(fp);
			ShowFilename();
		}

	}
	*/

	void Save()
	{

		if (strlen(name)<1) 
		{
			SaveAs(); 
			return; 
		}

		Commands::ClearAllTemporalData();

		int cnt = 0;
		cnt = SearchStackEnd(cnt)+1;

		FILE *fp;
		fp = fopen(name, "wb");
		fwrite(stack::data, cnt, 1, fp);
		fclose(fp);
	}

	void WriteUndoStage(int pos)
	{
		int cnt = SearchStackEnd(0)+1;

		*(stack::data+ cnt) = pos;
		cnt += 4;

		FILE *fp;
		char name[100];
		char aa[100];
		UndoNum++;
		_itoa(UndoNum, aa, 10);
		strcpy(name, undopath);
		strcat(name, aa);
		strcat(name, ".fxb");

		fp = fopen(name, "wb");
		fwrite(stack::data, cnt, 1, fp);
		fclose(fp);

		maxUndo = UndoNum;
	}

	int Undo(int pos)
	{
		char name[1000];

		if (UndoNum > 0)
		{
			UndoNum--;
			FILE *fp;

			char aa[100];
			_itoa(UndoNum, aa, 10);
			strcpy(name, undopath);
			strcat(name, aa);
			strcat(name, ".fxb");

			fp = fopen(name, "rb");
			fread(stack::data, 1, MAXDATASIZE, fp);
			fclose(fp);

			int cnt = SearchStackEnd(0)+1;
			int p = *(stack::data + cnt);
			return p;
		}

		return pos;
		
	}

	int Redo(int pos)
	{
		if (UndoNum >= maxUndo) return pos;

		UndoNum++;
		char name[1000];

		FILE *fp;
		char aa[100];
		_itoa(UndoNum, aa, 10);
		strcpy(name, undopath);
		strcat(name, aa);
		strcat(name, ".fxb");

		fp = fopen(name, "rb");
		fread(stack::data, 1, MAXDATASIZE, fp);
		fclose(fp);

		int cnt = SearchStackEnd(0) + 1;
		int p = *(stack::data + cnt);
		return p;

	}
}