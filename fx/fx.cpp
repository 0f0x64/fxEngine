#define EditMode

#define _CRT_SECURE_NO_WARNINGS
#define WINDOWS_IGNORE_PACKING_MISMATCH

#ifdef EditMode
	#include "resource.h"
#endif

#include "windows.h"

HINSTANCE hInst;                               
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);

#ifdef EditMode
	LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
#else
	LRESULT CALLBACK    WndProcPlayer(HWND, UINT, WPARAM, LPARAM);
#endif

HWND hWnd;

#include <d3d11.h>
#include <d3dcompiler.h>
#include "DirectXMath.h"
#include <DirectXPackedVector.h>
#include <Xaudio2.h>

#ifdef EditMode
	#include <stdio.h>
#endif

#define PT_BYTE 0
#define PT_SWORD 1
#define PT_ENUM 2
#define PT_TEXT 3
#define PT_LABEL 4
#define PT_INT 5
#define PT_ENUMASSIGN 6
#define PT_SBYTE 7
#define PT_WORD 8

int TypeSizeTable[9] = { 1,2,1,64000,32,4,1,1,2 };

#ifdef EditMode
	INT32 TypeMinTable[9] = { 0, -32768,0,  0,  0,   INT32_MIN, 0,  -128, 0 };
	INT32 TypeMaxTable[9] = { 255,32767,255,255,255, INT32_MAX, 255, 127, 65535 };
#endif

#include "timer.h"
#include "stack.h"

#define MAXPARAM 32
#define MAXPARAMNAMELEN 32
#define MAXPARAMENUM 255
#define MAXSWITCHERS 8

#include "sound.h"

#include "dxInit.h"
#include "format.h"
#include "commands.h"
#include "cmdInit.h"




#ifdef EditMode

	int midiPort = 0;
	char midiPortName[32];
	int midiPortCount = 0;
	#include "midi/RtMidi.h"
	RtMidiIn *midiin;
	#include "midi/midiInput.h"

	#include "io.h"
	#include "ui_dx.h"

	#include "ui.h"

#endif



int prev_c=0;
float AverageLoad = 0;
float —urrentLoad = 0;

float AverageLoadGPU = 0;
float —urrentLoadGPU = 0;


#ifdef EditMode
	bool lazystart = true;
#else
	bool lazystart = false;
#endif

#ifdef EditMode
	#define GPU_PROFILE
#endif

#ifdef GPU_PROFILE
	D3D11_QUERY_DATA_TIMESTAMP_DISJOINT timeStamp;
	ID3D11Query* freqQuery;
	ID3D11Query* Occlusion;
	ID3D11Query* PipelineStats;
	ID3D11Query* StartTimeQuery;
	ID3D11Query* EndTimeQuery;
	D3D11_QUERY_DESC qDesc;
	bool queryInit = false;
	float GPU_LOAD = 0;
#endif

//bool onLoop = true;

//DWORD WINAPI Loop(CONST LPVOID lpParam) 
void Loop()
{
	//while (onLoop)
	{
		double a = timer::GetCounter();
		timer::fp_delta = a - timer::prevtick;

		int c = (int)(playmode*(a - timer::StartTime) / (1.f / 60.f * 1000.f) + Commands::StartPosition);

		if ((playmode == 0 && (timer::fp_delta) >= (1000.0 / 60.0)) || (playmode == 1 && (prev_c != c)))
		{
			prev_c = c;

			timer::prevtick = a;

			#ifdef GPU_PROFILE
				if (!queryInit)
				{
					qDesc.MiscFlags = 0;

					qDesc.Query = D3D11_QUERY_OCCLUSION;
					dx::g_pd3dDevice->CreateQuery(&qDesc, &Occlusion);
					qDesc.Query = D3D11_QUERY_PIPELINE_STATISTICS;
					dx::g_pd3dDevice->CreateQuery(&qDesc, &PipelineStats);

					qDesc.Query = D3D11_QUERY_TIMESTAMP_DISJOINT;
					dx::g_pd3dDevice->CreateQuery(&qDesc, &freqQuery);
					qDesc.Query = D3D11_QUERY_TIMESTAMP;
					dx::g_pd3dDevice->CreateQuery(&qDesc, &StartTimeQuery);
					dx::g_pd3dDevice->CreateQuery(&qDesc, &EndTimeQuery);


					queryInit = true;
				}

				dx::g_pImmediateContext->Begin(Occlusion);
				dx::g_pImmediateContext->Begin(PipelineStats);
				dx::g_pImmediateContext->Begin(freqQuery);
				dx::g_pImmediateContext->End(StartTimeQuery);
			#endif			

#ifdef EditMode
			dxui::Clear();
			dx::g_pImmediateContext->OMSetDepthStencilState(pDSState[0], 1);
#endif		
			dx::g_pImmediateContext->ClearDepthStencilView(dx::g_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0.0);
			Commands::Render();

			if (!lazystart)
			{
				Commands::PlayWave((int)Commands::StartPosition);
				lazystart = true;
			}

#ifdef EditMode

			dx::g_pImmediateContext->OMSetDepthStencilState(pDSState[0], 1);
			dx::g_pImmediateContext->RSSetState(dx::g_pRasterState[0]);

			ui::ShowStack();

			if (a - ui::logTimer > 1500 && ui::logTimer >= 0)
			{
				io::ShowFilename();
				ui::logTimer = -1;
			}

			char t[220]; char t2[220];
			AverageLoad = lerp(AverageLoad, —urrentLoad, .01f);
			_itoa((int)(100.0f*AverageLoad / 16666.0f), t, 10);
			float sx = .88f / dx::aspect;
			float sy = 1.02-.05f;

			dxui::SetupDrawerBox();
			XMFLOAT4 c2 = XMFLOAT4(AverageLoad / 16666.0f, 1.0f - AverageLoad / 16666.0f, 0, 1.);;
			XMFLOAT4 c1 = colorScheme.loadback;
			float p = AverageLoad / 16666.0f;
			dxui::Box(sx, sy, .2f, ui::elementHeight*1.4*.9,c1,.1,false,false,NULL,-1,NULL,XMFLOAT4(p,0,0,0),c2);

			dxui::SetupDrawer();
			strcpy(t2, "cpu 0  ");
			float fo = dxui::StringLen(ui::elementHeight*.8, t2);
			strcat(t, "%");
			dxui::String(sx + ui::elementHeight*.1 - fo, sy - ui::elementHeight*.2, ui::elementHeight*.8, ui::elementHeight*.8, colorScheme.font, t2);
			dxui::String(sx+ ui::elementHeight*.1, sy- ui::elementHeight*.2, ui::elementHeight*.8, ui::elementHeight*.8, colorScheme.selectedfont, t);

			
			_itoa(Commands::objDrawCallCouter, t, 10);
			strcpy(t2, "draw calls  ");
			strcat(t2, t);
			dxui::String(sx + ui::elementHeight * .1- .4, sy - ui::elementHeight * .2, ui::elementHeight * .8, ui::elementHeight * .8, colorScheme.font, t2);


			_i64toa(Commands::totalPolyCount, t, 10);
			strcpy(t2, "total poly count  ");
			strcat(t2, t);
			dxui::String(sx + ui::elementHeight * .1 - .4, sy - ui::elementHeight * .2 - ui::elementHeight, ui::elementHeight * .8, ui::elementHeight * .8, colorScheme.font, t2);


			#ifdef GPU_PROFILE
				AverageLoadGPU = lerp(AverageLoadGPU, GPU_LOAD, .1f);
				_itoa((int)(100.0f*AverageLoadGPU), t, 10);
				p = AverageLoadGPU;
				sy -= ui::elementHeight*1.4;
				c2 = XMFLOAT4(AverageLoadGPU, 1.0f - AverageLoadGPU, 0, 1.);
				dxui::Box(sx, sy, .2f, ui::elementHeight*1.4*.9, c1, .1, false, false, NULL, -1, NULL, XMFLOAT4(p, 0, 0, 0), c2);
				dxui::SetupDrawer();
				strcpy(t2, "gpu  ");
				fo = dxui::StringLen(ui::elementHeight*.8, t2);
				strcat(t, "%");
				dxui::String(sx + ui::elementHeight*.1 - fo, sy - ui::elementHeight*.2, ui::elementHeight*.8, ui::elementHeight*.8, colorScheme.font, t2);
				dxui::String(sx + ui::elementHeight*.1, sy - ui::elementHeight*.2, ui::elementHeight*.8, ui::elementHeight*.8, colorScheme.selectedfont, t);
			#endif		

#endif
			—urrentLoad = (float)(timer::GetCounter() - a)*1000.0f;

			dx::g_pSwapChain->Present(1, 0);

			#ifdef GPU_PROFILE
				dx::g_pImmediateContext->End(EndTimeQuery);
				dx::g_pImmediateContext->End(freqQuery);
				dx::g_pImmediateContext->End(PipelineStats);
				dx::g_pImmediateContext->End(Occlusion);

				UINT64 StartTime;				
				UINT64 EndTime;
				while (S_OK != dx::g_pImmediateContext->GetData(StartTimeQuery, &StartTime, sizeof(UINT64), 0)) {};
				while (S_OK != dx::g_pImmediateContext->GetData(EndTimeQuery, &EndTime, sizeof(UINT64), 0)) {};
				while (S_OK != dx::g_pImmediateContext->GetData(freqQuery, &timeStamp, 16, 0)) {};

				float msFrame = float(EndTime - StartTime) / float(timeStamp.Frequency) * 1000.0f;
				GPU_LOAD = msFrame/16.6666;
			#endif

		}
	}

	//ExitThread(0);
	
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInstance = (HINSTANCE)GetModuleHandle(0);

    MyRegisterClass(hInstance);

    if (!InitInstance (hInstance, nCmdShow)) return FALSE;

	auto hr=dx::InitDevice();
	cmdInit();
	stack::init();
	Commands::loopPoint = stack::data;

#ifdef EditMode
	io::SelfLocate();
	io::LoadTemplate();
	io::WriteUndoStage(0);
	ui::init();

	ui::removeSelection();
	*(stack::data + CmdDesc[*stack::data].Size - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
	ui::lastSelectedI = 0;
	ui::CurrentPos = 0;

	SetFocus(hWnd);
#endif

	MSG msg = { 0 };

	timer::StartCounter();
	sound::Init();

#ifdef EditMode
	midiSetup();
	std::vector<unsigned char> ms;
#endif 

	//HANDLE hThread = CreateThread(NULL, 0, &Loop, NULL, 0, NULL);

	if (hr == S_OK)
	{
		//SetCursor(NULL);
#ifndef EditMode
		ShowCursor(false);
#endif
		// Main message loop
#ifdef EditMode
		while (WM_QUIT != msg.message)
#else
		while (WM_QUIT != msg.message && !GetAsyncKeyState(VK_ESCAPE) && timer::GetCounter() / 1000 < (60 * 2 + 15))
#endif
		{
			int FRAMES_PER_SECOND = 60;
			DWORD currentTick = GetTickCount();
			DWORD endTick = currentTick + 1000 / FRAMES_PER_SECOND;

				while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)&& msg.message != WM_QUIT)
				{
					#ifdef EditMode
					
					if (!IsDialogMessage(ui::PEditor, &msg))
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}

					#else

						TranslateMessage(&msg);
						DispatchMessage(&msg);				

					#endif		
				}

			#ifdef EditMode
		//		DWORD signal = WaitForSingleObject(ui::shaderCompileInProgressMutex, 0);
		//		if (signal == WAIT_OBJECT_0 || signal == WAIT_FAILED)
			#endif
				{
					#ifdef EditMode
						midiin->getMessage(&message);
					#endif

					Loop();
				}
		}


	}
	
	//onLoop = false;
	//WaitForSingleObject(hThread, 100);
	//CloseHandle(hThread);	

    return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
#ifdef EditMode
	wcex.lpfnWndProc = WndProc;
#else
	wcex.lpfnWndProc = WndProcPlayer;
#endif 
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = NULL;
    wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+2);
	wcex.lpszMenuName	= NULL;
    wcex.lpszClassName  = "fx";
    wcex.hIconSm        = NULL;

    return RegisterClassEx(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow("fx", "fx", WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }
   
   ShowWindow(hWnd, SW_MAXIMIZE);
   UpdateWindow(hWnd);

   return TRUE;
}



LRESULT CALLBACK WndProcPlayer(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CLOSE:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

return 0;
}


WPARAM playnoteKey = 0;

#ifdef EditMode
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{

	case WM_MOUSEMOVE:
		break;

	case WM_SETCURSOR:

		switch (dxui::cursorID)
		{
		case 0:
			SetCursor(LoadCursor(NULL, IDC_ARROW));
			break;
		case 1:
			SetCursor(LoadCursor(NULL, IDC_SIZEWE));
			break;
		case 2:
			SetCursor(LoadCursor(NULL, IDC_SIZEWE));
			break;
		case 3:
			SetCursor(LoadCursor(NULL, IDC_SIZENS));
			break;
		case 4:
			SetCursor(LoadCursor(NULL, IDC_CROSS));
			break;

		}
		break;

	case WM_MOUSEWHEEL:
	{
		WORD fwKeys = GET_KEYSTATE_WPARAM(wParam);
		int zDelta = GET_WHEEL_DELTA_WPARAM(wParam);

		if (zDelta != 0)
		{
			timer::pressedMKeyTimeStamp = (float)timer::GetCounter();
			strcpy(ui::pressedMKey, "mouse wheel");
		}

		if (ui::selectedParamID <1)
		{
			if (ui::isKeyDown(VK_CONTROL))
			{
				float delta = -zDelta / 2000.;
				XMVECTOR scale, rot, trans;
				XMMatrixDecompose(&scale, &rot, &trans, dx::View2);

				if ((delta < 0 && XMVectorGetZ(trans)>.1) || (delta > 0 && XMVectorGetZ(trans) < 5))
				{
					ui::cDistance += delta;
					XMMATRIX t2 = XMMatrixTranslation(0, 0, delta);
					dx::View2 = XMMatrixMultiply(dx::View2, t2);
				}
			}

			if (ui::isKeyDown(VK_SHIFT))
			{
				float delta = -zDelta / 10.;
				ui::cFov += delta;
				ui::cFov = clamp(ui::cFov, 15, 150);

			}
		}

		//if (ui::selectedElementPtr&&ui::selectedParamID>=0&&dxui::ClickRoutine!=ui::ClickNote &&!ui::isKeyDown(VK_MENU))
		if (ui::selectedElementPtr&&ui::selectedParamID >= 0 && 
			//dxui::ClickRoutine != ui::ClickNote &&
			CmdDesc[*ui::selectedElementPtr].Routine != Note &&
			!ui::isKeyDown(VK_MENU))
		{
			//ui::Log(CmdDesc[*ui::selectedElementPtr].Name);
			BYTE cmd = *ui::selectedElementPtr;
			int type = CmdDesc[cmd].ParamType[ui::selectedParamID];
			int offset = CmdDesc[cmd].ParamOffset[ui::selectedParamID];

			if (type == PT_BYTE || type == PT_SBYTE || type == PT_WORD || type == PT_SWORD || type == PT_INT)
			{
				int mul = 1;
				if (ui::isKeyDown(VK_CONTROL)) mul *= 10;
				if (ui::isKeyDown(VK_SHIFT)) mul *= 100;
				int sign = 0;
				if (zDelta > 0) sign = 1;
				if (zDelta < 0) sign = -1;
				INT32 v = ui::ReadNumericValue(ui::selectedElementPtr,ui::selectedParamID);
				v += mul * sign;
				ui::WriteValue(ui::selectedElementPtr,ui::selectedParamID, v);

				v = ui::ReadNumericValue(ui::selectedElementPtr,ui::selectedParamID);
				_itoa(v, ui::editBoxText, 10);

				ui::textCursorX = dxui::StringLen(ui::letterWidth, ui::editBoxText);
			}
		}

		if (zDelta != 0 && ui::isKeyDown(VK_MENU))
		{
			float tlc = (float)((Commands::cursorF)*ui::timelineScale) / (-60.f*100.f);

			float fP = ui::TimeToScreenS16(Commands::cursorF);
			if (fP < ui::MusicTimelineLeftMargin || fP > 2 || playmode == 1)
			{
				tlc = ui::ScreenToTimeS16(0);
				tlc = (float)((tlc)*ui::timelineScale) / (-60.f*100.f);
			}
			

			

			if (zDelta > 0 && ui::timelineScale < maxTimelineScale)
			{
				ui::timelinePos.x += tlc*.2;
				ui::timelineScale *= 1.2f;
				ui::timelineScale = fmin (ui::timelineScale, maxTimelineScale);
				
			}
			if (zDelta < 0 && ui::timelineScale>minTimelineScale)
			{
				ui::timelinePos.x -= tlc * .2;
				ui::timelineScale *= .8f;
				ui::timelineScale = fmax(ui::timelineScale, minTimelineScale);
			}

			//ui::timelinePos.x = min(ui::timelinePos.x, ui::MusicTimelineLeftMargin);
		}

		break;
	}

	

	case WM_LBUTTONDBLCLK:
	{
		timer::pressedMKeyTimeStamp = (float)timer::GetCounter();
		strcpy(ui::pressedMKey, "mouse LButton doubleclick");

		float mx = ((2.*dxui::mousePos.x) / dx::width - 1) / dx::aspect;
		float my = -((2.*dxui::mousePos.y) / dx::height - 1);
		
		if (!dxui::ClickRoutine)
		{
		//	break;
		}

		if (dxui::mousePtr&&CmdDesc[*dxui::mousePtr].Routine == Commands::CreateShader)
		{
			ui::EditShader();
		}

		if (dxui::mousePtr&&CmdDesc[*dxui::mousePtr].Routine == Commands::NullDrawer && dxui::mouseParam == 0)
		{
			Commands::NullDrawer_* in = (Commands::NullDrawer_*)(dxui::mousePtr + 1);
			int shader = in->shader;
			BYTE* ptr = resourceCreationPtr[0][in->shader];
			if (ptr)
			{
				ui::CurrentPos = ptr - stack::data-1;
				ui::EditShader();
				break;
			}
		}

		if (dxui::mousePtr&&dxui::ClickRoutine == ui::ClickSlider && dxui::mouseParam>=0)
		{
			int v = CmdDesc[*dxui::mousePtr].ParamDefault[dxui::mouseParam];
			ui::WriteValue(dxui::mousePtr, dxui::mouseParam, v);
			char s[11];
			_itoa(v, s, 10);
			strcpy(ui::editBoxText, s);
			io::WriteUndoStage(ui::CurrentPos);
			break;
		}

		if (dxui::mousePtr&&CmdDesc[*dxui::mousePtr].Routine == Commands::Point && mx>ui::MusicTimelineLeftMargin)
		{
			BYTE* i = stack::data; 
			BYTE* prev=i;

			while (*i != 0 && i < dxui::mousePtr)
			{
				prev = i;
				i += CmdDesc[*i].Size;
			}

			if (CmdDesc[*prev].Routine == Point ||
				CmdDesc[*(dxui::mousePtr + CmdDesc[*dxui::mousePtr].Size)].Routine == Point)
			{
				ui::removeCmd(dxui::mousePtr);
				io::WriteUndoStage(ui::CurrentPos);
			}
			break;
		}

		if (dxui::mousePtr&&CmdDesc[*dxui::mousePtr].Routine == Commands::Envelope&& mx > ui::MusicTimelineLeftMargin)
		{
			BYTE* j = dxui::mousePtr;
			int level = ui::GetCmdLevel(j);
			j += CmdDesc[*j].Size;
			while (ui::GetCmdLevel(j) > level)
			{
				ui::removeCmd(j);
			}
			
			ui::removeCmd(dxui::mousePtr);
			io::WriteUndoStage(ui::CurrentPos);
			break;
		}

		if (ui::insertPointPtr && dxui::mousePos.x > ui::MusicTimelineLeftMargin)
		{
			int t = ui::CurrentPos;
			ui::CurrentPos = ui::insertPointPtr - stack::data;
			ui::InsertCmd(ui::SearchCmdByRoutine(Commands::Point));
			Commands::Point_* in = (Commands::Point_*)(ui::insertPointPtr + CmdDesc[*ui::insertPointPtr].Size + 1);
			in->x = ui::currentLinePoint.x;
			in->y = ui::currentLinePoint.y;

			ui::CurrentPos = t;
			io::WriteUndoStage(ui::CurrentPos);
			break;
		}

		if (!dxui::mousePtr)
			break;


		ui::SetElementOpenStatus(dxui::mousePtr, 1-ui::GetElementOpenStatus(dxui::mousePtr));
		io::WriteUndoStage(ui::CurrentPos);
		break;

		/*
		ui::removeSelection();

		int i = 0;
		if (wParam == MK_CONTROL + 1) i = 1;
		if (wParam == MK_SHIFT + 1) i = 2;
		ui::Select(i);

		BYTE* j = stack::data;
		BYTE level = ui::GetCmdLevel(ui::selectedElementPtr);
		BYTE* hierarhy[100];
		char* s[100];

		if (!ui::GetElementOpenStatus(ui::selectedElementPtr))
		{
			while (*j != 0 && j <= ui::selectedElementPtr)
			{
				BYTE currentLevel = ui::GetCmdLevel(*j);
				s[currentLevel] = CmdDesc[*j].Name;
				hierarhy[currentLevel] = j;
				j += CmdDesc[*j].Size;
			}

			for (int x = 0; x <= level; x++)
			{
				ui::SetElementOpenStatus(hierarhy[x], true);
			}
		}
		else 
		{
			BYTE* ptr = ui::selectedElementPtr;
			ui::SetElementOpenStatus(ptr, false);
		}

		ui::LButtonDown = true;*/
		
		break;
	}

	case WM_LBUTTONUP:
		ui::LButtonDown = false;
		break;

	case WM_RBUTTONUP:
		break;

	case WM_LBUTTONDOWN:
	{
		timer::pressedTimer = (float)timer::GetCounter();
		timer::pressedMKeyTimeStamp = (float)timer::GetCounter();
		strcpy(ui::pressedMKey, "mouse LButton");

		if (!dxui::anyElementOver)
		{
			ui::prevSelectedParamID = -1;
			ui::selectedParamID = -1;
			ui::selectedElementPtr = NULL;
			ui::LButtonDown = true;
			break;
		}


		if (!(dxui::mousePtr|| 
			dxui::ClickRoutine == ui::ClickMainMenu ||
			dxui::ClickRoutine == ui::ClickCommand ||
			dxui::ClickRoutine == ui::ClickInsertCmd ||
			dxui::ClickRoutine == ui::ClickProjectMenu||
			dxui::ClickRoutine == ui::ClickViewController||
			dxui::ClickRoutine == ui::ClickOct||
			dxui::ClickRoutine == ui::ClickMidiDevice||
			dxui::ClickRoutine == ui::ClickVelocityMapping)) break;

		ui::prevSelectedParamID = ui::selectedParamID;
		ui::selectedParamID = dxui::mouseParam;
		ui::selectedElementRect = dxui::mouseElementRect;
		ui::selectedElementPtr = dxui::mousePtr;
		ui::selectedElementOrientation = dxui::mouseElementOrientation;
		ui::LButtonDown = true;

		if (dxui::ClickRoutine)
		{
			dxui::ClickRoutine();
		}
		else
		{
			ui::Click();
		}

		dxui::LastClickedRoutine = dxui::ClickRoutine;
		break;
	}


	//case WM_RBUTTONDBLCLK:
	case WM_RBUTTONDOWN:
	{
		timer::pressedMKeyTimeStamp = (float)timer::GetCounter();
		strcpy(ui::pressedMKey, "mouse RButton");

		if (ui::isKeyDown(VK_SHIFT))
		{
		//	ui::ProcessContextMenu();
		}
		break;
	}

	case WM_SYSKEYDOWN:
	{
		break;
	}

	case WM_KEYDOWN:
	{
		ui::anyKeyPressed = true;
		//if (ui::isKeyDown(VK_OEM_3))  ui::stackViewMode = !ui::stackViewMode;

		ui::LogKeyPressed(wParam);

		//BYTE cmd = *ui::selectedElementPtr;
		if (!ui::selectedElementPtr) {
			ui::selectedElementPtr = stack::data;
			ui::selectedParamID = -1;
		}

		//edit box key host
		if (dxui::LastClickedRoutine == ui::ClickParam&&ui::selectedParamID >= 0)
		{
			char textCopy[64];
			int  type = ui::GetCmdType(ui::selectedElementPtr,ui::selectedParamID);
			BYTE cmd = *ui::selectedElementPtr ;
			int offset = CmdDesc[cmd].ParamOffset[ui::selectedParamID];

			int v = ui::ReadNumericValue(ui::selectedElementPtr,ui::selectedParamID);

			ui::textCursor = min(ui::textCursor, strlen(ui::editBoxText));
			ui::textCursor = max(ui::textCursor, 0);

			if (wParam == VK_LEFT&&ui::textCursor>0) ui::textCursor--;
			if (wParam == VK_RIGHT&&ui::textCursor < strlen(ui::editBoxText)) ui::textCursor++;
			if (wParam == VK_DELETE&&ui::textCursor<strlen(ui::editBoxText))
			{
				strcpy(textCopy, ui::editBoxText);
				strcpy(ui::editBoxText + ui::textCursor, textCopy + ui::textCursor+1);
			}
			if (wParam == VK_BACK&&ui::textCursor > 0)
			{
				strcpy(textCopy, ui::editBoxText);
				strcpy(ui::editBoxText +max(ui::textCursor-1,0), textCopy+ui::textCursor);
				ui::textCursor--;
			}

			if (type == PT_LABEL)
			{
				if (strlen(ui::editBoxText) < TypeSizeTable[PT_LABEL])
				{
					char kc = (char)wParam;
					if ((kc >= '0' && kc <= '9') || (kc >= 'A' && kc <= 'Z') || (kc >= 'a' && kc <= 'z'))
					{
						bool shift = !GetAsyncKeyState(VK_SHIFT);
						bool caps = GetKeyState(VK_CAPITAL);
						if (shift^caps)
						{
							if (kc >= 'A' && kc <= 'Z')
							{
								kc=tolower(kc);
							}
						}

						strcpy(textCopy, ui::editBoxText);
						strcpy(ui::editBoxText + ui::textCursor + 1, textCopy + ui::textCursor);
						ui::editBoxText[ui::textCursor] = (char)kc;
						ui::textCursor++;
					}
				}
				strncpy((char*)(ui::selectedElementPtr + offset), ui::editBoxText, TypeSizeTable[PT_LABEL]);

			}
			else
			{
				if (wParam >= '0' && wParam <= '9')
				{
					strcpy(textCopy, ui::editBoxText);
					strcpy(ui::editBoxText + ui::textCursor + 1, textCopy + ui::textCursor);
					ui::editBoxText[ui::textCursor] = (char)wParam;
					ui::textCursor++;
				}

				if (wParam == VK_OEM_MINUS && ui::textCursor == 0)
				{
					strcpy(textCopy, ui::editBoxText);
					strcpy(ui::editBoxText + ui::textCursor + 1, textCopy + ui::textCursor);
					ui::editBoxText[ui::textCursor] = '-';
					ui::textCursor++;
				}

				if (strlen(ui::editBoxText) > 1 || (strlen(ui::editBoxText) > 0 && ui::editBoxText[0] != '-'))
				{
					v = atoi(ui::editBoxText);
					ui::WriteValue(ui::selectedElementPtr,ui::selectedParamID, v);

					v = ui::ReadNumericValue(ui::selectedElementPtr,ui::selectedParamID);
					_itoa(v, ui::editBoxText, 10);
				}
			}

			ui::textCursor = min(ui::textCursor, strlen(ui::editBoxText));
			ui::textCursor = max(ui::textCursor, 0);

			strcpy(textCopy, ui::editBoxText);
			textCopy[ui::textCursor] = 0;
			ui::textCursorX = dxui::StringLen(ui::letterWidth, textCopy);

			break;
		}

		//mute&solo
		if (ui::isKeyDown(VK_CONTROL) && CmdDesc[*ui::selectedElementPtr].Routine == Channel)
		{
			Commands::Channel_* in = (Channel_*)(ui::selectedElementPtr + 1);	
			if (wParam == 'S'){ in->solo = 1 - in->solo; }
			if (wParam == 'M') { in->mute = 1 - in->mute; }				
		}

		//bypass
		if (wParam == 'B')
		{
			ui::ToggleBypass();
		}

		//loop
		if (wParam == 'L')
		{
			BYTE* loopPtr = ui::FindCommandInStack(stack::data, TimeLoop);
			if (loopPtr)
			{
				BYTE* i = stack::data;
				INT32 minX, maxX;
				minX = 32767 * 735; maxX = 0;
				Clip_* in = NULL; int notecount = 0; float noteSize = 0;

				if (!ui::isKeyDown(VK_CONTROL))
				{
					while (*i != 0)
					{
						if (ui::isElementSelected(i))
						{
							if (CmdDesc[*i].Routine == ShowScene)
							{
								ShowScene_* in = (ShowScene_*)(i + 1);
								INT32 left = in->x * 735;
								INT32 len = in->length * 735;
								INT32 right = left + len;

								if (left < minX) minX = left;
								if (right > maxX) maxX = right;
							}

							if (CmdDesc[*i].Routine == Clip)
							{
								Clip_* in = (Clip_*)(i + 1);
								INT32 left = in->x;
								INT32 len = in->repeat*in->length*SAMPLERATE*60. / (Commands::MasterBPM*in->bpmScale);;
								INT32 right = left + len;

								if (left < minX) minX = left;
								if (right > maxX) maxX = right;
							}

						}

						i += CmdDesc[*i].Size;
					}

					TimeLoop_* inT = (TimeLoop_*)(loopPtr + 1);
					INT32 oldMin = inT->x;
					INT32 oldMax = inT->length;

					if (minX < maxX)
					{
						inT->x = minX;
						inT->length = maxX - minX;
					}
						
				}

				if (ui::isKeyDown(VK_CONTROL))
				{
					TimeLoop_* inT = (TimeLoop_*)(loopPtr + 1);
					inT->active = 1 - inT->active;
				}
			}
		}

		//noteStep&transpose
		if (ui::isKeyDown(VK_CONTROL))
		{
			if (wParam >= '0'&& wParam < '9')
			{
				ui::autoNextNote = wParam - '0';
				char octx[10];
				_itoa(ui::autoNextNote, octx, 10);
				char octxt[32];
				strcpy(octxt, "step changed to ");
				strcat(octxt, octx);
				ui::Log(octxt);	
			}

			if ((wParam == VK_UP) || (wParam == VK_DOWN) || (wParam == VK_PRIOR) || (wParam == VK_NEXT))
			{
				int dir = 0;
				if (wParam == VK_UP) dir = 1;
				if (wParam == VK_DOWN) dir = -1;
				if (wParam == VK_NEXT) dir = -12;
				if (wParam == VK_PRIOR) dir = 12;

				//todo:: traspose notes

				BYTE* nptr = stack::data;
				while (*nptr != 0 && CmdDesc[*nptr].Routine != EndMusic)
				{
					if (CmdDesc[*nptr].Routine == Note && ui::isElementSelected(nptr))
					{
						Commands::Note_* n = (Commands::Note_*)(nptr + 1);
						if (n->Note > 0 && n->Note < 255)
						{
							n->Note = clamp(n->Note+dir, 1, 12*8);
						}
					}
					nptr += CmdDesc[*nptr].Size;
				}

				io::WriteUndoStage(ui::CurrentPos);

			}
			
		}

		if (!ui::isKeyDown(VK_CONTROL) && 
			!ui::isKeyDown(VK_MENU)&& 
			!ui::isKeyDown(VK_SPACE) &&
			ui::selectedElementPtr &&
			CmdDesc[*ui::selectedElementPtr].Routine == Commands::Note)
		{
			BYTE* ptr = stack::data;
			BYTE* ParentClip = NULL;

			while (!(*ptr == 0 || ptr == ui::selectedElementPtr))
			{
				if (CmdDesc[*ptr].Routine == Clip)
				{
					ParentClip = ptr;
				}

				ptr += CmdDesc[*ptr].Size;
			}

			int p = -1;
			BYTE cOctave = ui::cOctave;

			switch (wParam)
			{
				case VK_RETURN: p = 0; break;
				case 'Z':p = 0 + cOctave * 12 + 1; break;//C
				case 'S':p = 1 + cOctave * 12 + 1; break;//C#
				case 'X':p = 2 + cOctave * 12 + 1; break;//D
				case 'D':p = 3 + cOctave * 12 + 1; break;//D#
				case 'C':p = 4 + cOctave * 12 + 1; break;//E
				case 'V':p = 5 + cOctave * 12 + 1; break;//F
				case 'G':p = 6 + cOctave * 12 + 1; break;//F#
				case 'B':p = 7 + cOctave * 12 + 1; break;//G
				case 'H':p = 8 + cOctave * 12 + 1; break;//G#
				case 'N':p = 9 + cOctave * 12 + 1; break;//A
				case 'J':p = 10 + cOctave * 12 + 1; break;//A#
				case 'M':p = 11 + cOctave * 12 + 1; break;//B
				case 'Q':p = 0 + cOctave * 12 + 1 + 12; break;//C
				case '2':p = 1 + cOctave * 12 + 1 + 12; break;//C#
				case 'W':p = 2 + cOctave * 12 + 1 + 12; break;//D
				case '3':p = 3 + cOctave * 12 + 1 + 12; break;//D#
				case 'E':p = 4 + cOctave * 12 + 1 + 12; break;//E
				case 'R':p = 5 + cOctave * 12 + 1 + 12; break;//F
				case '5':p = 6 + cOctave * 12 + 1 + 12; break;//F#
				case 'T':p = 7 + cOctave * 12 + 1 + 12; break;//G
				case '6':p = 8 + cOctave * 12 + 1 + 12; break;//G#
				case 'Y':p = 9 + cOctave * 12 + 1 + 12; break;//A
				case '7':p = 10 + cOctave * 12 + 1 + 12; break;//A#
				case 'U':p = 11 + cOctave * 12 + 1 + 12; break;//B
				case VK_OEM_3:p = 255; break;//note off

				case VK_OEM_4: 
				{
					if (ui::cOctave > 0)
					{
						ui::cOctave--;
						char octx[10];
						_itoa(ui::cOctave, octx, 10);
						char octxt[32];
						strcpy(octxt, "Octave changed to ");
						strcat(octxt, octx);
						ui::Log(octxt);
					}
				}
				break;

				case VK_OEM_6: 
				{
					if (ui::cOctave < 9)
					{
						ui::cOctave++;
						char octx[10];
						_itoa(ui::cOctave, octx, 10);
						char octxt[32];
						strcpy(octxt, "Octave changed to ");
						strcat(octxt, octx);
						ui::Log(octxt);
					}
				}
				break;

				case VK_LEFT:
				{
					int noteSize = CmdDesc[*ui::currentNotePtr].Size;
					*(ui::currentNotePtr + noteSize - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)0;
					BYTE* firstNoterPtr = ParentClip +CmdDesc[*ParentClip].Size;
					if (ui::currentNotePtr!= firstNoterPtr) ui::currentNotePtr-= CmdDesc[*ui::currentNotePtr].Size;
					*(ui::currentNotePtr + noteSize - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
					ui::selectedElementPtr = ui::currentNotePtr;
					break;
				}

				case VK_RIGHT:
				{
					int noteSize = CmdDesc[*ui::currentNotePtr].Size;
					*(ui::currentNotePtr + noteSize - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)0;
					if (CmdDesc[*(ui::currentNotePtr + CmdDesc[*ui::currentNotePtr].Size)].Routine == Note)
					{
						ui::currentNotePtr += CmdDesc[*ui::currentNotePtr].Size;
					}
					*(ui::currentNotePtr + noteSize - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
					ui::selectedElementPtr = ui::currentNotePtr;
					break;
				}

				case VK_INSERT:
				{
					BYTE* firstNotePtr = ParentClip + CmdDesc[*ParentClip].Size;
					Clip_* clipIn = (Clip_*)(ParentClip + 1);
					int noteSize = CmdDesc[*ui::currentNotePtr].Size;
					int CurrentNoteNum = (ui::currentNotePtr - firstNotePtr) / noteSize;
					int Count = clipIn->length - CurrentNoteNum -1;

					*(ui::currentNotePtr + noteSize - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)0;

					memmove(ui::currentNotePtr + noteSize, ui::currentNotePtr, Count*noteSize);
					Note_* in = (Note_*)(ui::currentNotePtr + 1);
					in->Note = 0;
					in->s0 = 0;
					in->s1 = 0;
					in->s2 = 0;
					in->s3 = 0;
					in->Variation = 0;
					in->Retrigger = 0;
					in->Slide = 0;
					*(ui::currentNotePtr + noteSize - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;

					io::WriteUndoStage(ui::CurrentPos);
					break;
				}

				case VK_DELETE:
				{
					BYTE* firstNotePtr = ParentClip + CmdDesc[*ParentClip].Size;
					Clip_* clipIn = (Clip_*)(ParentClip + 1);
					int nCount = clipIn->length;
					int noteSize = CmdDesc[*ui::currentNotePtr].Size;
					int Count = nCount - (ui::currentNotePtr - firstNotePtr) / noteSize-1;
					memmove(ui::currentNotePtr, ui::currentNotePtr + noteSize, Count*noteSize);
					*(ui::currentNotePtr + noteSize - EditorInfoOffset + EditorInfoOffsetSelected) = (BYTE)1;
					Note_* in = (Note_*)(firstNotePtr + (nCount-1)*noteSize+1);
					in->Note = 0;
					in->s0 = 0;
					in->s1 = 0;
					in->s2 = 0;
					in->s3 = 0;
					in->Variation = 0;
					in->Retrigger = 0;
					in->Slide = 0;
					io::WriteUndoStage(ui::CurrentPos);
					break;
				}

				case VK_TAB:
				{
					ui::noteViewMode++;
					if (ui::noteViewMode>7) ui::noteViewMode=0;
					break;
				}
			}


				Note_* in = (Note_*)(ui::currentNotePtr +1);

				if (wParam == VK_OEM_PERIOD || wParam == VK_DECIMAL)
				{
					in->Retrigger = 1- in->Retrigger;
					io::WriteUndoStage(ui::CurrentPos);
					break;
				}

				if (ui::isKeyDown(VK_SHIFT) && wParam >= '0' &&wParam <= '9')
				{
					p = wParam - '0';
					in->Variation = p;
					io::WriteUndoStage(ui::CurrentPos);
					break;
				}

				if (wParam >= VK_NUMPAD0 && wParam <= VK_NUMPAD9)
				{
					p = wParam - VK_NUMPAD0;
					in->Variation = (unsigned char)p;
					io::WriteUndoStage(ui::CurrentPos);
					break;
				}


				switch (ui::noteViewMode)
				{
					case 0:
					{
						if (p >= 0)
						{
							in->Note = (unsigned char)p;
							io::WriteUndoStage(ui::CurrentPos);
						}
						break;
					}

					case 2:
					{
						if (wParam >= '0' &&wParam <= '9')
						{
							p = wParam - '0';
							in->Slide = (unsigned char)(p/9.*255.);
							io::WriteUndoStage(ui::CurrentPos);
						}
						break;
					}

					case 3:
					{
						if (wParam >= '0' &&wParam <= '9')
						{
							p = wParam - '0';
							in->Variation = p;
							io::WriteUndoStage(ui::CurrentPos);
						}
						break;
					}

					case 4:
					{
						if (wParam >= '0' &&wParam <= '9')
						{
							p = wParam - '0';
							in->s0 = (unsigned char)(p /9. * 255);
							io::WriteUndoStage(ui::CurrentPos);
						}
						break;
					}
					case 5:
					{
						if (wParam >= '0' &&wParam <= '9')
						{
							p = wParam - '0';
							in->s1 = (unsigned char)(p / 9. * 255);
							io::WriteUndoStage(ui::CurrentPos);
						}
						break;
					}

					case 6:
					{
						if (wParam >= '0' &&wParam <= '9')
						{
							p = wParam - '0';
							in->s2 = (unsigned char)(p / 9. * 255);
							io::WriteUndoStage(ui::CurrentPos);
						}
						break;
					}

					case 7:
					{
						if (wParam >= '0' &&wParam <= '9')
						{
							p = wParam - '0';
							in->s3 = (unsigned char)(p / 9. * 255);
							io::WriteUndoStage(ui::CurrentPos);
						}
						break;
					}

				}

			if (p >= 0)
			{
				Clip_* clipIn = (Clip_*)(ParentClip + 1);
				int nCount = clipIn->length;

				BYTE* firstNotePtr = ParentClip + CmdDesc[*ParentClip].Size;
				int noteSize = CmdDesc[*ui::currentNotePtr].Size;
				int noteNum = (ui::currentNotePtr - firstNotePtr) / noteSize;

				ui::removeSelection();
				
				float ns = ui::GetNoteSize(stack::data + ui::CurrentPos);
				
				if (Commands::playonenoteFlag == false && playmode==0)
				{
					Commands::playonenoteFlag = true;
					PlayOneNote(ui::currentNotePtr);
					playnoteKey = wParam;	
				}

				if (noteNum + ui::autoNextNote < nCount)
				{
					ui::currentNotePtr += CmdDesc[*ui::currentNotePtr].Size*ui::autoNextNote;
					ui::selectedElementPtr = ui::currentNotePtr;
				}
				ui::SetElementSelection(ui::currentNotePtr, 1);

			}
		
		}
		
		//common keys : play/stop, copy-cut-paste,del undo...
		//if (CmdDesc[*ui::selectedElementPtr].Routine != Commands::Channel)
		{
			switch (wParam)
			{
			case VK_SPACE:
			{
				if (Commands::playmode == 0)
				{
					Commands::PlayWave((int)Commands::StartPosition);
				}
				else
				{
					Commands::StartPosition = Commands::cursorF;
					if (Commands::StartPosition < 0) Commands::StartPosition = 0;
					Commands::StopWave();
					Commands::playmode = 0;
				}
				break;
			}
			case VK_DELETE:
			{
				if (CmdDesc[*ui::selectedElementPtr].Routine != Commands::Note)
				{
					ui::removeSelected();
					ui::SetCurSelect();
					ui::Select(0);
					ui::Log("deleted");
					io::WriteUndoStage(ui::CurrentPos);
				}
				break;
			}
			case 'C':
				if (ui::isKeyDown(VK_CONTROL))
				{
					ui::copySelected();
					ui::removeSelection();
					ui::SetCurSelect();
					ui::Log("copied");
				}
				break;

			case 'V':
				if (ui::isKeyDown(VK_CONTROL))
				{
					if (ui::paste())
					{
						ui::removeSelection();
						ui::SetCurSelect();
						ui::Log("pasted");
						io::WriteUndoStage(ui::CurrentPos);
					}
					else
					{
						ui::Log("incorrect type combination for paste");
					}
				}
				break;
			case 'X':
				if (ui::isKeyDown(VK_CONTROL))
				{
					ui::copySelected();
					ui::removeSelected();
					ui::removeSelection();
					ui::SetCurSelect();
					ui::Select(0);
					ui::Log("cutted");
					io::WriteUndoStage(ui::CurrentPos);
				}
				break;

			case 'Y':
				if (ui::isKeyDown(VK_CONTROL))
				{
					ui::CurrentPos = io::Redo(ui::CurrentPos);
					ui::Log("redo complete");
				}
				break;

			case 'Z':

				if (ui::isKeyDown(VK_CONTROL))
				{
					ui::CurrentPos = io::Undo(ui::CurrentPos);
					ui::Log("undo complete");
				}
				break;
			}
		}

		break;
	}

	case WM_KEYUP:
	{
		ui::anyKeyPressed = false;
		if (!ui::isKeyDown(playnoteKey)&& Commands::playonenoteFlag)
		{
			Commands::playonenoteFlag = false;
			Commands::playmode = 0;
			Commands::StopWave();			
		}
		break;
	}

    case WM_CLOSE:
		 PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

#endif
