typedef void(*PVFN)();//func pointer type for commands

//PVFN CurrentCmd0, CurrentCmd1, CurrentCmd2;



typedef struct { float color[3]; } color3;
typedef struct { float color[4]; } color4;

typedef struct
{
	PVFN Routine;
	signed char Level;
	int Size;
	int	 ParamOffset[MAXPARAM];
	BYTE ParamType[MAXPARAM];
	void* SturctPtr;
#ifdef EditMode
	char Name[MAXPARAMNAMELEN];
	int	 Category;
	char ParamName[MAXPARAM][MAXPARAMNAMELEN];
	INT32  ParamMin[MAXPARAM];
	INT32  ParamMax[MAXPARAM];
	int  ParamDefault[MAXPARAM];
	char ParamEnum[MAXPARAM][MAXPARAMENUM][MAXPARAMNAMELEN];
	bool ParamEnvControlled[MAXPARAM];
	PVFN ParamDraw;
	XMFLOAT4 Color;
	int posType;
	int switchersNum;
	char switchersText[MAXSWITCHERS];
	PVFN Draw;
	PVFN Click;
#endif

} Cmd;

Cmd CmdDesc[256];//limit for 1-byte encoding