#ifdef EditMode
	#include "AllCmdDefine.h"
#else
	#include "UsedDefine.h"
#endif


#define CAT_SCENE 1
#define CAT_CMD 2
#define CAT_COPY 3
#define CAT_3D 4
#define CAT_PARTICLE 5
#define CAT_SOUND 6

using namespace Commands;

#ifdef EditMode
	struct uiColor 
	{
		XMVECTORF32 background = XMVECTORF32{ 0.15,0.15,0.15,1.0 };
		XMFLOAT4 boxcat[7] = 
		{
			XMFLOAT4(0.5,0.5,0.5,.99),//main menu
			XMFLOAT4(0.5,0.2,0.2,.99),//scene
			XMFLOAT4(0.4,0.4,0.4,.99),//cmd
			XMFLOAT4(0.2,0.5,0.2,.99),//copy
			XMFLOAT4(0.2,0.2,0.5,.99),//3d
			XMFLOAT4(0.2,0.2,0.5,.99),//particle
			XMFLOAT4(0.2,0.2,0.2,.99),//sound
		};
		XMFLOAT4 font = XMFLOAT4 (1,1,1,1);
		XMFLOAT4 selectedfont = XMFLOAT4 (0,0,0,1);
		XMFLOAT4 selectedbox = XMFLOAT4 (1,1,1,1);
		XMFLOAT4 clipcopy = XMFLOAT4(0.0, 0.25f, 0.0, 1.0);
		XMFLOAT4 notestrong = XMFLOAT4(0.4, 0.4f, 0.4, .99);
		XMFLOAT4 notevolume = XMFLOAT4 (0.75,0.75f,0.75,.99f);
		XMFLOAT4 notepan = XMFLOAT4 (1,1,1,.85f);
		XMFLOAT4 peakmeter = XMFLOAT4 (1,1,1,.85f);
		XMFLOAT4 activechannel = XMFLOAT4(0.5, 0.5f, 0.5, 1.0);
		XMFLOAT4 mutedchannel = XMFLOAT4(0.25, 0.25f, 0.25, 1.0);
		XMFLOAT4 envelopeline = XMFLOAT4(0, 0, 0, 1);
		XMFLOAT4 point = XMFLOAT4(1, .5, 0, 1);
		XMFLOAT4 envelopeboundsline = XMFLOAT4(0, 0, 0, 1);
		XMFLOAT4 masterwave = XMFLOAT4(0.2f, 0.2f, 0.2f, .825f);
		XMFLOAT4 masterwaveline = XMFLOAT4(.70f,.70f,.70f, .85f);
		XMFLOAT4 colorpoint = XMFLOAT4(1, 1, 0, 1);
		XMFLOAT4 fxpoint = XMFLOAT4(1, 1, 0, 1);
		XMFLOAT4 cursor = XMFLOAT4(0, 0, 0, .85f);
		XMFLOAT4 loopoff = XMFLOAT4(.5f, .5f, .5f, 1);
		XMFLOAT4 loopon = XMFLOAT4(1.0f, .5f, .5f, 1);
		XMFLOAT4 loadback = XMFLOAT4(.5f, .5f, .5f, 1.f);
		XMFLOAT4 timelinelines = XMFLOAT4(1, 1, 1, 1.f);
		XMFLOAT4 timelinefont = XMFLOAT4(1, 1, 1, 1.f);
	} colorScheme;

#endif

#ifdef EditMode

	char ProjectPath[1000];
	char* fileBuffer;

	void SetProjectPath()
	{
		char *i = NULL;
		char tmpstr[1000];		
		int entrycount = 0;

		strcpy(tmpstr, GetCommandLine());
		int tmpstrl = strlen(tmpstr);

		for (int s = tmpstrl - 1; s >= 0; s--)
		{
			if (tmpstr[s] == '\\') entrycount++;
			if (entrycount == 2)
			{
				i = &tmpstr[s];
				break;
			}
		}

		if (!i)
		{
			MessageBox(hWnd, "fx.exe", "Error reading source file", MB_OK);
			exit(0);
		}

		*(i + 1) = 0;
		strcat(tmpstr, "fx\\");
		strcpy(ProjectPath, tmpstr + 1);

		fileBuffer = new char[1024 * 1024];
	}

	char str[255];
	char* strPtr = nullptr;
	char* strStart = nullptr;

	bool GetDesc(char* s, char* end)
	{
		while (s < end && *s!='-' && !isalpha(*s) && !isalnum(*s)) s++;
		if (s == end) return false;

		strStart = s;

		while (s < end && (isalpha(*s) || isalnum(*s) || *s=='_')) s++;
		if (s == end) return false;

		memcpy(str, strStart, s - strStart);
		str[s - strStart] = 0;

		strPtr = s;
		return true;
	}

	bool GetDescSpecial(char* s, char* end)
	{
		while (s < end && *s==' ') s++;

		strStart = s;

		while (s < end && *s!=' '&&*s!=',') s++;

		memcpy(str, strStart, s - strStart);
		str[s - strStart] = 0;

		strPtr = s;
		return true;
	}

	void FillCmdDescFromCode(char* filename, int n)
	{
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
		CmdDesc[n].ParamDraw = NULL;
		CmdDesc[n].Draw = NULL;

		char Path[1000];
		strcpy(Path, ProjectPath);
		strcat(Path, filename);
		FILE *fp;
		fp = fopen(Path, "rb");

		if (!fp) MessageBox(hWnd, filename, "Error reading source file", MB_OK);
		fread(fileBuffer, 1, 1024 * 1024, fp);
		fclose(fp);

		char CmdName[255];
		strcpy(CmdName, "struct ");
		strcat(CmdName,CmdDesc[n].Name);
		strcat(CmdName, "_");

		char* ptr = strstr(fileBuffer, CmdName);

		int offset = 1;

		if (ptr)
		{
			char* start = strstr(ptr, "{");

			if (start)
			{
				char* end = strstr(start, "}");
				char* pend = strstr(start, "CMD_DATA");

				if (pend)
				{
					if (pend < end)
					{
						end = pend;
					}
				}

				char* c = start;

				int p = 0;//parameter num counter			
				GetDesc(c, end);
				c = strStart;
				char* strEnd = NULL;

				while (c < end)
				{
					strEnd = strstr(c, "\n");

					char* VarEnd = strstr(c, ";") + 1;

					GetDesc(c, VarEnd);
					CmdDesc[n].ParamType[p] = 255;
					if (!strcmp(str, "pU8")) CmdDesc[n].ParamType[p] = PT_BYTE;
					if (!strcmp(str, "pS8")) CmdDesc[n].ParamType[p] = PT_SBYTE;
					if (!strcmp(str, "pU16")) CmdDesc[n].ParamType[p] = PT_WORD;
					if (!strcmp(str, "pS16")) CmdDesc[n].ParamType[p] = PT_SWORD;
					if (!strcmp(str, "pS32")) CmdDesc[n].ParamType[p] = PT_INT;
					if (!strcmp(str, "pName")) CmdDesc[n].ParamType[p] = PT_LABEL;
					if (!strcmp(str, "pEnum")) CmdDesc[n].ParamType[p] = PT_ENUM;
					if (!strcmp(str, "pEnumAssign")) CmdDesc[n].ParamType[p] = PT_ENUMASSIGN;
					if (!strcmp(str, "pText")) CmdDesc[n].ParamType[p] = PT_TEXT;

					if (CmdDesc[n].ParamType[p] == 255)
					{
						c = strEnd + 2;
						MessageBox(hWnd, "unknown type", "cmd base init", MB_OK);
					}
					else
					{
						c = strPtr;

						if (CmdDesc[n].ParamType[p] == PT_LABEL)
						{
							strcpy(CmdDesc[n].ParamName[p], "Name");
						}
						else
						{
							GetDesc(c, VarEnd);
							strcpy(CmdDesc[n].ParamName[p], str);
						}

						CmdDesc[n].ParamOffset[p] = offset;
						int pSize = TypeSizeTable[CmdDesc[n].ParamType[p]];
						offset += pSize;

						c = strPtr;

						CmdDesc[n].ParamEnvControlled[p] = false;

						char* envOnPtr = strstr(c, "EnvOn");
						if (envOnPtr)
						{
							if (envOnPtr < strEnd)
							{
								CmdDesc[n].ParamEnvControlled[p] = true;
							}
						}

						char* defValuePtr = strstr(c, "DefValue(");
						char* defValuePtrS = strstr(c, "DefValue ");
						if (!defValuePtr&&defValuePtrS) defValuePtr = defValuePtrS;

						CmdDesc[n].ParamMin[p] = TypeMinTable[CmdDesc[n].ParamType[p]];
						CmdDesc[n].ParamMax[p] = TypeMaxTable[CmdDesc[n].ParamType[p]];

						if (CmdDesc[n].Routine == Envelope && p==7)
						{
							int ttt = 0;
						}

						if (defValuePtr)
						{
							if (defValuePtr < strEnd)
							{
								char* rightbrecket = strstr(defValuePtr, ")");
								char* leftbrecket = strstr(defValuePtr, "(");

								if (leftbrecket&&rightbrecket)
								{
									c = leftbrecket;
									rightbrecket++;

									if (GetDesc(c, rightbrecket))
									{
										c = strPtr;
										if (isalnum(*str))	CmdDesc[n].ParamMin[p] = atoi(str);
									}

									if (GetDesc(c, rightbrecket))
									{
										c = strPtr;
										if (isalnum(*str))	CmdDesc[n].ParamDefault[p] = atoi(str);
									}

									if (GetDesc(c, rightbrecket))
									{
										c = strPtr;
										if (isalnum(*str))	CmdDesc[n].ParamMax[p] = atoi(str);
									}
								}
							}
						}

						char* enumPtr = strstr(c, "EnumList");
						if (enumPtr)
						{
							if (enumPtr < strEnd)
							{
								char* rightbrecket = strstr(enumPtr, ")");
								char* leftbrecket = strstr(enumPtr, "(");

								int commas = 1;

								if (leftbrecket&&rightbrecket)
								{
									char* Eptr = leftbrecket + 1;
									while (Eptr < rightbrecket + 1)
									{
										if (*Eptr == ',') commas++;
										Eptr++;
									}

									c = leftbrecket + 1;

									for (int i = 0; i < commas; i++)
									{
										GetDescSpecial(c, rightbrecket);
										strcpy(CmdDesc[n].ParamEnum[p][i], str);
										c = strPtr + 1;
									}
								}

							}

						}

						c = strEnd + 2;
						if (GetDesc(c, end))
						{
							c = strStart;
							p++;
						}
						else
						{
							c = end;
						}

					}
				}

			}

		//	int sz = sizeof
		//	CmdDesc[n].Size = offset;
		}
	
	}

	void CalcOfsAndSize(int n)
	{
		int ofs = 1; int c = 0;
		while ((BYTE*)*CmdDesc[n].ParamName[c] != 0)
		{
			CmdDesc[n].ParamOffset[c] = ofs;
			ofs += TypeSizeTable[CmdDesc[n].ParamType[c]];
			c++;
		}

	CmdDesc[n].Size = ofs;
	}

#endif

void cmdInit()
{
#ifdef EditMode
	SetProjectPath();
#endif

	ZeroMemory(CmdDesc, sizeof(CmdDesc));

	int n = 0;
	CmdDesc[n].Size = 0;
	CmdDesc[n].Level = 0;
	CmdDesc[n].Routine = Release;
	n++;

#ifdef used_Project
	CmdDesc[n].Routine = Project;
	CmdDesc[n].Level = 0;
	CmdDesc[n].Size = sizeof (Project_);
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Project");
		CmdDesc[n].Category = CAT_SCENE;
		FillCmdDescFromCode("commands.h", n);
	#endif
#endif
	n++;

#ifdef used_EndDraw
	CmdDesc[n].Routine = EndDraw;
	CmdDesc[n].Level = 0;
	CmdDesc[n].Size = 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "EndDraw");
		CmdDesc[n].Category = CAT_SCENE;
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	#endif
#endif
	n++;


#ifdef used_MainLoop
	CmdDesc[n].Routine = MainLoop;
	CmdDesc[n].Level = 0;
	CmdDesc[n].Size = 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "MainLoop");
		CmdDesc[n].Category = CAT_SCENE;
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	#endif
#endif
	n++;


#ifdef used_Scene
	CmdDesc[n].Routine = Scene;
	CmdDesc[n].Level = 0;
	CmdDesc[n].Size = sizeof (Scene_)+1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Scene");
		CmdDesc[n].Category = CAT_SCENE;
		FillCmdDescFromCode("commands.h", n);
	#endif
#endif
	n++;

#ifdef used_EndScene
	CmdDesc[n].Routine = EndScene;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "EndScene");
		CmdDesc[n].Category = CAT_SCENE;
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	#endif
#endif
	n++;

#ifdef used_ShowScene
	CmdDesc[n].Routine = ShowScene;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(ShowScene_) + 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "ShowScene");
		CmdDesc[n].Category = CAT_SCENE;
		FillCmdDescFromCode("commands.h", n);
		CmdDesc[n].Color = XMFLOAT4(.5, .5, .5, .8);
	#endif
#endif
	n++;

#ifdef used_Music
	CmdDesc[n].Routine = Music;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(Music_) + 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Music");
		CmdDesc[n].Category = CAT_SOUND;
		FillCmdDescFromCode("cmd_music.h", n);
	#endif
#endif
	n++;

#ifdef used_Channel
	CmdDesc[n].Routine = Channel;
	CmdDesc[n].Level = 2;
	CmdDesc[n].Size = sizeof(Channel_) + 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Channel");
		CmdDesc[n].Category = CAT_SOUND;
		FillCmdDescFromCode("cmd_music.h", n);
	#endif
#endif
	n++;

#ifdef used_Clip
	CmdDesc[n].Routine = Clip;
	CmdDesc[n].Level = 3;
	CmdDesc[n].Size = sizeof(Clip_) + 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Clip");
		CmdDesc[n].Category = CAT_SOUND;
		FillCmdDescFromCode("cmd_music.h", n);
	#endif

#endif
	n++;

#ifdef used_Note
	CmdDesc[n].Routine = Note;
	CmdDesc[n].Level = 4;
	CmdDesc[n].Size = sizeof(Note_) + 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Note");
		CmdDesc[n].Category = CAT_SOUND;
		FillCmdDescFromCode("cmd_music.h", n);
	#endif

#endif
	n++;

#ifdef used_Envelope
	CmdDesc[n].Routine = Envelope;
	CmdDesc[n].Level = 4;
	CmdDesc[n].Size = sizeof(Envelope_) + 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Envelope");
		CmdDesc[n].Category = CAT_SOUND;
		FillCmdDescFromCode("cmd_music.h", n);
	#endif
#endif
	n++;

#ifdef used_Point
	CmdDesc[n].Routine = Point;
	CmdDesc[n].Level = 5;
	CmdDesc[n].Size = sizeof (Point_)+1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Point");
		CmdDesc[n].Category = CAT_SOUND;
		FillCmdDescFromCode("cmd_music.h", n);
	#endif
#endif
	n++;

#ifdef used_Oscillator
	CmdDesc[n].Routine = Oscillator;
	CmdDesc[n].Level = 3;
	CmdDesc[n].Size = sizeof(Oscillator_) + 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Oscillator");
		CmdDesc[n].Category = CAT_SOUND;
		FillCmdDescFromCode("cmd_music.h", n);
	#endif
#endif
	n++;


#ifdef used_EndMusic
	CmdDesc[n].Routine = EndMusic;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "EndMusic");
		CmdDesc[n].Category = CAT_SOUND;
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	#endif

#endif
	n++;

#ifdef used_MasterChannel
	CmdDesc[n].Routine = MasterChannel;
	CmdDesc[n].Level = 2;
	CmdDesc[n].Size = sizeof(MasterChannel_) + 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "MasterChannel");
		CmdDesc[n].Category = CAT_SOUND;
		CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
		FillCmdDescFromCode("cmd_music.h", n);
	#endif
#endif
	n++;

#ifdef used_NullDrawer
	CmdDesc[n].Routine = NullDrawer;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(NullDrawer_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "NullDrawer");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = XMFLOAT4(0,.5,.7,1);
#endif
#endif
	n++;

#ifdef used_CreateShader
	CmdDesc[n].Routine = CreateShader;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(CreateShader_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "CreateShader");
	CmdDesc[n].Category = CAT_CMD;
	FillCmdDescFromCode("commands.h", n);
#endif
#endif
	n++;

#ifdef used_CreateVB
	CmdDesc[n].Routine = CreateVB;
	CmdDesc[n].Level = 0;
	CmdDesc[n].Size = sizeof(CreateVB_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "CreateVB");
	CmdDesc[n].Category = CAT_CMD;
	FillCmdDescFromCode("commands.h", n); 
#endif
#endif
	n++;

#ifdef used_TimeLoop
	CmdDesc[n].Routine = TimeLoop;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(TimeLoop_) + 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "TimeLoop");
		CmdDesc[n].Category = CAT_SCENE;
		FillCmdDescFromCode("cmd_music.h", n);
	#endif
#endif
	n++;

#ifdef used_Crunch
	CmdDesc[n].Routine = Crunch;
	CmdDesc[n].Level = 3;
	CmdDesc[n].Size = sizeof(Crunch_) + 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Crunch");
		CmdDesc[n].Category = CAT_SOUND;
		FillCmdDescFromCode("cmd_music.h", n);
	#endif
#endif
	n++;

#ifdef used_Chorus
	CmdDesc[n].Routine = Chorus;
	CmdDesc[n].Level = 3;
	CmdDesc[n].Size = sizeof(Chorus_) + 1;
	#ifdef EditMode
		strcpy_s(CmdDesc[n].Name, "Chorus");
		CmdDesc[n].Category = CAT_SOUND;
		FillCmdDescFromCode("cmd_music.h", n);
	#endif
#endif
	n++;
	
#ifdef used_LoadTexture
	CmdDesc[n].Routine = LoadTexture;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(LoadTexture_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "LoadTexture");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
#endif
#endif
	n++;

#ifdef used_BlendMode
	CmdDesc[n].Routine = BlendMode;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(BlendMode_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "BlendMode");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
#endif
#endif
	n++;

#ifdef used_CreateTexture
	CmdDesc[n].Routine = CreateTexture;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(CreateTexture_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "CreateTexture");
	CmdDesc[n].Category = CAT_CMD;
	FillCmdDescFromCode("commands.h", n);
#endif
#endif
	n++;

#ifdef used_SetRT
	CmdDesc[n].Routine = SetRT;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(SetRT_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SetRT");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = XMFLOAT4(0.7, .5, .2, 1);
#endif
#endif
	n++;

#ifdef used_ResetRT
	CmdDesc[n].Routine = ResetRT;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "ResetRT");
	CmdDesc[n].Category = CAT_3D;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_SetZBuffer
	CmdDesc[n].Routine = SetZBuffer;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(SetZBuffer_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SetZBuffer");
	CmdDesc[n].Category = CAT_3D;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
	FillCmdDescFromCode("commands.h", n);
#endif
#endif
	n++;

#ifdef used_SetTexture
	CmdDesc[n].Routine = SetTexture;
	CmdDesc[n].Level = 2;
	CmdDesc[n].Size = sizeof(SetTexture_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SetTexture");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
#endif
#endif
	n++;

#ifdef used_Waveshaper
	CmdDesc[n].Routine = Waveshaper;
	CmdDesc[n].Level = 3;
	CmdDesc[n].Size = sizeof(Waveshaper_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Waveshaper");
	CmdDesc[n].Category = CAT_SOUND;
	FillCmdDescFromCode("cmd_music.h", n);
#endif
#endif
	n++;

#ifdef used_Equalizer
	CmdDesc[n].Routine = Equalizer;
	CmdDesc[n].Level = 3;
	CmdDesc[n].Size = sizeof(Equalizer_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Equalizer");
	CmdDesc[n].Category = CAT_SOUND;
	FillCmdDescFromCode("cmd_music.h", n);
#endif
#endif
	n++;

#ifdef used_Return
	CmdDesc[n].Routine = Return;
	CmdDesc[n].Level = 3;
	CmdDesc[n].Size = sizeof(Return_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Return");
	CmdDesc[n].Category = CAT_SOUND;
	FillCmdDescFromCode("cmd_music.h", n);
#endif
#endif
	n++;

#ifdef used_DelayLine
	CmdDesc[n].Routine = DelayLine;
	CmdDesc[n].Level = 3;
	CmdDesc[n].Size = sizeof(DelayLine_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "DelayLine");
	CmdDesc[n].Category = CAT_SOUND;
	FillCmdDescFromCode("cmd_music.h", n);
#endif
#endif
	n++;

#ifdef used_SendToEnv
	CmdDesc[n].Routine = SendToEnv;
	CmdDesc[n].Level = 4;
	CmdDesc[n].Size = sizeof(SendToEnv_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SendToEnv");
	CmdDesc[n].Category = CAT_SOUND;
	FillCmdDescFromCode("cmd_music.h", n);
#endif
#endif
	n++;

#ifdef used_SetCull
	CmdDesc[n].Routine = SetCull;
	CmdDesc[n].Level = 2;
	CmdDesc[n].Size = sizeof(SetCull_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SetCull");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
#endif
#endif
	n++;


#ifdef used_Curve
	CmdDesc[n].Routine = Curve;
	CmdDesc[n].Level = 3;
	CmdDesc[n].Size = sizeof(Curve_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Curve");
	CmdDesc[n].Category = CAT_PARTICLE;
	FillCmdDescFromCode("commands.h", n);
#endif
#endif
	n++;

#ifdef used_Point3d
	CmdDesc[n].Routine = Point3d;
	CmdDesc[n].Level = 4;
	CmdDesc[n].Size = sizeof(Point3d_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Point3d");
	CmdDesc[n].Category = CAT_PARTICLE;
	FillCmdDescFromCode("commands.h", n);
#endif
#endif
	n++;

#ifdef used_CameraYPR
	CmdDesc[n].Routine = CameraYPR;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(CameraYPR_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "CameraYPR");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = XMFLOAT4(.5, .5, .5, .8);
#endif
#endif
	n++;

#ifdef used_CreateMipMap
	CmdDesc[n].Routine = CreateMipMap;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "CreateMipMap");
	CmdDesc[n].Category = CAT_3D;
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_Override
	CmdDesc[n].Routine = Override;
	CmdDesc[n].Level = 2;
	CmdDesc[n].Size = sizeof(Override_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Override");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_Clear
	CmdDesc[n].Routine = Clear;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(Clear_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Clear");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_DefineParams
	CmdDesc[n].Routine = DefineParams;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(DefineParams_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "DefineParams");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_ApplyParams
	CmdDesc[n].Routine = ApplyParams;
	CmdDesc[n].Level = 2;
	CmdDesc[n].Size = sizeof(ApplyParams_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "ApplyParams");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_SetPrevStageFilter
	CmdDesc[n].Routine = SetPrevStageFilter;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(SetPrevStageFilter_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SetPrevStageFilter");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_SetParams
	CmdDesc[n].Routine = SetParams;
	CmdDesc[n].Level = 2;
	CmdDesc[n].Size = sizeof(SetParams_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SetParams");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_LodParams
	CmdDesc[n].Routine = LodParams;
	CmdDesc[n].Level = 2;
	CmdDesc[n].Size = sizeof(LodParams_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "LodParams");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_CreateMRT
	CmdDesc[n].Routine = CreateMRT;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(CreateMRT_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "CreateMRT");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_CreateRTforMRT
	CmdDesc[n].Routine = CreateRTforMRT;
	CmdDesc[n].Level = 2;
	CmdDesc[n].Size = sizeof(CreateRTforMRT_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "CreateRTforMRT");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_SetMRT
	CmdDesc[n].Routine = SetMRT;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(SetMRT_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SetMRT");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_Array
	CmdDesc[n].Routine = Array;
	CmdDesc[n].Level = 2;
	CmdDesc[n].Size = sizeof(Array_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Array");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_Transform
	CmdDesc[n].Routine = Transform;
	CmdDesc[n].Level = 2;
	CmdDesc[n].Size = sizeof(Transform_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "Transform");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_SubdivFactor
	CmdDesc[n].Routine = SubdivFactor;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(SubdivFactor_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SubdivFactor");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;

#ifdef used_SetTexturesQuality
	CmdDesc[n].Routine = SetTexturesQuality;
	CmdDesc[n].Level = 1;
	CmdDesc[n].Size = sizeof(SetTexturesQuality_) + 1;
#ifdef EditMode
	strcpy_s(CmdDesc[n].Name, "SetTexturesQuality");
	CmdDesc[n].Category = CAT_3D;
	FillCmdDescFromCode("commands.h", n);
	CmdDesc[n].Color = colorScheme.boxcat[CmdDesc[n].Category];
#endif
#endif
	n++;


	stack::CommandsCount = n;

	for (int x = 1; x < n; x++)
	{
		CmdDesc[x].ParamOffset[0] = 1;
	}

//#ifdef EditMode
	//last M bytes for editor. todo:remove in runtime
	//int16 x,y bool selected bool mode
	for (int x = 1; x < n; x++)
	{
		CmdDesc[x].Size += EditorInfoOffset;
#ifdef EditMode
		CmdDesc[x].Draw = NULL;
		CmdDesc[x].Click = NULL;
#endif
	}
//#endif

}
